#!/bin/bash

function compress(){
	echo "Compressing $filename"
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default \
		  -dNOPAUSE -dQUIET -dBATCH -dDetectDuplicateImages \
		  -dCompressFonts=true -r150 -sOutputFile="../gfx/$filename" "$filename"
}


filename=01_lipid_bilayer_to_hypersurface.pdf
compress

filename=02_filament.pdf
compress

filename=03_BAR_domain.pdf
compress

filename=04_inclusion.pdf
compress

filename=05_nano_particle.pdf
compress

filename=06_scaffold.pdf
compress
