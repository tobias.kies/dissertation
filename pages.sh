# This script extracts information on number of bw/color pages from a given PDF file.

# Filename.
file=diss.pdf

# Total number of pages.
pdfinfo $file | grep "Pages:"
echo ""

# Get some color information on the file.
s=$(gs -o - -sDEVICE=inkcov $file)

# Total number of color pages.
nCol=$(echo "$s" | grep -v "^ 0.00000  0.00000  0.00000" | grep "^ " | wc -l)
echo "Color pages:	$nCol"
echo ""

# List color pages.
echo "List of color pages:"
echo "$s" | grep -v "^ 0.00000  0.00000  0.00000" | grep "^ " -B1 | grep "^Page"
echo ""

