% This file compares data from my simulations with those from Jan.
clear variables;
close all;

angle = 20;

data = importdata( sprintf('~/fu-work/srv/movingParticlesFDTests/crcpre/jan%d/values.txt', angle) );
X    = data(:,1);
fX   = data(:,2);

data = importdata( sprintf('~/fu-work/storage/janData/ur%d_wo_header.xvg', angle) );
X2   = data(:,1);
fX2  = 0.5*(data(:,2) + data(:,4));

% Plot function values.
h = figure('Visible', 'On');
plot(X, fX);
hold on;
plot(X2, fX2);
plot(X2,data(:,2),'color','red')
plot(X2,data(:,4),'color','red')
title('X vs. fX');