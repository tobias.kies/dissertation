% This script is used to visualize the 
% linearization of the mean curvature
%   H = -div( grad(u)/Q(u) )
% and of the Gaussian curvature
%   K = det( D(grad(u)/Q(u)) ) = ( u_11*u_22 - u_12*u21 )/Q(u)
% where
%   Q = sqrt(1 + u_1 + u_2)
% and u_i are partial derivatives.

% More specifically, we argue that x/sqrt(1+x1^2+x^2)
% linearizes to the identity. Hence, H is approximated
% by the (negative) bilaplacian and K is approximated
% by the determinant of the Hessian.

% Actually... we do not need to visualize this really.
% But we can put here some terms that I used to compute
% the Gaussian curvature term:
pkg load symbolic
syms u1 u2 u11 u12 u22

% Area element and its square.
Q  = sqrt(1+u1^2+u2^2);
Q2 = 1+u1^2+u2^2;

% The following is the derivative of (the first two components of)
% the normal derivative. Note how we already used u12=u21 there.
% I hope I did not to any mistake there -- maybe re-check this
% with the symbolic toolbox as well?
H11 = (u11 - (u1^2*u11+u1*u2*u12)/Q2);
H12 = (u12 - (u1^2*u12+u1*u2*u22)/Q2);
H21 = (u12 - (u1*u2*u11+u2^2*u12)/Q2);
H22 = (u22 - (u1*u2*u12+u2^2*u22)/Q2);
H = [H11, H12; H21, H22]/Q;

% This is the Gaussian curvature.
k = simplify(det(H))

% This is the mean curvature.
h = simplify(trace(H))