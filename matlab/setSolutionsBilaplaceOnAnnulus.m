% Sets the solutions for the BilaplaceOnAnnulus computations.

% General solution.
g1 = 1;
g2 = r^2;
g3 = log(r);
g4 = r^2*(log(r)-1/2);
g  = c1*g1 + c2*g2 + c3*g3 + c4*g4;

% Solution to boundary value problem.
if( fVariableHeight )
  vars = [c1 c2 c3 c4 gamma];
  sols = [ -(b*r1*r2^2*(2*log(r2) - 1))/(2*(r1^2 - r2^2)), -(b*r1)/(2*(r1^2 - r2^2)), (b*r1*r2^2)/(r1^2 - r2^2), 0, (b*r1*(2*r2^2*log(r1) - 2*r2^2*log(r2) - r1^2 + r2^2))/(2*(r1^2 - r2^2))];
else
  vars = [c1 c2 c3 c4];
  sols = [ -(a*r2^4 - a*r1^2*r2^2 - 2*a*r1^2*r2^2*log(r1) + 2*a*r1^2*r2^2*log(r2) - b*r1^3*r2^2*log(r1) - 4*a*r1^2*r2^2*log(r2)^2 - 2*b*r1^3*r2^2*log(r2)^2 + b*r1*r2^4*log(r1) + 4*a*r1^2*r2^2*log(r1)*log(r2) + 2*b*r1^3*r2^2*log(r1)*log(r2))/(2*r1^2*r2^2 - r1^4 - r2^4 + 4*r1^2*r2^2*log(r1)^2 + 4*r1^2*r2^2*log(r2)^2 - 8*r1^2*r2^2*log(r1)*log(r2)), ...
            (b*r1^3 - b*r1*r2^2 - 4*a*r1^2*log(r1) + 4*a*r2^2*log(r2) - 2*b*r1^3*log(r1) - 4*b*r1*r2^2*log(r2)^2 + 2*b*r1*r2^2*log(r2) + 4*b*r1*r2^2*log(r1)*log(r2))/(2*(2*r1^2*r2^2 - r1^4 - r2^4 + 4*r1^2*r2^2*log(r1)^2 + 4*r1^2*r2^2*log(r2)^2 - 8*r1^2*r2^2*log(r1)*log(r2))), ...
            (r1*(b*r2^4 - b*r1^2*r2^2 + 2*b*r1^2*r2^2*log(r1) - 2*b*r1^2*r2^2*log(r2) + 4*a*r1*r2^2*log(r1) - 4*a*r1*r2^2*log(r2)))/(2*r1^2*r2^2 - r1^4 - r2^4 + 4*r1^2*r2^2*log(r1)^2 + 4*r1^2*r2^2*log(r2)^2 - 8*r1^2*r2^2*log(r1)*log(r2)), ...
            (2*a*r1^2 - 2*a*r2^2 + b*r1^3 - b*r1*r2^2 - 2*b*r1*r2^2*log(r1) + 2*b*r1*r2^2*log(r2))/(2*r1^2*r2^2 - r1^4 - r2^4 + 4*r1^2*r2^2*log(r1)^2 + 4*r1^2*r2^2*log(r2)^2 - 8*r1^2*r2^2*log(r1)*log(r2)) ];

end

gsol = subs(g, vars, sols);