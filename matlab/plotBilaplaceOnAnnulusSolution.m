% This file plots the solution for the solution of the bilaplace equation
% with Dirichlet boundary conditions on an annulus.

% Since Octave can't deal with the abstract system, we have here
% the solution from Matlab:
% Preparations.
close all;
%clear variables;
pkg load symbolic;
syms a b c1 c2 c3 c4 gamma r r1 r2;

% Configuration: Explicit values for later plotting.
R1 = 1;
R2 = 10;
A  = 0;
B  = 1;
fVariableHeight = true;

% Solution to boundary value problem.
if( ~exist('gsol','var') )
  setSolutionsBilaplaceOnAnnulus;
end

% Substitute explicit values.
gexp = subs(gsol, [r1 r2 a b], [R1 R2 A B] );

% Verify boundary values.
fprintf( 'Function value %f, normal derivative %f.\n', ...
          double(subs(gexp,r,R1)), double(-subs(diff(gexp,r),r,R1)) );

% Plot function.
X  = linspace(R1, R2, 50);
h  = figure('Visible', 'off');
F  = matlabFunction(gexp);
Y  = arrayfun(F, X);
plot(X,Y);
grid on
xlen = max(X)-min(X);
ylen = max(Y)-min(Y);
pbaspect([xlen ylen 1])
print(h, 'annulusSolution.png', '-dpng');