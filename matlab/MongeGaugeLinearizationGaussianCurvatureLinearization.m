syms x y u u1 u2 u11 u12 u21 u22 t

% Define DPhi, metric tensor and integration element.
DP = [1, 0; 0, 1; t*u1, t*u2];
G  = DP.' * DP;
Q  = sqrt(det(G));
Gi = inv(G);

% Define nu and induced matrix N.
nu = [-t*u1; -t*u2; 1]/Q;
N  = nu*nu.';

% Define the surface derivative of the normal.
Dnu_  = t/Q^3 * [t^2*u1*u2*u12 - t^2*u2^2*u11 - u11, t^2 * u1 * u2*u22 - t^2*u2^2*u12-u12; ...
                 t^2*u1*u2*u11-t^2*u2^2*u12-u12, t^2 * u1 * u2 *u12 - t^2*u1^2*u22 - u22; ...
                 t*(u1*u11+u2*u12), t*(u1*u12+u2*u22)];
Dnu   = Dnu_ * Gi * DP.';

% Define the overall regularized extended Weingarten map and
% compute its material derivative.
adjH   = adjoint(simplify(Dnu + N));
adjHt  = simplify(diff(adjH,t));
adjHt0 = simplify(subs(adjHt,t,0));

