% Compute the exact solution of the homogeneous bilaplace equation
% on an annulus.
clear variables;
close all;
%pkg load symbolic; % Octave only.
syms f(r)

fVariableHeight = true;

f1 = diff(f,r);
f2 = diff(f1,r);
f3 = diff(f2,r);
f4 = diff(f3,r);

eqn = f4 + 2/r*f3 - 1/r^2*f2 + 1/r^3*f1;
sol = dsolve(eqn==0); % Does not work in Octave.

% The solution consists out of the following basis functions:
syms r;
g1 = 1;
g2 = r^2;
g3 = log(r);
g4 = r^2*(log(r)-1/2);

% Setting up the system on an annulus.
syms r1 r2 c1 c2 c3 c4 a b gamma
g  = c1*g1 + c2*g2 + c3*g3 + c4*g4;
gn = diff(g,r);

if( fVariableHeight )
  eq1 =  subs(g,r,r1) == 0*a + gamma; % Here we note that a is made obsolete if gamma is present.
else
  eq1 =  subs(g,r,r1) == a;
end
eq2 = -subs(gn,r,r1) == b;
eq3 =  subs(g,r,r2) == 0;
eq4 =  subs(gn,r,r2) == 0;

% The variable height sets another condition.
% This one can be obtained from a simple variational argument.
syms x y
u   = subs(g, r, sqrt(x^2+y^2));
Deltan_u  = diff(diff(u,x,2)+diff(u,y,2),x);
eq5 = subs(Deltan_u,[x,y],[r1,0]) == 0;

% Solve the equation and set solution variable.
if( fVariableHeight )
  eqs = [eq1 eq2 eq3 eq4 eq5];
  vars= [c1 c2 c3 c4 gamma];
  [s1 s2 s3 s4 s5] = solve( eqs, vars );
  sols = [s1 s2 s3 s4 s5];
else
  eqs = [eq1 eq2 eq3 eq4];
  vars= [c1 c2 c3 c4];
  [s1 s2 s3 s4] = solve( eqs, vars );
  sols = [s1 s2 s3 s4];
end

gsol= subs(g, vars, sols);

% Substitute explicit values and plot the solution.
% --> in another file.