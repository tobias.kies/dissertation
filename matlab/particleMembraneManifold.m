% This script tries to give some intuition on what the tangential
% space for the fully coupled membrane-particle interaction manifold
% should look like.
%
% Here we consider the unit interval.

pkg load symbolic;
close all;
syms p x;
assume(p, 'real');
assume(x, 'real');

% We define a specific map X.
% a) Some non-induced X.
X     = x + p*(x^2-1);
Xinv  = (-1 + sqrt(1+4*p*(p+x)))/(2*p);

% xDefine u(p0).
%~ u0  = 1-abs(x);
u0  = 1-x^2;

% Define the general u(p) = u o Xinv(p)
tic;
u   = simplify(subs( u0, x, Xinv ));
u_func  = matlabFunction(u);
fprintf( 'Set up u in %f.\n', toc ); tic;

% Plot u(p,x).
[I,J] = meshgrid( linspace(-0.5, 0.5, 20), linspace(-1, 1, 40) );
Z = arrayfun( u_func, I, J );
fprintf( 'Evaluated u in %f.\n', toc ); tic;
h = figure;
surf(I,J,Z);
xlabel('p');
ylabel('x');

% Plot u_p(p,x).
up  = simplify(diff(u,p));
up_func = matlabFunction(up);
fprintf( 'Set up up in %f.\n', toc ); tic;
Z = arrayfun( up_func, I, J );
fprintf( 'Evaluated up in %f.\n', toc ); tic;
h = figure;
surf(I,J,Z);
xlabel('p');
ylabel('x');

% Plot u_p(0,x)
J = linspace(-1,1,40);
Z = arrayfun( @(x) up_func(0.01,x), J );
h = figure;
plot(J, Z);

