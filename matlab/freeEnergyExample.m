clear variables;
close all;


%%%
% Actual program.
%%%

init();

% Approximate energy differences.
%~ Q = quadEstimate();
%~ Q
%~ tic;
%~ global T;
%~ global taus;
%~ F = arrayfun(@(tau) perturbationMethod(tau,round(T/tau)), taus);
%~ F
%~ toc;


% Compute free energy differences.
tic;
S = linspace(0, 1, 1001);
Q = arrayfun(@quadEstimate, S);
toc;

fh = fopen('../data/free_energy_differences.txt', 'w');
fprintf(fh, '%.15f %.15f\n', [S(:), Q(:)].');
fclose(fh);


function init()
  global beta;
  global R;
  global fR;
  global DfR;
  global taus;
  global slope0;
  global slope1;
  global T;

  % Set general problem parameters.
  beta    = 25;
  slope0  = 0.1;
  slope1  = 0.2;
  L       = 0.9;

  T       = 10;
  taus    = 0.1.^(1:3);

  % Load data.
  data = importdata('../data/free_energy_slope.txt');

  % Sort by first column.
  data = sortrows(data, 1);

  % Remove outliers.
  % -> Not needed.

  % Extract R, f and Df.
  R   = data(:,1);
  fR  = data(:,2);
  DfR = data(:,3);
end


% Indexing function.
function id = idx(r)
  global R;
  N = 1:length(R);
  I = max(N(R <= r));
  
  id = I;
  if(id == length(R))
    id = id-1;
  end
  %~ if(I < length(R) && abs(R(I+1)-r) < abs(R(I)-r))
    %~ id = I+1;
  %~ end
end


% A function for evaluation of the energy.
function fx = f(x)
  global R;
  global fR;
  Rx      = sqrt(x(:,1).^2 + x(:,2).^2);
  rIds    = arrayfun(@idx, Rx);
  lambda  = (Rx-R(rIds))./(R(rIds+1)-R(rIds));
  fx      = (1-lambda).*fR(rIds) + lambda.*fR(rIds+1);
  %~ fx0      = (1-lambda)*fR(rIds) + lambda*fR(rIds+1);
  %~ fx      = fR(rIds);
end

function fx = fr(r)
  fx = f([r(:), 0*r(:)]);
  if(size(fx) ~= size(r))
    fx = fx.';
  end
end


% A function for evaluation of the Jacobian.
function Dfx = Df(x)
  global R;
  global DfR;
  Rx      = sqrt(x(:,1).^2 + x(:,2).^2);
  rIds    = arrayfun(@idx, Rx);
  lambda  = (Rx-R(rIds))./(R(rIds+1)-R(rIds));
  Dfr     = (1-lambda).*DfR(rIds) + lambda.*DfR(rIds+1);
  Rx(Rx == 0) = Inf;
  Dfx     = [Dfr.*x(:,1)./Rx, Dfr.*x(:,2)./Rx];
  %~ Dfx     = [DfR(rIds).*x(:,1)./Rx, DfR(rIds).*x(:,2)./Rx];
end


% Compute a sequence of iterates.
% To-do: Could consider starting in a random point.
function x = eulerMaruyama(tau, N)
  global beta;
  global R;
  x   = zeros(N,2);
  G   = randn(N,2);
  for i = 2:N
    x(i,:)  = x(i-1,:) - tau*Df(x(i-1,:)) + sqrt(2*tau/beta) * G(i,:);
    while(norm(x(i,:)) > max(R))
      x(i,:) = x(i,:) * (2*max(R)/norm(x(i,:))-1);
    end
  end
end


% The observable: exp(-beta*(V1-H0)) = exp(-beta*(s1^2/s0^2-1)*f)
function A = observable(x)
  global slope0;
  global slope1;
  global beta;
  A = exp(-beta*(slope1^2/slope0^2-1)*f(x));
end


% Quadrature-estimate of the free energy difference.
function Q = quadEstimate(s)
  global R;
  global beta;
  global slope1;
  global slope0;
  if nargin < 1
    s = slope1;
  end
  c = beta*s^2/slope0^2;

  % Compute the partition function.
  %Z = 2*pi*integral( @(r) r.*exp(-beta*(f([r(:), 0*r(:)]))), min(R), max(R));
  %Z = 2*pi*quad( @(r) r.*exp(-beta*(f([r(:), 0*r(:)]))), min(R), max(R));
  Z = 2*pi*quad( @(r) r.*exp(-beta*(fr(r))), min(R), max(R));
  Q = 2*pi*quad( @(r) r.*exp(-beta*s^2/slope0^2*fr(r)), min(R), max(R));
  Q = Q/Z;

  % Evaluate the integral over the observable.
  %~ Q = 0;
  %~ for i = 1:(length(R)-1)
    %~ % Q = Q + 0.5*(R(i+1)-R(i))*(observable([R(i) 0])*R(i) + observable([R(i+1) 0])*R(i+1));
    %~ Q = Q + 0.5*(R(i+1)-R(i)) ...
        %~ * ( exp(-c*f([R(i) 0]))*R(i) + exp(-c*f([R(i+1) 0]))* R(i+1));
  %~ end
  %~ Q = Q * 2*pi / Z;
  
  % Then apply the transformation.
  Q = -log(Q)/beta;
  
  %~ Z1 = 2*pi*quad( @(r) r.*exp(-beta*slope1^2/slope0^2*fr(r)), min(R), max(R) );
  %~ Z0 = 2*pi*quad( @(r) r.*exp(-beta*fr(r)), min(R), max(R) );
  %~ Q = (log(Z0)-log(Z1))/beta;
end


% Perturbation method estimate of the free energy difference.
function F = perturbationMethod(tau, N)
  global beta;
  fprintf('Applying perturbationMethod to %.2g and %d.\n', tau, N);
  x   = eulerMaruyama(tau,N);
  %~ [x, sqrt(x(:,1).^2 + x(:,2).^2), f(x)]
  EF  = sum(observable(x))/N
  F   = -log(EF)/beta;
end
