#!/bin/bash

# This script was used to compress some of the graphics with excessive file sizes.
res=2
for file in ../gfx/*.eps
do
  echo $file
  cp $file "$file.uncompressed"
  epstopdf --res=$res $file out.pdf
  pdftops -r $res -eps out.pdf "$file"
done

rm -f out.pdf
