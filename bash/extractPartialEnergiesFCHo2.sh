# Set of partial energies for first iterate.
grep -E "(Accepted|Partial energies|Abort)" ../data/FCHo2.log | head -n1 | cut -f5,7 -d" " > ../data/FCHo2energies.txt

# Partial energies for all subsequent iterates.
grep -E "(Accepted|Partial energies|Abort)" ../data/FCHo2.log | grep -E "(Accepted|Abort)" -B1 --no-group-separator | grep -vE "(Accepted|Abort)" | cut -f5,7 -d" " >> ../data/FCHo2energies.txt
