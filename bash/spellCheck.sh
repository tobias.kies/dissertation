files=$(find ../chapters -name "*.tex" | sort)

i=0
for file in $files
do
  echo "$i: $file"
  i=$((i+1))
done
i=$((i-1))
echo ""

read -p "Which file do you want to spell check? (0-$i): " ID

i=0
for file in $files
do
  if [ $i -eq $ID ]; then
    echo "Going to check $file."
    aspell -t -c -d en_US $file
  fi
  i=$((i+1))
done
