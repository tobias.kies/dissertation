DOCUMENT=diss
mkdir -p "bin"

pdflatex --file-line-error-style --output-dir="bin" "$DOCUMENT.tex" && mv "./bin/$DOCUMENT.pdf" "./$DOCUMENT.pdf"
#bibtex "./bin/$DOCUMENT"
