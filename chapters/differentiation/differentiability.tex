%This section is concerned with an abstract differentiability result for the interaction potential.
%At the heart of the proof is the well-known implicit function theorem.
The knowledge from the previous section on structure-preserving bijections between the admissible sets $\Uad(\p)$ is now used to transform the elastic energy
\begin{align*}
  \E(\p,u) = \int_{\Omega(\p)} \rho\left(x,u(x),Du(x),D^2u(x)\right) \, \mathrm{d}x
\end{align*}
locally onto the domain $\Omega(\p^0)$.
This is done in such a way that an equivalent minimization problem for the transformed energy is stated over a fixed reference space $\Uad(\p^0)$.
Afterwards, the differentiability of the transformed energy is shown and the implicit function theorem is applied in order to reveal the differentiability of the interaction potential.

\begin{lemma}
  \label{thm:transformationFirst}
  Let $\Omega_1,\Omega_2 \subseteq \R^n$ and $X\colon \Omega_1 \rightarrow \Omega_2$ be a diffeomorphism.
  Suppose $u \in H^1(\Omega_2)$ and define $\wt{u} := u \circ X$.
  Then
  \begin{align}
    \label{eq:transformationFirst}
    \nabla u \circ X & = (DX)^{-T} \nabla \wt{u}\text{.}
  \end{align}
\end{lemma}

\begin{proof}
  \cref{thm:sobolevCoordinateChange} asserts $\wt{u} \in H^1(\Omega_1)$.
  Equation \eqref{eq:transformationFirst} is a direct consequence of
  \begin{align*}
    \nabla \wt{u} = \nabla (u\circ X) = (DX)^T \left(\nabla u \circ X\right)
  \end{align*}
  and the invertibility of $DX$ due to the fact that $X$ is a diffeomorphism.
\end{proof}


\begin{lemma}
  \label{thm:transformationSecond}
  Let $\Omega_1,\Omega_2 \subseteq \R^n$ and $X\colon \Omega_1 \rightarrow \Omega_2$ be a $2$-diffeomorphism.
  Suppose $u \in H^2(\Omega_2)$, let $\gamma = \abs{\det(DX)}$, and define $\wt{u} := u \circ X$.
  Then
  \begin{align}
    \label{eq:transformationSecond}
    D^2 u \circ X & = \frac{1}{\gamma} \sum_{k=1}^n \partial_k \left( \gamma (DX^{-T} \nabla \wt{u}) \otimes (DX^{-T}e_k)\right)
    %~ \text{.}
  \end{align}
  where $e_k \in \R^n$ denotes the $k$-th canonical Euclidean basis vector.
\end{lemma}

\begin{proof}
  \cref{thm:sobolevCoordinateChange} implies $\wt{u} \in H^2(\Omega_1)$.
  Moreover, either $\gamma > 0$ or $\gamma < 0$ holds uniformly on $\Omega_1$ because $X$ is a diffeomorphism.
  Therefore, $\abs{\gamma}$ is differentiable (see \cref{thm:matrixDeterminantDerivative} in the appendix).
  Let $\wt{\phi} \in C_0^\infty(\Omega_1)$ be an arbitrary smooth test function with compact support in $\Omega_1$.
  Then $\wt{\phi}\circ X^{-1} \in H^2_0(\Omega_2)$ and application of the transformation formula yields
  \begin{align}
    \label{eq:helper1808021706}
    \int_{\Omega_2} D^2 u \, \phi \d{x}
    & = \sigma \int_{\Omega_1} \gamma (D^2 u \circ X) \, \wt{\phi}\d{x}
    \text{.}
  \end{align}
  where $\sigma := \operatorname{sign}(\gamma)$.
  Integration by parts, the transformation formula and the transformation identity~\eqref{eq:transformationFirst} show
  \begin{align}
    \label{eq:helper1808021707}
    \begin{aligned}
      \int_{\Omega_2} D^2 u \, \phi \d{x}
      & = - \int_{\Omega_2} \nabla u \otimes \nabla \phi \d{x}
      \\ & = -\sigma\int_{\Omega_1} \gamma (DX^{-T}\nabla \wt{u}) \otimes (DX^{-T} \nabla \wt{\phi}) \d{x}
      \\ & = -\sigma\sum_{k=1}^n \int_{\Omega_1} \left( \gamma (DX^{-T}\nabla \wt{u}) \otimes (DX^{-T} e_k)\right) \partial_k \wt{\phi} \d{x}
      \\ & = \sigma\sum_{k=1}^n \int_{\Omega_1} \partial_k \left( \gamma (DX^{-T}\nabla \wt{u}) \otimes (DX^{-T} e_k)\right) \wt{\phi} \d{x}
      \text{.}
    \end{aligned}
  \end{align}
  Comparison of \eqref{eq:helper1808021706} with \eqref{eq:helper1808021707} yields in view of the fundamental theorem of calculus of variations the identity
  \begin{align*}
    \gamma \, D^2 u \circ X = \sum_{k=1}^n \partial_k \left( \gamma (DX^{-T}\nabla \wt{u}) \otimes (DX^{-T} e_k)\right)\text{,}
  \end{align*}
  which proves the transformation formula \eqref{eq:transformationSecond}.
\end{proof}


\begin{proposition}
  \label{thm:ElocDifferentiability}
  Let $(\cX(\p))_{\p \in {\cB}}$ be a family of $m$-diffeomorphisms as in \cref{thm:odeInducedDiffeomorphisms} and let $\xi \in C^m(\cB\times \Omega)$ be as in \cref{thm:wInducedConstraintMaps}.
  Define for every $u \in H^2(\Omega(\p^0))$ the transformed elastic energy by
  \begin{align*}
    \Eloc(\p;u) := \E\left(\p, \left(u+\xi(\p)\right) \circ \cX^{-1}(\p)\right)\text{.}
  \end{align*}
  Then:
  \begin{itemize}
    \item
      For all $\p \in \cB$ and $v \in \Uad(\p)$ holds $\E(\p,v) = \Eloc(\p,v\circ \cX(\p) - \xi(\p))$.
      In particular, every minimizer of the original energy~$\E(\p)$ over $\Uad(\p)$ induces a minimizer of the transformed energy~$\Eloc(\p)$ over $\Uad(\p^0)$, and vice-versa.
  
    \item
      If $\rho \in C^{m-2}(\overline{\Omega}\times\R\times\R^2\times\R^{2\times 2})$ and if the derivatives of the transformed integrand in \eqref{eq:helper1807311231} below are integrable, then $\Eloc \in C^{m-2}({\cB} \times \Uad(\p^0))$.
  \end{itemize}
\end{proposition}

\begin{proof}
  The equivalence of the energies is immediately obtained from
  \begin{align*}
    \E(\p;v)
    & = \E\left(\p; \left(\left(v \circ \cX(\p) - \xi(\p)\right) + \xi(\p) \right) \circ \cX^{-1}(\p) \right)
    \\ & = \Eloc\left(\p; v\circ \cX(\p) - \xi(\p)\right)\text{.}
  \end{align*}
  As of \cref{thm:admissibleSetIsomorphism}, the equivalence of the minimization problems is then apparent from the bijectivity of the map $\Phi(\p)^{-1}\colon \Uad(\p) \rightarrow \Uad(\p^0), v \mapsto  v \circ \cX(\p) - \xi(\p)$.

  In order to prove the differentiability of $\Eloc$, let $\gamma(\p) := \det(D\cX(\p))$, $v(\p) := u + \xi(\p)$
  and
  \begin{align*}
    \zeta(\p) & := \Bigg(\cX(\p),\ v(\p),\ D\cX(\p)^{-T}\nabla v(\p), 
    \\ & \qquad\quad\frac{1}{\gamma(\p)} \sum_{k=1}^2 \partial_k \left( \gamma(\p) (D\cX(\p)^{-T} \nabla v(\p)) \otimes (D\cX(\p)^{-T}e_k)\right)\Bigg)
    \text{.}
  \end{align*}
  In view of \cref{thm:transformationFirst} and \cref{thm:transformationSecond} the transformation formula yields
  \begin{align}
    \label{eq:helper1807311231}
    \Eloc(\p,\wt{u})
    & = \int_{\Omega(\p^0)} \gamma(\p)\,\rho\left(\zeta(\p)\right)\d{x}
    \text{.}
  \end{align}
  \cref{thm:matrixDeterminantDerivative} implies $\gamma \in C^{m-1}$, and by assumption holds $\rho \in C^{m-2}$.
  Closer investigation of $\zeta$ reveals $\zeta \in C^{m-2}$ by assumptions on $\cX$ and $\xi$.
  Altogether, this implies $\Eloc \in C^{m-2}$ if the derivatives of the transformed integrand $\gamma(\p)\rho(\zeta(\p))$ are integrable.
\end{proof}

\noindent
The proof of differentiability of the interaction potential is based on the following formulation of the implicit function theorem:

\begin{theorem}[Implicit function theorem]
\label{thm:implicitFunctionTheorem}
Let $\cB$, $\cU$, $\cW$ be real Banach spaces, $(\p^0,v^0) \in \cB \times \cU$, and let $\cA \subseteq \cB \times \cU$ be an open neighborhood of $(\p^0,v^0)$.
Let $f \in C^k(\cA,\cW)$, $k\geq 1$ such that $f(\p^0,v^0) = 0$ and the partial derivative $f_u(\p^0,v^0)\colon \cU\rightarrow \cW$ is bijective.
Then there exists a neighborhood $\hat{\cB}$ of $\p^0$ and a unique function $v \in C^k(\hat{\cB},\cU)$ such that $(\p,v(\p)) \in \cA$ and $f(\p,v(\p)) = 0$ for all $\p \in \hat{\cB}$.
%~ Furthermore holds
%~ \begin{align*}
  %~ \dd{\p} u(\p) = -F_u(\p,u(\p))^{-1}F_{\p}(\p,u(\p))
  %~ \text{.}
%~ \end{align*}
\end{theorem}
%~ \snote{Check what the definition of $C^k$ in Banach spaces is. Also, should we clarify that we need Fr\'echet partial derivatives?}

\begin{proof}
  This is a special case of \cite[Theorem 4.B]{Zeidler85}.
\end{proof}

%~ The differentiability result simply transforms the minimization problems appearing inside the definition of the interaction potential onto a common reference set. Under suitable conditions it is possible to conclude differentiability of the interaction potential and to state a formula for the computation of its derivative.

\begin{theorem}
  \label{thm:differentiabilityGeneral}
  Suppose $\Eloc$ as in \cref{thm:ElocDifferentiability} with $\Eloc \in C^{m-2}(\cB\times \Uad(\p^0))$ and $m\geq 4$.
  If $u^0$ is the unique global minimizer of $\E(\p^0)$ over $\Uad(\p^0)$ and the second variation $\E_{uu}(\p^0,u^0)\colon \Uad^0(\p^0) \rightarrow \left(\Uad^0(\p^0)\right)'$ is bijective and locally coercive, then there exists a neighborhood $\hat{\cB} \subseteq \cB$ of $\p^0$ such that $\EI \in \C^{m-3}(\hat{\cB})$ and
  \begin{align*}
    \dd{\p} \EI(\p^0) = \Eloc_{\p}(\p^0,u^0)
    \text{.}
  \end{align*}

  \begin{comment}
  %~ Let $\cB \subseteq \cD$ a neighborhood of $\p^0$.
  Suppose there exists a Banach space~$\cU$, an open subset $U_0 \subseteq \cU$, and a family of maps $\Phi(\p)\colon U_0 \rightarrow \Uad(\p)$ such that $\Phi(\p)$ is a bijection for every $\p \in \cB$.
  Furthermore, define the map
  \begin{align}
    \label{eq:defEloc}
    \begin{aligned}
      \Eloc\colon \cB \times U_0 & \longrightarrow \R
      \\ (\p,u) & \longmapsto \E\left(\p, \Phi(\p;u)\right)\text{,}
    \end{aligned}
  \end{align}
  suppose $\argmin_{v \in \Uad(\p^0)} \E(\p^0,v) = \{u^\ast\}$ and define $u^0 := \Phi(\p^0)^{-1}(u^\ast)$.
  Assume that the partial Fr\'echet-derivative $\Eloc_u$ exists on $\cB \times U_0$, that $\Eloc_u \in C^m(\cB\times U_0)$, and that $\Eloc_{uu}(\p^0,u^0)\colon \cU \rightarrow \cU'$ is a bijection.
  Then there exists a neighborhood $\hat{\cB}\subseteq\cB$ of $\p^0$ such that $\EI \in C^m(\cB)$ and
  \begin{align*}
    \dd{\p} \EI(\p^0) = \Eloc_{\p}(\p^0,u^0)
    \text{.}
  \end{align*}
  %~ \snote{A tiny hole in the proof is that differentiability is only proven on some $\hat{\cB}$. But this does not matter, actually. So we might fix this by adding a simple sentence to the proof.}
  \end{comment}
\end{theorem}

\begin{proof}
  Let $\cU := \Uad^0(\p^0)$ the homogeneous admissible set, define $\cW := \cU'$ to be its dual space, and let $\cA := \cB \times \cU$.
  Define the corresponding shifted energy function by $F(\p,v) := \Eloc(\p,u^0+v)$ for all $v \in \Uad^0(\p^0)$.
  Then the minimizer of $F(\p^0,\cdot)$ over $\cU$ is given by $v^0 := 0 \in \cU$,
  and $f := F_u \in C^{m-3}(\cA)$ by assumption on $\Eloc$.

  Because $u^0$ is a minimizer of $\E(\p^0)$, it is in particular a critical point of $\E(\p^0)$ and so its first variation yields $\E_u(\p^0,u^0) = 0$.
  Consequently, also
  \begin{align*}
    f(\p^0,v^0) = F_u(\p^0,0) = \Eloc_u(\p^0,u^0) = \E_u(\p^0,u^0) = 0\text{.}
  \end{align*}
  Again by assumption, $J_{uu}(\p^0,u^0) = f_u(\p^0,v^0)\colon \cU \rightarrow \cW$ is invertible, and therefore the implicit function theorem is applicable to $f$ for $k:=m-3$.
  Hence, a neighborhood $\hat{\cB}$ of $\p^0$ and a function $v \in C^{m-3}(\hat{\cB},\cU)$ exist such that $v(\p^0) = v^0$ and $\Eloc_u(\p,v(\p)+u^0) = f(\p,v(\p)) = 0$ for all $\p \in \hat{\cB}$.  
  For a sufficiently small neighborhood $\hat{\cB}$, the uniqueness of the minimizer $u^0$ and the continuity properties of $\Eloc$ together with its local coercivity imply that every $u(\p) := v(\p) + u_0 \in \Uad(\p^0)$ is again a global minimizer of $\Eloc(\p)$ over $\Uad(\p^0)$.
  %~ To see this, assume that for every neighborhood $\hat{\cB}$ of $\p^0$ exist $\p \in \hat{\cB}$ and $\wt{u}(\p) \in \Uad(\p^0)$ such that $\Eloc(\p,\wt{u}(\p)) < \Eloc(\p,u(\p))$.
  %~ \snote{Check once more if really true. Maybe mention the local coercivity.}  
  Since the $u(\p)$ are global minimizers it holds
  \begin{align*}
    \EI(\p)
    = \min_{u \in \Uad(\p)} \E(\p,u)
    = \min_{u \in \Uad(\p^0)} \Eloc(\p,u)
    = \Eloc(\p,u(\p))\text{.}
  \end{align*}
  Because $u(\p) \in C^{m-3}(\hat{\cB})$ and because $\Eloc \in C^{m-2}(\cB \times \Uad(\p^0))$ is fulfilled, it follows $\EI(\p) \in C^{m-3}(\hat{\cB})$.
  Finally, taking the first derivative of $\EI$ leads via the chain rule to
  \begin{equation*}
    \dd{\p} \EI(\p^0)
    = \Eloc_\p\left(\p^0,u(\p^0)\right) + \Eloc_u\left(\p^0,u(\p^0)\right) \cdot \dd{\p} u(\p^0)
    = \Eloc_\p\left(\p^0,u(\p^0)\right)
    = \Eloc_\p(\p^0,u^0)\text{.}
    %~ \qedhere
  \end{equation*}
  and completes the proof.
  
  \begin{comment}
  Let $\cP$ the linear hull of $\cD$, let $\cW := \cU'$ be the dual space of $\cU$, let $\cA := \cB \times U_0$, and $F := \Eloc_u$.
  By the given assumptions the requirements for the implicit function theorem in the sense of \cref{thm:implicitFunctionTheorem} are met and therefore a neighborhood $\hat{\cB} \subseteq \cB$ of $\p^0$ and a unique function $u \in C^m(\hat{\cB},\cU)$ exist such that $\Eloc_{u}(\p,u(\p)) = 0$ for all $\p \in \hat{\cB}$.
  %~ \TK[Here is a little missing link: Why are the $u(\p)$ again minimizers of $\Eloc(\p,\cdot)$? It should follow by continuity and the optimality conditions. Is there an elegant and short argument?]{By continuity it follows that $u(\p) \in \argmin_{u \in U_0} \Eloc(\p,u)$.}
  By uniqueness of the minimizer $u^\ast$ and the continuity properties of $J$ it may be assumed that $u(\p) \in \argmin_{u \in U_0} \Eloc(\p,u)$ holds; else $\hat{\cB}$ is restricted to a suitable subset.
  Thus, by definition of the interaction potential and by bijectivity of the $\Phi(\p)$ holds on $\hat{\cB}$
  \begin{align*}
    \EI(\p)
    = \min_{u \in \Uad(\p)} \E(\p,u)
    = \min_{u \in U_0} \E(\p,\Phi(\p;u))
    = \min_{u \in U_0} \Eloc(\p,u)
    = \Eloc(\p,u(\p))\text{.}
  \end{align*}
  Consequently, and as of the definition and differentiability of the $u(\p)$,
  \begin{align*}
    \dd{\p} \EI(\p^0)
    & = \dd{\p} \Eloc(\p,u(\p))
    = \Eloc_{\p}(\p,u(\p)) + \Eloc_{u}(\p,u(\p)) \dd{\p} u(\p)
    = \Eloc_{\p}(\p,u(\p))
    \text{.}
    \qedhere
  \end{align*}
  \end{comment}
\end{proof}

\begin{remark}[Extension to local minimizers]
  \label{thm:differentiabilityLocalMinimizer}
  A similar result holds true also in the case where $u^0$ merely is a local and not necessarily the unique global minimizer.
  In that case a neighborhood $\cB_{\p} \times \cB_u \subseteq \cB \times \Uad(\p^0)$ of the point~$(\p^0,u^0)$ exists such that $u^0$ is the unique global minimizer of $\E(\p^0)$ over $\cB_u$.
  With the same arguments as above, one obtains the differentiability of the \emph{local interaction potential}
  \begin{align*}
    \EI_{\text{loc}}(\p) := \min_{v \in \Phi(\p;\cB_u)} \E(\p,v)
  \end{align*}
  in a neighborhood of $\p^0$ as well as
  \begin{align*}
    \dd{\p} \EI_{\text{loc}}(\p^0) = \Eloc_{\p}(\p^0,u^0)
    \text{.}
  \end{align*}
  Here $\Phi(\p)$ is the bijection between $\Uad(\p^0)$ and $\Uad(\p)$ as defined in \cref{thm:admissibleSetIsomorphism}.
\end{remark}

\begin{remark}[Relaxing the differentiability requirements]
  It is possible to relax the smoothness assumptions for $\Eloc$ in \cref{thm:differentiabilityGeneral} to a certain extent, even without major changes in the given proofs.
  More specifically, one observes for example that the actual proof only requires the differentiability $\Eloc_u \in C^{m-3}$, but not really the stricter condition $\Eloc \in C^{m-2}$.
  Similarly, several of the assumptions in previous results can be refined in order to reduce smoothness requirements, if necessary.
  
  This aspect is not treated in greater detail within this work as smoothness requirements usually do not pose any relevant restriction in the model problems that are considered later on.
  Therefore, a more rigorous treatment of the smoothness conditions is omitted
  %generous treatment of the smoothness conditions is preferred
  for the sake of a simpler presentation.
\end{remark}


\begin{comment}
\begin{lemma}
\label{thm:differentiabilityLinear}
Let $\p^0 \in \cD^0$ and $\cB \subseteq \cD$ a neighborhood of $\p^0$.
Suppose that there exists a Banach space $\cU$ and a family of maps $\Phi(\p)\colon \cU \rightarrow \Uad(\p)$ such that $\Phi(\p)$ is a bijection for every $\p \in \cB$, and define $\Eloc$ by \eqref{eq:defEloc}.
Assume that $a(v,w) := \E_{uu}(\p^0,u;v,w)$ defines an elliptic bilinear form on $\Uad(\p^0)$ for any $u \in \Uad(\p^0)$ and that $\Eloc \in C^1(\cB \times \cU)$.
Then $\EI \in C^1(\cB)$ and with $u^\ast := \argmin_{u \in \Uad(\p^0)} \E(\p^0,u)$ and $u^0 := \Phi(\p^0)^{-1}(u^\ast)$ holds
\begin{align*}
  \dd{\p} \EI(\p^0) = \Eloc_{\p}(\p^0,u^0)
\text{.}
\end{align*}
\end{lemma}

\begin{proof}
By the well-known theorem of Lax-Milgram, the ellipticity of $a$ implies the invertibility of $\Eloc_{uu}(\p^0,u^0)$ and the existence and uniqueness of $u^\ast$.
\snote{Actually, may not be that obvious. Also: Reference?}
The claim then follows as a direct application of \cref{thm:differentiabilityGeneral}.
\end{proof}
\snote{It appears to be overkill to basically repeat the full theorem only to do the slight modification of introducing $a$. Alternatives would be to just only state the linear version or to make it a little lemma where ellipticity implies those other properties and which the then can use later when proving things about our linear model. Maybe the part on ellipticity even has been mentioned earlier such that we can just make the result a remark or little corollary. Also: try to be consistent in the notation!}

\TK{To-do: Results are too abstract, especially with the assumption that $\Eloc$ needs to be differentiable. Maybe rather choose a class of functions for which we know that $\Eloc$ will be differentiable, or at least discuss such a class.}
\end{comment}
