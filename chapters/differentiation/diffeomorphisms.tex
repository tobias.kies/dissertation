The following paragraphs are concerned with linking the spaces $\Uad(\p)$ to each other through so-called trace preserving diffeomorphisms between the domains~$\Omega(\p)$
%~ together with 
as well as
so-called constraint preserving maps.

\begin{comment}
Before the implicit function theorem can be applied, we need to transform the energies $\E(\p,\cdot)$ in a neighborhood of $\p^0$ onto a common reference space.
Such transformations can be obtained from diffeomorphisms between the domains $\Omega(\p)$ if they have a constraint preserving property.
This idea is made precise by the next two statements, right after we clarify some notation.
\label{sec:diffeomorphisms}

For ease of presentation we restrict ourselves in the following to curve constraints that are parameterized over a domain $\Omega(\p)$ where the exterior boundary $\partial\Omega$ is equipped with homogeneous Dirichlet boundary conditions.
This means that
\begin{align*}
  \Uad(\p) = \left\{ v \in \hat{H}(\p) \mid T(\p;v) = g(\p) \right\}
\end{align*}
where $g(\p) \in H^{3/2}(\Gamma(\p)) \times H^{1/2}(\Gamma(\p))$ and
\begin{align*}
  \hat{H}(\p) = \left\{ v \in H^2(\Omega(\p)) \mid v|_{\partial\Omega} = \partial_\nu v|_{\partial\Omega} = 0 \right\}\text{.}
\end{align*}
In order to relate the trace spaces for the different $\p$ to each other, we assume that a map $\psi \in C^2(\cB \times \Omega, \R^2)$ is known such that $\psi(\p,\cdot)|_{\partial\Omega} = \id$, $\psi(\p^0,\cdot)|_{\Gamma(\p^0)} = \id$ and such that $\psi(\p,\cdot)|_{\Gamma(\p^0)}$ is bijective onto $\Gamma(\p)$.
Then we define a family of modified trace operators by
\begin{align*}
  \hatT(\p)\colon H^2(\Omega(\p)) & \longrightarrow H^{3/2}(\partial\Omega(\p)) \times H^{1/2}(\partial\Omega(\p))
  \\ v & \longmapsto (u|_{\partial\Omega(\p)}\circ \psi(\p), \partial_\nu u|_{\partial\Omega(\p)}\circ \psi(\p) )
\end{align*}
as well as $\hatg(p) = g(p) \circ \psi(\p)$ with the convention $\hatg(\p)|_{\partial\Omega} \equiv 0$.
As a consequence, the admissible sets may also be stated as
\begin{align*}
  \Uad(\p) = \left\{ v \in H^2(\Omega(\p)) \mid \hatT(\p;v) = \hatg(\p) \right\}
\end{align*}

%~ Recall that the admissible sets are defined as
%~ \begin{align*}
  %~ \Uad(\p) = \left\{ v \in \hat{H}(\p) \mid T(\p;v) = g(\p) \right\}
%~ \end{align*}
%~ where $T\colon \hat{H}(\p) \rightarrow W(\p)$ is a bounded linear operator into some vector space $W(\p)$ and $\hat{H}(\p) \subseteq H^2(\Omega(\p))$ is a linear subspace.
%~ For ease of presentation we restrict ourselves in the following to curve constraints, \ie the case where $T$ is the trace operator on $\Omega(\p)$, and to
%~ \begin{align*}
  %~ \hat{H}(\p) = \left\{ v \in H^2(\Omega(\p)) \mid v|_{\partial\Omega} = \partial_\nu v|_{\partial\Omega} = 0 \right\}\text{,}
%~ \end{align*}
%~ \ie Dirichlet boundary conditions of the parameterization domain $\Omega$.

Note that later on we also remark on generalizations of the transformation results below to point constraints and average constraints, as well as to domains with periodic boundary conditions.
\end{comment}

\begin{theorem}
  \label{thm:sobolevCoordinateChange}
  Let $\Omega_1,\Omega_2\subseteq \R^2$ and suppose a $k$-diffeomorphism $X\colon \Omega_1 \rightarrow \Omega_2$ where $k \in \N$.
  Then the map
  \begin{align*}
    H^k(\Omega_1) & \longrightarrow H^k(\Omega_2)
    \\ u & \longmapsto u \circ X^{-1}
  \end{align*}
  is well-defined and an isomorphism.
\end{theorem}

\begin{proof}
  This statement is proven in \cite[Theorem 3.35]{Adams75}.
\end{proof}


\begin{lemma}
  \label{thm:admissibleSetIsomorphism}
  Suppose $p \in \cD$ and that $X\colon \Omega(\p^0) \rightarrow \Omega(\p)$ is a $2$-diffeomorphism with the \emph{trace preserving property}
  \begin{align}
    \label{eq:tracePreservation}
    \forall{u \in H^2(\Omega(\p^0))}\colon \ \hatT(\p; u \circ X^{-1}) = \hatT(\p^0;u) \circ X^{-1}|_{\Gamma(\p)}
    %~ \text{.}
  \end{align}
  and with Dirichlet boundary conditions
  \begin{align}
    \label{eq:tracePreservationDirichlet}
    X|_{\partial\Omega} & = \operatorname{id}_{\partial\Omega}
    ,\qquad DX\nu|_{\partial\Omega} = \nu
    \text{.}
  \end{align}
  Furthermore, let $\xi \in H^2(\Omega(\p^0))$ with the \emph{constraint preserving property}
  \begin{align}
    \label{eq:xi_condition1}
    \hatT(\p^0;\xi) = \hatg(\p)\circ X|_{\Gamma(\p^0)}-\hatg(\p^0)
    %~ \text{.}
  \end{align}
  and with homogeneous Dirichlet boundary conditions
  \begin{align}
    \label{eq:xi_condition2}
    \xi|_{\partial\Omega} & = 0
    ,\qquad \partial_\nu\xi|_{\partial\Omega} = 0
    \text{.}
  \end{align}
  Then
  \begin{align*}
    \phi\colon \Uad(\p^0) & \longrightarrow \Uad(\p)
    \\ u & \longmapsto (u+\xi) \circ X^{-1}
  \end{align*}
  is a bijection.
\end{lemma}

\begin{proof}
  Well-definedness of $\phi$ as a map $\Uad(\p) \rightarrow H_\ast^2(\Omega(\p))$ is given by \cref{thm:sobolevCoordinateChange} and the Dirichlet condition~\eqref{eq:xi_condition2}.
  The linearity of $\hatT$, the trace preserving property of $X$, and the constraint preserving property of $\xi$ yield for all $u \in \Uad(\p^0)$
  \begin{align*}
    \hatT(\p;\phi(u))
    & = \hatT(\p;u\circ X^{-1}) + \hatT(\p;\xi\circ X^{-1})
    \\ & = \hatT(\p^0;u)\circ X^{-1}|_{\Gamma(\p)} + \hatT(\p^0;\xi)\circ X^{-1}|_{\Gamma(\p)}
    \\ & = g(\p^0) \circ X^{-1}|_{\Gamma(\p)} + g(\p) - g(\p^0) \circ X^{-1}|_{\Gamma(\p)}
    \\ & = \hatg(\p)\text{.}
  \end{align*}
  Here also the definition of $\Uad(\p^0)$ and the bijectivity of $X$ entered.
  Similarly, from the Dirichlet conditions~\eqref{eq:tracePreservationDirichlet} and~\eqref{eq:xi_condition2} also the outer boundary conditions
  \begin{align*}
    (u+\xi)\circ X^{-1}|_{\partial\Omega} = (u+\xi)|_{\partial\Omega} = 0
  \end{align*}
  and
  \begin{align*}
    \partial_\nu \left( (u+\xi)\circ X^{-1} \right)|_{\partial\Omega}
    & = \nabla (u+\xi) \cdot \left((DX)^{-1} \circ X^{-1}\right)\nu|_{\partial\Omega}
    %~ = \nabla (u+\xi) \cdot (DX)^{-1}\nu
    %~ = \nabla (u+\xi)\cdot \nu
    = \partial_\nu u|_{\partial\Omega} + \partial_\nu \xi|_{\partial\Omega}
    = 0
  \end{align*}
  are verified.
  Therefore, $\phi(u) \in \Uad(\p)$ is true.
  Since \cref{thm:sobolevCoordinateChange} implies injectivity of $\phi$, it only remains to prove surjectivity.
  To this end, let $\wt{u} \in \Uad(\p)$ and define the function $u:= \wt{u}\circ X-\xi$.
  Analogously to before, and again by \cref{thm:sobolevCoordinateChange} and the properties~\eqref{eq:tracePreservation},~\eqref{eq:xi_condition1} and~\eqref{eq:xi_condition2} it is observed that $u \in H_0^2(\Omega(\p^0))$ holds as well as
  \begin{align*}
    \hatT(\p^0;u)
    & = \hatT(\p;\wt{u}) \circ X|_{\Gamma(\p^0)}- \hatT(\p^0;\xi)
    = \hatg(\p^0)\text{,}
  \end{align*}
  implying the desired surjectivity of $\phi$.
\end{proof}

%~ \noindent
%~ In the next step, smooth families of trace preserving diffeomorphisms $\cX(\p)$ are constructed in a generic neighborhood~$\cB$ of $\p^0$.
In the next step, a family $\cX$ of trace preserving diffeomorphisms $\cX(\p)\colon \Omega(\p^0) \rightarrow \Omega(\p)$ is constructed over a generic neighborhood~$\cB$ of $\p^0$.
The construction makes use of the fact that the domains $\Omega(\p)$ are determined by their interfaces $\Gamma(\p)$ and that those interfaces are related to each other through a smooth family
%~ of bijections
$\psi(\p)\colon \Gamma(\p^0) \rightarrow \Gamma(\p)$
of bijections.
These maps $\psi(\p)$ are induced by the geometric properties of the model and rely on the parameterization map $\varphi$, the transformation map $\Psi$ from the definition of the parameterized particle manifolds $\cG(\p_i) := \Psi(\p_i;\cG_0)$, the local projection $\pi$ onto the reference manifold $\cM_0$ and its local inverse $\copi(\p_i)$ with respect to $\cG(\p_i)$.
For $x \in \Gamma_i(\p_i^0)$ one then defines
\begin{align*}
  \psi(\p,x) :=
    \varphi^{-1}\Bigg(
      \pi\Bigg(
        \Psi\bigg(\p_i;
          \Psi^{-1}\Big(\p_i^0;
            \copi\left(\p_i^0;
              \varphi(x)
            \right)
          \Big)
        \bigg)
      \Bigg)
    \Bigg)\text{.}
\end{align*}
%~ Here the abbreviating notation $\Psi^{-1}(\p,\cdot) := \Psi(\p)^{-1}$ is used.
In words, any point $x$ on an interface of $\Gamma(\p_i^0)$ is mapped back onto the reference manifold $\cM_0$ by $\varphi$ and then mapped back onto the parametric particle manifold $\cG(\p_i^0)$ through the local inverse projection $\copi$.
Then the transformation of $\Psi(\p_i^0)$ is undone and replaced by the transformation $\Psi(\p_i)$ in order to obtain a point of $\cG(\p_i)$, which then is projected onto the reference manifold $\cM_0$ and finally mapped back onto the parameterization domain $\Omega$ by $\varphi^{-1}$.
A geometric interpretation of $\psi$ is depicted in \cref{fig:Psi}.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.59\textwidth]{psiDiagram.pdf}
  \end{center}
  \caption{The red arrow shows the action of the map $\psi(\p;\cdot)$ on an interface point~$x$. It is defined by the concatenation of the geometric operations indicated by the black arrows.}
  \label{fig:Psi}
\end{figure}

By definition, $\psi$ admits a canonical extension to a sufficiently small neighborhood $\wt{\Gamma}_0$ of $\Gamma(\p^0)$.
Since the smoothness of the involved manifolds and transformations is directly inherited by $\psi$, it is in many cases justified to make generous smoothness assumptions on $\psi$ over $\wt{\Gamma}_0$.

%~ Note that the map $\psi(\p)\colon \Gamma(\p^0) \rightarrow \Gamma(\p)$ admits a canonical extension to a sufficiently small neighborhood $\wt{\Gamma}_0$ of $\Gamma(\p^0)$.

\begin{proposition}
  \label{thm:odeInducedDiffeomorphisms}
  Let $m \in \N_{\geq 2}$ and suppose $\psi \in C^{m+1}(\cB \times \wt{\Gamma}_0, \R^2)$.
  Furthermore, let $\cV \in C^m([0,1] \times \cB \times \R^2, \R^2)$ be a time- and configuration-dependent vector field such that the following properties are fulfilled:
  \begin{itemize}
    \item All partial derivatives of $\cV$ are bounded.
    \item For all $(t,x) \in [0,1] \times \R^2$ holds $\cV(t,\p^0,x) = 0$ 
    \item For all $(t,\p) \in [0,1] \times \cB$ holds
      \begin{align}
        \label{eq:cV_traceCondition1}
        \cV(t,\p,\cdot)|_{\Gamma(\p)} & = \psi_{\p}\left(\p(t),\psi^{-1}(\p(t),\cdot)\right)(\p-\p^0)
      \end{align}
      where $\p(t) := \p^0+t(\p-\p^0)$.
    \item
      If $T$ denotes the classical trace operator for curves and not the point evaluation operator, then for all $(t,\p) \in [0,1] \times \cB$ also holds
      \begin{align}
        \label{eq:cV_traceCondition2}
        D\cV(t,\p,\psi(\p(t),\cdot))|_{\Gamma(\p^0)} \cdot \nu(t) & = \nu'(t)
      \end{align}
      where $\nu(t) := \nu|_{\Gamma(\p(t))} \circ \psi(\p(t))$.
    \item
      The Dirichlet boundary conditions
      \begin{align}
        \label{eq:cV_traceCondition3}
        \begin{aligned}
          \cV(t,\p,\cdot)|_{\partial\Omega} & = 0
          \\ D\cV(t,\p,\psi(\p(t),\cdot))\nu|_{\partial\Omega} & = 0
        \end{aligned}
      \end{align}
      hold.
  \end{itemize}
  Define $\cX(\p,x) := \eta(1,\p,x)$ where $\eta(\cdot,\p,x)$ solves the ordinary differential equation
  \begin{align}
    \label{eq:odeEta}
    \begin{aligned}
      \dd{t}\eta(t,\p,x) & = \cV\left(t,\p,\eta(t,\p,x)\right)
      \\ \eta(0,\p,x) & = x\text{.}
    \end{aligned}
  \end{align}
  Then $\cX \in C^m(\cB \times \R^2, \R^2)$, and $\cX(\p^0,\cdot) = \id_{\R^2}$, and for all $\p \in \cB$ the restriction $\cX(\p)|_{\Omega(\p^0)}$ is an $m$-diffeo\-morphism onto $\Omega(\p)$ that fulfills the trace preserving property~\eqref{eq:tracePreservation} and the Dirichlet condition~\eqref{eq:tracePreservationDirichlet}.
\end{proposition}

\begin{proof}
  The solution function $\eta$ exists and is unique in view of the Picard-Lindel\"of theorem~\ref{thm:odePicardLindelof} since the bounded first derivatives of $\cV$ imply its Lipschitz-continuity.
  %~ First note that $\eta$ is well-defined in view of \cref{thm:odePicardLindelof} as the bounded first derivatives of $\cV$ imply its Lipschitz-continuity.
  The property $\cX(\p^0,\cdot) = \id_{\R^2}$ is immediate because $\cV(\cdot,\p^0,\cdot) \equiv 0$ holds and therefore the solution of the ODE \eqref{eq:odeEta} in $(\p^0,x)$ is given by $\eta(t,\p^0,x) := x$.
  The smoothness $\cX \in C^m(\cB\times \R^2,\R^2)$ is a consequence of a standard result of ODE theory, see also \cref{thm:odeSmoothness} in the appendix.
  Note that, analogously, by considering the inverse ODE of~\eqref{eq:odeEta} the existence of $\cX(\p)^{-1} \in C^m(\R^2,\R^2)$ is asserted.
  
  %and the smoothness $\cX \in C^m$ (as well as $\cX(\p)^{-1} \in C^m$) is a consequence of 
  From $\cV$'s boundary condition~\eqref{eq:cV_traceCondition1} and the uniqueness of $\eta$ one infers for every $x \in \Gamma_0$ that $\eta(t,\p,x) = \psi(\p(t),x)$ holds.
  This is because of $\psi(\p^0,x) = x$ and
  \begin{align*}
    \dd{t} \psi(\p(t),x)
    & = \psi_{\p}(\p(t),x) (\p-\p^0)
    \\ & = \psi_{\p}\left(\p(t), \psi^{-1}(\p(t),\psi(\p(t),x))\right) (\p-\p^0)
    \\ & = \cV(t,\p,\psi(\p(t),x))\text{.}
  \end{align*}
  In particular, $\cX(\p)|_{\Gamma(\p^0)} = \psi(\p)$, which further implies that $\cX(\p)|_{\Gamma(\p^0)}$ is a bijection onto the interface $\Gamma(\p)$.
  Analogously, the outer boundary condition~\eqref{eq:cV_traceCondition3} implies the identity $\cX(\p)|_{\partial\Omega} = \operatorname{id}_{\partial\Omega}$,
  and therefore $\cX|_{\partial\Omega(\p^0)}$ is bijective onto ${\partial\Omega(\p)}$.
  %by construction of $\psi$,
  In consequence,
  %it is readily obtained that
  the restriction $\cX(\p)|_{\Omega(\p^0)}$ is an $m$-diffeomorphism onto $\Omega(\p)$.
  
  If $T$ is the point evaluation operator, then the trace preserving property~\eqref{eq:tracePreservation} is immediate from the bijectivity of $\cX(\p)|_{\Gamma(\p^0)}$ onto $\Gamma(\p)$ because
  \begin{align}
    \label{eq:helper201808021251}
    \begin{aligned}
      T(\p; u \circ \cX(\p)^{-1})
      & = u \circ \cX(\p)^{-1}|_{\Gamma(\p)}
      \\ & = u|_{\Gamma(\p^0)} \circ \cX(\p)^{-1}|_{\Gamma(\p)}
      \\ & = T(\p^0;u) \circ \cX(\p)^{-1}|_{\Gamma(\p)}
      \text{.}
    \end{aligned}
  \end{align}
  If $T$ is the curve trace operator, then \eqref{eq:helper201808021251} analogously holds for the first component of $T$, but it remains to show that also the normal derivatives are preserved.
  %To this end, let $\tau$ a unit tangent vector field on $\partial\Omega(\p^0)$.
  
  \begin{comment}
  \myhrule
  %From $\cX(\p)|_{\Gamma(\p^0)} = \psi(\p)$ follows $D\cX(\p)\tau = D\psi(\p)\tau$.
  %~ \cref{thm:odeDerivative} states $D\cX = \wt{\eta}(1,\p,x)$ where $\wt{\eta}$ solves the ordinary differential equation
  %~ \begin{align*}
    %~ \dd{t} \wt{\eta}(t,\p,x) & = DV(t,\p,\eta(t,\p,x)) \wt{\eta}(t,\p,x)
    %~ \\ \wt{\eta}(0,\p,x) & = \id_{\R^2}\text{.}
  %~ \end{align*}
  Property \eqref{eq:cV_traceCondition2} yields
  \begin{align*}
    \nu'(t)\nu(0)^T\nu(0) &  = DV(t,\p,\psi(\p(t),\cdot))\nu(t)\nu(0)^T\nu(0)\text{,}
  \end{align*}
  which implies $\wt{\eta}(t,\p,x)\nu(0) = \nu(t)\nu(0)^T$.
  Therefore, $D\cX(\p)|_{\Gamma(\p^0)} \cdot \nu|_{\Gamma(\p^0)} = \nu|_{\Gamma(\p)} \circ \psi(\p)$ holds true.
  Similarly, \eqref{eq:cV_traceCondition3} implies $D\cX(\p)|_{\partial\Omega} \cdot \nu|_{\Gamma(\p^0)} = \nu|_{\Gamma(\p^0)}$.
  
  Consequently, for $u \in H^2(\Omega(\p^0))$ and $\wt{u} := u \circ \cX(\p)^{-1}$ holds
  \begin{align*}
    \partial_\nu \wt{u} \circ \psi(\p)|_{\partial\Omega(\p^0)}
    & = (D\cX(\p))^T \nabla u \cdot \nu|_{\partial\Omega(\p^0)}
    = \nabla u \cdot \nu|_{\partial\Omega(\p^0)} = \partial_\nu u\text{.}
  \end{align*}
  \myhrule
  \end{comment}
  
  To this end, note that by another basic result of ODE theory, the derivative of $\cX$ is again described by the solution of an ODE (see \cref{thm:odeDerivative} in the appendix).
  It holds $D\cX(\p,x) = \wt{\eta}(1,\p,x)$ where $\wt{\eta}$ solves the initial value problem
  \begin{align*}
    \dd{t} \wt{\eta}(t,\p,x) & = D\cV(t,\p,\eta(t,\p,x)) \wt{\eta}(t,\p,x)
    \\ \wt{\eta}(0,\p,x) & = \id_{\R^2}\text{.}
  \end{align*}
  %~ In particular and as of $\eta(t,\p,x) = \psi(\p(t),x)$ on $\Gamma(\p^0)$, the normal derivative $\partial_\nu \cX(\p,x)$ is described by the solution $\hat{\eta}$ of
  %~ \begin{align*}
    %~ \dd{t} \hat{\eta}(t,\p,x) & = D\cV(t,\p,\psi(\p(t),x)) \hat{\eta}(t,\p,x)
    %~ \\ \hat{\eta}(0,\p,x) & = \nu|_{\Gamma(\p^0)}(x)\text{.}
  %~ \end{align*}
  For $x \in \Gamma(\p^0)$ a solution candidate is given by
  \begin{align*}
    y(t,\p,x) & := D\psi(\p(t),x) \tau \otimes \tau + \nu(t) \otimes \nu^0, \qquad \p(t) = \p^0 + t(\p-\p^0)
  \end{align*}
  where $\nu^0 := \nu|_{\Gamma(\p^0)}$ is the outer unit normal field on $\Gamma(\p^0)$ and  $\tau = (-\nu^0_2, \nu^0_1)^T$ is a unit tangent field on $\Gamma(\p^0)$.
  The solution property is verified in multiple steps:
  Firstly, because of $\psi(\p^0) = \id|_{\Gamma(\p^0)}$ and $\nu(0) = \nu^0$ it holds
  \begin{align*}
    y(0,\p,x) = \tau \otimes \tau + \nu^0 \otimes \nu^0 = \operatorname{\id}_{\R^2}
  \end{align*}
  and so the initial condition is met for the candidate $y(\cdot,\p,x)$.
  Moreover, the boundary condition~\eqref{eq:cV_traceCondition1} for $\cV$ gives the identity
  \begin{align*}
    \cV\left(t,\p,\psi(\p(t),\cdot)\right)|_{\Gamma(\p^0)} & = \psi_\p(\p(t),\cdot)(\p-\p^0)
    \text{,}
  \end{align*}
  which again implies on $\Gamma(\p^0)$ by taking the tangential derivative
  \begin{align}
    \label{eq:helper1808022316}
    D\cV\left(t,\p,\psi(\p(t),\cdot)\right) D\psi(\p(t),\cdot) \tau|_{\Gamma(\p^0)}
    & = D\psi_\p(\p(t),\cdot)(\p-\p^0)\text{.}
  \end{align}
  Combining the identity~\eqref{eq:helper1808022316} with the boundary condition~\eqref{eq:cV_traceCondition2} for $D\cV$ leads together with $\eta(t,\p,\cdot)|_{\Gamma(\p^0)} = \psi(\p(t),\cdot)$ in the right hand side of the initial value problem to
  \begin{align*}
    D\cV\left(t,\p,\eta(t,\p,x)\right) y(t,\p,x)
    & = D\cV\left(t,\p,\psi(\p(t))\right)\left(D\psi(\p(t),x) \tau \otimes \tau + \nu(t) \otimes \nu^0\right)
    \\ & = D\psi_\p(\p(t),x)(\p-\p^0)\tau\otimes\tau + \nu'(t) \otimes \nu^0
    \text{.}
  \end{align*}
  On the other hand, differentiation of $y$ in $t$ yields
  \begin{align*}
    \dd{t} y(t,\p,x)
    & = D\psi_\p(\p(t),x)(\p-\p^0)\tau\otimes\tau + \nu'(t) \otimes \nu^0
    \text{,}
  \end{align*}
  which indeed shows that $\wt{\eta}(t,\p,x) = y(t,\p,x)$ is the solution on $\Gamma(\p^0)$.
  In particular,
  \begin{align*}
    D\cX(\p)\nu|_{\Gamma(\p^0)} = \nu|_{\Gamma(\p)} \circ \psi(\p) = \nu|_{\Gamma(\p)} \circ \cX(\p)|_{\Gamma(\p^0)}\text{,}
  \end{align*}
  or equivalently,
  \begin{align*}
    \left((D\cX(\p))^{-1} \circ \cX(\p)^{-1} \right)\nu|_{\Gamma(\p)} = \nu|_{\Gamma(\p^0)} \circ \cX(\p)^{-1}|_{\Gamma(\p)}
  \end{align*}
  holds.
  This leads to
  \begin{align*}
    T_2(\p; u \circ \cX(\p)^{-1})
    & = \partial_\nu (u \circ \cX(\p)^{-1})|_{\Gamma(\p)}
    \\ & = \left.\left((D\cX(\p))^{-T} \circ \cX(\p)^{-1} \cdot \nabla u \right) \cdot \nu \right|_{\Gamma(\p)}
    \\ & = \left. \nabla u \cdot ((D\cX(\p))^{-1} \circ \cX(\p)^{-1}) \nu \right|_{\Gamma(\p)}
    \\ & = \partial_\nu u \circ \cX(\p)^{-1}|_{\Gamma(\p)}
    \text{,}
  \end{align*}
  which confirms the trace preserving property~\eqref{eq:tracePreservation} also for the curve trace operator.
  
  Analogously, the Dirichlet conditions~\eqref{eq:cV_traceCondition3} of $\cV$ imply the Dirichlet conditions~\eqref{eq:tracePreservationDirichlet} of $\cX$.
  This completes the proof.
\end{proof}

\noindent
The later practical derivative formulations do not require knowledge of the full family of diffeomorphisms $\cX(\p)$, but instead only directional derivatives $\partial_\q \cX(\p^0)$ are needed.
The next two results show how a single time- and $\p$-independent vector field may be used to induce a family of maps $\cX(\p)$ where the derivative $\partial_\q \cX(\p^0)$ is known for one prescribed direction $\q$.

\begin{lemma}
  \label{thm:cX_qDerivative}
  Suppose $\cX$ and $\cV$ as in \cref{thm:odeInducedDiffeomorphisms}.
  %~ Let $\p \in\cB$ and $\q := \p - \p^0$.
  Let $\q \in \cB - \p^0$ and $\p := \p^0 + \q$.
  If $\cV$ fulfills the scaling condition
  \begin{align}
    \label{eq:cV_extraCondition}
    \forall{t,\lambda \in [0,1]\colon\ } \cV(t,\p^0 + \lambda \q, \cdot) = \lambda \cV(\lambda t, \p^0 + \q, \cdot)\text{,}
  \end{align}
  then the derivative of $\cX$ in $\p^0$ in direction $\q$ is given by
  \begin{align*}
    \partial_{\q} \cX(\p^0) = \cV(0,\p,\cdot)\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Again, basic ODE theory may be used to characterize $\partial_\q \cX$ as the solution of another ODE (see \cref{thm:odeDerivative2}).
  It holds $\partial_{\q} \cX(\p^0;x) = \wt{\eta}(1,x)$ where $\wt{\eta}$ solves the initial value problem
  \begin{align*}
    \dd{t} \wt{\eta}(t,x)
    & = D\cV\left(t,\p^0,\eta(t,\p^0,x)\right) \cdot \wt{\eta}(t,x) + \dd{\p} \cV\left(t,\p^0,\eta(t,\p^0,x)\right) \cdot \q
    \\ & = \partial_{\q} \cV(t,\p^0,x)
  \end{align*}
  with $\wt{\eta}(0,x) = 0$.
  Here the property $\cV(\cdot,\p^0,\cdot) = 0$, implying $D\cV(t,\p^0,\cdot) = 0$, as well as $\eta(t,\p^0,x) = x$ were used.
  The scaling condition~\eqref{eq:cV_extraCondition} and $\cV(\cdot,\p^0,\cdot) = 0$ imply
  \begin{align*}
    \partial_{\q} \cV(t,\p^0,x)
    & = \lim_{\lambda \searrow 0} \frac{\cV(t,\p^0+\lambda\q,x) - \cV(t,\p^0,x)}{\lambda}
    = \lim_{\lambda \searrow 0} \cV(\lambda t, \p^0+\q,x)
    = \cV(0,\p,x)
  \end{align*}
  and so one arrives at
  \begin{align*}
    \partial_{\q} \cX(\p^0,x)
    & = \wt{\eta}(1,x)
    = \int_0^1 \partial_{\q} \cV(t,\p^0,x) \d{t}
    = \cV(0,\p,x)
    \text{.}
    \qedhere
  \end{align*}
\end{proof}


\begin{lemma}
  \label{thm:VinducedDiffeomorphisms}
  Let $V \in C^m({\Omega})$ and $\q \in \cB - \p^0$.
  %, and $\psi \in C^{m+1}(\cB \times \wt{\Gamma}_0,\R^2)$.
  Assume that the boundary condition
  \begin{align}
    \label{eq:helper1808031043}
    V|_{\Gamma(\p^0)} & = \partial_\q \psi(\p^0)
  \end{align}
  holds and, if $T$ is the curve trace operator, also
  \begin{align}
    \label{eq:helper1808031044}
    DV\nu|_{\Gamma(\p^0)} & = \partial_\q (\nu|_{\Gamma(\p)} \circ \psi(\p))
    \text{.}
  \end{align}
  Assume that the right hand sides in~\eqref{eq:helper1808031043} and~\eqref{eq:helper1808031044} are well-defined and admit local $C^m$-extensions to a neighborhood of $\{\p^0\} \times \Gamma(\p^0)$.
  Moreover, suppose on the outer boundary the Dirichlet boundary condition
  \begin{align*}
    V|_{\partial\Omega} & = 0, \qquad DV|_{\partial\Omega} = 0\text{.}
  \end{align*}
  Then a family of trace preserving diffeomorphisms $(\cX(\p))_{\p \in \cB}$ exists in the sense of \cref{thm:odeInducedDiffeomorphisms} with $\partial_{\q} \cX(\p^0) = V$.
\end{lemma}

\begin{proof}
  Extend $V$ to a $C^m$-smooth vector field $\cV(t,\p,x)$ such that $\cV(0,\p^0,\cdot) = V$ holds and the conditions
  %~ \eqref{eq:cV_traceCondition1}, \eqref{eq:cV_traceCondition2}, \eqref{eq:cV_traceCondition3}, \eqref{eq:cV_extraCondition}
  \eqref{eq:cV_traceCondition1} through \eqref{eq:cV_traceCondition3} and \eqref{eq:cV_extraCondition}
  are fulfilled.
  The statement then is a direct consequence of \cref{thm:odeInducedDiffeomorphisms} and \cref{thm:cX_qDerivative}.
\end{proof}

\noindent
%In particular, a single time-independent vector field $V$ can be used to induce a family of trace-preserving diffeomorphisms
Similarly, a single time- and $\p$-independent function can be used to induce a family of constraint preserving maps $\xi(\p)$ with known directional derivative $\partial_\q \xi(\p^0)$.
In this case the construction is even simpler because no moving interfaces need to be taken into account directly.
%~ The following \TK{lemma} states the full result.

\begin{lemma}
  \label{thm:wInducedConstraintMaps}
  Let $\q \in \cB - \p^0$.
  Suppose $g \circ \psi \in C^{m}$ in a neighborhood of $\{\p^0\} \times \Gamma(\p^0)$ and $w \in C^{m-1}(\overline{\Omega})$ with
  \begin{align*}
    T(\p^0;w) & = \partial_\q \left( g(\p^0) \circ \psi(\p^0) \right)
    \text{.}
  \end{align*}
  Moreover, assume on the outer boundary Dirichlet conditions
  \begin{align*}
    w|_{\partial\Omega} & = 0
    ,\qquad \partial_\nu w|_{\partial\Omega} = 0\text{.}
  \end{align*}
  Then $\xi \in C^m(\cB \times \Omega)$ exists such that $\xi(\p^0) = 0$, $\partial_{\q} \xi(\p^0) = w$,
  \begin{align*}
    \xi(\p)|_{\partial\Omega} & = 0
    ,\qquad \partial_\nu\xi(\p)|_{\partial\Omega} = 0\text{,}
  \end{align*}
  and for all $\p \in \cB$ the constraint-preserving property
  \begin{align*}
    \hatT(\p^0;\xi) = \hatg(\p)\circ \psi(\p)-\hatg(\p^0)
  \end{align*}
  is fulfilled.
\end{lemma}

\begin{proof}
  The existence of an appropriate map $\xi \in C^m(\cB\times\Omega)$ follows immediately by application of an extension theorem to the following boundary values:
  \begin{itemize}
    \item
      For all $x \in \Omega$ holds $\xi(\p^0,x) = 0$ and $\partial_\q \xi(\p^0,x) = w(x)$.

    \item
      For all $(\p,x) \in [0,1]\times\cB\times\partial\Omega$ holds $\xi(\p,x) = \partial_\nu \xi(\p,x) = 0$.
      
    \item
      For all $(\p,x) \in [0,1]\times\cB\times\Gamma(\p^0)$ holds
      \begin{align*}
        \xi(\p;x) = g_1(\p;\psi(\p;x))
      \end{align*}
      and if $T$ is the curve trace operator also
      \begin{equation*}
        \partial_\nu \xi(\p;x) = g_2(\p;\psi(\p;x))
        \text{.}
        \qedhere
      \end{equation*}
  \end{itemize}

  \begin{comment}
  Extend $w$ to a map $W \in C^m([0,1] \times \cB \times \Omega)$ that has the following properties:
  \begin{itemize}
    \item
      For all $(t,x) \in [0,1]\times \Omega$ holds $W(t,\p^0,x) = 0$.

    \item
      For all $(t,\p,x) \in [0,1]\times\cB\times\partial\Omega$ holds $W(t,\p,x) = \partial_\nu W(t,\p,x) = 0$.

    \item
      Let $F(\p;x) := g(\p;\psi(\p;x))$.
      For all $(t,\p,x) \in [0,1]\times\cB\times\Gamma(\p^0)$ holds
      \begin{align}
        \label{eq:W_condition1}
        W(t,\p,x) = \dd{\p} F_1(\p^0 + t(\p-\p^0)) \cdot (\p-\p^0)
      \end{align}
      and if $T$ is the trace operator also
      \begin{align}
        \label{eq:W_condition2}
        \partial_\nu W(t,\p,x) = \dd{\p} F_2(\p^0 + t(\p-\p^0)) \cdot (\p-\p^0)
        \text{.}
      \end{align}

    \item
      TODO
      \begin{align}
        \label{eq:W_condition3}
        .
      \end{align}
  \end{itemize}
  Define $\xi(\p;x) := \int_0^1 W(t,\p,x) \d{t}$.
  From the properties of $W$ follows $\xi \in C^m(\cB\times \Omega)$ as well as $\xi(\p^0) = 0$ and $\xi(\p)|_{\partial\Omega} = \partial_\nu\xi(\p)|_{\partial\Omega} = 0$.
  Property \eqref{eq:W_condition1} implies by virtue of the fundamental theorem of calculus for every $(\p,x) \in \cB\times\Gamma(\p^0)$
  \begin{align*}
    \xi(\p;x)
    & = \int_0^1 W(t,\p,x) \d{t}
    \\ &= \int_0^1 \dd{t} g_1\left(\p^0+t(\p-\p^0); \psi(\p^0+t(\p-\p^0);x) \right) \d{t}
    \\ &= g_1(\p;\psi(\p;x))
  \end{align*}
  and analogously, if $T$ is the trace operator, \eqref{eq:W_condition2} implies
  \begin{align*}
    \partial_\nu \xi(\p;x)
    & = \int_0^1 \partial_\nu W(t,\p,x) \d{t}
    = g_2(\p;\psi(\p;x))
    \text{,}
  \end{align*}
  which verifies the constraint-preserving property.
  Finally, \eqref{eq:W_condition3} leads to
  \TK{TODO DERIVATIVE}
  \end{comment}

\end{proof}


\begin{comment}
\begin{corollary}
  \label{thm:odeInducedBijections}
  Let $u_0 \in \Uad(\p^0)$, define
  \begin{align*}
    \xi_0(\p) := \left. \left(g(\p) \circ \psi(\p) - u_0\right) \right|_{\Gamma(\p^0)}
    %, \qquad x \in \partial\Omega(\p^0)
    \text{,}
  \end{align*}
  and suppose $\psi$ and $\cX$ as in \cref{thm:odeInducedDiffeomorphisms}.
  If $\xi(\p)$ is an $H^2(\Omega(\p))$-extension of $\xi_0(\p)$ for every $\p \in \hat{\cB}$, then
  \begin{align*}
    \Phi(\p;u)  := (u+\xi(\p)) \circ \cX(\p)^{-1}
    %\text{.}
  \end{align*}
   defines a family of bijections $\Phi(\p)\colon \Uad(\p^0) \rightarrow \Uad(\p)$.
\end{corollary}

\begin{proof}
  Recall $T(\p^0;u_0) = g(\p^0)$ as of $u_0 \in \Uad(\p^0)$ and $\cX(\p)|_{\Gamma(\p^0)} = \psi(\p)$ as is shown in the proof of \cref{thm:odeInducedDiffeomorphisms}.
  Therefore,
  \begin{align*}
    T(\p^0;\xi(\p))
      = g(\p) \circ \cX(\p)|_{\Gamma(\p^0)} - g(\p^0)
  \end{align*}
  is fulfilled for all $\p \in \hat{\cB}$.
  Hence, the claimed statement is a direct consequence of \cref{thm:admissibleSetIsomorphism} and \cref{thm:odeInducedDiffeomorphisms}.
\end{proof}
\end{comment}

\noindent
Combining the previous results yields:

\begin{corollary}
  Let $\q \in \cB - \p^0$, $V \in C^m(\Omega,\R^2)$ and $w \in C^{m-1}(\Omega)$ as in \cref{thm:VinducedDiffeomorphisms} and \cref{thm:wInducedConstraintMaps}, respectively.
  Then $\cX \in C^m(\cB \times \Omega)$ and $\xi \in C^m(\cB\times \Omega)$ exist such that a family of bijections $\Phi(\p)\colon \Uad(\p^0) \rightarrow \Uad(\p)$, $\p \in \cB$ is defined by
  \begin{align*}
    %~ \Phi(\p)\colon \Uad(\p^0) & \longrightarrow \Uad(\p)
    %~ \\ u & \longmapsto (u+\xi(\p))\circ \cX(\p)^{-1}
    \Phi(\p;u) := (u+\xi(\p)) \circ \cX(\p)^{-1}
  \end{align*}
  and such that $\partial_\q \cX(\p^0) = V$ and $\partial_\q \xi(\p^0) = w$ hold.
\end{corollary}

%~ \begin{remarks} \ %
  %~ \label{thm:diffeomorphismRemarks}
  %~ \begin{comment}
  %~ \begin{itemize}
    %~ \item
      %~ Under mild assumptions on the boundaries $\partial\Omega(\p)$, the existence if suitable vector fields $\cV$ in \cref{thm:odeInducedDiffeomorphisms} is asserted by \emph{Whitney's extension theorem} (cf.~\cite{Whitney34}).

    %~ \item
      %~ The combination of \cref{thm:odeInducedDiffeomorphisms} and \cref{thm:admissibleSetIsomorphism} proves existence of a family of bijections $\phi(\p)\colon \Uad(\p^0) \rightarrow \Uad(\p)$, $\p \in \cB$ for admissible sets $\Uad(\p)$.
      
    %~ \item
      %~ The proofs of \cref{thm:odeInducedDiffeomorphisms} and \cref{thm:admissibleSetIsomorphism} do not explicitly rely on the fact that the trace preserving property is stated over a domain of codimension $1$ and the definitions and statements can easily be generalized to other types of subdomains~$\Gamma$ and constraint operators $T$.
      %~ Specifically, in case of point value constraints it suffices to replace $T$ by the appropriate \emph{point evaluation operator} and to drop condition~\eqref{eq:cV_traceCondition2} on $\Gamma(\p)$ as point value constraints do not require preservation of first order derivatives on the subdomain~$\Gamma(\p)$.
    %~ \end{comment}
      
    %~ \begin{comment}
    %~ \item
      %~ Generalizations to larger classes of constraints are possible as well.
      %~ For example, if we work with curve constraints and a linear operator $G\colon H^{3/2}(\Gamma(\p^0)) \times H^{1/2}(\Gamma(\p^0)) \rightarrow W$ where $W$ is some vector space, and we are given $\hatg(\p) \in W$, we may define the admissible sets
      %~ \begin{align*}
        %~ \Uad(\p) = \left\{ v \in H^2(\Omega(\p)) \mid \hat{G}\hatT(\p) = \hatg(\p) \right\}\text{.}
      %~ \end{align*}
      %~ where $\hat{G}(v_1,v_2) = (G(v_1|_{\Gamma(\p)},v_2|_{\Gamma(\p)}), (v_1|_{\partial\Omega}, v_2|_{\partial\Omega}) )$.
      %~ Because the trace preserving property implies $\hat{G}\hatT(\p; u \circ X^{-1}) = \hat{G}\hatT(\p^0;u)$, it is readily seen that \cref{thm:admissibleSetIsomorphism} is also valid in this setting.
      %~ \snote{Does this show that the above results also hold true for average constraints?}
      %~ %%~ In particular, the above results also hold true for average constraints. ... actually, I am not sure if that is even true because of possible position dependence of the averaging operator
    %~ \end{comment}
      
    %~ \begin{comment}
    %~ \item
      %~ Suppose the reference surface~$\cM_0$ is a tube of length $L$ whose ends are fixed.
      %~ If we use the canonical parameterization over $\Omega = [0,L] \times [0,2\pi]$ from \cref{thm:exampleTubeParameterization}, then we need to require Dirichlet boundary conditions on the part $\partial\Omega_D := \{0,L\} \times [0,2\pi]$ which parameterizes the tube's ends.
      %~ The remaining parts $\partial\Omega_P^1 = [0,L] \times \{0\}$ and $\partial\Omega_P^2 = [0,L] \times \{2\pi\}$ are equipped with periodic boundary conditions.
      %~ More precisely,
      %~ \begin{align*}
        %~ \hat{H}(\p) & := \left\{ v \in H^2(\Omega(\p)) \mid v|_{\partial\Omega_D} = \partial_\nu v|_{\partial\Omega_D} = 0, \right.
        %~ \\ & \left.\qquad\quad u|_{\partial\Omega_P^1} = u|_{\partial\Omega_P^2}, \ \partial_\nu u|_{\partial\Omega_P^1} = - \partial_\nu u|_{\partial\Omega_P^2}  \right\}\text{.}
      %~ \end{align*}
      %~ If modified trace operators $\hatT$ are defined on $\partial\Omega_D \cup \Gamma(\p)$ instead of on all of $\partial\Omega(\p)$, then it is easy to show that \cref{thm:admissibleSetIsomorphism} still holds true as long as $\xi$ is periodic on $\partial\Omega_P$.
      %~ Concerning \cref{thm:odeInducedDiffeomorphisms}, it suffices to add the condition that $\cV$ also is periodic on $\partial\Omega_P$.
  %~ \end{itemize}
  %~ \end{comment}
%~ \end{remarks}


\begin{remark}[Whitney extension theorem]
  %~ \label{thm:diffeomorphismRemarks}
  Both
  %~ \cref{thm:odeInducedDiffeomorphisms}, \cref{thm:cX_qDerivative},
  \cref{thm:VinducedDiffeomorphisms} and \cref{thm:wInducedConstraintMaps} rely on the existence of sufficiently smooth extension functions with prescribed values and derivatives on some subset of the extension domain.
  Under mild assumptions on the boundaries and the boundary data, the existence of such functions is asserted by \emph{Whitney's extension theorem} (cf.~\cite{Whitney34}). It implies:
  
  Let $m \in \N$, let $A \subset \R^n$ a closed subset and suppose boundary condition functions $f_\alpha\colon A \rightarrow \R$ for multi-indices $\alpha$ with $\abs{\alpha} \leq m$.
  If the $f_\alpha$ fulfill for all $x,y \in A$ the Taylor-type compatibility condition
  \begin{align*}
    %\forall{x,y \in A\colon} \ 
    f_\alpha(x) = \sum_{\abs{\beta} \leq m -\abs{\alpha}} \frac{f_{\alpha+\beta}(x)}{\beta!} (x-y)^\beta + o\left(\abs{x-y}^{m-\abs{\alpha}}\right)\ \text{ for $\abs{x-y} \to 0$} \text{,}
  \end{align*}
  then a function $f \in C^m(\R^n)$ exists such that $D^\alpha f|_A = f_\alpha$ for $\abs{\alpha} \leq m$.
  
  Due to the fact that the verification of the compatibility conditions is rather technical and of only moderate importance in the given context, the explicit application of this theorem is omitted in this work.
\end{remark}


%~ \begin{remark}[Arbitrary directions $\q$]
  %~ Note that \cref{thm:VinducedDiffeomorphisms} and \cref{thm:wInducedConstraintMaps} may be stated also for arbitrary $\q \in \R^{N\times k}$.
  %~ In that case, however, \TK{TODO}
%~ \end{remark}

%~ \snote{Technicality: Later we actually allow arbitrary $\q$. How to sell this? (Here or later.)}
