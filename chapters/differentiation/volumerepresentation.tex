So far, the differentiability of interaction potentials induced by elastic energies of the type
\begin{align*}
  \E(\p,u) = \int_{\Omega(\p)} \rho\left(x,u(x),Du(x),D^2u(x)\right) \, \mathrm{d}x
\end{align*}
has been investigated.
Now the results of the previous sections are combined in order to derive an explicit expression for the gradient which is feasible within the context of finite element approximations.
The following calculations predominantly rely on standard methods from matrix calculus.
%where $\rho \in C^1(\overline{\Omega}\times\R\times\R^2\times\R^{2\times 2})$ is such that $\E$ is well-defined.
%The purpose of this section is two-fold:
%For once we establish that the differentiability result \cref{thm:differentiabilityGeneral} is applicable in the case where the family of bijections $\Phi(\p)$ is induced by a family of diffeomorphisms $\cX(\p)$, as constructed in \cref{sec:diffeomorphisms}.
%Furthermore, we derive an explicit representation of the derivative in terms of volume integrals that exclusively depend on $\rho$, $\cX$ and $u$.


\begin{lemma}
  \label{thm:volumeDerivatives}
  Let $t^\ast \in \R_{>0}$.
  For $t \in [0,t^\ast]$,
  suppose a family of domains $\Omega(t) \subseteq \R^n$ and a family of $2$-diffeomorphisms $X(t)\colon \Omega(0) \rightarrow \Omega(t)$ with $X(0) = \id_{\Omega(0)}$.
  Furthermore, suppose that the derivatives $\dd{t} D^k X$ exist and are continuous for $k\leq 2$.
  Define the perturbation vector field $V := \dd{t} X|_{t=0}$.
  Then
  %\snote{Existence in what sense? Also need interchangeability of derivatives.}
  %~ Then
  \begin{align}
    \label{eq:transformationDerivativeGamma}
    \left.\dd{t} \det(DX(t)) \right|_{t=0} = \div(V)\text{,}
  \end{align}
  and
  \begin{align}
    \label{eq:transformationFirstDerivative}
    \left.\dd{t} DX(t)^{-1} \right|_{t=0} = -DV\text{,}
  \end{align}
  and for any $\wt{u} \in H^2(\Omega(0))$
  \begin{align}
    \label{eq:transformationSecondDerivative}
    \left.\dd{t} \frac{1}{\gamma} \sum_{k=1}^2 \partial_k \left( \gamma (DX^{-T} \nabla \wt{u}) \otimes (DX^{-T}e_k)\right) \right|_{t=0}
    %~ & = -DV^TD^2\wt{u} - D^2\wt{u}DV - \sum_{k=1}^2 D^2V_k \, \partial_k \wt{u}
    & = -\Sym(D^2\wt{u}DV) -D^2V \nabla \wt{u}
    %~ \text{.}
  \end{align}
  where $\gamma(t) := \det(DX(t))$ and were $e_k \in \R^2$ is the $k$-th canonical Euclidean basis vector.
\end{lemma}

\begin{proof}
  For this proof, recall $\Sym(A) := A + A^T$ and $D^2V\nabla\wt{u} := \sum_{k=1}^2 D^2V_k \, \partial_k \wt{u}$, and note $DX(0) = \id_{\R^2}$ as well as $\det(DX(0)) = 1$.
  \cref{thm:matrixDeterminantDerivative} in the appendix on the differentiation of the determinant implies
  \begin{align*}
    \left. \dd{t} \det(DX(t))\right|_{t=0}
    & = \left.\det(DX(t)) \Tr\left(DX(t)^{-1} \dd{t}DX(t) \right)\right|_{t=0}
    = \Tr(DV) = \div(V)
    \text{.}
  \end{align*}
  Similarly, \cref{thm:matrixInverseDerivative} concerning differentiation of the matrix inverse yields
  \begin{align*}
    \left. \dd{t} DX(t)^{-1} \right|_{t=0}
    & = \left. - DX(t)^{-1} \left(\dd{t} DX(t)\right) DX(t)^{-1} \right|_{t=0}
    = -DV
    \text{.}
  \end{align*}
  This shows the identities~\eqref{eq:transformationDerivativeGamma} and~\eqref{eq:transformationFirstDerivative}.
  Now, note that
  \begin{align*}
    \sum_{k=1}^2 \partial_k \nabla\wt{u} \otimes e_k = D^2\wt{u}
  \end{align*}
  and in particular also
  \begin{align*}
    \left.\sum_{k=1}^2 \partial_k \left( \gamma (DX^{-T} \nabla \wt{u}) \otimes (DX^{-T}e_k)\right)\right|_{t=0} = D^2\wt{u}
    \text{.}
  \end{align*}
  Together with $X(0) = \id_{\Omega(0)}$, \eqref{eq:transformationDerivativeGamma}, \eqref{eq:transformationFirstDerivative} and other elemental transformations this gives
  \begin{align*}
    & \left.\dd{t} \frac{1}{\gamma} \sum_{k=1}^2 \partial_k \left( \gamma (DX^{-T} \nabla \wt{u}) \otimes (DX^{-T}e_k)\right) \right|_{t=0}
    \\ & \qquad = -\div(V) D^2\wt{u}
      + \sum_{k=1}^2 \partial_k \div(V) \nabla \wt{u} \otimes e_k
      - \sum_{k=1}^2 \partial_kDV^T\nabla\wt{u}\otimes e_k
    \\ & \qquad\quad -\sum_{k=1}^2 \nabla\wt{u}\otimes\left(\partial_k DV^T e_k\right) + \div(V)D^2\wt{u} - DV^TD^2\wt{u} - D^2\wt{u}DV
    \\ & \qquad = -\sum_{k=1}^2 D^2V_k \partial_k \wt{u} - DV^TD^2\wt{u} - D^2\wt{u}DV
    \text{,}
    %~ \qedhere
  \end{align*}
  which shows equation~\eqref{eq:transformationSecondDerivative} and completes the proof.
\end{proof}


\begin{proposition}
  \label{thm:interactionVolumeDerivative}
  Let $\cX(\p)$ be a family of trace-preserving $3$-diffeomorphisms induced by a parameter-dependent vector field $\cV$ as in \cref{thm:odeInducedDiffeomorphisms}, and let $\xi(\p) \in C^3(\cB \times \Omega)$ be a family of constraint-preserving maps as in \cref{thm:wInducedConstraintMaps}.
  %~ Suppose that $\hat{\cB}$ is convex and that $u^\ast$ is the unique global minimizer of $J(\p^0,\cdot)$ on $\Uad(\p^0)$.
  %~ Suppose convexity of $\cB$ and $u \in \Uad(\p^0)$.
  Furthermore, let $u \in \Uad(\p^0)$, $\q \in \cB - \p^0$, $V := \cV(0,\p^0 + \q,\cdot)$ and assume that $\cV$ fulfills the scaling condition \eqref{eq:cV_extraCondition}.
  If the transformed energy $\Eloc$ from \cref{thm:ElocDifferentiability} is differentiable, then the derivative in direction $\q$ is given by
  \begin{equation}
    \label{eq:interactionDerivative}
    %~ \begin{aligned}
      %~ \partial_{\q} \Eloc(\p^0,u)
      %~ & = \int_{\Omega(\p^0)} \div(V) \rho(\zeta_0)
        %~ + \rho_x(\zeta_0)V
        %~ - \rho_z(\zeta_0) \cdot (DV\nabla u) \d{x}
      %~ \\
        %~ & \qquad - \int_{\Omega(\p^0)}  \rho_Z(\zeta_0) : \left(D^2V\nabla u + \Sym(D^2u \cdot DV)\right) \d{x}
      %~ \\ & \qquad + \int_{\Omega(\p^0)} \rho_y(\zeta_0)\xi'_0 + \rho_z(\zeta_0)\nabla\xi'_0 + \rho_Z(\zeta_0) : D^2\xi'_0 \d{x}
    %~ \end{aligned}
    \begin{aligned}
      \partial_{\q} \Eloc(\p^0,u)
      & = \int_{\Omega(\p^0)} \div(V) \rho(\zeta_0)
        + \rho_x(\zeta_0)V
        + \rho_y(\zeta_0)\xi'_0
        + \rho_z(\zeta_0) \cdot (\nabla\xi'_0-DV\nabla u) \d{x}
      \\
        & \qquad + \int_{\Omega(\p^0)}  \rho_Z(\zeta_0) : \left(D^2\xi'_0 - D^2V\nabla u - \Sym(D^2u \cdot DV)\right) \d{x}
    \end{aligned}
  \end{equation}
  where $\zeta_0(x) := (x,u(x),\nabla u(x),D^2u(x))$, $\xi'_0 := \partial_q \xi(\p^0)$, and where $\rho_x$, $\rho_y$, $\rho_z$ and $\rho_Z$ are the partial derivatives of $\rho$ with respect to its first, second, third and fourth component, respectively.
  %~ Let $\Omega_1,\Omega_2 \subseteq \R^n$ and $X\colon \Omega_1 \rightarrow \Omega_2$ be a $2$-diffeomorphism.
  
  %~ Let $V$ such that \TK{it fulfills basically the $\cV$ property}.
  %~ Then $\EI$ is differentiable in $\p^0$ and it holds
  %~ \begin{align*}
    %~ \partial_{\q} \EI(\p^0)
    %~ & = \int_{\Omega(\p^0)} \div(V) \rho(\zeta_0)
      %~ + \rho_x(\zeta_0)V
      %~ - \rho_z(\zeta_0) \cdot (DV\nabla u^0) \d{x}
    %~ \\
      %~ & \qquad - \int_{\Omega(\p^0)}  \rho_Z(\zeta_0) : \left(\sum_{k=1}^2 D^2V_k \partial_k u^0 + DV^TD^2u^0 + D^2u^0DV\right) \d{x}
  %~ \end{align*}
  %~ where we assume that $\rho$ is such that the above integrals exist.
\end{proposition}

\begin{proof}
  %~ We extend $V$ to a map $\cV$ that \TK{fulfills the properties from a previous statement} and such that $\partial_{\q} \cX(\p^0) = V$.
  As in the proof of \cref{thm:ElocDifferentiability}, define
  $\gamma(\p) := \det(D\cX(\p))$,
  $v(\p) := u + \xi(\p)$
  and
  \begin{align*}
    \zeta(\p) & := \Bigg(\cX(\p),\ v(\p),\ D\cX(\p)^{-T}\nabla v(\p),
      \\ & \qquad\quad \frac{1}{\gamma(\p)} \sum_{k=1}^2 \partial_k \left( \gamma(\p) (D\cX(\p)^{-T} \nabla v(\p)) \otimes (D\cX(\p)^{-T}e_k)\right)\Bigg)
    %~ \text{.}
  \end{align*}
  to obtain the transformed energy
  \begin{align*}
    \Eloc(\p,u)
    := \E(\p,v(\p)\circ\cX(\p))
    & = \int_{\Omega(\p^0)} \gamma(\p)\,\rho\left(\zeta(\p)\right)\d{x}
    \text{.}
  \end{align*}
  Let $X(t) := \cX(\p^0 + t(\p-\p_0))$ and note $\dd{t} X|_{t=0} = V = \cV(0,\p,\cdot)$ as of \cref{thm:cX_qDerivative}.
  As of $X \in C^3$ and $\cX(\p^0) = \id_{\Omega(\p^0)}$, the identities from \cref{thm:volumeDerivatives} are applicable and one obtains via the product and the chain rules
  \begin{align*}
    %~ \label{eq:interactionDerivative}
    \begin{aligned}
      \partial_{\q} \Eloc(\p^0,u)
      & = \left.\dd{t} \Eloc(\p^0+t\q,u) \right|_{t=0}
      \\ & = \int_{\Omega(\p^0)} \div(V) \rho(\zeta_0)
        + \rho_x(\zeta_0)V
        - \rho_z(\zeta_0) \cdot (DV\nabla u) \d{x}
      \\
        & \qquad - \int_{\Omega(\p^0)}  \rho_Z(\zeta_0) : \left(D^2V\nabla u + \Sym(D^2u \cdot DV) \right) \d{x}
      \\ & \qquad + \int_{\Omega(\p^0)} \rho_y(\zeta_0)\xi'_0 + \rho_z(\zeta_0)\nabla\xi'_0 + \rho_Z(\zeta_0) : D^2\xi'_0 \d{x}
      \text{,}
    \end{aligned}
  \end{align*}
  which is equivalent to \eqref{eq:interactionDerivative}.
  %~ The claimed statement follows from \cref{thm:differentiabilityGeneral}, which is applicable due to \cref{thm:ElocDifferentiability} and the uniqueness of $u^\ast$.
  %~ It implies
  %~ \begin{equation*}
    %~ \partial_{\q} \EI(\p^0) = \partial_{\q} \Eloc(\p^0,u^\ast)\text{.}
    %~ \qedhere
  %~ \end{equation*}
\end{proof}

\noindent In summary, the previous paragraphs lead to the following differentiability result:

\begin{theorem}
  \label{thm:summarizedDifferentiability}
  Assume that $u^0 \in \Uad(\p^0)$ is the unique minimizer of $\E(\p^0)$ over $\Uad(\p^0)$ and that the second variation $\E_{uu}(\p^0,u^0)\colon \Uad^0(\p^0) \rightarrow (\Uad^0(\p^0))'$ is invertible.
  Let $\q \in \R^{N\times k}$ and suppose $V \in H^2(\Omega(\p^0),\R^2)$ such that
  \begin{align}
    \label{eq:V1_cond}
    V|_{\Gamma(\p^0)} & = \partial_\q \psi(\p^0)
  \end{align}
  and, if $T$ is the curve trace operator,
  \begin{align}
    \label{eq:DV_cond}
    DV\nu|_{\Gamma(\p^0)} & = \partial_\q \left(\nu|_{\Gamma(\p^0)} \circ \psi(\p^0) \right)
    \text{.}
  \end{align}
  Also suppose $w \in H^2(\Omega(\p^0))$ with
  \begin{align}
    \label{eq:xi_cond}
    T(\p^0;w) & = \partial_\q \left(g(\p^0) \circ \psi(\p^0)\right)\text{.}
  \end{align}
  Furthermore, have $V$ and $w$ fulfill the Dirichlet boundary conditions
  \begin{align*}
    %~ \label{eq:dirichlet_cond}
    \begin{aligned}
      V|_{\partial\Omega} & = 0 & & \qquad & w|_{\partial\Omega} & = 0
      \\ DV\nu|_{\partial\Omega} & = 0 & && \partial_\nu w|_{\partial\Omega} & = 0\text{.}
    \end{aligned}
  \end{align*}
  Finally, assume that the transformed energy $\Eloc$ as in \cref{thm:ElocDifferentiability} is differentiable twice and that the right hand sides in equations \eqref{eq:V1_cond},
  \eqref{eq:DV_cond}
  %~ through
  and
  \eqref{eq:xi_cond} are well-defined and admit $C^4$-extensions to a neighborhood of ${\p^0} \times \Gamma(\p^0)$.
  %, $\psi \in C^3(\wt{\Gamma}_0)$, and $\xi \in C^3(\hat{\cB} \times \Omega)$.
  %\TK{Also assume that Implicit function theorem is applicable.}
  Then the interaction potential $\EI$ is differentiable in $\p^0$ and the value of the directional derivative~$\partial_\q \EI(\p^0)$ is given by equation~\eqref{eq:interactionDerivative} with $\xi_0' = w$.
\end{theorem}

\begin{proof}
  Assume at first $V \in C^4$ and $w \in C^3$ as well as $\q \in \cB - \p^0$.
  Then by \cref{thm:VinducedDiffeomorphisms} and \cref{thm:wInducedConstraintMaps} the existence of $C^4$-smooth constraint-preserving families of maps $\cX(\p)$ and $\xi(\p)$ is ensured.
  In particular, \cref{thm:interactionVolumeDerivative} is applicable and $\partial_\q \Eloc(\p^0, u)$ may be computed as stated in equation~\eqref{eq:interactionDerivative}.
  Moreover, by the assumptions on $\E$ and $\Eloc$, \cref{thm:differentiabilityGeneral} is applicable and hence the interaction potential $\EI$ is differentiable in $\p^0$ and the directional derivative $\partial_\q \EI(\p^0)$ is given by equation~\eqref{eq:interactionDerivative} with $u = u(\p^0)$ and $\xi_0' = w$.
  
  For arbitrary $\q \in \R^{N \times k}$ note that there exists an $\eps \in \R_{>0}$ such that $\q_{\eps} := \eps\q \in \cB-\p^0$.
  Then the above argumentation is applicable with the scaled functions $V_\eps := \eps V$ and $w_\eps := \eps w$.
  This yields that $\partial_{\q_\eps} \EI(\p^0)$ is given by the formula~\eqref{eq:interactionDerivative} with $(V_\eps, w_\eps)$ instead of $(V,w)$.
  However, because of the linearity $\partial_{\q_\eps} \EI(\p^0) = \eps \partial_\q \EI(\p^0)$ and because the formula~\eqref{eq:interactionDerivative} is linear in $(V,w)$, it follows that $\partial_\q \EI(\p^0)$ still may be computed by equation~\eqref{eq:interactionDerivative} with just $(V,w)$.
  
  Finally, a classical density argument extends this result to the case $V \in H^2(\Omega(\p^0))$ and $\xi_0' \in H^2(\Omega(\p^0))$.
  This completes the proof.
  

  \begin{comment}
  Assume at first $V \in C^4$ and extend it to a $C^4$-smooth vector field $\cV(t,\p,x)$ with $\cV(0,\p^0,\cdot) = V$ that fulfills the trace conditions \eqref{eq:cV_traceCondition1}, \eqref{eq:cV_traceCondition2}, \eqref{eq:cV_traceCondition3} and the scaling condition \eqref{eq:cV_extraCondition}.
  By \cref{thm:odeInducedDiffeomorphisms} this induces in a neighborhood of $\p^0$ a $C^4$-smooth family of trace-preserving 4-diffeomorphisms $\cX$.
  
  Also assume $\xi_0' \in TODO$ \TK{inducing $\xi$}
  Together with $\cX$, this again induces a family of bijections $\Phi(\p)\colon \Uad(\p^0) \rightarrow \Uad(\p)$ in the sense of \cref{thm:odeInducedBijections}.
  
  Because the data is sufficiently smooth and $\rho \in C^1$ holds, the transformed energy $\Eloc(\p,u) := \E(\p,\Phi(\p;u))$ is differentiable in $\p^0$ by \cref{thm:ElocDifferentiability}.
  By assumption on $\E$ the second variation $\Eloc_{uu}(\cdot,u(\p^0))$ is invertible over $\Uad^0(\p^0)$.
  Hence, \cref{thm:differentiabilityGeneral} is applicable and yields
  \begin{equation*}
    \partial_{\q} \EI(\p^0) = \partial_{\q} \Eloc(\p^0,u(\p^0))\text{.}
  \end{equation*}
  As the requirements for \cref{thm:interactionVolumeDerivative} are fulfilled, this derivative is computable by equation \eqref{eq:interactionVolumeDerivative}.
  Finally, a classical density argument extends this result to the case $V \in H^2(\Omega(\p^0))$ and $\xi_0' \in H^2(\Omega(\p^0))$.
  %~ \TK{Assume smooth $V$ first.}
  %~ \TK{Extend $V$ to $\cV$.}
  %~ \TK{First construct $\cV$ and $\cX$.}
  %~ \TK{This gives a family $\Phi$ of constraint-preserving diffeomorphisms.}
  %~ As of $\rho \in C^1$, the transformed energy $\Eloc$ is differentiable in $\p^0$ by \cref{thm:ElocDifferentiability}.
  %~ \TK{Now the implicit function theorem is applicable to state differentiability of the interaction potential.}
  %~ \TK{The previous result and the construction of $\cX$ imply that the derivative is given by the above formula.}
  %~ \TK{By a density argument, this extends to arbitrary $V \in H^2$.}
  \end{comment}
\end{proof}

\begin{comment}
\TK{TODO FORMULATE THE FOLLOWING OUT?}
\begin{itemize}
  \item The great result is that only a perturbation vector field needs to be known.
  \item The remarkable property is that the gradient that only a finite number of vector fields needs to be constructed in order to compute the 
  \item It is worth noting emphasizing the link to shape calculus (and shape optimization), although here everything is finite-dimensional.
  \item Usually, it is even possible to express the derivative solely in terms of a boundary integral, which removes the ``freedom of choice'' in $V$. However, this usually includes higher derivatives in $u$, which are not accessible in a discrete setting. Therefore, we keep the volume representation.
\end{itemize}
\snote{Note also that the extension-property is usually not really a restriction}
\end{comment}

\begin{remark}[Periodic boundary conditions]
  \label{thm:periodicBoundaryConditions}
  As mentioned earlier, all of the above results are stated for the case where the $\p$-independent boundary $\partial\Omega$ is equipped with Dirichlet-type boundary conditions.
  In many applications, however, also periodic boundary conditions are of interest, either because it is required by the parameterization (\eg when perturbations of tubular membranes are considered) or because it is a pure model decision.
  
  Suppose $\partial\Omega \supseteq \Gamma_D \dot{\cup} \Gamma_P$ where $\Gamma_D$ is the Dirichlet-boundary and $\Gamma_P$ is the periodic boundary, which again is split into its counter-parts $\Gamma_P = \Gamma_{P,1} \dot{\cup} \Gamma_{P,2}$.
  In that situation the analogue of \cref{thm:summarizedDifferentiability} is simply obtained by replacing the boundary conditions for $V$ and $w$ by
  \begin{align*}
    %~ \label{eq:mixed_cond}
    \begin{aligned}
      V|_{\Gamma_D} & = 0 & & \qquad & w|_{\Gamma_D} & = 0
      \\ DV\nu|_{\Gamma_D} & = 0 & && \partial_\nu w|_{\Gamma_D} & = 0
      \\ V|_{\Gamma_{P,1}} & = V|_{\Gamma_{P,2}} & & \qquad & w|_{\Gamma_{P,1}} & = w|_{\Gamma_{P,2}}
      \\ DV\nu|_{\Gamma_{P,1}} & = -DV\nu|_{\Gamma_{P,2}} & && \partial_\nu w|_{\Gamma_{P,1}} & = -\partial_\nu w|_{\Gamma_{P,2}}
      \text{.}
    \end{aligned}
  \end{align*}
  The general proof remains identical once the corresponding analogues of \cref{thm:VinducedDiffeomorphisms} and \cref{thm:wInducedConstraintMaps} are established, which again imply the existence of families of suitable bijections $\Phi(\p)\colon \Uad(\p^0) \rightarrow \Uad(\p)$ induced by the functions $V$ and $w$.
  
\end{remark}

\begin{comment}
\begin{remarks} \ %
  \begin{itemize}
    \item
      \TK{Cool that only $V$ is needed. Argue with extension property. Note that $V$ alone encodes the dependence on the type of constraints while $\rho$ encodes the type of energy.}
      
    \item
      \TK{Establish similarity to shape calculus.}
      
    \item
      \TK{Maybe mention Hadamard-form.}
      
    \item
      \TK{Emphasize well-definedness in finite element sense and simplicity of construction.}
      
    \item
      \TK{Make density argument to reduce requirements on $V$.}
  \end{itemize}
\end{remarks}
\end{comment}

%~ \snote{Was the relation to shape calculus already established somewhere? This is basically where the idea for perturbation vector fields comes from.}

\noindent
The following examples discuss the derivative formula for
%~ \TK{two -- not true anymore}
some special interaction models.
%namely the Monge-gauge and for the nonlinear Willmore energy in the graph case.
It is assumed that all quantities are given in the sense of \cref{thm:summarizedDifferentiability}, unless it is stated otherwise.

\begin{example}[Monge-gauge]
  \label{thm:derivativeMongeGauge}
  Recall \cref{thm:linearizationMongeGauge} where the linearization of the elastic energy without Gaussian curvature over a flat reference domain is stated, also known as Monge-gauge formulation.
  In that case the energy is up to a constant given by
  \begin{align*}
    \E(\p,u)  = \int_{\Omega(\p)} \frac{\kappa}{2} (\Delta u)^2 + \frac{\sigma}{2} \norm{\nabla u}^2 \d{x}
    %~ \text{.}
  \end{align*}
  and the homogeneous admissible spaces are $\Uad^0(\p) = H_0^2(\Omega(\p))$.
  
  From the theory of linear elliptic operators it is well-known that a minimizer of $\E(\p,\cdot)$ over the closed affine subspace $\Uad(\p)$ exists and is unique because the second variation
  \begin{align*}
    J_{uu}(\p,u;v_1,v_2) = \int_{\Omega(\p)} \kappa \Delta v_1 \, \Delta v_2 + \sigma \nabla v_1 \cdot \nabla v_2 \d{x}
  \end{align*}
  is elliptic over $H_0^2(\Omega(\p))$ for any $u \in H^2(\Omega(\p))$ (see also \cite{ElGrHoKoWo16}).
  In particular, $J_{uu}\colon \Uad^0(\p) \rightarrow (\Uad^0(\p))'$ is always invertible.
  
  Given $\cX \in C^m$ and $\xi \in C^m$, the transformed energy $\Eloc$ in $\p^0$ reads
  \begin{align*}
    \Eloc(\p,u) & = \frac{1}{2} \int_{\Omega(\p)} \frac{\kappa\div(A\nabla u)^2}{\det(D\cX(\p))}  + \sigma \nabla u \cdot A\nabla u \,\d{x}
  \end{align*}
  where $A = \det(D\cX(\p)) D\cX(\p)^{-1} D\cX(\p)^{-T}$.
  Hence, $\Eloc$ is $\infty$-smooth in $u$ and $m$-smooth in $\p$.
  Putting all these properties together and given that the boundary data is sufficiently smooth, the differentiability result in \cref{thm:summarizedDifferentiability} is applicable for $m \geq 2$,
  
  The integrand in $\E(\p,\cdot)$ is described by
  \begin{align*}
    \rho(x,y,z,Z) = \frac{\kappa}{2} (Z_{11} + Z_{22})^2 + \frac{\sigma}{2} (z_1^2 + z_2^2)
    %~ \text{.}
  \end{align*}
  and its derivatives in a point $\zeta = (x, u, Du, D^2u)$ are given by
  \begin{align*}
    \rho_x(\zeta)  & = 0
    , \qquad\rho_y(\zeta) = 0
    , \qquad\rho_z(\zeta) = \sigma Du
    , \qquad\rho_Z(\zeta) = \kappa \Delta u \mat{1 & 0 \\ 0 & 1}
    \text{.}
  \end{align*}
  Therefore, the derivative formula \eqref{eq:interactionDerivative} reads
  \begin{align*}
    \partial_{\q} \EI(\p^0)
    %~ & = \int_{\Omega(\p^0)} \frac{1}{2}\div(V) \left(\kappa (\Delta u^\ast)^2 + \sigma \norm{\nabla u^\ast}^2\right)
      %~ - \sigma \nabla u^\ast \cdot (DV\nabla u^\ast) \d{x}
    %~ \\
      %~ & \qquad - \int_{\Omega(\p^0)} \kappa \Delta u^\ast\,\Tr\left(\sum_{k=1}^2 D^2V_k \partial_k u^\ast + DV^TD^2u^\ast + D^2u^\ast DV\right) \d{x}
    %~ \\
      %~ & \qquad + \rho_y \xi_0' + \rho_z \nabla \xi_0' + \rho_Z : D^2\xi_0' \d{x}
    & = \int_{\Omega(\p^0)} \frac{1}{2}\div(V) \left(\kappa (\Delta u)^2 + \sigma \norm{\nabla u}^2\right)
      + \sigma \nabla u \cdot (\nabla \xi_0' - DV\nabla u) \d{x}
    \\
      & \qquad + \int_{\Omega(\p^0)} \kappa \Delta u \, \left( \Delta \xi_0' - \Delta V \cdot \nabla u - 2 DV : D^2u \right) \d{x}
    %~ \\
      %~ & \qquad + \int_{\Omega(\p^0)} \sigma \nabla u \cdot \nabla \xi_0' + \kappa \Delta u \, \Delta \xi_0' \d{x}
    \text{.}
  \end{align*}
\end{example}

\begin{example}[Willmore energy in graph case]
  Consider the nonlinear elastic bending energy parameterized over a flat reference surface, as shown in \cref{thm:WillmoreGraphCase}, but with $c_0 = \kappa_G = 0$.
  Then
  \begin{align*}
    \E(\p,u) = \int_{\Omega(\p)} \frac{\kappa}{2} \cdot \frac{\left(\Delta u + \nabla u \cdot A(u)\nabla u\right)^2}{(1+\norm{\nabla u}^2)^{5/2}} + \sigma \sqrt{1 + \norm{\nabla u}^2} \d{x}
  \end{align*}
  where
  \begin{align*}
    A(u) & = \begin{pmatrix} u_{22} & -u_{12} \\ -u_{12} & u_{11} \end{pmatrix}
    \text{.}
  \end{align*}
  In this setting
  it is mathematically challenging to establish whether a global minimizer of $\E(\p)$ exists in $\Uad(\p)$ and if it is unique.
  Moreover, solution methods might only be able to find local minimums.
  Therefore, it is simply assumed here that the differentiability result \cref{thm:summarizedDifferentiability} is applicable and, in the spirit of \cref{thm:differentiabilityLocalMinimizer}, that the derivative formula is considered for a local minimizer $u$.

  Let $\hat{H} := \hat{H}(z,Z) = Z_{11}(1+z_2^2) + Z_{22}(1+z_1^2) - z_1 z_2 Z_{12} - z_1 z_2 Z_{21}$ the numerator of the mean curvature term.
  Then the integrand of the energy functional is described by
  \begin{align*}
    \rho(x,y,z,Z)
    & = \frac{\kappa}{2} \cdot \frac{\hat{H}^2}{(1 + z_1^2 + z_2^2)^{5/2}} + \sigma \sqrt{1 + z_1^2 + z_2^2}
    %~ \text{.}
  \end{align*}
  and the relevant derivatives in a point $\zeta = (x,y,z,Z)$ are given by
  \begin{align*}
    \rho_x(\zeta) = 0
    ,\qquad\rho_y(\zeta) = 0
    ,\qquad\rho_Z(\zeta) = \frac{\kappa \hat{H}}{(1+z_1^2+z_2^2)^{5/2}}\begin{pmatrix} 1+z_2^2 & -z_1 z_2 \\ -z_1 z_2 & 1+z_1^2 \end{pmatrix} \text{.}
  \end{align*}
  and
  \begin{align*}
    \rho_z(\zeta) & = \frac{\kappa \hat{H}}{(1+z_1^2+z_2^2)^{5/2}} \mat{2z_1 Z_{22} - z_2 (Z_{12} + Z_{21}) \\ 2z_2 Z_{11} - z_1 (Z_{12} + Z_{21}) }
    \\ & \qquad - \frac{5\kappa \hat{H}^2}{2(1+z_1^2+z_2^2)^{7/2}} \mat{z_1 \\ z_2} + \frac{\sigma}{\sqrt{1+z_1^2+z_2^2}} \mat{z_1 \\ z_2}
    \text{.}
  \end{align*}
  Using $Q := \sqrt{1 + \norm{\nabla u}^2}$ and $\hat{H} := \hat{H}(Du, D^2u)$, the derivative formula \eqref{eq:interactionDerivative} reads
  \begin{align*}
    \partial_\q \EI(\p^0)
      & = \int_{\Omega(\p^0)} \div(V) \left( \frac{\kappa \hat{H}^2}{2Q^5} + \sigma Q \right) \d{x}
      \\ & \qquad
        + \int_{\Omega(\p^0)} \left( \frac{2\kappa\hat{H}}{Q^5} \mat{u_1 u_{22} - u_2 u_{12} \\ u_2 u_{11} - u_1 u_{12}} + \left(\frac{\sigma}{Q} - \frac{5\kappa\hat{H}^2}{2Q^7}\right) \nabla u \right)\cdot (\nabla \xi_0' - DV\nabla u) \d{x}
      \\
        %~ & \qquad + \int_{\Omega(\p^0)}  \frac{\kappa}{2Q^2} \mat{1+u_2^2 & -u_1u_2 \\ -u_1u_2 & 1+u_1^2} : \left(D^2\xi'_0 - \sum_{k=1}^2 D^2V_k \partial_k \wt{u} - DV^TD^2\wt{u} - D^2\wt{u}DV\right) \d{x}
        & \qquad + \int_{\Omega(\p^0)}  \frac{\kappa\hat{H}}{Q^5} \mat{1+u_2^2 & -u_1u_2 \\ -u_1u_2 & 1+u_1^2} : \left(D^2\xi'_0 - D^2V\nabla u - \Sym(D^2u DV)\right) \d{x}
      \text{.}
  \end{align*}
\end{example}

\begin{example}[linearized energy over tube]
  \label{thm:derivativeTube}
  Given a tubular reference manifold, the linearized elastic energy parameterized over a domain $\Omega = [0,L] \times [0,2\pi]$ is stated in \cref{thm:linearizationTube}.
  For the sake of simplicity the non-physical assumption is made that the mean curvature of the tube is matched with the spontaneous curvature, \ie let $c_0 = \frac{1}{r}$.
  After neglecting the Gaussian curvature term, the resulting linearized energy is up to a constant given by
  \begin{align*}
    \E(\p,u) = \frac{r}{2} \int_{\Omega(\p)} \kappa\left( u_{11} + \frac{u+u_{22}}{r^2}\right)^2 + \sigma \left(u_1^2+\frac{u_2^2}{r^2} \right) + \frac{2\sigma u}{r} \d{x}\text{.}
  \end{align*}
  Correspondingly,
  \begin{align*}
    \rho(x,y,z,Z) = \frac{r\kappa}{2} \left(Z_{11} + \frac{y+Z_{22}}{r^2} \right)^2 + \frac{r\sigma}{2}\left(z_1^2 + \frac{2y}{r} + \frac{z_2^2}{r^2}\right)
  \end{align*}
  with derivatives
  \begin{align*}
    \rho_x(\zeta) & = 0
    , &\rho_y(\zeta) & = r\kappa \left(Z_{11} + \frac{y+Z_{22}}{r^2} \right) + \sigma,
    \\ \rho_z(\zeta) & = r\sigma \mat{z_1 \\ \frac{z_2}{r^2}}
    , & \rho_Z(\zeta) & = r\kappa \left(Z_{11} + \frac{y+Z_{22}}{r^2} \right) \mat{1 & 0 \\ 0 & \frac{1}{r^2}}
    \text{.}
  \end{align*}
  %~ \snote{Attention! $\rho_y$ is wrong -- correct this and adapt the consecutive paragraph.}
  Hence, if $\Uad(\p)$ is chosen such that $J(\p)$ is elliptic, the interaction potential is differentiable and directional derivatives may be computed by formula \eqref{eq:interactionDerivative}.
  With the shortcuts $\nabla_r := (\partial_1, \partial_2/r)$, $\Delta_r := \partial_{11} + \partial_{22}/r^2$and $\Tr_r(A) := A_{11} + A_{22}/r^2$ it reads
  \begin{align*}
    \partial_\q \EI(\p^0)
    & = \frac{r}{2} \int_{\Omega(\p^0)} \div(V) \left(\kappa\left( \Delta_r u + \frac{u}{r^2}\right)^2 + \sigma \norm{\nabla_r u}^2 + \frac{2\sigma u}{r}\right) \d{x}
    \\ & \qquad + \int_{\Omega(\p^0)} \left(r\kappa \left(\Delta_r u + \frac{u}{r^2}\right) \sigma \right) \xi_0' + \sigma \nabla_r u \cdot \left(\nabla \xi_0' - DV\nabla u\right) \d{x}
    \\ & \qquad + \int_{\Omega(\p^0)} r\kappa \left(\Delta_r u + \frac{u}{r^2}\right) \left(\Delta_r \xi_0' - \Delta_r V \cdot \nabla u - 2 \operatorname{Tr}_r(D^2u DV) \right) \d{x}
    \text{.}
  \end{align*}
  Note that the functions $V$ and $w$ here need to fulfill periodic boundary conditions along the segments $\Gamma_{P,1} = [0,L] \times \{0\}$ and $\Gamma_{P,2} = [0,L] \times \{2\pi\}$ in the spirit of \cref{thm:periodicBoundaryConditions}.
\end{example}

