\subsection{Level set description of parametric interfaces and constraints}
\label{sec:levelSet}
While the overall grid in the discretization is chosen independently of the particle configuration~$\p$, it is still necessary to keep track of the interface~$\Gamma(\p)$ and the boundary conditions~$g(\p)$ for assembling purposes.
In the case of point value constraints this is done relatively easily, however for curve constraints more work is required, which this section elaborates on.
The core idea is to represent the $\Gamma(\p)$ interfaces via level sets.
For simplicity of notation, only one single particle is considered, \ie $\p \in \R^{n \times k}$ with $n=1$.
The generalization to multiple particles is straight-forward.

First, recall and re-state the relevant definitions in the spirit of \cref{sec:curveConstraints,sec:parametricConfigurations}.
For the particle model, suppose a particle reference hypersurface~$\wt{\cG}_0\subseteq\R^3$, a reference interface~$\Gamma_0 \subseteq \R^2$ in the plane and a diffeomorphism $\gamma\colon \wt{\Gamma}_0 \rightarrow \operatorname{im}(\gamma) \subseteq \wt{\cG}_0$ that is defined in a neighborhood $\wt{\Gamma}_0$ of $\Gamma_0$.
The set $\gamma(\Gamma_0)$ describes the interface along which the membrane connects to the particle and therefore induces the later constraints and interfaces.
A smooth particle transformation map~$\Psi\colon \R^k \times \R^3 \rightarrow \R^3$ is postulated such that $\Psi(\p;\cdot)$ is bijective and the transformed particle hypersurface is defined by $\wt{\cG}(\p) := \Psi(\p;\wt{\cG}_0)$.
In particular, the transformed membrane--particle interface is given by
\begin{align*}
  \cG(\p) := \Psi\left(\p;\mat{\Gamma_0 \\ \gamma(\Gamma_0)}\right)\text{.}
\end{align*}

Concerning the membrane model, assume that a reference surface~$\cM_0$, a parameterization $\varphi\colon\Omega\rightarrow\cM_0$, a local projection~$\pi$ and a local distance function~$d$ are given.
The inverse local projection onto $\wt{\cG}(\p)$ is again denoted by $\copi(\p)$, and it is assumed that all of the following expressions containing these local functions are well-defined in sufficiently large neighborhoods of the interfaces.

The transformed particle-interface on the reference domain $\Omega$ is defined by
\begin{align*}
  \Gamma(\p) := \varphi^{-1}\left(
                  \pi\left(
                    \Psi\left(\p;\mat{\Gamma_0 \\ \gamma(\Gamma_0)}\right)
                  \right)
                \right)
  \text{,}
\end{align*}
\ie the transformed membrane--particle interface~$\cG(\p)$ is projected onto the reference manifold~$\cM_0$ and then pulled back to the parameterization domain~$\Omega$.

In order to express the interface~$\Gamma(\p)$ via a level set function, suppose first a level set function~$f_0\colon \R^2 \rightarrow \R^2$ such that its zero level set describes the reference interface $\Gamma_0$, \ie
\begin{align*}
  \Gamma_0 = \left\{ x \in \R^2 \mid f_0(x) = 0 \right\}
\end{align*}
holds.
Moreover, it is assumed for later that $f_0 > 0$ holds in the interior of $\Gamma_0$ and $f_0 < 0$ on the outside.

By considering the inverse of the function in the definition of~$\Gamma(\p)$, a zero level set function for $\Gamma(\p)$ is given by
\begin{align*}
  f(\p;x) := f_0(\wt{y}(\p;x))
\end{align*}
where
\begin{align*}
  y(\p;x) := \Psi^{-1}\left(\p; \copi\left(\p;\varphi(x)\right)\right)
  ,\qquad \wt{y}(\p;x) = \mat{y_1(\p;x) \\ y_2(\p;x)}
  \text{.}
\end{align*}
If no such $y$ exists, for example because $x$ is ``too far away'' from the interface $\Gamma(\p)$, then let $f(\p;x)$ stay undefined, \ie $f(\p;x) = \operatorname{NaN}$ in programming terms.

The transformed constraint function~$g(\p) = (g_1(\p),g_2(\p))$ is defined on the interface~$\Gamma(\p)$ and extended to the full domain~$\Omega$ by
\begin{align*}
  g_1(\p) := d \circ \copi(\pi) \circ \varphi
  ,\qquad g_2(\p) := \partial_{\wt{\nu}} g_1(\p)
\end{align*}
where $\wt{\nu}$ is an arbitrary extension of the outer unit normal field~$\nu$ on $\partial\Omega(\p)$ to a vector field in all of $\Omega(\p)$.
Hence, $g_1(x)$ simply is the distance of the point $\varphi(x) \in \cM_0$ in direction of the normal~$\nu_{\cM_0}$ on $\cM_0$ to the transformed surface~$\wt{\cG}(\p)$.
This means that if $g_1(x)$ is defined it has the property
\begin{align}
  \label{eq:psiEquation}
  \Psi\left(\p;\mat{\wt{y}(\p;x) \\ \gamma(\wt{y}(\p;x))}\right) = \varphi(x) + g_1(x) \nu_0(\varphi(x))
  \text{.}
\end{align}
Again, if $g(\p;x)$ is not well-defined in a point $x \in \Omega$, let $g(\p;x) = \operatorname{NaN}$.

Within a practical implementation all relevant quantities are readily accessible once it is clear how to evaluate $\wt{y}$ and $g_1$ and an extension vector field~$\wt{\nu}$ is constructed.
Under the assumption that the equation~\eqref{eq:psiEquation} may be used to characterize $\wt{y}(\p;x)$ and $g_1(\p;x)$, \cref{alg:eval_yg} is proposed.

\begin{algorithm}
\caption{Evaluation of $\wt{y}(\p;x)$ and $g_1(\p;x)$}
\label{alg:eval_yg}
  \begin{algorithmic}[1]
      \STATE Let $\operatorname{tol} \in \R_{>0}$ and
        \begin{align*}
          F(\wt{x},t) := \frac{1}{2} \norm{\Psi\left(\p; \mat{\wt{x} \\ \gamma(\wt{x})} \right) - \varphi(x) - t \nu_{\cM_0}\left(\varphi(x)\right)}^2
          \text{.}
        \end{align*}
        
      \STATE Minimize $F$ (\eg using Newton's method)
        %~ and the start values
        %~ \begin{align*}
          %~ TODO
        %~ \end{align*}
        yielding final iterates $(\wt{x}^\ast, t^\ast)$.
        \vspace{0.5\baselineskip}
      \IF{$F(\wt{x}^\ast, t^\ast) < \operatorname{tol}$}
        \RETURN $(\wt{x}^\ast,t^\ast)$
      \ELSE
        \RETURN $\operatorname{NaN}$
      \ENDIF
  \end{algorithmic}
\end{algorithm}
With this algorithm at hand the evaluation of $f(\p)$ is possible as well, which enables the approximation of $\Gamma(\p)$ and the computation of the sub-grids $\cT_{0}$ and $\cT_{\Gamma}$ by standard level set methods.
Furthermore, an extension of the unit normal vector field is given in a neighborhood of the interface~$\Gamma(\p)$ by
\begin{align*}
  \wt{\nu}(\p;x) := \frac{\nabla f(\p;x)}{\norm{\nabla f(\p;x)}}\text{.}
\end{align*}

Finally, whenever needed, differentiation of $\wt{y}$ and $g_1$ can for example be carried out through a sensitivity analysis of the corresponding implicit equation~\eqref{eq:psiEquation} or also only approximatively via classical difference methods.


%The goal is to construct the relevant geometric quantities for a given configuration~$\p$ from the a priori known particle model functions $\Psi$, $f_0$, $\gamma$, and the membrane model's parameterization function~$\varphi$.

\subsection{Approximation algorithm for stationary membranes}
Suppose a configuration~$\p \in \cD^\circ$ and a grid size parameter~$h \in (0,1]$ as well as a rectangular bulk domain $\Omega \supseteq \Omega(\p)$.
A prototypical algorithm for the discrete approximation of the stationary membrane~$u(\p)$ subject to curve constraints is described in \cref{alg:approxCurve}.
\begin{algorithm}
\caption{Approximation of a stationary membrane for curve constraints}
\label{alg:approxCurve}
  \begin{algorithmic}[1]
    \STATE Create an $h$-dependent grid $\cT$ of rectangles covering the bulk domain~$\Omega$.
    \STATE Equip the grid with a $\cQ_k$ finite element space and basis functions $\varphi_i$, $i = 1,\dots,M$.
    \STATE Compute the required $\p$-dependent geometric information, such as $\cT_0$, $\cT_\Gamma$, quadrature points on $\cT_\Gamma$ and values of the constraint right hand sides~$g(\p)$.
    \STATE Assemble the system matrix $A \in \R^{M\times M}$ and right hand side vector $b \in \R^M$ over the sub-grid $\cT_0$ where
      \begin{align*}
        A_{ij} := a(\varphi_i,\varphi_j), \qquad b_i := \ell(\varphi_i)
      \end{align*}
      is defined in the sense of the fictitious domain stabilized Nitsche method~\eqref{eq:cutFEMa} and~\eqref{eq:cutFEMell}.
    \STATE Modify $A$ and $b$ in order to incorporate periodic and/or homogeneous Dirichlet boundary conditions along the outer boundary~$\partial\Omega$.
    \STATE Solve the linear system $A\du = b$.
  \end{algorithmic}
\end{algorithm}
%~ \begin{enumerate}
  %~ \item Create an $h$-dependent grid $\cT$ of rectangles covering the bulk domain~$\Omega$.
  %~ \item Equip the grid with a $\cQ_k$ finite element space and basis functions $\varphi_i$, $i = 1,\dots,M$.
  %~ \item Compute the required $\p$-dependent geometric information, such as $\cT_0$, $\cT_\Gamma$, quadrature points on $\cT_\Gamma$ and values of the constraint right hand sides~$g(\p)$.
  %~ \item Assemble the system matrix $A \in \R^{M\times M}$ and right hand side vector $b \in \R^M$ over the sub-grid $\cT_0$ where
    %~ \begin{align*}
      %~ A_{ij} := a(\varphi_i,\varphi_j), \qquad b_i := \ell(\varphi_i)
    %~ \end{align*}
    %~ is defined in the sense of the fictitious domain stabilized Nitsche method~\eqref{eq:cutFEMa} and~\eqref{eq:cutFEMell}.
  %~ \item Modify $A$ and $b$ in order to incorporate periodic and/or homogeneous Dirichlet boundary conditions along the outer boundary~$\partial\Omega$.
  %~ \item Solve the linear system $A\du = b$.
%~ \end{enumerate}
The analogous prototypical algorithm for point constraints in the sense of \cref{sec:point} is shown in \cref{alg:approxPoint}.
\begin{algorithm}
\caption{Approximation of a stationary membrane for point constraints}
\label{alg:approxPoint}
  \begin{algorithmic}[1]
    \STATE Create an $h$-dependent grid $\cT$ of rectangles covering the bulk domain~$\Omega$.
    \STATE Equip the grid with a $\cQ_k$ finite element space and basis functions $\varphi_i$, $i = 1,\dots,M$.
    \STATE Compute the required $\p$-dependent geometric information, such as the constraint right hand side~$g(\p) \in \R^N$.
    \STATE Compute a transformed basis $\wt{\varphi_i}$, $i = 1,\dots,M-N$ based on local QR-decompositions~\eqref{eq:qrLocal} and set up the basis transformation matrix~$\wt{Q} \in \R^{M\times(M-N)}$ as in~\eqref{eq:Qtilda}.
    \STATE Compute a feasible vector~$\du^0 \in \R^M$ by formula~\eqref{eq:du0formula}.
    \STATE Assemble the system matrix $\wt{A} \in \R^{(M-N)\times (M-N)}$ and right hand side vector $\wt{b} \in \R^{M-N}$ over the sub-grid $\cT_0$ where
      \begin{align*}
        \wt{A}_{ij} := a_{\Omega(\p)}(\wt{\varphi}_i,\wt{\varphi}_j), \qquad \wt{b}_i := \ell(\p;\wt{\varphi}_i) - \sum_{j=1}^M a_{\Omega(\p)}(\du^0_j \varphi_j, \varphi_i)
      \end{align*}
      is defined in the sense of equation~\eqref{eq:reducedLinearSystem}
    \STATE If necessary, modify $\wt{A}$ and $\wt{b}$ in order to incorporate periodic and/or homogeneous Dirichlet boundary conditions along the outer boundary~$\partial\Omega$.
    \STATE Solve the linear system $\wt{A}\wt{\du} = \wt{b}$ and set $\du := \wt{Q}\wt{\du} + \du^0$.
  \end{algorithmic}
\end{algorithm}
%~ \begin{enumerate}
  %~ \item \TK{TODO}
%~ \end{enumerate}

Corresponding implementations are done in \textsc{Dune}, a C++~toolbox for solving partial differential equations \cite{Dune08a,Dune08b,Dune16}.
The grids are created as uniform grids of rectangles using the \textsc{Dune-Uggrid} module (cf.~\cite{UG97}), and are equipped with Bogner--Fox--Schmit finite elements.
Interface-dependent quantities are computed by a level set ansatz in the sense of \cref{sec:levelSet}.
The assembly of the system itself and the incorporation of remaining boundary conditions is then realized by standard techniques.
The line integrals along $\Gamma(\p)$ are approximated by means of the techniques described in~\cite{AtVe93}.
For the volume quadrature on $\cT_{\Gamma}$ element-specific quadrature rules for implicitly defined domains in the sense of \cite{OlSa16} are applied, and on $\cT_0\setminus \cT_{\Gamma}$ quadrature is performed exactly using classical Gauss quadrature rules.
Finally, the linear system is solved using the direct solver \textsc{SuperLU}~\cite{Li05}.
%This implementation used for all of the following numerical computations.

It is noted that the implementation restricts itself to a simple level set algorithm.
Its precise treatment is omitted for the sake of brevity, but it is emphasized that it works sufficiently well for the subsequent examples and applications.
However, in rare situations this implementation fails to reconstruct the full interfaces, leading to disproportionate discretization errors.
Such issues will be addressed appropriately when they occur.
More sophisticated methods and algorithms for improved level set computations are presented for example in~\cite{Sethian99}.

\subsection{Evaluation of gradients of the interaction potential}

%~ \snote{Try to avoid confusion with $\xi$ here and $\xi_0'$ from the summarized differentiability theorem. I.\,e., put ``$\xi_0'$'' here, too.}

In view of the derivative formula in \cref{thm:interactionVolumeDerivative} for the computation of directional derivatives, the evaluation of the gradient~$\nabla \EI(\p)$ of the interaction potential requires the construction of one vector field~$V$ and one constraint-preserving function~$\xi_0'$ for each degree of freedom in the configuration parameter~$\p \in \R^{n\times k}$, \ie $n\cdot k$ constructions are necessary.

For the sake of a shorter notation those functions are taken together into a single vector field $W:=(V,\xi)\colon\Omega(\p) \rightarrow\R^3$.
Following \cref{thm:summarizedDifferentiability}, the evaluation of $\partial_{(i,j)} \EI(\p)$, $i = 1,\dots,n$, $j = 1,\dots,k$ requires the vector field~$W$ to fulfill the boundary conditions on $\Gamma(\p)$
\begin{align}
  \label{eq:bcWInner}
  W|_{\Gamma(\p)} = \mat{\partial_i \psi(\p) \\ \partial_i (g_1(\p) \circ \psi(\p))}
  ,\qquad DW\nu|_{\Gamma(\p)} = \mat{\partial_i (\wt{\nu}(\p) \circ \psi(\p) \\ \partial_i (g_2(\p) \circ \psi(\p)}
\end{align}
where the partial derivatives $\partial_{(i,j)}$ here are always understood with respect to the $\p$-variable.
In the spirit of \cref{thm:periodicBoundaryConditions} the conditions on the $\p$-independent boundary~$\partial\Omega$ read
\begin{align}
  \label{eq:bcWOuter}
  \begin{aligned}
    W|_{\Gamma_D} & = 0 \qquad & W|_{\Gamma_{P,1}} & = W|_{\Gamma_{P,2}}
    \\  DW\nu|_{\Gamma_D} & = 0 & DW\nu|_{\Gamma_{P,1}} & = -DW\nu|_{\Gamma_{P,2}}
  \end{aligned}
\end{align}
where $\Gamma_D$ denotes the $\p$-independent Dirichlet boundary and $\Gamma_P = \Gamma_{P,1} \dot{\cup} \Gamma_{P,2}$ is the periodic boundary.
%with corresponding modifications in the sense of\cref{thm:periodicBoundaryConditions} if also periodic boundary conditions are present.

In general, one is free in the choice of extension for $W$, however the error result \cref{thm:gradientErrorEstimate} on the approximation of the gradient suggests to choose $W$ such that the derivatives up to second order are bounded grid-independently.
This may be achieved for example if the components~$W_l$, $l=1,2,3$ each solve the minimization problem
\begin{align*}
  \min_{v \in H^2(\Omega(\p))} \frac{1}{2} a_{\Omega(\p)}(v,v)
  , \qquad & \text{s.\,t. $v$ fulfills the $l$-th boundary}
  \\ & \text{condition in \eqref{eq:bcWInner} and \eqref{eq:bcWOuter}.}
\end{align*}
This problem is essentially identical to the minimization problem associated to the stationary membrane~$u(\p)$, but with different right hand sides in the constraints.
In particular, the same discretization schemes are applicable for the approximative computation of the components~$W_{l,h} \approx W_l$ as for the computation of the approximation~$u_h\approx u(\p)$.
Within the setting of curve constraints, the required smoothness property $W \in H^{2,\infty}(\Omega_0,\R^3)$ in \cref{thm:gradientErrorEstimate} is fulfilled under the typical smoothness conditions for the boundary data from elliptic regularity theory, which generally hold true in the upcoming example computations.
For point constraints, however, this regularity property is not obtained as easily and hence the behavior of this extension approach technically requires further investigation.
% within that context.

%~ \TK{Motivate pre-assembling of derivative field}

In conclusion, the evaluation of the gradient may for example be implemented along the lines of \cref{alg:gradientEvaluation}.

\begin{algorithm}
\caption{Approximation $G$ of the gradient of the interaction potential $\nabla \EI(\p)$}
\label{alg:gradientEvaluation}
  \begin{algorithmic}[1]
    \STATE Suppose an approximation vector~$\du$ is known from \cref{alg:approxCurve} (or \cref{alg:approxPoint}).
    \vspace{0.5\baselineskip}
    \FOR{$i = 1,\dots,n$ and $j = 1,\dots,k$}
      \FOR{$l = 1,2,3$}
        \STATE Assemble a right hand side $b^{(l)}$ as in \cref{alg:approxCurve} (or \cref{alg:approxPoint}) incorporating the boundary conditions from $W_l$.
        \STATE Approximate $W_l$ by solving the system $A\textbf{W}^{(l)} = b^{(l)}$ (or $\wt{A}\textbf{W}^{(l)} = b^{(l)}$).% with $A$ (or $\wt{A}$) from \cref{alg:approxCurve} (or \cref{alg:approxPoint}).
      \ENDFOR
      \vspace{0.5\baselineskip}
      \STATE Compute $G_{(i,j)} \approx \partial_{(i,j)} \EI(\p)$ by carrying out the integration in the volume formula~\eqref{eq:interactionDerivative} with the approximations $u_h \approx u$ and $W_h \approx (V,\xi)$.
    \ENDFOR
    \vspace{0.5\baselineskip}
    \RETURN $G$
  \end{algorithmic}
\end{algorithm}

Within the \textsc{Dune} implementation of this algorithm the solution of the involved linear systems is comparatively inexpensive because a factorization of $A$ (or $\wt{A}$) is already known from the computation of the approximation vector~$\du$ via the direct solver \textsc{SuperLU},
and a similar efficiency should be achievable by using \eg iterative solvers based on geometric multigrid methods.
Moreover, the quantities required for the assembly of the right hand sides are indirectly available from the considerations in \cref{sec:levelSet}, and the integration is done by similar quadrature rules as for the assembly of the system matrices.

\begin{comment}
\section{Implementation remarks}
\begin{enumerate}
  \item
    Finite Element space: Finite element implementation in DUNE: Bogner--Fox--Schmit elements and UG-grid (here: uniform grids). Sometimes we use periodic and sometimes we use Dirichlet boundary conditions.
    
  \item
    Representation of interfaces and boundary conditions: Interfaces are represented by level sets. Boundary conditions are induced by graphs (\ie functions $\R^2 \rightarrow \R$).
    
  \item
    Assembling the system: We use exact Gauss quadrature on interior elements. On boundary elements we use Olshanskii's method for volume integration and parametric integration for the interface.
    (\TK{to-do: remark on order of quadrature?})
    
  \item
    Also concerning assembling: The map $\psi$ is evaluated using Newton's method (possibly subject to change: either pure or with strict Wolfe--Powell step rule).
    
  \item
    Solving the system: The system is solved using superLU.
    
  \item
    Evaluation of derivatives: The vector fields are computing by solving more PDEs (to get a as smooth solution as possible); this can be done rather efficiently by re-using the factorization from superLU. Since the derivative is linear in $V$ we may also pre-assemble the functional as a vector.
\end{enumerate}
\end{comment}
