\label{chap:differentiation}

\begin{comment}
This chapter discusses the differentiability of interaction potentials induced by energies of the form
\begin{align*}
  \E(\p,u) & = \int_{\Omega(\p)} \rho(x,u,Du,D^2u) \d{x}
  \text{.}
  %~ \text{,}
\end{align*}
%~ where $\rho \in $ \TK{TODO}.
For ease of presentation we restrict ourselves in the following to curve constraints and
\begin{align*}
  \hat{H}(\p) = \left\{ v \in H^2(\Omega(\p)) \mid v|_{\partial\Omega} = \partial_\nu v|_{\partial\Omega} = 0 \right\}\text{,}
\end{align*}
\ie Dirichlet boundary conditions of the parameterization domain $\Omega$.
At the end of this chapter we will also remark on generalizations to point constraints and average constraints, and to domains with periodic boundary conditions.
\end{comment}


The differentiability of the interaction potential is crucial for a wide range of mathematical methods related to its minimization.
%~ One is tempted to approach this question by simply applying the chain rule to the definition of the interaction potential, \ie to write
%~ A tempting \emph{formal} approach is to solve this problem rather directly via the definition of the interaction potential and the chain rule by
It is tempting to argue for the differentiability of the interaction potential via the chain rule
\begin{align*}
  \dd{\p} \EI(\p) = \dd{\p} J(\p, u(\p)) = J_{\p}(\p,u(\p)) + J_u(\p,u(\p)) \cdot \dd{\p} u(\p) = J_{\p}(\p,u(\p))
\end{align*}
where the last step uses the fact that $J_u(\p,u(\p)) = 0$ holds as $u(\p)$ is a critical point of $J(\p,\cdot)$.
%~ However, this method bears two problems.
However, at this point that computation only makes sense formally.
This is because it implicitly relies on the existence of the derivatives $J_{\p}$, $J_u$ and $\dd{\p} u(\p)$, which is not yet known at all.
While generally the definition and existence of the derivative $J_u$ is relatively clear, it is less obvious what the meaning of the expressions $J_{\p}$ and $\dd{\p} u(\p)$ is
%in first place
as the membrane functions $u(\p)$ originate from different function spaces $\Uad(\p)$ with different domains $\Omega(\p)$.
Another potential and more practical issue is about the feasibility of the resulting expression in numerical schemes.
%~ If the formal result $J_{\p}(\p,u(\p))$ is correct, then an explicit representation of $J_{\p}$ can in many cases, at least formally, be derived by virtue of the Reynold's transport theorem \cite{Reynolds1903} for the differentiation of domain-dependent integrals.
%~ But unfortunately, the resulting expressions usually require higher regularity in the optimal membrane shape $u(\p)$ (see also~\cite{ElGrHoKoWo16}).
For example, in~\cite{ElGrHoKoWo16} a formal differentiation approach for the interaction potential is taken based on the Reynolds transport theorem \cite{Reynolds1903}.
As a result, an expression for the gradient is derived there which consists out of boundary integrals over $\partial\Omega(\p)$ containing derivatives of $u(\p)$ up to third order.
This is problematic in numerical schemes, because even if $u(\p)$ is smooth, numerical approximation schemes for $u(\p)$ are in general not able to provide error estimates for derivatives of higher than second order.
As a consequence,
it would not be possible to obtain suitable error estimates for approximations of the gradient of the interaction potential.
%~ also the error in the approximation of the gradient of the interaction potential cannot be bounded.

%~ Therefore, this chapter's objective is to both establish a mathematically sound differentiability result, and to represent the gradient in a way that is potentially accessible to error estimates in finite element methods.
Therefore, this chapter has two objectives:
The first is to establish a mathematically sound differentiability result for a sufficiently large class of interaction models,
and the second one is to derive an expression for the gradient which is accessible to error estimates within the framework of finite element methods.
These goals are accomplished by the following steps:
In the first section the admissible sets $\Uad(\p)$ are related to each other via so-called trace preserving diffeomorphisms between the domains $\Omega(\p)$.
Such diffeomorphisms are constructed rather explicitly by a domain perturbation ansatz from shape calculus (see \cite{SoZo92}, \cite{DeZo11}), which sets the stage for a differentiability concept similar to material derivatives in surface calculus.
Afterwards, the second section transforms the elastic energies and their minimization problems onto a common reference domain and space.
Then the finite dimensionality of the state space is exploited in order to reduce the matter of differentiability to an application of the implicit function theorem.
Finally, the third section derives a numerically feasible representation of the gradient in the above sense by means of known methods from matrix calculus.
In this context also explicit derivative formulas for various relevant interaction models are stated.

\begin{comment}
This chapter is organized as follows: In the first section we introduce and discuss the concept and construction of so-called constraint-preserving diffeomorphisms, which are going to be a key element in the second section where we prove differentiability of the interaction potential based on the implicit functions theorem.
The third section gives an explicit formula for the first derivative of the interaction potential in terms of volume integrals.
This representation is numerically feasible in the sense that it is exclusively defined in terms of quantities whose asymptotic discretization errors are accessible in a finite element setting.
Finally, the fourth section applies the abstract results to some examples that will be relevant later on.
\end{comment}

Recall the definition of the admissible sets as
\begin{equation*}
  \Uad(\p) := \left\{ u \in H^2_\ast(\Omega(\p)) \mid T(\p;u) = g(\p)\right\}
\end{equation*}
where $H^2_\ast(\Omega(\p)) \subseteq H^2(\Omega(\p))$ is a linear subspace encoding $\p$-independent constraints,
%$G(\p)$ is a linear bounded operator,
$T(\p)$ is either a point evaluation or trace operator over the parametric interface $\Gamma(\p)$, and where $g(\p) \in \operatorname{range}(T(\p))$ is the constraint right hand side.
For the sake of a simple presentation, it is assumed in the following sections that $H_\ast^2(\Omega) = H_0^2(\Omega)$ holds, \ie that the $\p$-independent constraints are of Dirichlet-type and span the full boundary.
The mild modifications that are necessary when $H_\ast^2(\Omega)$ incorporates periodic boundary conditions or spans only a subset of the boundary are discussed in \cref{thm:periodicBoundaryConditions}.

Concerning notation, throughout the whole chapter $\p^0 \in \cD^\circ$ is a point in the interior of the configuration space $\cD \subseteq \R^{N\times k}$ and $\cB$ is a generic (\ie non-fixed) convex neighborhood of $\p^0$ in~$\cD^\circ$.
Like in the introductory formal calculation, indices of functions indicate partial derivatives, \eg given a function $f(\p,x)$ one defines $f_{\p} := \dd{\p} f$.
The differentiation operator $D$ is always meant with respect to spatial variables only, $\ie$ $D := \dd{x}$.
In addition, $\Sym(A) := A + A^T$ is defined for all matrices $A \in \mathbb{R}^{n\times n}$, and for every vector field $V\colon \R^2 \rightarrow \R^2$ and every vector $y \in \R^2$ the application of the Hessian is defined as $D^2V\cdot y := \sum_{k=1}^2 D^2V_k y_k$.
The homogeneous admissible set is defined as
\begin{align*}
  \Uad^0(\p) := \left\{ u \in H_\ast^2(\Omega(\p)) \mid T(\p;u) = 0\right\}
  \text{.}
\end{align*}
Lastly and in a slight abuse of notation, whenever a $\p$-dependent function $f(\p;x)$ is inverted, the inversion is meant for fixed $\p$, \ie $f^{-1}(\p;x) := f(\p;\cdot)^{-1}(x)$.

\incsection[differentiation/diffeomorphisms]{Trace and constraint preserving maps}
\incsection[differentiation/differentiability]{Differentiability}
\incsection[differentiation/volumerepresentation]{Volume integral representation of the gradient}
% Maybe not: \incsection[differentiation/application]{Application to the example models}
% Instead we could just add some examples where needed.
