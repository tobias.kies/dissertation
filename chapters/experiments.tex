\label{chap:experiments}

%~ \TK{Remark somewhere that we did not consider the squared gradient in the errror results.}

This chapter is divided into two parts:
The first one sketches an implementation for the computation of stationary membranes in linearized particle--membrane interaction models based on the established finite element discretizations for linearized energies in \cref{chap:discretization}.
In addition, it introduces a level set ansatz for the description of the configuration-dependent domains and interfaces.
Such an implementation is then used in the second part in order to illustrate the previously derived discretization error estimates for finite element approximations of stationary membranes within various different models.

\incsection[experiments/implementation]{Implementation remarks}

\section{Curve constraints: Optimal orders of convergence}
  \subsection{Radially symmetric example}
    \label{sec:exp1}
    This example considers the Monge-gauge parameterization with bending rigidity~$\kappa = 1$ and vanishing surface tension~$\sigma = 0$ coupled to curve constraints.
    The reference domain is assumed to be a two-dimensional $R$-sphere, \ie such that $\Omega = B_R \subseteq \R^2$ holds, and the projection of the particle--membrane-interface onto $\Omega$ is supposed to be circular and centered around the origin, \ie such that $\Gamma = \partial B_r$ holds.
    In addition, it is assumed that the outer boundary $\partial\Omega$ fulfills zero Dirichlet boundary conditions and that the particle induces radially symmetric Dirichlet boundary conditions along the interface~$\Gamma$.
    Effectively, this leaves the domain of computation $\Omega_0 := B_R \setminus B_r$.
    Altogether, this reduces the computation of the stationary membrane to the solution of the fourth order elliptic partial differential equation with Dirichlet boundary conditions
    \begin{align*}
      \Delta^2 u & = 0 \text{ on $\Omega_0$}, \qquad Tu =
        \begin{cases}
          (0,0) & \text{on $\partial B_R$}
          \\ (c_1,c_2) & \text{on $\partial B_r$}
        \end{cases}
    \end{align*}
    for some constants $c_1,c_2 \in \R$.
    
    Due to radial symmetry the solution takes the form $u(x) = u_0(\norm{x})$ where $u_0$ solves the differential equation
    \begin{align*}
      u_0''''(t) + \frac{2}{t} u_0'''(t) - \frac{1}{t^2} u_0''(t) + \frac{1}{t^3} u_0'(t) = 0
    \end{align*}
    subject to the boundary conditions
    \begin{align*}
      u_0(r) = c_1, \qquad u_0'(r) = c_2, \qquad u(R) = 0, \qquad u_0'(R) = 0
      \text{.}
    \end{align*}
    For the numerical computations the special choices $r = \frac{1}{e}$, $R = 1$, $c_1 = \frac{e^2-3}{4}$, and $c_2 = -e$ are made.
    The solution $u_0$ is therefore given by
    \begin{align*}
      u_0(t) = \frac{e^2}{4} \left( 1 - t^2 + 2t^2 \log(t)\right)\text{.}
    \end{align*}
    See \cref{fig:convergenceCurveExactSolution} for a visualization of the resulting stationary membrane~$u$.

    \begin{figure}
      \begin{center}
        \includegraphics[width=0.5\textwidth]{curveExact.eps}
      \end{center}
      \caption{Exact solution of the example problem stated in \cref{sec:exp1}. The coloring encodes the function's elevation.}
      \label{fig:convergenceCurveExactSolution}
    \end{figure}

    The numerical approximation of the stationary membrane is performed using the fictitious domain stabilized Nitsche method applied to a uniform background mesh of square elements over the domain~$[-1,1]^2$ with varying grid sizes $h \in \R_{>0}$.
    In view of the error results from \cref{sec:cutFEMerror}, the smoothness of $u$ over $\Omega_0$, and the approximation properties of Bogner--Fox--Schmit finite elements, one expects within the given \textsc{Dune} implementation the asymptotic convergence
    \begin{align*}
      \norm{u - u_h}_{H^2(\Omega_0)} \in \mathcal{O}(h^2) \text{ for } h \to 0
      \text{.}
    \end{align*}
    This convergence behavior is explored numerically in \cref{fig:convergenceCurveExact} where the discretization errors $\norm{D^i(u-u_h)}_{L^2(\Omega_0)}$ are plotted over the range of grid sizes $h = \frac{2}{N}$ for $i \in \{0,1,2\}$ and $N \in \{16,\dots,512\}$.
    Each error was computed via a high order quadrature rule with respect to the given grid, and the penalty weighting parameter~$\lambda = 1024$ was used within the discretization scheme.

    %~ \cref{fig:convergenceCurveExact} shows the errors in dependence of $h$.
    %~ In concordance with the theoretical results the convergence rate $2$ is observed for the $H^2$ error.
    %~ Also the $L^2$ and $H^1$ error rates behave as expected as they are of order $4$ and $3$, respectively.
    %~ The outliers in the graphs are assumed to be attributable to instabilities in the implementation of quadrature rules and interface detection.

    \begin{figure}
      \begin{center}
        %~ \includegraphics[width=0.7\textwidth]{convergence_curve_exact_L2.png}
        \incsvg{0.3\textheight}{convergence_curve_exact_L2}\\
        \incsvg{0.3\textheight}{convergence_curve_exact_H1}\\
        \incsvg{0.3\textheight}{convergence_curve_exact_H2}\\
      \end{center}
      \caption{Double logarithmic plot of the $L^2$-, $H^1$- and $H^2$ discretization error (from top to bottom) over the range $h \in [1/256, 1/8]$ of grid sizes for the example problem in \cref{sec:exp1}.}
      \label{fig:convergenceCurveExact}
    \end{figure}

    The $H^2$-norm of the error generally is in good agreement with the reference line and thus confirms the predicted convergence order $2$.
    Similarly, the plots of the errors in the $L^2$-norm and the $H^1$-norm closely follow their respective reference lines and so show the convergence orders $4$ and $3$, respectively.
    This is the typical expected behavior where every order less of differentiation in the error norm yields an extra order of convergence.
    Altogether, this corresponds to an optimal convergence behavior.
    
    However, there are outliers within the error plots that seemingly disagree with the observed convergence behavior, especially in the $H^2$-norm.
    As the theory states grid-independence of the constants in the approximation estimates for the given discretization scheme, these outliers are assumed to be attributable to instabilities in the used implementation of the level set description and the resulting errors in the quadrature rules.
    Similarly, the divergence of the $L^2$-error around the threshold $2^{-31} \approx 5\cdot 10^{-10}$ might be explained by rounding errors and numerical instabilities in the quadrature rules within the computation of the $L^2$-norm.

  \subsection{Non-symmetric example}
    \label{sec:exp2}
    
    This example investigates the rates of convergence in a more complicated, non-symmetric case with two particles.
    As before, the Monge-gauge parameterization with bending rigidity~$\kappa = 1$ and vanishing surface tension~$\sigma = 0$ as well as zero Dirichlet boundary conditions on the outer boundary~$\partial\Omega$ is considered.
    However, this time the parameterization domain is given by $\Omega = [-1,1]^2$ and the particle--membrane interface~$\Gamma(\p)$ is induced by the following elliptic particle model:
    Suppose the semi axes are $a = \frac{1}{3}$ and $b = \frac{2}{9}$ and the slope is $s = 1$.
    The reference particle manifold is given by
    \begin{align*}
      \wt{\cG}_0 := \left\{ x \in \R^3 \ \Big \vert \ \frac{2x_3}{as} = 1 - \frac{x_1^2}{a^2} - \frac{x_2^2}{b^2}\right\}
    \end{align*}
    and the reference particle--membrane interface is defined as
    \begin{align*}
      \cG_0 := \left\{ x \in \R^2 \times \{0\} \ \Big \vert \ \frac{x_1^2}{a^2} + \frac{x_2^2}{b^2} = 1 \right\} \subseteq \wt{\cG}_0
      \text{.}
    \end{align*}
    Furthermore, each particle is equipped with six degrees of freedom describing both its position and the orientation.
    Hence, following \cref{thm:variablePositionOrientation} the transformation map may be defined by
    \begin{align*}
      \Psi(p;x) := R_3(p_6)R_2(p_5)R_1(p_4) x + \mat{p_1 & p_2 & p_3}^T 
    \end{align*}
    where $R_i(\alpha)$ denotes the counter-clockwise rotation around the $x_i$-axis by the angle $\alpha$.
    The particle configuration parameter chosen in the example is
    \begin{align*}
      \p :=
        \begin{pmatrix}
          -0.4\phantom{0} & \phantom{-}0.4\phantom{0} & 0.3 & \phantom{-}10^\circ & \phantom{-}15^\circ & 30^\circ
          \\ \phantom{-}0.25 & -0.35 & 0.3 & -10^\circ & -15^\circ & 60^\circ
        \end{pmatrix}
      \text{.}
    \end{align*}
    Altogether, the complete problem reads
    \begin{align*}
      \Delta^2 u & = 0 \text{ on $\Omega(\p)$}
      ,\qquad u = \partial_\nu u = 0 \text{ on $\partial\Omega$}
      , \qquad \mat{u \\ \partial_\nu u} = g(\p) \text{ on $\Gamma(\p)$}
    \end{align*}
    with an implicit interface~$\Gamma(\p)$ and boundary conditions $g(\p)$.
    A discrete solution of this problem is depicted in \cref{fig:convergenceCurveInexactFineSolution}.

    \begin{figure}
      \begin{center}
        \includegraphics[width=0.5\textwidth]{curveInexact.eps}
      \end{center}
      \caption{Fine grid solution of the example problem stated in \cref{sec:exp2}. The coloring encodes the function's elevation.}
      \label{fig:convergenceCurveInexactFineSolution}
    \end{figure}

    As for the symmetric example, the solution is approximated numerically via the fictitious domain stabilized Nitsche method applied to a uniform mesh of squares.
    From regularity theory also the smoothness of $u$ is apparent and thus again quadratic convergence of the approximation in the $H^2$-norm is expected.
    As no analytical solution~$u$ is available, the error computations approximate it by a fine grid solution $u_H$ with $H = 1/128$.
    Corresponding plots of the approximate errors $\norm{D^i(u_H-u_h)}_{L^2(\Omega_0)}$, $i\in\{0,1,2\}$ are shown in \cref{fig:convergenceCurveInexact} for the grid sizes $h = \frac{1}{N}$, $N \in \{16,\dots,127\}$.
    Each error was computed by a high order quadrature rule with respect to the given fine grid.
    The penalty weighting parameter was set to $\lambda = 1024$.

    \begin{figure}
      \begin{center}
        \incsvg{0.3\textheight}{convergence_curve_inexact2_L2}\\
        \incsvg{0.3\textheight}{convergence_curve_inexact2_H1}\\
        \incsvg{0.3\textheight}{convergence_curve_inexact2_H2}\\
      \end{center}
      \caption{Double logarithmic plot of the approximate $L^2$-, $H^1$- and $H^2$ discretization error (from top to bottom) over the range $h \in [1/128, 1/16]$ of grid sizes for the example problem in \cref{sec:exp2}.}
      \label{fig:convergenceCurveInexact}
    \end{figure}
    
    Like in the symmetric case, the error plots follow the given reference lines relatively well and thus exhibit the convergence orders $4$, $3$ and $2$ for the $L^2$-, $H^1$- and $H^2$-norm of the error, respectively.
    Thus, optimal convergence is observed in this example as well.
    Again, fluctuations in the error plots might be explained by grid-dependent instabilities within the given implementation.
    As is to be expected, the error curves degenerate and lose their significance for $h \to H$.
    
\section{Point value constraints: Non-optimal regularity}
  \subsection{Radially symmetric example}
    \label{sec:exp3}
    
    This example uses the analogue to the problem in \cref{sec:exp1} but with point value constraints instead of curve constraints.
    Let $\Omega := B_R \subseteq\R^2$ bean $R$-sphere and suppose a single point value constraint in the origin, while maintaining zero Dirichlet boundary conditions.
    Again with the Monge-gauge parameterization and the parameters $\kappa = 1$ and $\sigma = 0$ this corresponds to the equation
    \begin{align*}
      \Delta^2 u = 0 \text{ on $\Omega$}
      ,\qquad u = \partial_\nu u = 0 \text{ on $\partial\Omega$}
      ,\qquad u(0) = g
    \end{align*}
    for some constant $g \in \R$.
    
    As in \cref{sec:exp1}, due to radial symmetry the solution of this problem takes the form $u(x) = u_0(\norm{x})$ where $u_0$ solves the boundary value problem
    \begin{align*}
      u_0''''(t) + \frac{2}{t} u_0'''(t) - \frac{1}{t^2} u_0''(t) + \frac{1}{t^3} u_0'(t) = 0
      ,\qquad u_0(0) = g
      ,\qquad u_0(R) = 0
      \text{.}
    \end{align*}
    For the numerical computations the radius is set to $R = 1$ and the point constraint value is set to $g = 1$.
    Hence, one has
    \begin{align*}
      u_0(t) = 1 - t^2 + t^2 \log(t)
      \text{.}
    \end{align*}
    The resulting solution function~$u$ is depicted in \cref{fig:convergencePointExactSolution}.

    \begin{figure}
      \begin{center}
        \includegraphics[width=0.5\textwidth]{pointExact.eps}
      \end{center}
      \caption{Exact solution of the example problem stated in \cref{sec:exp3}. The coloring encodes the function's elevation.}
      \label{fig:convergencePointExactSolution}
    \end{figure}
    
    Because of the logarithmic term $t^2\log(t)$ in the definition of~$u_0$, the solution~$u$ exhibits a singular behavior around the constraint point~$x = 0$.
    This leads to a loss of regularity, and so instead of $H^4$-regularity only $u \in H^{3-\eps}(\Omega)$ is obtained for arbitrary $\eps \in \R_{>0}$.
    %~ \snote{To-do: Verify this regularity result ($u \in H^2$ is clear and $u\notin H^3$ I think was too, but the estimate in between is tough).}
    
    The continuous solution is approximated numerically by the conforming transformation method as presented in \cref{sec:point} with a uniform background grid of squares and grid size~$h \in \R_{>0}$.
    The outer Dirichlet boundary conditions on the curved boundary~$\partial\Omega$ are enforced weakly through the fictitious domain stabilized Nitsche method.
    As of $u \notin H^4(\Omega)$ the error estimates derived in \cref{sec:cutFEMerror} for the stabilized Nitsche method are technically not applicable.
    Still, in view of the typical approximation properties as mentioned in \cref{sec:point}, one expects at best that the $H^2$-norm of the discretization error converges with order~$1-\eps \approx 1$.
    Plots of the error norms~$\norm{D^i(u-u_h)}_{L^2(\Omega)}$ for $i \in \{0,1,2\}$ and $h = \frac{2}{N}$ with $N = \{16,\dots,512\}$ are shown in \cref{fig:convergencePointExact}.
    The error was computed using high order quadrature rules on the respective grid.

\begin{comment}
    Note that $u \in H^{3-\eps}$ and therefore the best we may expect in terms of $H^2$-convergence is order $1$.
    \TK{TODO: Prove that this is actually true? (So far, this simply is a conjecture of mine.)}
    
    We again approximate the solution numerically with the fictitious domain stabilized Nitsche method on a uniform background mesh over $[-1,1]^2$ for various grid sizes $h \in TODO$.
    The discretization errors are approximately evaluated by (\TK{what order?}) quadrature rules on the respective grids.

    \cref{fig:convergencePointExact} shows the errors in dependence of $h$.
    As expected, the convergence rate $1$ is observed for the $H^2$ error.
    Also the $H^1$ error behaves as expected with a rate of convergence of order $3$.
    The $L^2$ error is slower than this, which we cannot explain.
    Also it is surprising that in this example the outliers exhibit a significantly lower error than the average iterates.
    \TK{Todo: Investigate this further; implement the better algorithm for point constraints.}
\end{comment}

    \begin{figure}
      \begin{center}
        \incsvg{0.3\textheight}{convergence_point_exact_L2}\\
        \incsvg{0.3\textheight}{convergence_point_exact_H1}\\
        \incsvg{0.3\textheight}{convergence_point_exact_H2}\\
      \end{center}
      \caption{Double logarithmic plot of the $L^2$-, $H^1$- and $H^2$ discretization error (from top to bottom) over the range $h \in [1/256, 1/8]$ of grid sizes for the example problem in \cref{sec:exp3}.}
      \label{fig:convergencePointExact}
    \end{figure}
    
    The graph for the $H^2$-error is in excellent agreement with the drawn reference line, which supports the claim for a convergence rate of order~$1$.
    Similarly, the $H^1$-error and the $L^2$-error strictly follow their respective reference lines and so each suggest convergence rates of order~$2$.
    The quadratic convergence of the $H^1$-error is expected due to the linear $H^2$-error and the typically observed gain of one convergence order per order of differentiation less.
    In that sense it may seem surprising that the $L^2$-error only exhibits a quadratic convergence behavior as well.
    However, in view of the Aubin--Nitsche trick the convergence order of the $L^2$-error can at most be expected to be twice of the order of the $H^2$-error.
    Therefore, the limitation to quadratic convergence in the $L^2$-norm is explained by the reduced regularity of the solution.
    Interestingly, the plots show a fluctuating behavior, mostly depending on whether the $N$ in $h = 2/N$ is even or odd, which again decides whether the constraint point $x = 0$ is resolved by the grid exactly or not.
    Although such a behavior hints at a grid-dependence of the employed method, this does not impact the overall observed rate of convergence.

    In summary, the realistically expectable convergence speed is reduced in comparison to the curve constraint examples because of the non-optimal regularity properties of the continuous solution, and so the convergence properties of the discretization appear to be optimal with respect to the regularity of~$u$.
    But they are certainly non-optimal with respect to the given finite element space as the use of Bogner--Fox--Schmit elements is inefficient when no quadratic convergence in the $H^2$-norm is obtained.
    

  \subsection{Non-symmetric example}
    \label{sec:exp4}
    This example considers a more generic point value constraint problem for two point-value type particles whose solution is not radially symmetric and which admits a conforming discretization without the use of the fictitious domain stabilized Nitsche method.
    As usual, it uses the Monge-gauge parameterization with $\kappa = 1$ and $\sigma = 0$ over some square domain $\Omega := [-4,4]^2$.
    However, this time the outer boundary $\partial\Omega$ is to be equipped with periodic boundary conditions.
    For the particle model the reference point set
    \begin{align*}
      \cG_0 := \left\{
            \mat{ -0.35 \\ -0.35 \\  \phantom{-}0.3\phantom{0}},
            \mat{ \phantom{-}0.35 \\ -0.35 \\ -0.3\phantom{0}},
            \mat{  \phantom{-}0.35 \\  \phantom{-}0.35 \\  \phantom{-}0.3\phantom{0}},
            \mat{ -0.35 \\  \phantom{-}0.35 \\ -0.3\phantom{0}}
      \right\}
    \end{align*}
    is defined and each particle is equipped with six degrees of freedom to describe its position and rotation in space, \ie as in \cref{thm:variablePositionOrientation} and \cref{sec:exp2} one has
    \begin{align*}
      \Psi(p;x) := R_3(p_6)R_2(p_5)R_1(p_4) x + \mat{p_1 & p_2 & p_3}^T 
      \text{.}
    \end{align*}
    The concrete particle configuration chosen in this example is
    \begin{align*}
      \p :=
      \begin{pmatrix}
        2 & -2 & -0.2 & 0 & 0 & 30^\circ
        \\ -2 & 2 & \phantom{-}0.1 & 0 & 0 & 10^\circ
      \end{pmatrix}
      \text{.}
    \end{align*}
    Altogether, this results in the point-constraint problem
    \begin{align*}
      \Delta^2 u & = 0 \text{ on $\Omega$}
      ,\qquad u(x_i(\p)) = g_i(\p) \text{ for $i = 1,\dots, 8$}
    \end{align*}
    with periodic boundary conditions
    \begin{align*}
      u(-2,\cdot) & = u(2,\cdot), & u(\cdot,-2) & = u(\cdot,2)
      \\ \nabla u(-2,\cdot) & = \nabla u(2,\cdot), & \nabla u(\cdot,-2) & = \nabla u(\cdot,2)
      %~ ,\qquad u = \partial_\nu u = 0 \text{ on $\partial\Omega$}
    \end{align*}
    and parametric points $x_i(\p) \in \Omega$, $i = 1,\dots, 8$ and constraints~$g(\p) \in \R^8$.
    A discrete solution of this problem is depicted in \cref{fig:convergencePointInexactSolution}.
    
    \begin{figure}
      \begin{center}
        \includegraphics[width=0.5\textwidth]{pointInexact.eps}
      \end{center}
      \caption{Discrete solution of the example problem stated in \cref{sec:exp4}. The coloring encodes the function's elevation.}
      \label{fig:convergencePointInexactSolution}
    \end{figure}
    %~ \TK{Are periodic boundary conditions mentioned in the text?}
    
    A standard finite element discretization is applied which is made conforming by restriction to a subspace which fulfills the periodic boundary conditions on $\partial\Omega$ and the point value constraints over the $x_i(\p)$.
    As before, this space is constructed using the transformation technique described in \cref{sec:point}.
    
    It appears reasonable to expect that the point value constraints would again lead to singularities that behave like $r^2 \log(r)$ around the constraint points $x_i(\p)$.
    If that is indeed true, then the regularity $u \in H^{3-\eps}(\Omega)$ is to be expected again together with the convergence orders $2$, $2$ and $1$ for the errors in the $L^2$-, $H^1$- and $H^2$-norm, respectively.

    \begin{figure}
      \begin{center}
        \incsvg{0.3\textheight}{convergence_point_inexact2_L2}\\
        \incsvg{0.3\textheight}{convergence_point_inexact2_H1}\\
        \incsvg{0.3\textheight}{convergence_point_inexact2_H2}\\
      \end{center}
      \caption{Double logarithmic plot of the $L^2$-, $H^1$- and $H^2$ discretization error (from top to bottom) over the range $h \in [\frac{1}{32},\frac{1}{4}]$ of grid sizes for the example problem in \cref{sec:exp4}.}
      \label{fig:convergencePointInexact}
    \end{figure}
    
    The approximate error plots of the $\Vert D^i(u_H - u_h)\Vert_{L^2(\Omega)}$, $i \in \{0,1,2\}$ as obtained for the above mentioned discretization are depicted in \cref{fig:convergencePointInexact} for the grid sizes $h = \frac{4}{N}$, $N \in \{16,\dots,127\}$.
    Here, the fine grid approximation $u_H$ corresponds to the discrete solution obtained with $H = \frac{1}{32}$.
    The errors were computed by a high order quadrature method with respect to the grid associated to $u_H$.
    
    The error curves in the plots closely follow their corresponding reference lines and thus confirm the anticipated orders of convergence.
    This may be seen as an indicator for the claimed regularity $u \in H^{3-\eps}(\Omega)$ which then justifies an identical argumentation for the observed convergence rates as in \cref{sec:exp3}.
    Altogether, this again hints at an optimal convergence with respect to the regularity of the solution~$u$, but also to sub-optimal efficiency of the chosen discretization scheme.


\section{Gradient evaluation: Non-optimal error estimates}
  \subsection{Curve constraints}
    \label{sec:gradientExp2}
    This example numerically assesses the rate of convergence for the discrete approximation of the gradient of the interaction potential.
    To this end, the model in \cref{sec:exp2} for two ellipsoidal particles with free translation and rotation is re-used and again the configuration parameter is set to
    \begin{align*}
      \p := 
        \begin{pmatrix}
          -0.4\phantom{0} & \phantom{-}0.4\phantom{0} & 0.3 & \phantom{-}10^\circ & \phantom{-}15^\circ & 30^\circ
          \\ \phantom{-}0.25 & -0.35 & 0.3 & -10^\circ & -15^\circ & 60^\circ
        \end{pmatrix}
        \in \R^{2 \times 6}
      \text{.}
    \end{align*}
    Approximations $G_h$ of the gradient $\nabla \EI(\p)$ are obtained by \cref{alg:gradientEvaluation} using the same discrete approximations~$u_h$ of the membrane function as in \cref{sec:exp2}.
    As of the earlier observed quadratic convergence $\norm{u - u_h}_{H^2(\Omega(\p))} \in \mathcal{O}(h^2)$ for $h\to 0$ and the error estimate in \cref{thm:gradientErrorEstimate}, one expects also quadratic convergence in the gradient errors, \ie $\norm{\nabla \EI(\p) - G_h} \in \mathcal{O}(h^2)$ for $h\to 0$.
    
    In \cref{fig:gradientsCurveInexact} approximate gradient errors are shown for grid sizes $h = \frac{2}{N}$, $N \in \{16,\dots,256\}$.
    There the unknown gradient $\nabla \EI(\p)$ is substituted by the fine grid approximation~$G_H$ with $H = \frac{1}{128}$.
    On the left hand side, the approximate gradient errors $\norm{G_H - G_h}$ are shown, whereas the right hand side shows the errors $\Vert{G_H - \wt{G}_h}\Vert$ where the $\wt{G}_h$ stem from a central different quotient approximation of $\nabla\EI(\p)$.
    More precisely, the components of $\wt{G}_h$ are defined by
    \begin{align}
      \label{eq:centralDifQuotApproximation}
      \wt{G}_{h,i} := \frac{1}{2h} \left( \E(\p+ he_i; u_h(\p+he_i)) - \E(\p-he_i; u_h(\p-he_i)) \right)%, \quad i \in \{1,\dots,12\}
    \end{align}
    where the $e_i$ are the canonical Euclidean basis vectors and the $u_h(\p\pm t e_i)$ are the discrete solutions with respect to the perturbed configurations $\p \pm h e_i$.
    %~ By using standard estimates one easily verifies that the $\wt{G}_h$ approximate the actual gradient~$\nabla\EI(\p)$ quadratically with respect to~$h$, \ie $\Vert{\nabla \EI(\p) - \wt{G}_h}\Vert \in \mathcal{O}(h^2)$ holds for $h\to 0$.
    
    \begin{figure}
      \begin{center}
        \hfill
        \incsvg{0.45\textwidth}{gradients_curve_inexact}\hfill
        \incsvg{0.45\textwidth}{gradients_curve_inexact_2}\hfill
      \end{center}
      \caption{Double logarithmic error plots for the approximate gradient $G_H \approx \nabla\EI(\p)$ over grid sizes $h \in [\frac{1}{128}, \frac{1}{8}]$ with $H = \frac{1}{128}$ for the curve constraint problem considered in \cref{sec:gradientExp2} Left: Plot of $\norm{G_H - G_h}$. Right: Plot of $\Vert{G_H - \wt{G}_h}\Vert$ with difference quotient approximations $\wt{G}_h$ of $\nabla\EI(\p)$.}
      \label{fig:gradientsCurveInexact}
    \end{figure}
    
    Somewhat surprisingly, the errors~$\norm{G_H - G_h}$ actually indicate a quartic rate of convergence, which is twice what is predicted by the analytic results.
    Moreover, the quadratic convergence of the errors~$\Vert G_H - \wt{G}_h\Vert$ confirms that the $G_h$ actually converge towards the gradient~$\nabla\EI(\p)$.
    As in the previous examples, the fluctuations and some of the outliers in the error plots are likely to be explained by instabilities within the implementation of the level set method introduced in \cref{sec:levelSet}.
    The degeneration of the convergence rate for $h \to H$ is seen to be typical.
    Altogether, it may be concluded that the $G_h$ indeed converge at least quadratically towards the gradient~$\nabla\EI(\p)$.
    However, at this point it is unclear whether really fourth order convergence is obtained within the given approximation scheme.
    Investigation of this question requires a more refined error analysis in order to rule out discretization artifacts in the observations.
    It is emphasized that during the numerical computations indeed the $2$-norms of the gradients were calculated and not their squares.
  
  \subsection{Point value constraints}
    \label{sec:gradientExp4}
    
    This example considers again the non-symmetric point model from \cref{sec:exp4} and the configuration
    \begin{align*}
      \p :=
      \begin{pmatrix}
        2 & -2 & -0.2 & 0 & 0 & 30^\circ
        \\ -2 & 2 & \phantom{-}0.1 & 0 & 0 & 10^\circ
      \end{pmatrix}
      \in \R^{2\times 6}
      \text{.}
    \end{align*}
    Just as in the previous example, the gradient~$\nabla\EI(\p)$ is approximated by vectors~$G_h$ as obtained by \cref{alg:gradientEvaluation} using the discrete approximations~$u_h$ from \cref{sec:exp4}.
    This time, in view of the observation $\norm{u - u_h}_{H^2(\Omega)} \in \mathcal{O}(h)$ for $h\to 0$ and \cref{thm:gradientErrorEstimate}, it is justified to expect linear convergence $\norm{\nabla\EI(\p) - G_h} \in \mathcal{O}(h)$ for $h\to 0$.
    
    In \cref{fig:gradientsPointInexact} approximate gradient errors are shown for grid sizes $h = \frac{8}{N}$, $N \in \{16,\dots,256\}$ with $G_H \approx \nabla\EI(\p)$ and $H := \frac{1}{32}$.
    The plot on the left side shows the errors $\norm{G_H - G_h}$, and the right hand plot depicts the progress of $\Vert{G_H - \wt{G}_h}\Vert$ where the $\wt{G}_h$ are the central difference quotient approximations of the gradient~$\nabla\EI(\p)$ as defined in~\eqref{eq:centralDifQuotApproximation}.
    %~ Here, the convergence 
    
    \begin{figure}
      \begin{center}
        \hfill
        \incsvg{0.45\textwidth}{gradients_point_inexact}\hfill
        \incsvg{0.45\textwidth}{gradients_point_inexact_2}\hfill
      \end{center}
      \caption{Double logarithmic error plots for the approximate gradient $G_H \approx \nabla\EI(\p)$ over grid sizes $h \in [\frac{1}{32}, \frac{1}{2}]$ with $H = \frac{1}{32}$ for the point constraint problem considered in \cref{sec:gradientExp4} Left: Plot of $\norm{G_H - G_h}$. Right: Plot of $\Vert{G_H - \wt{G}_h}\Vert$ with difference quotient approximations $\wt{G}_h$ of $\nabla\EI(\p)$.}
      \label{fig:gradientsPointInexact}
    \end{figure}
    
    Like in \cref{sec:gradientExp2}, the convergence order observed for $\norm{G_H - G_h}$ is twice the expected magnitude, \ie quadratic in this case.
    The errors $\Vert G_H - \wt{G}_h\Vert$ again suggest quadratic convergence.
    Therefore, it appears justified to assume that the $G_h$ indeed converge quadratically towards the gradient~$\nabla\EI(\p)$.
    However, in view of the previous analysis this claim still remains to be proven, and hence only linear convergence is certain.
    Also for this experiment it is emphasized that indeed the $2$-norms of the gradients were computed and not their squares.
