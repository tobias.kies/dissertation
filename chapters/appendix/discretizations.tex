\label{sec:appendixDiscretizations}

%~ \subsubsection*{Overview of related discretization schemes}
  %~ In order to put the above discretization in the context of existing schemes, the remainder of this section briefly reviews the hierarchy of approximation schemes leading to the presented method.
  In order to put the fictitious domain stabilized Nitsche method as presented in \cref{sec:fictitiousNitscheDiscretization} in the context of simpler existing schemes, the following paragraphs briefly review a hierarchy of approximation schemes for linear elliptic PDEs that lead to this method.
  
  %~ \subsubsection{Standard Galerkin scheme}
  %~ \paragraph{Standard Galerkin scheme}
  \section*{Standard Galerkin scheme}
    A standard and arguably the most known approach for the approximation of elliptic PDEs is to construct a conforming discrete subspace $\cS' \subseteq \Uad$ (or to approximate such a space sufficiently well in the sense of Strang) and define the discrete approximation function via the discrete variational equation
    \begin{align*}
      \forall{v \in \cS'\colon\ } a_{\Omega_0}(\uh, v-\uh) = \ell_0(v-\uh)\text{,}
    \end{align*}
    which is equivalent to the solution of a linear equation.
    
    Within this framework, approximation estimates are readily derived, for example by virtue of the C\'ea-Lemma which implies the asymptotic order of convergence $k-1$ for $h\to 0$ if $u \in H^{k+1}(\Omega_0)$.
    Moreover, one also finds that the condition number of the discrete system behaves for fourth order problems like $\mathcal{O}(h^4)$ for $h\to 0$.
    
    The main disadvantage of this scheme in the given setting is that it cannot work with unfitted grids, which is however desirable in order to avoid re-meshing whenever the configuration parameter~$\p$ changes in the greater scheme.
    The reason behind this is the possibility that there could exist elements with vanishing relative measure~$\abs{E \cap \Omega_0} / \abs{E} \to 0$ for $h\to 0$, which in turn would lead to a blow-up in the condition number of the system matrix and would hence make this method numerically infeasible.
    Moreover, it may be relatively expensive to incorporate the boundary constraints (approximately) into the discrete space~$\cS'$ if the boundary is not resolved exactly.
  
    \begin{comment}
    If a discrete affine subspace $\cS'$ is known such that $\cS' \subseteq \Uad$, the solution $u^\ast$ can easily be approximated by solving the variational equation
    Within this setting approximation errors are readily obtained by virtue of the well-known C\'{e}a lemma and, given the approximation properties above, a rate of convergence of order $k-1$ is to be expected.
    Furthermore the condition number of the discrete system behaves as $\cO(h^4)$ for $h\to 0$.
    \snote{Similar to the above discretization for point value constraints.}
    A disadvantage is that it is difficult to construct the subspace $\cS'$, and within a finite element setting it usually will be impossible.
    However, in view of Strang's lemma, it generally is nonetheless still possible to approximate $\cS'$ sufficiently well in order to recover optimal approximation error rates.
    Another disadvantage lies within the greater picture: Because we need to solve many PDEs for different particle configurations, this approach requires us to construct many different finite element spaces $\cS'$, which requires fitting a mesh to the configuration-dependent domain $\Omega_0$.
    In our approach we would like to avoid this by choosing a configuration-independent background mesh.
    As a consequence the above standard Galerkin scheme is invalid because even if a feasible subspace $\cS'$ is constructed from $\cS$, due to the possibility that there exist elements with $\abs{E\cap \Omega_0}/\abs{E} \to 0$ for $h \to 0$, the condition number of the discrete system may become unstable, which makes the scheme numerically infeasible.
    \end{comment}
  
  %~ \subsubsection{Penalty formulation}
  %~ \paragraph{Penalty formulation}
  \section*{Penalty formulation}
    Penalty formulations avoid the construction of a conforming subspace $\cS'$ by incorporating the Dirichlet boundary conditions inexactly and implicitly via a penalization term in the overall energy.
    The augmented minimization problem reads
    \begin{align*}
      \min_{v \in \cS} \frac{1}{2} \Big( a_{\Omega_0}(v,v) + \aPen(v,v) \Big) - \ell_0(v) - \lPen(v)
    \end{align*}
    and is equivalent to the variational equation
    \begin{align*}
      \forall{v\in\cS\colon\ } a_{\Omega_0}(\uh,v) + \aPen(\uh,v) = \ell_0(v) + \lPen(v)
      \text{.}
    \end{align*}
    Within the setting of elliptic PDEs analogous and related penalty approaches and related ones have been studied for example in \cite{BraSch70}, \cite{Babuska73}, \cite{BarEll86}, \cite{GrKi17} both for fitted and unfitted grids.
    The main disadvantage of these methods generally is that they usually lead to sub-optimal rates of convergence due to a violation of the consistency property~\eqref{eq:consistency} or, just like the standard Galerkin scheme, they may suffer from a blow-up in the condition number if there are elements with vanishing relative measure~$\abs{E \cap \Omega_0} / \abs{E} \to 0$ for $h\to 0$.
  
    \begin{comment}
    In order to avoid explicit construction of (approximations of) subspaces of $\Uad$ that incorporate the Dirichlet conditions, one can impose those constraints weakly by augmenting the minimization problem to
    \begin{align*}
      \min_{v \in \cS} \frac{1}{2} \left( a_{\Omega_0}(v,v) + \aPen(v,v) \right) - \ell_0(v) - \lPen(v)
    \end{align*}
    or, equivalently,
    \begin{align*}
      \forall{v\in\cS\colon\ } a_{\Omega_0}(\uh,v) + \aPen(\uh,v) = \ell_0(v) + \lPen(v)
      \text{.}
    \end{align*}
    Such approaches in the setting of elliptic PDEs have been studied by \TK{[little review]}.
    The penalty term assures that $T\uh \to g$ as $h\to 0$, and it is readily proven that also $\uh\to u^\ast$ for $h\to 0$.
    However, a common drawback of penalty approaches are \TK{non-optimal rates of convergence}, even for matched grids.
    Moreover, the condition number of the discrete system may degenerate of the weighting in the penalty term is not chosen appropriately.
    \end{comment}
  
  %~ \subsubsection{Nitsche formulation}
  %~ \paragraph{Nitsche formulation}
  \section*{Nitsche formulation}
    A remedy for the problem of reduced rates of convergence in penalty formulations was found by Nitsche in~\cite{Nitsche71} for the Laplace problem with Dirichlet boundary conditions on matched grids.
    In that paper, an analogous term to $\aNit$ in \cref{sec:fictitiousNitscheDiscretization} is introduced which preserves the consistency~\eqref{eq:consistency} over the full unconstrained function space, and a discrete minimization problem analogous to
    \begin{align*}
      \min_{v\in \cS_\ast} \frac{1}{2} \wt{a}(v,v) - \wt{\ell}(v)
    \end{align*}
    with
    \begin{align*}
      \wt{a}(v,w) & = a_{\Omega_0}(v,w) + \aPen(v,w) + \aNit(v,w) + \aNit(w,v)
      \\ \wt{\ell}(v) & = \ell_0(v) + \lPen(v) + \aNit(v,u)
    \end{align*}
    is considered.
    
    While this modification preserves symmetry in $\wt{a}$, it in general breaks the ellipticity because $\aNit$ is indefinite.
    However, Nitsche shows for fitted grids that the bilinear form~$\wt{a}$ is positive definite at least on the discrete subspace $\cS_\ast$ if the $h$-independent penalty coefficient $\lambda$ is large enough, depending on the constant $c_{\text{inv}}$ from the inverse estimates as in \cref{asp:inverse}.
    Under these conditions optimal rates of convergence are also proven.
    
    The disadvantage that remains, however, is that this method is not suitable for the application to unfitted grids.
    This is because the constant $c_{\text{inv}}$ in the inverse estimates for a boundary segment~$\Gamma \cap E$ and a cut element~$E\cap \Omega_0$ is in general unbounded with respect to the grid size parameter~$h$ if the cut volume vanishes, \ie if $\abs{E \cap \Omega_0} / \abs{E} \to 0$ holds for $h\to 0$.
    Then also the penalty weighting parameter~$\lambda$ is $h$-dependent and unbounded, which leads to a deterioration of the condition number of the discrete system and numerical infeasibility of the method.
    
    \begin{comment}
    In his paper he observes that it is crucial that a discrete penalty problem
    \begin{align*}
      \min_{v\in \cS} \frac{1}{2} \wt{a}(v,v) - \wt{\ell}(v)
    \end{align*}
    is consistent in the sense that
    \begin{align*}
      \forall{v \in \cS\colon \ } \wt{a}(u^\ast,v) = \wt{\ell}(v)
      \text{.}
    \end{align*}
    This condition is generally not fulfilled for classical penalty formulations due to $\cS \not\subseteq \Uad$, but integration by parts reveals that consistency can be realized by augmenting the penalty formulation by the Nitsche terms, i.\,e., by considering a system of the type
    \begin{align*}
      \wt{a}(u,v) & = a_{\Omega_0}(u,v) + \aPen(u,v) + \aNit(u,v) + \aNit(v,u)
      \\ \wt{\ell}(v) & = \ell_0(v) + \lPen(v) + \aNit(v,u^\ast)\text{.}
    \end{align*}
    While this modification preserves symmetry of $\wt{a}$, it at first seems to be infeasible as of the indefiniteness of the Nitsche term.
    However, Nitsche shows for fitted grids that the bilinear form is positive definite on the subspace $\cS$ if the coefficient $\lambda$ in the penalty term is chosen to be large enough, where the threshold depends the constant from the inverse estimates.\snote{Link it?}
    In this setting he is able to prove optimal rates of convergence.
    Unfortunately, in the setting of unfitted grids, it turns out that $\lambda \to \infty$ as $\abs{E\cap \Omega_0} / \abs{E} \to 0$ for $h\to 0$, which makes the method numerically unstable.
    \end{comment}
    
  %~ \subsubsection{Stabilized Nitsche formulation}
  %~ \paragraph{Stabilized Nitsche formulation}
  \section*{Stabilized Nitsche formulation}
    A solution to the above mentioned stability problem for spurious elements is called the stabilized Nitsche formulation, or also the cut finite element method, and is presented in \cite{HaHa02} and \cite{BuHa12}.
    For the Laplace problem and linear finite elements it is shown there that the analogue of the above stabilized discretization scheme is able to preserve both optimal rates of convergence while also maintaining the original behavior of the condition number, independently of how the mesh~$\cT$ is located relative to the domain~$\Omega_0$.
    Since then various other works applied this type of discretization to a plethora of different problems and derived corresponding results.
    An extensive review of those is out of the scope of this thesis and therefore omitted.
    
    The key ingredient in these schemes usually is a stability property in the spirit of \cref{thm:jumpLemma} that allows the relation of $L^2$-norms of piecewise polynomials on neighboring elements to each other up to terms from a stabilization bilinear form~$\aStab$, which is defined on the discrete space $\cS$ only.
    With the help of this it is possible to circumvent the application of inverse inequalities with respect to restricted elements $E\cap\Omega_0$ as in the standard Nitsche method.
    Instead, the inverse inequalities are applied to the full elements $E$ and, if necessary, related to a fully contained interior element $E' \subseteq \Omega_0$ through said stability property.
    As a result, the $h$-independent penalty weight $\lambda$ may be chosen grid-independently such that the condition number remains stable and optimal convergence rates are achieved.
    %The following sections make this description more precise by stating the corresponding mathematical statements and carrying out the relevant proofs.
    
    %
    %~ Recently, in \cite{HaLaLa17} this type of method has also been applied to a fourth order problem in the context of linear elasticity equations

    %~ The following results stay in the same spirit.
    %~ The key ingredients for proving the desired properties is a combination of a stabilization and various elementary approximation inequalities.
    %~ Those estimates are shown in the upcoming sections.
