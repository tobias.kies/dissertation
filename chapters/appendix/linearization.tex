\label{sec:appendixLinearization}

This part is concerned with the computation of an expression for the linearized elastic energy $\ELin$ in the sense of \cref{sec:geometricLinearization} which does not rely explicitly on geometric quantities of the perturbed surfaces~$\cM(t)$, but instead only uses terms in the perturbation function~$u$ and the reference surface~$\cM_0$.
The following derivation of such a representation of $\ELin$ relies crucially on results for the analysis of surface PDEs and surface differentials in the sense of \cite{DzEl13}.
Moreover, the general approach is also based on ideas from \cite{Hobbs16} where first and second variations of the Willmore functional are derived with the help of such methods.
In the spirit of developing a widely applicable particle--membrane interaction framework, those results are now extended to more general energies which include spontaneous curvature and Gaussian curvature.
In addition, the analysis is carried out differently in order to avoid non-symmetric terms entering the expressions in case of a non-vanishing boundary~$\partial\cM_0$,
and for the sake of easier accessibility of the formulas the presentation is carried out on the basis of matrix calculus as far as possible.
Similar results for even more general energies but only for variations up to first order have been derived in \cite{DoNo12}.

%~ At this point it is worth emphasizing that while the parameterization $\varphi\colon\Omega\rightarrow\cM_0$ enters the analysis, the essential steps reveal that the final expressions are independent of the explicit choice of parameterization.

First, the necessary notation and some basic results are introduced.
To this end, let $\varphi\colon\Omega\rightarrow\cM_0$ a parameterization of $\cM_0$ as before and
let a map of $t$-dependent parameterizations of the perturbed surfaces~$\cM(t)$ be given by
\begin{align*}
  \phi\colon [0,1]\times \Omega & \longrightarrow \R^3
  \\ (t,x) & \longmapsto \varphi(x) + t\,  u(\varphi(x)) \, \nu_0(\varphi(x))\text{.}
\end{align*}
The space--time manifold of perturbed surfaces is defined by
\begin{equation*}
  \MT := \bigcup_{t\in [-1,1]} \{t\} \times \cM(t)\text{.}
\end{equation*}
In a slight abuse of notation, the maps $u$ and $\nu_0$ are extended to $\MT$ via the definition $u(t,x) := u(\varphi(\phi(t)^{-1}(x)))$ and $\nu_0(t,x) := \nu_0(\varphi(\phi(t)^{-1}(x)))$.

By $\nu\colon \MT \rightarrow \R^3$ the family of oriented unit normal vector fields on $\cM(t)$ is denoted which is continuous with respect to $t$ and which fulfills $\nu(0) = \nu_0$.
Note that generally $\nu_0(t) \neq \nu(t)$ holds for $t \neq 0$.

The \emph{metric tensor on $\cM(t)$} is given by $G(t) := D\phi(t)^TD\phi(t)$,
where here and in the following the letter $D$ always denotes the Jacobian with respect to the spatial variables only, \ie excluding the time-component.

For any function $f\colon \MT \rightarrow \R^k$, define the associated \emph{parameterized function of $f$} by $\wt{f}(t,x) := f(t,\phi(t,x))$ for all $(t,x) \in [-1,1]\times\Omega$.
The \emph{material derivative of $f$} is defined implicitly by
\begin{align}
  \label{eq:dmat}
  \dmat f \circ \phi(t) := \dd{t} \wt{f}
  \text{.}
\end{align}
The \emph{surface derivative of $f(t)$} is defined by
\begin{align}
  \label{eq:Dsurf}
  \Dsurf f(t) \circ \phi(t) := D\wt{f}(t)\, G(t)^{-1} D\phi(t)^T
\end{align}
and the \emph{surface gradient} is $\gsurf f(t) := (\Dsurf f(t))^T$.
For $k=1$ the \emph{surface Hessian of $f(t)$} is
\begin{align*}
  %~ \label{eq:D2surf}
  %~ \Dsurf^2 f(t) := (\Dsurf\gsurf f(t))^T\text{.}
  \Dsurf^2 f(t) := \gsurf\gsurf f(t)\text{.}
\end{align*}
%~ It can be shown that
Both the material derivatives and the surface derivatives are independent of the choice of the parameterization~$\varphi$.

The families $H(t)$ and $K(t)$ denote the mean and Gaussian curvature on $\cM(t)$, respectively, and $\kappa_1(t)$ and $\kappa_2(t)$ are the principal curvatures with principal directions $\mu_1(t)$ and $\mu_2(t)$, respectively.
In case $\kappa_1(t,x) = \kappa_2(t,x)$ holds for some $(t,x) \in \MT$, it is assumed that the $\mu_i$ are chosen such that the $\kappa_i(t)$ and $\mu_i(t)$ are differentiable almost-everywhere.
%~ Else differentiability is a consequence of the implicit function theorem. <- Sounds misleading

The \emph{extended Weingarten map on $\cM$} is defined by $\cH(t) := \Dsurf \nu(t)$.
The family $\cH(t)$ defines a field of pointwise symmetric matrices and it holds $\cH(t)\nu(t) = 0$ as well as $\cH(t)\mu_i(t) = \kappa_i(t) \mu_i(t)$.
In addition, a regularization of the extended Weingarten map $\hat{\cH}(t) := \cH(t) + \nu\otimes \nu$ is used, which fulfills $K = \det(\hat{\cH(t)})$.
Here, $a \otimes b := ab^T$ denotes the outer vector product.

In this context the identity (cf. \cite[Lemma 2.7]{DzEl13})
\begin{align}\label{eq:surfaceHessianSymmetry}
  \Dsurf^2 f(t) - \Dsurf^2 f(t)^T = \nu(t) \otimes (\cH\gsurf f(t)) - (\cH\gsurf f(t)) \otimes \nu(t)
\end{align}
is noteworthy.
Most importantly, it reveals that, unlike in the Euclidean case, the surface Hessian $\Dsurf^2 f(t)$ needs not be symmetric,
but $\Dsurf^2 f(t) - \nu(t) \otimes (\cH\gsurf f(t))$ is.
From this it is readily shown that $\Dsurf^2 f(t)$ induces a symmetric bilinear form on the tangent space of $\cM(t)$.

The parameter $t$ is dropped whenever the dependence and implied value are clear from the context.
Furthermore, for convenience and clarity of presentation the shortcut $H_0 := H(0)$ is defined.
This completes the preparations for the actual linearization of the elastic bending energy.


\subsection{Material derivatives of common quantities}
\begin{lemma}
  \label{thm:linearizationCommonQuantities}
  Let $\wt{u} := u \circ \phi$ and $\wt{\nu}_0 := \nu_0 \circ \phi$.
  It holds
  \begin{align}
    \dd{t}\phi & = \wt{u} \,\wt{\nu}_0 \label{eq:tphi}
    \\ \dd{t} D\phi & = \wt{\nu}_0 \otimes \nabla \wt{u} + \wt{u} D\wt{\nu}_0 \label{eq:tDphi}
    \\ \dd{t} G & = D\phi^T \left(\wt{\nu}_0 \otimes \nabla \wt{u} + \wt{u} D\wt{\nu}_0\right) 
                    + \left( \nabla\wt{u} \otimes \wt{\nu}_0 + \wt{u} D\wt{\nu}_0^T \right) D\phi\label{eq:tG}
    \\ \dd{t} G^{-1} & = -G^{-1} \left( \dd{t} G \right) G^{-1} \label{eq:tGinv}
  \end{align}
\end{lemma}

\begin{proof}
  The identities \eqref{eq:tphi}, \eqref{eq:tDphi} and \eqref{eq:tG} are immediately clear from the definition of the involved quantities.
  Equation \eqref{eq:tGinv} corresponds to the result from \cref{thm:matrixInverseDerivative} on differentiation of matrix inverses.
\end{proof}

\begin{lemma}
  \label{thm:dmatdsurf}
  Let
  \begin{align*}
    \cC & := \gsurf u \otimes (\nu\nu^T\nu_0) - \nu_0 \otimes \gsurf u - u\Dsurf \nu_0
    \\ \cC_0 & := \gsurfn u \otimes \nu_0 - u \cH_0
    \text{.}
  \end{align*}
  Then
  \begin{align*}
    \dmat \Dsurf{f} = \left(\Dsurf{f}\right)\cC + \Dsurf{\dmat{f}}
  \end{align*}
  and
  \begin{align*}
    \dmatn{\Dsurf{f}} = \left(\Dsurfn{f}\right)\cC_0 + \Dsurfn{\dmatn{f}}
  \end{align*}
\end{lemma}

\begin{proof}
  Application of \cref{thm:linearizationCommonQuantities} yields
  \begin{align}
    \label{eq:helper1803061022}
    \begin{aligned}
      D\wt{f} G^{-1} \dd{t} D\phi^T
      & = D\wt{f} G^{-1} \left( \nabla \wt{u} \otimes \wt{\nu}_0 + \wt{u} D\wt{\nu}_0^T \right)
      \\ & = D\wt{f}G^{-1} D\phi^T \left( D\phi G^{-1} \wt{u} \otimes \wt{\nu}_0 + \wt{u} D\phi G^{-1} D\wt{\nu}_0^T \right)
      \\ & = \left( \Dsurf{f} \left(\gsurf u \otimes \nu_0 + u (\Dsurf \nu_0)^T \right) \right) \circ \phi
      \text{.}
    \end{aligned}
  \end{align}
  The fact that $\Pi := D\phi G^{-1} D\phi^{-1}$ is an orthogonal projection onto the tangent space implies $D\phi G^{-1} D\phi^T \left( \Dsurf \nu_0 \circ \phi\right) = \Dsurf \nu_0 \circ \phi$.
  Therefore,
  \begin{align}
    \label{eq:helper1803061023}
    \begin{aligned}
      D\wt{f} \left(\dd{t} G^{-1}\right) D\phi^T
      & = -D\wt{f}G^{-1} D\phi^T \left(\wt{\nu}_0 \otimes \nabla \wt{u} + \wt{u} D\wt{\nu}_0\right)G^{-1} D\phi^T
      \\ & \,\quad - D\wt{f} G^{-1} D\phi^T\left( D\phi G^{-1}\nabla\wt{u} \otimes \wt{\nu}_0 + \wt{u} D\phi G^{-1}D\wt{\nu}_0^T \right) D\phi G^{-1}D\phi^T
      \\ & = - \left(\Dsurf f \left(\nu_0 \otimes \gsurf{u} + u \Dsurf{\nu_0} \right) \right) \circ \phi
      \\ & \,\quad - \left(\Dsurf f \left(\gsurf{u} \otimes (\Pi\nu_0) + u(\Dsurf{\nu_0})^T\right)\right) \circ \phi\text{.}
    \end{aligned}
  \end{align}
  Note that the orthogonal complement of the projection $\Pi$ fulfills $\nu_0 - \Pi \nu_{0} = \nu\nu^T\nu_0$.
  Altogether, it is concluded from the definitions in \eqref{eq:dmat} and \eqref{eq:Dsurf} and by combining \eqref{eq:helper1803061022} and \eqref{eq:helper1803061023} that
  \begin{align*}
    \dmat{\Dsurf{f}} \circ \phi
    & = \dd{t} \left( D\wt{f}\, G^{-1} D\phi^T \right)
    \\ & = D\left(\dd{t} \wt{f}\right) \, G^{-1} D\phi^T
            + D\wt{f}\, \dd{t} \left(G^{-1} D\phi\right)
    \\ & = \left( \Dsurf\dmat{f}
            + \left(\Dsurf{f}\right)\cC \right) \circ \phi
    \text{,}
  \end{align*}
  which proves the first equation.
  The second equation for $t = 0$ is an immediate consequence of $\left.\nu\nu^T \nu_0\right|_{t=0} = \nu_0$ and $\left.\left(\Dsurf{f}\right)\nu_0\right|_{t=0} = 0$.
\end{proof}


\begin{lemma}
  \label{thm:dmatunuhelper}
  It holds
  \begin{align*}
    \dmat{u}  & = 0
    & \dmat{\nu_0} & = 0
    \\ \dmat{\gsurf{u}} & = \cC^T\gsurf{u}
    & \dmat{\Dsurf{\nu_0}} & = \Dsurf{\nu_0}\,\cC
  \end{align*}
\end{lemma}

\begin{proof}
  This is a direct consequence of \cref{thm:dmatdsurf} and the definition of $u$ and $\nu_0$.
\end{proof}


\begin{lemma}\label{thm:linearizationDmats}
  It holds
  \begin{align}
    \label{eq:dmat_nu}  \dmat{\nu}  & = -\left(\gsurf{u}\otimes \nu_0 + u \Dsurf \nu_0\right)\nu
    %~ \\ \label{eq:dmat_nu2} \dmat^2{\nu} & = \left( \left( \gsurf{u}\otimes \nu_0 + u \Dsurf \nu_0\right)^2 - \left( \cC^T\gsurf{u} \otimes \nu_0 + u \Dsurf\nu_0\cC\right)\right) \nu 
    \\ \label{eq:dmat_nu2} \dmat^2{\nu} & = \left( \left( \gsurf{u}\otimes \nu_0 + u \Dsurf \nu_0\right)^2 - \cC^T\left( \gsurf{u} \otimes \nu_0 + u \Dsurf\nu_0^T\right)\right) \nu 
  \end{align}
  and
  \begin{align}
    \label{eq:dmat_nu0}  \left.\dmat{\nu}\right|_{t=0}  & = -\gsurfn{u}
    %~ \\ \label{eq:dmat_nu02} \left.\dmat^2{\nu}\right|_{t=0} & = u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0
    \\ \label{eq:dmat_nu02} \left.\dmat^2{\nu}\right|_{t=0} & = 2u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0
    \text{.}
  \end{align}
\end{lemma}

\begin{proof}
  Equation \eqref{eq:tDphi} implies
  \begin{align*}
    0
    & = \dd{t} \left( D\phi^T \wt{\nu} \right)
    = \left(\nabla\wt{u} \otimes \wt{\nu}_0 + \wt{u} (D\wt{\nu}_0)^T\right)\wt{\nu} + D\phi^T\dd{t}\wt{\nu}
  \end{align*}
  and therefore
  \begin{align}
    \label{eq:helper1803071218}
    D\phi^T\left(\dmat{\nu} \circ \phi\right) = -\left(\nabla\wt{u} \otimes \wt{\nu}_0 + \wt{u} (D\wt{\nu}_0)^T\right)\wt{\nu}
    \text{.}
  \end{align}
  In addition,
  \begin{align*}
    0 = \dmat (\nu^T\nu) = 2\nu^T\dmat\nu\text{,}
  \end{align*}
  from which it is inferred that $\dmat{\nu}$ is an element of the tangent space.
  Consequently, $\Pi\dmat{\nu} = \dmat{\nu}$ holds where $\Pi = D\phi G^{-1} D\phi^T$ is the orthogonal projection onto the tangent space.
  In combination with \eqref{eq:helper1803071218} equation \eqref{eq:dmat_nu} is obtained by
  \begin{align*}
    \dmat{\nu}\circ\phi
    & = D\phi G^{-1} D\phi^T\left(\dmat{\nu}\circ\phi\right)
    \\ & = -D\phi G^{-1}\left(\nabla\wt{u} \otimes \wt{\nu}_0 + \wt{u} (D\wt{\nu}_0)^T\right)\wt{\nu}
    \\ & = -\left( \left(\gsurf{u}\otimes \nu_0 + u (\Dsurf \nu_0)^T\right)\nu \right) \circ \phi
    \text{.}
  \end{align*}
  Application of \cref{thm:dmatdsurf} and \cref{thm:dmatunuhelper} yields \eqref{eq:dmat_nu2} by
  \begin{align*}
    \dmat \left( \dmat \nu \right)
    & = -\dmat \left( \gsurf{u}\otimes \nu_0 + u (\Dsurf \nu_0)^T \right) \nu - \left( \gsurf{u}\otimes \nu_0 + u (\Dsurf \nu_0)^T\right) \dmat\nu
    %~ \\ & = - \left( \cC^T\gsurf{u} \otimes \nu_0 + u (\Dsurf\nu_0)^T\cC\right) \nu +  \left( \gsurf{u}\otimes \nu_0 + u (\Dsurf \nu_0)^T\right)^2 \nu\text{.}
    \\ & = - \cC^T\left( \gsurf{u} \otimes \nu_0 + u (\Dsurf\nu_0)^T\right) \nu +  \left( \gsurf{u}\otimes \nu_0 + u (\Dsurf \nu_0)^T\right)^2 \nu\text{.}
  \end{align*}
  Equation \eqref{eq:dmat_nu0} is immediately apparent from $\nu|_{t=0}=\nu_0$ and $\Dsurfn{\nu_0}\cdot \nu_0 = 0$.
  Finally, for \eqref{eq:dmat_nu02} the symmetry of $\cH_0 = \Dsurfn \nu_0$ is used to conclude
  %~ \begin{align*}
   %~ \left.\dmat^2{\nu}\right|_{t=0} & = \left( \left( \gsurfn{u}\otimes \nu_0 + u \Dsurfn \nu_0\right)^2 - \left( \cC_0^T\gsurfn{u} \otimes \nu_0 + u \Dsurfn\nu_0\cC_0\right)\right) \nu_0 
   %~ \\ & = u\Dsurfn\nu_0\gsurfn{u} - \cC_0^T\gsurfn{u} - u \Dsurfn\nu_0\cC_0\nu_0
   %~ \\ & = u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0 + u\cH_0\gsurfn{u} - u \cH_0\gsurfn{u} + u^2 \cH_0^2\nu_0
   %~ \\ & = u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0
   %~ \text{.}
   %~ \qedhere
  %~ \end{align*}
  \begin{align*}
   \left.\dmat^2{\nu}\right|_{t=0} & = \left( \left( \gsurfn{u}\otimes \nu_0 + u \Dsurfn \nu_0\right)^2 - \cC_0^T \left( \gsurfn{u} \otimes \nu_0 + u \Dsurfn\nu_0\right)\right) \nu_0 
   \\ & = u\Dsurfn\nu_0\gsurfn{u} - \cC_0^T\gsurfn{u}
   \\ & = 2u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0
   \text{.}
   \qedhere
  \end{align*}
\end{proof}


\begin{lemma}
  \label{thm:dmatC}
  Let
  \begin{align*}
    \dot{\cC}_0 := u^2 \cH_0^2 -2u\cH_0\gsurfn{u}\otimes\nu_0 - \gsurfn{u}\otimes\gsurfn{u}
    \text{.}
  \end{align*}
  It holds
  %~ \begin{align*}
    %~ \dmat{\cC} = ...
  %~ \end{align*}
  %~ and
  \begin{align*}
    \Dsurfn{f} \, \dmat\cC|_{t=0} = \Dsurfn{f}\,\dot{\cC}_0
    \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  In view of \cref{thm:dmatunuhelper} and \cref{thm:linearizationDmats} holds $\dmat\left(\nu\nu^T\nu_0\right)|_{t=0} = -\gsurfn{u}$.
  Therefore,
  \begin{align*}
    \dmat\cC|_{t=0}
    & = \left.\dmat\left(\gsurf u \otimes (\nu\nu^T\nu_0) - \nu_0 \otimes \gsurf u - u\Dsurf \nu_0\right)\right|_{t=0}
    \\ & = \cC_0^T\gsurfn{u} \otimes \nu_0 - \gsurfn{u} \otimes \gsurfn{u} - \nu_0 \otimes (\cC_0^T\gsurfn{u}) - u \cH_0\cC_0
    \text{.}
  \end{align*}
  As of
  \begin{align*}
    \cC_0 = \gsurfn u \otimes \nu_0 - u \cH_0
  \end{align*}
  and $\Dsurfn{f}\nu_0 = 0$
  the identity
  \begin{align*}
    \Dsurfn{f}\,\dmat{\cC}|_{t=0}
    & = \Dsurfn{f} \left( -u\cH_0\gsurfn{u}\otimes\nu_0 - \gsurfn{u}\otimes\gsurfn{u} - u\cH_0\gsurfn{u}\otimes\nu_0 + u^2 \cH_0^2\right)
  \end{align*}
  is true and completes the proof.
\end{proof}


\begin{lemma}
  \label{thm:dmatdsurf2}
  It holds
  \begin{align*}
    \dmat^2 \Dsurf{f} = \Dsurf{f} \left(\cC^2 + \dmat{\cC}\right) + 2 \left(\Dsurf\dmat{f}\right) \cC + \Dsurf\dmat^2{f}
  \end{align*}
  and
  \begin{align*}
    \dmat^2 \Dsurf{f}|_{t=0}
    & = \Dsurfn{f}\left(\cC_0^2 + \dot{\cC_0}\right) + 2 \left(\Dsurfn\dmat f\right)\cC_0 + \Dsurfn{\dmat^2{f}}
  \end{align*}
  where
  \begin{align*}
    %~ \Dsurfn{f}\left(\cC_0^2 + \dot{\cC_0}\right)
    %~ & = \Dsurfn{f}\left(2u^2\cH_0^2 - 3u\cH_0\gsurfn{u}\otimes\nu_0 - \gsurfn{u}\otimes\gsurfn{u}\right)\text{.}
    \cC_0^2 + \dot{\cC_0}
    & = 2u^2\cH_0^2 - 3u\cH_0\gsurfn{u}\otimes\nu_0 - \gsurfn{u}\otimes\gsurfn{u}\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Repeated application of \cref{thm:dmatdsurf} yields
  \begin{align*}
    \dmat \left(\dmat \Dsurf{f}\right)
    & = \dmat \left( \Dsurf{f}\, \cC + \Dsurf\dmat{f} \right)
    \\ & = \left(\dmat\Dsurf{f}\right) \cC + \left(\Dsurf{f}\right) \dmat{\cC} + \left(\Dsurf\dmat{f}\right)\cC + \Dsurf \dmat^2{f}
    \\ & = \Dsurf{f} \left( \cC^2 + \dmat{\cC} \right) + 2\left(\Dsurf\dmat{f}\right) \cC + \Dsurf \dmat^2{f}\text{.}
  \end{align*}
  The remainder of the statement is quickly verified by invoking \cref{thm:dmatC}.
\end{proof}


\begin{lemma}
  \label{thm:dmatExtendedWeingarten}
  It holds
  \begin{align*}
    %~ \dmat{\Dsurf\nu}|_{t=0} = \cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T
    \dmat{\cH}|_{t=0} = \cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T
  \end{align*}
  and
  %~ \begin{align*}
    %~ \dmat^2{\cH}|_{t=0}
    %~ & = 2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0 + \cH_0\left(u(\Dsurfn^2{u})^T - \norm{\gsurfn{u}}^2 I\right)
          %~ \\ & \qquad + 2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + u\Dsurfn{\cH_0}\gsurfn{u}\text{.}
  %~ \end{align*}
  \begin{align*}
    \dmat^2{\cH}|_{t=0}
    & = 2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0
          \\ & \qquad + \cH_0\left(\gsurfn{u} \otimes \gsurfn{u} + 2u(\Dsurfn^2{u})^T - \norm{\gsurfn{u}}^2 I\right)
          \\ & \qquad + 2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + 2u\Dsurfn{\cH_0}\gsurfn{u}\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  From \cref{thm:dmatdsurf}, \cref{thm:linearizationDmats} and $\nu|_{t=0} = \nu_0$ the equation
  \begin{align*}
    \dmat{\Dsurf\nu}|_{t=0}
    & = \Dsurfn\nu_0 \, \cC_0 - \Dsurfn\gsurfn{u}
    \\ & = \cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T
  \end{align*}
  is obtained.
  Similarly, application of \cref{thm:dmatdsurf2} yields
  \begin{align*}
    \dmat^2{\Dsurf\nu}|_{t=0}
    %~ & = \cH_0\left(\cC_0^2 + \dot{\cC}_0 \right) - 2(\Dsurfn^2{u})^T\:\cC_0 + \Dsurfn \left(u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2\nu_0 \right)
    & = \cH_0\left(\cC_0^2 + \dot{\cC}_0 \right) - 2(\Dsurfn^2{u})^T\:\cC_0 + \Dsurfn \left(2u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2\nu_0 \right)
    \\ & = 2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0 - \cH_0\gsurfn{u}\otimes\gsurfn{u}
          \\ & \qquad + 2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right)
          %~ \\ & \qquad + \cH_0 \gsurfn{u} \otimes \gsurfn{u} + u\cH_0(\Dsurfn^2{u})^T + u\Dsurfn{\cH_0}\gsurfn{u} 
          \\ & \qquad + 2\cH_0 \gsurfn{u} \otimes \gsurfn{u} + 2u\cH_0(\Dsurfn^2{u})^T + 2u\Dsurfn{\cH_0}\gsurfn{u} 
          \\ & \qquad - 2 \gsurfn{u}^T(\Dsurfn^2{u})^T\,\nu_0 - \norm{\gsurfn{u}}^2\cH_0
  \end{align*}
  where $\Dsurfn\cH_0\gsurfn{u} = \sum_{k=1}^2 \Dsurfn^2\nu_{0,k} \gsurfn{u}_k$.
  The proof is completed by collecting terms and by making use of $(\Dsurfn^2{u})^T\,\nu_0 = 0$.
\end{proof}


\begin{lemma}
  \label{thm:dmatRegularizedExtendedWeingarten}
  It holds
  \begin{align*}
    \dmat{\hat{\cH}}|_{t=0} = \cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T - \gsurfn{u} \otimes \nu_0 - \nu_0 \otimes \gsurfn{u}
  \end{align*}
  and
  %~ \begin{align*}
    %~ \dmat^2{\hat\cH}|_{t=0}
    %~ & = 2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0 + \cH_0\left(u(\Dsurfn^2{u})^T - \norm{\gsurfn{u}}^2 I\right)
          %~ \\ & \qquad + 2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + u\Dsurfn{\cH_0}\gsurfn{u}
          %~ \\ & \qquad + \left(u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0\right) \otimes \nu_0 + \nu_0\otimes\left(u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0\right)
          %~ \\ & \qquad - 2 \gsurfn{u}\otimes\gsurfn{u}
          %~ \text{.}
  %~ \end{align*}
  \begin{align*}
    \dmat^2{\hat\cH}|_{t=0}
    & = 2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0
          \\ & \qquad + \cH_0\left(\gsurfn{u} \otimes \gsurfn{u} + 2u(\Dsurfn^2{u})^T - \norm{\gsurfn{u}}^2 I\right)
          \\ & \qquad + 2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + 2u\Dsurfn{\cH_0}\gsurfn{u}
          \\ & \qquad + \left(2u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0\right) \otimes \nu_0 + \nu_0\otimes\left(2u\cH_0\gsurfn{u} - \norm{\gsurfn{u}}^2 \nu_0\right)
          \\ & \qquad + 2 \gsurfn{u}\otimes\gsurfn{u}
          \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  The statement is a combination of \cref{thm:linearizationDmats} and \cref{thm:dmatExtendedWeingarten}.
\end{proof}


\begin{proposition}
  \label{thm:dmatH}
  \dmatH
\end{proposition}

%~ \snote{To-do for proof-reading: Consequently plug in $\Dsurfn^2{u}^T$ in the following proofs and statements (and check for other mistakes). ATTENTION: Maybe this should not be transposed.}

\begin{proof}
  The proof makes use of the identities
  \begin{align*}
    \Tr(a \otimes b) & = a^Tb, \qquad
    \Tr(AB^T) = A : B, \qquad
    A : A = \norm{A}_F^2
  \end{align*}
  for vectors $a,b$ and matrices $A,B$.
  From the properties of the extended Weingarten map the equality $H = \Tr(\cH)$ and the symmetry of $\cH$ are known.
  With the help of \cref{thm:dmatExtendedWeingarten} one concludes
  \begin{align*}
    \dmat{H}|_{t=0}
    & =  \Tr\left(\dmat\cH|_{t=0}\right)
    \\ & = \Tr\left(\cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - \Dsurfn^2{u}\right)
    \\ & = \nu_0^T\cH_0\gsurfn{u} - u\Tr(\cH_0^2) - \Tr(\Dsurfn^2{u})
    \\ & = -u\norm{\cH_0}_F^2 - \Deltasurfn{u}
  \end{align*}
  as well as
  %~ \begin{align*}
    %~ \dmat^2 H|_{t=0}
    %~ & = \Tr\left(\dmat^2\cH|_{t=0}\right)
    %~ \\ & = \Tr\left(2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0 + \cH_0\left(u\Dsurfn^2{u} - \norm{\gsurfn{u}}^2 I\right)\right)
          %~ \\ & \qquad + \Tr\left(2\Dsurfn^2{u}\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + u\Dsurfn{\cH_0}\gsurfn{u}\right)
    %~ \\ & = 2u^2\Tr(\cH_0^3) + u \cH_0 : \Dsurfn^2{u} - \norm{\gsurfn{u}}^2 \Tr(\cH_0)
          %~ \\ & \qquad + 2 u \cH_0 : \Dsurfn^2{u} + u \Tr(\Dsurfn\cH_0\gsurfn{u})
    %~ \text{.}
  %~ \end{align*}
  \begin{align*}
    \dmat^2 H|_{t=0}
    & = \Tr\left(\dmat^2\cH|_{t=0}\right)
    \\ & = \Tr\left(2u^2\cH_0^3 - 3u\cH_0^2\gsurfn{u}\otimes\nu_0\right)
          \\ & \qquad + \Tr\left(\cH_0\left(\gsurfn{u} \otimes \gsurfn{u} + 2u(\Dsurfn^2{u})^T - \norm{\gsurfn{u}}^2 I\right)\right)
          \\ & \qquad + \Tr\left(2(\Dsurfn^2{u})^T\left(u\cH_0 - \gsurfn{u}\otimes\nu_0\right) + 2u\Dsurfn{\cH_0}\gsurfn{u}\right)
    \\ & = 2u^2\Tr(\cH_0^3) + \gsurfn{u}^T\cH_0\gsurfn{u} + 2u \cH_0 : \Dsurfn^2{u} - \norm{\gsurfn{u}}^2 \Tr(\cH_0)
          \\ & \qquad + 2 u \cH_0 : \Dsurfn^2{u} - 2 \nu_0^T (\Dsurfn^2u)^T \gsurfn{u} + 2u \Tr(\Dsurfn\cH_0\gsurfn{u})
    \text{.}
  \end{align*}
  One further recognizes
  \begin{align*}
    \nu_0^T (\Dsurfn^2u)^T\gsurfn{u} = \gsurfn{u}^T \cH_0 \gsurfn{u}
  \end{align*}
  by virtue of \eqref{eq:surfaceHessianSymmetry} as well as
  \begin{align*}
    \Tr(\Dsurfn\cH_0\gsurfn{u})
    & = \sum_{i,k=1}^2 (\Dsurfn^2\nu_{0,k})_{ii} \gsurfn{u}_{k}
    = \sum_{k=1}^2 \Deltasurfn \nu_{0,k} \gsurfn{u}_{k}
    = \Deltasurfn \nu_0 \cdot \gsurfn{u}
    \text{.}
  \end{align*}
  Plugging these identities into the above formula completes the proof.
\end{proof}


\begin{comment}
\begin{lemma}
  \label{thm:dmatkappa}
  It holds
  \begin{align}\label{eq:dmatKappaFirst}
    \dmat\kappa_i|_{t=0} = - (\mu_i^0)^T \left( \Dsurfn^2{u} + u\cH_0^2 \right) \mu_i^0
    %~ \dmat\kappa_i|_{t=0} = (\mu_i^0)^T \left(\dmat{\cH}|_{t=0}\right) \mu_i^0
    %~ \dmat\kappa_i|_{t=0} = \mu_i^T \left(\cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T\right) \mu_i
  \end{align}
  and
  \begin{align}\label{eq:dmatKappaSecond}
    \dmat^2\kappa_i|_{t=0} = 2(\kappa_i^0-\kappa_j^0)^+\left((\mu_i^0)^T \left( \Dsurfn^2{u} + u \cH_0^2\right) \mu_j^0\right)^2 + (\mu_i^0)^T \left(\dmat^2{\cH}|_{t=0} \right) \mu_i^0
  \end{align}
  where $\dmat^2{\cH}|_{t=0}$ is as in \cref{thm:dmatExtendedWeingarten} and $(\,\cdot\,)^+$ denotes the Moore--Penrose pseudoinverse.
\end{lemma}

\begin{proof}
  %~ We extend the notation such that $\kappa_1(t), \kappa_2(t)$ denote the principal curvatures of $\cM(t)$ and $\mu_1(t)$ and $\mu_2(t)$ denote the respective principal directions.
  Let $i \in \{1,2\}$ be one index and let $j$ be the other index, \ie $j = 3-i$.
  We remind of $\cH\nu = 0$, $\cH\mu_i = \kappa_i \mu_i$ and $\cH\mu_j = \kappa_j \mu_j$.
  By \cref{thm:matrixEigenDerivative} on derivatives of eigenvalues and eigenvectors we have
  \begin{align*}
    \dmat{\kappa_i}
    & = \mu_i^T \left(\dmat\cH\right) \mu_i\text{,}
  \end{align*}
  which directly implies \eqref{eq:dmatKappaFirst} because application of \cref{thm:dmatExtendedWeingarten} and the symmetry property \eqref{eq:surfaceHessianSymmetry} yield
  \begin{align*}
    \left( \mu_i^T \left(\dmat\cH\right) \mu_i \right)|_{t=0}
    & = (\mu_i^0)^T \left( \cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T \right) \mu_i^0
    \\ & = - (\mu_i^0)^T \left( \Dsurfn^2{u} + u\cH_0^2 \right) \mu_i^0
    \text{.}
  \end{align*}
  With regard to the second equation, \eqref{eq:dmatKappaSecond}, consider that by \cref{thm:matrixEigenDerivative} also holds
  \begin{align*}
    \dmat{\mu_i}
    & = \left(\kappa_i \id - \cH\right)^+ \left(\dmat\cH\right)\mu_i
    \text{.}
  \end{align*}
  Knowing the eigenpairs of $\cH$ and the fact that $(\nu, \mu_i, \mu_j)$ forms an orthonormal basis of $\R^3$ gives us
  \begin{align*}
    \cH = \kappa_i \mu_i\otimes \mu_i + \kappa_j \mu_j\otimes\mu_j
    \text{.}
  \end{align*}
  Thus,
  \begin{align*}
     \left(\kappa_i \id - \cH\right)^+
     & = \left(\kappa_i \mu_i\otimes\mu_i + \kappa_i \mu_j\otimes\mu_j + \kappa_i\nu\otimes\nu - \cH\right)^+
     \\ & = \left( (\kappa_i-\kappa_j) \mu_j\otimes\mu_j + \kappa_i \nu\otimes\nu \right)^+
     \\ & = (\kappa_i-\kappa_j)^+ \mu_j\otimes\mu_j + \kappa_i^+ \nu\otimes\nu
  \end{align*}
  where we used the orthonormality of $(\nu, \mu_i, \mu_j)$ both in the first and in the last step.
  In particular,
  \begin{align}\label{eq:helper1804201246}
    \dmat{\mu_i} = \left((\kappa_i-\kappa_j)^+ \mu_j\otimes\mu_j + \kappa_i^+ \nu\otimes\nu\right) \left(\dmat\cH\right)\mu_i\text{.}
  \end{align}
  Applying the product rule to $\dmat\kappa_i$ now yields
  \begin{align*}
    \dmat^2\kappa_i
    & = 2\mu_i^T\left(\dmat\cH\right)\dmat\mu_i + \mu_i^T \left(\dmat^2\cH\right)\mu_i\text{.}
  \end{align*}
  With the help of \cref{thm:dmatExtendedWeingarten} we conclude, also using the symmetry property \eqref{eq:surfaceHessianSymmetry},
  \begin{align}\label{eq:helper1804201247}
    \begin{aligned}
      (\mu_i^0)^T\left(\dmat\cH|_{t=0}\right)\mu_j^0
      & = (\mu_i^0)^T\left(\cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T\right)\mu_j^0
      \\ & = - (\mu_i^0)^T \left( \Dsurfn^2{u} + u \cH_0^2\right) \mu_j^0
    \end{aligned}
  \end{align}
  and
  \begin{align}\label{eq:helper1804201248}
    \begin{aligned}
      (\mu_i^0)^T\left(\dmat\cH|_{t=0}\right)\nu_0
      & = (\mu_i^0)^T\left(\cH_0\gsurfn{u}\otimes\nu_0 - u\cH_0^2 - (\Dsurfn^2{u})^T\right)\nu_0
      \\ & = (\mu_i^0)^T\left(\nu_0 \otimes \cH_0\gsurfn{u} - u\cH_0^2 - \Dsurfn^2{u}\right)\nu_0
      \\ & = 0\text{.}
    \end{aligned}
  \end{align}
  %, identity \eqref{eq:surfaceHessianSymmetry} and the orthonormality properties we conclude
  Combining \eqref{eq:helper1804201246}, \eqref{eq:helper1804201247} and \eqref{eq:helper1804201248} implies by symmetry of $\cH$
  \begin{align*}
    \left( \mu_i^T\left(\dmat\cH\right)\dmat\mu_i\right)|_{t=0}
    & = (\mu_i^0)^T \left(\dmat\cH|_{t=0}\right) \left((\kappa_i^0-\kappa_j^0)^+ \mu_j^0\otimes\mu_j^0 + (\kappa_i^0)^+ \nu_0\otimes\nu_0\right) \left(\dmat\cH|_{t=0}\right) \mu_i^0
    \\ & = (\kappa_i^0-\kappa_j^0)^+\left((\mu_i^0)^T \left( \Dsurfn^2{u} + u \cH_0^2\right) \mu_j^0\right)^2\text{,}
  \end{align*}
  which completes the proof.
\end{proof}
\snote{One could try plugging in $\dmat^2\cH|_{t=0}$, which would probably lead to some terms dropping. However, this would make the proof even more lengthy.}


\begin{lemma}
  \label{thm:dmatK}
  Let $A := \Dsurfn^2{u} + u\cH_0^2$.
  It holds
  \begin{align*}
    \dmat{K}|_{t=0}
    & =
    -\kappa_2\,\mu_1^T A \mu_1
    -\kappa_1\,\mu_2^T A \mu_2 
  \end{align*}
  and
  \begin{align*}
    \dmat^2{K}|_{t=0}
    & = 2\kappa_2 \left( (\kappa_1^0-\kappa_2^0)^+\left(\mu_1^T A \mu_2\right)^2 + (\mu_1)^T \left(\dmat^2{\cH}|_{t=0} \right) \mu_1 \right)
    \\ & \qquad + 2\left(\mu_1^T A \mu_1\right)\left( \mu_2^T A \mu_2\right)
    \\ & \qquad + 2\kappa_1 \left( (\kappa_2^0-\kappa_1^0)^+\left(\mu_2^T A \mu_1\right)^2 + (\mu_2)^T \left(\dmat^2{\cH}|_{t=0} \right) \mu_2 \right)
    \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  This is a direct consequence of $K = \kappa_1\kappa_2$, the product rule and \cref{thm:dmatkappa}.
\end{proof}

\snote{Have to justify why we do not use the $\kappa_i$ derivatives for $\dmat H$ as well.}
\end{comment}

\begin{proposition}
  \label{thm:dmatK}
  \dmatK
\end{proposition}

\begin{proof}
  This is a direct consequence of Jacobi's formula in \cref{thm:matrixDeterminantDerivative}, the product rule, and \cref{thm:matrixInverseDerivative} for the differentiation of inverse matrices.
\end{proof}

%~ \snote{The inverse of $\hat\cH$ can be written down explicitly, but then it would be necessary to know the (appropriate?) principal directions.}

\subsection{Derivatives of integrals over geometric quantities}  
In this section integration of a hypersurface $\cM(t)$ is always taken with respect to the corresponding Hausdorff-measure $\mathbb{H}(\cM(t))$.
In the following statements this is only indicated by the shorthand $\dH$ for ease of notation.

\begin{theorem}[Transport formula]
  %~ \snote{Is this result true for general surfaces or does is it restricted to closed surfaces?}
  \label{thm:transportFormula}
  Let $V$ such that $\dd{t} \phi = V \circ \phi$.
  %~ \snote{Here: The normal velocity is independent of the parameterization, but what about the influence of tangential parts? (Are there any?) -- Actually, it appears that we exclusively look at tangential parts.}
  It holds
  \begin{align*}
    %~ \label{eq:transport1}
    \dd{t} \int_{\cM(t)} f \d{\mathbb{H}}
    & = \int_{\cM(t)} \dmat{f} + f \divsurf{V} \d{\mathbb{H}}
  \end{align*}
  and
  \begin{align*}
    %~ \label{eq:transport2}
    \frac{\partial^2}{\partial t^2} \int_{\cM(t)} f \dH
    & = \int_{\cM(t)} \dmat^2{f} + 2 \dmat{f} \divsurf{V} + f \left( \left(\divsurf{V}\right)^2 + \dmat\divsurf{V} \right) \dH\text{.}
  \end{align*}
\end{theorem}

\begin{proof}
  The proof for the first equation is found in \cite[Theorem 5.1]{DzEl13}.
  The second equation follows directly by repeated application of the first.
\end{proof}


\begin{lemma}
  \label{thm:dmatV}
  Let $V$ such that $\dd{t}\phi = V\circ\phi$.
  Then
  \begin{align*}
    \divsurfn{V} = u H_0
  \end{align*}
  and
  \begin{align*}
    \dmat\divsurf{V}|_{t=0} = \norm{\gsurfn{u}}^2 - u^2 \norm{\cH_0}_F^2
  \end{align*}
\end{lemma}

\begin{proof}
  The definition of $V$ and \cref{thm:linearizationCommonQuantities} give for $\wt{V} := V \circ \phi$
  \begin{align*}
    \Dsurf V \circ \phi
    & = D\wt{V} G^{-1} D\phi^T
    = \left(\dd{t} D\phi\right) G^{-1}D\phi^T
    = \left(\nu_0 \otimes \gsurf{u} + u \Dsurf{\nu_0}\right) \circ \phi
    \text{.}
  \end{align*}
  Consequently,
  \begin{align*}
    \divsurfn V
    & = \Tr(\Dsurfn V)
    = \Tr(\nu_0\otimes\gsurfn{u} + u\cH_0)
    = \gsurfn{u}^T\nu_0 + u\Tr(\cH_0)
    = u H_0
    \text{.}
  \end{align*}
  As of
  \begin{align*}
    \dmat V \circ \phi = \dd{t} \wt{V} = \frac{\partial^2}{\partial t^2} \phi = \dd{t} (\wt{u}\wt{\nu}_0) = 0
  \end{align*}
  \cref{thm:dmatdsurf} yields
  \begin{align*}
    \dmat\divsurf{V}|_{t=0}
    & = \Tr\left( \dmat\Dsurf V|_{t=0}\right)
    \\& = \Tr\left( \Dsurfn V \, \cC_0 \right)
    \\& = \Tr\left( \left(\nu_0 \otimes \gsurfn{u} + u \cH_0\right) \left(\gsurfn{u} \otimes \nu_0 - u \cH_0\right) \right)
    \\& = \Tr\left( \norm{\gsurfn{u}}^2 \nu_0\otimes\nu_0 + u\cH_0\gsurfn{u}\otimes\nu_0 - \nu_0 \otimes(\cH_0 \gsurfn{u}) - u^2\cH_0^2 \right)
    \\& = \norm{\gsurfn{u}}^2 - u^2 \norm{\cH_0}_F^2
    \text{.}
    \qedhere
  \end{align*}
\end{proof}


\begin{theorem}
  \label{thm:derivativeInt1}
  \derivativeIntC
\end{theorem}

\begin{proof}
  The first equation is a direct consequence of $\dmat 1 = \dmat^2 1 = 0$, $\Dsurfn 1 = 0$, \cref{thm:transportFormula} and \cref{thm:dmatV}.
  Similarly, one also obtains for the second equation
  \begin{align*}
    \dtfn{\int_{\cM(t)} 1 \dH}{2} = \int_{\cM_0} u^2 H_0^2 + \norm{\gsurfn{u}}^2 - u^2 \norm{\cH_0}_F^2 \dH\text{.}
  \end{align*}
  Since the Weingarten map $\cH_0$ has the eigenvalues $0$, $\kappa_1$ and $\kappa_2$ with corresponding eigenvalues $\nu_0$, $\mu_1$, $\mu_2$, respectively, the matrix $\cH_0^2$ has the eigenvalues $0$, $\kappa_1^2$ and $\kappa_2^2$.
  Together with the symmetry of $\cH_0$ this implies
  \begin{align*}
    \norm{\cH_0}_F^2 = \cH_0 : \cH_0 = \Tr(\cH_0^2) = \kappa_1^2 + \kappa_2^2
    \text{.}
  \end{align*}
  As of $H_0 = \kappa_1 + \kappa_2$ one further has
  \begin{align*}
    H_0^2 - \norm{\cH_0}_F^2 = (\kappa_1 + \kappa_2)^2 - (\kappa_1^2 + \kappa_2^2) = 2\kappa_1\kappa_2 = 2K_0
    \text{.}
  \end{align*}
  Plugging this into the above identity completes the proof.
\end{proof}


\begin{theorem}
  \label{thm:derivativeIntH}
  \derivativeIntH
\end{theorem}

\begin{proof}
  Those equations are a direct consequence of \cref{thm:dmatH}, \cref{thm:transportFormula} and \cref{thm:dmatV}.
\end{proof}


\begin{theorem}
  %~ \snote{Not sure if or under what conditions this result is really true. To-do: Check this again!}
  \label{thm:derivativeIntK}
  \derivativeIntK
\end{theorem}

\begin{proof}
  Those equations are a direct consequence of \cref{thm:dmatK}, \cref{thm:transportFormula} and \cref{thm:dmatV}.
\end{proof}
