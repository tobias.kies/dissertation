\label{sec:appendixRepulsion}

\newcommand{\ELJ}{E_{\text{LJ}}}
\newcommand{\cELJ}{\cE_{\text{LJ}}}

As mentioned in \cref{sec:appFCHo2}, a particle interaction potential for FCHo2~F-BAR~domains that is purely driven by membrane elasticity is infeasible within a point value constraint model due to the possibility of overlapping.
One way of solving this issue is by augmenting said interaction potential by a repulsive direct particle--particle interaction.
The idea in this section to introduce such a particle--particle interaction potential based on pairwise atomic forces.

To this end, suppose two FCHo2 particles as well as a particle transformation function~$\Psi\colon \R^4 \times \R^3 \rightarrow \R^3$ with some particle configuration~$\p \in \R^{2\times 4}$ as introduced in \cref{sec:appFCHo2}.
The reference particle has $N := 4478$ reference atoms~$\wt{a}_i \in \R^3$, $i \in \{1,\dots,N\}$,
and correspondingly the transformed atom positions are $\wt{a}^k_i := \Psi(\p_k;a_i)$ for $k\in \{1,2\}$ and $i \in \{1,\dots,N\}$.
For reasons of simplicity and in view of the particle model in \cref{sec:appFCHo2}, the following paragraphs only consider the projections $a_i := (\wt{a}_{i,1}, \wt{a}_{i,2}) \in \R^2$ and $a^k_i := (\wt{a}^k_{i,1}, \wt{a}^k_{i,2}) \in \R^2$ of these atoms onto the Euclidean plane.
However, generalizations of the subsequent methods to the original three-dimensional space are readily available.

The \emph{Lennard--Jones potential} \cite{Jones1924} is used to describe the interaction of two atoms at a given distance $r \in \R_{>0}$.
It is defined by
\begin{align*}
  \ELJ^0(r) := \frac{A}{r^{12}} - \frac{B}{r^6}
\end{align*}
where $A,B \in \R_{>0}$ are constants depending on the type of the atoms.
%~ \TK{Choices of parameters.}
Here,
the $A$-term is a heuristic description of the repulsion of electrons at short ranges,
and
the $B$-term corresponds to the long-range attractive van der Waals force.
As attention is restricted to repulsive interactions only, the $B$-term is neglected for the remainder of this section by defining $B:=0$.

\begin{figure}
  \begin{center}
    \includegraphics[width=1.0\textwidth,trim={3cm 0cm 0cm 0cm},clip]{fcho2_lj_all.eps}
  \end{center}
  \caption{Level set plots of various functions.
  From top to bottom:
  a) Lennard--Jones interaction potential~$\ELJ$ (logarithmic).
  b) Dirac approximation~$g_\eps$ of the delta distributions used in the Lennard--Jones interaction potential.
  c) Low-dimensional approximation $\wt{g}_\eps$ of $g_\eps$.
  d) Kernel used in the definition of the final particle--particle interaction potential~$\EI_{\text{particle}}$.}
  \label{fig:repulsions}
\end{figure}

Based on the Lennard--Jones potential, a repulsive interaction potential between the two given FCHo2~particles is therefore defined by
\begin{align*}
  \cELJ := \sum_{i=1}^N \sum_{j=1}^N \ELJ^0(\norm{a^1_i - a^2_j})
  \text{.}
\end{align*}
While being physically justified, this potential is not necessarily suitable for being used as a particle--particle repulsion in conjunction with an elasticity-driven membrane-mediated interaction.
The reason behind this is of numerical nature and is explained with the help of \cref{fig:repulsions}~a) where a level set view of the function
\begin{align*}
  \ELJ\colon \R^2 \longrightarrow \R, \qquad x \longmapsto \sum_{i=1}^n \ELJ^0\left(\norm{x - a_i}\right)
\end{align*}
is plotted over the domain~$[-110,110]\times [-40,40]$.
There the constant $A$ in $\ELJ^0$ is set to $A := 28\cdot 3.5^{12}$, which is roughly in the range of physically relevant parameter choices but not justified here any further.
Within the plot the outer green curve emphasizes the level set~$\{\ELJ = 10^{-3}\}$, and the inner green curve corresponds to the level set~$\{\ELJ = 10\}$.
It is observed that $\ELJ$ exhibits a highly singular structure with geometrically complicated level sets.
Considering that the elastic membrane interaction energy in practical applications usually spans only one order of magnitude, these vastly different behaviors in contrast imply a considerable challenge for a gradient-type descent methods.
This is because once two particles come close enough to each other, the gradient of the Lennard--Jones interaction potential would disproportionately dominate the elastic membrane interaction and either lead to a particle configuration which is largely unrelated to the previous one or restrict the line search to step lengths which would effectively make the gradient method come to a halt.
Ideally, an approximation of the Lennard--Jones interaction potential is wanted whose gradients are of lower or comparable order to those originating from the elastic membrane interaction and which nonetheless leads to a similar effective system behavior.

Using a formal calculation and two-dimensional delta-distributions~$\delta^k_i := \delta(\cdot - a^k_i)$, the Lennard--Jones interaction may be rewritten as
\begin{align*}
  \cELJ & = \sum_{i=1}^N \sum_{j=1}^N \ELJ^0(\norm{a^1_i - a^2_j})
    \\ & = \sum_{i=1}^N \sum_{j=1}^N \int_{\R^2 \times \R^2} \ELJ^0(\norm{x-y})\, \delta_i^1(x)\, \delta_j^2(y) \d{(x,y)}
    \\ & = \int_{\R^2 \times \R^2} \ELJ^0\left(\norm{\Psi(\p_1;x)-\Psi(\p_2;y)}\right) \, g(x) \, g(y) \d{(x,y)}
\end{align*}
where $g := \sum_{i=1}^N \delta_i$ with $\delta_i := \delta(\cdot - a_i)$.
Note how this step implicitly uses the fact that the chosen transformation function~$\Psi$ is orthonormal and compatible with the projection of the atoms onto the Euclidean plane.
In more general cases the integration would necessarily need to be carried out over the full space $\R^3 \times \R^3$.

Define furthermore a $C^1$-smooth two-dimensional Dirac sequence by $\phi_\eps(x) := \frac{1}{\eps^2} \phi(\norm{x}/\eps)$ for $\eps \in \R_{>0}$ where
\begin{align*}
  \phi(r) :=
  \begin{cases}
    \frac{3}{\pi} (r^2-1)^2 & \text{ for $r \leq 1$,}
    \\ 0 & \text{ for $r > 1$.}
  \end{cases}
\end{align*}
The sum of delta distributions~$g$ is then further approximated by
\begin{align*}
  g_{\eps}(x) := \sum_{i=1}^N \phi_\eps(x - a_i)
  \text{.}
\end{align*}
A level set view plot of $g_\eps$ for $\eps=10$ is shown in \cref{fig:repulsions}~b).
There it becomes apparent that also this approximation is not numerically feasible yet due to the presence of still relatively large fluctuations in $g_\eps$ and because of the large number $N$ of summands, which would imply a considerable computational expense when carrying out a numerical quadrature scheme using $g_\eps$ in the integral above.
Therefore, this function is further approximated by the sparse representation
\begin{align*}
  \wt{g}_\eps(x) := \frac{N}{\abs{I}} \sum_{i \in I} \phi_\eps(x - a_i)
\end{align*}
where $I \subseteq \{1,\dots,N\}$.
In \cref{fig:repulsions}~c) the level set view of $\wt{g}_\eps$ resulting from $\eps := 30$ and
\begin{align*}
  I := \left \{ 1+100k \mid k \in \N,\, 1+100k \leq N \right\}
\end{align*}
is depicted and shows a significantly smoother structure.
Of course, at this step also any other low-dimensional approximation of $g_\eps$ might be considered as well and the given choice is merely motivated by its simplicity.

Finally, the relatively singular behavior of $\ELJ^0$ is softened by transitioning to the kernel $K(r) := (100+r^2)^{-1}$ instead.
Altogether, given $n \in \N$ particles and a particle configuration~$\p \in \R^{n\times 4}$, a numerically feasible repulsive particle--particle interaction potential is defined by
\begin{align*}
  \EI_{\text{particle}}(\p)
    := \frac{1}{2} \sum_{i,j=1}^n \int_{\R^2 \times \R^2} K(\norm{\Psi(\p_i;x)-\Psi(\p_j;y)}) \, \wt{g}_\eps(x) \, \wt{g}_\eps(y) \d{(x,y)}\text{.} 
\end{align*}
This is the interaction chosen for the application example in \cref{sec:appFCHo2}.
It is emphasized that the integration in $\EI_{\text{particle}}$ may be carried out approximatively using standard quadrature schemes as of the local support of~$\wt{g}_\eps$.

The choice of $K$ is motivated both by the desire to have an upper bound on the gradient in the interaction potential and to admit as coarse a quadrature scheme as reasonably possible.
In \cref{fig:repulsions}~d) a plot of the function $x \mapsto \int_{\R^2} K(\norm{x-y}) \wt{g}_\eps(y) \d{x}$ is shown.
It illustrates how the the integrand in $\EI_{\text{particle}}$ is smooth enough to avoid any disproportional domination of the elastic membrane interaction which could lead to numerical instabilities while still being able to penalize particle overlapping sufficiently strongly and decaying reasonably fast to zero in a neighborhood of the particle.
This makes the repulsive interaction above not only a numerically feasible but also a potentially physically plausible choice.
