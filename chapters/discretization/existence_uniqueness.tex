Before it is possible to prove existence and uniqueness of the discrete approximation $\uh$ within the proposed scheme, it is necessary to show some technical results first.

For the sake of a more concise notation later on, define the \emph{triple-norm on $\cS_\ast$} by
\begin{align*}
  \vertiii{v} := \norm{v}_{\aOmega + \aPen + \aStab} = \sqrt{\aOmega(v,v) + \aPen(v,v) + \aStab(v,v)}\text{.}
\end{align*}
Furthermore, let the \emph{trace norm on $H^2(\Omega_0)$} be defined by
\begin{align*}
  \norm{v}_\Gamma := \sqrt{\sum_{i=0}^1 \norm{T_i v}^2_{L^2(\Gamma)}}
\end{align*}
and define the \emph{weighted extended trace norm} by
\begin{align*}
  \norm{v}_{\Gamma,h} := \sqrt{\sum_{i=0}^1 \norm{h^{i-3/2}\wt{T}_i v}^2_{L^2(\Gamma)}}
  \text{.}
\end{align*}

One very crucial component in the upcoming proofs is an estimate for piecewise polynomials that relates the $L^2$-norms on adjacent elements to each other via the stabilization jump terms along their joint interface.
The following result is an adaption of \cite[Lemma 5.1]{Massing14} to the special case at hand.
\begin{lemma}
  \label{thm:jumpLemma}
  %~ \snote{This lemma uses quadratic elements. Should we introduce this to the above setting or rather generalize this lemma?}
  Suppose $k \in \N$, $\sigma \in \R_{\geq 1}$ and $h, h_1, h_2 \in \R_{>0}$ with $\frac{h_1}{h_2} \leq \sigma$.
  Let $E_1 := [-h_1,0] \times [0,h]$ and $E_2 := [0,h_2] \times [0,h]$ be two connected elements with the joint face $F := \{0\} \times [0,h]$.
  %~ Let $k \in \N$, $h \in \R_{>0}$ and $E_1 := [-h,0] \times [0,h]$, $E_2 := [0,h] \times [0,h]$, $F := \{0\} \times [0,h]$.
  There exists an $h$- and $\sigma$-independent constant $c \in \R_{>0}$ such that for all $v\colon E_1\cup E_2 \rightarrow \R$ with $v|_{E_i} \in \cQ_k$ holds
  \begin{align*}
    \norm{v}_{L^2(E_1)}^2 \leq c \sigma \left( \norm{v}_{L^2(E_2)}^2 + \sum_{j = 0}^k \frac{h^{2j+1}}{(j!)^2 (2j+1)} \norm{\jmp{\partial_\nu^j v}}_{L^2(F)}^2 \right) \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Let
  \begin{align*}
    \pi\colon E_1\cup E_2 & \longrightarrow F
    ,\qquad x \longmapsto \mat{0 \\ x_2}
  \end{align*}
  the orthogonal projection from $E_1 \cup E_2$ onto the joint face $F$.
  Furthermore, denote by $v_i\colon \R^2 \rightarrow \R \in \cQ_k$ the canonical extension of the restriction $v|_{E_i}$ to $\R^2$.
  Given $x \in E_1$, a Taylor expansion of $v_i(x)$ in $\pi(x)$ leads to
  \begin{align*}
    v_i(x) & = \sum_{j=0}^\infty \frac{1}{j!} \left.\frac{\partial^j}{\partial t^j} v_i\left(\pi(x) + t(x-\pi(x))\right)\right|_{t=0}
    %\\ &
    = \sum_{j=0}^k \frac{x_1^j}{j!} v_i(\pi(x))
    \text{.}
  \end{align*}
  This implies
  \begin{align*}
    v_1(x) = v_2(x) + \sum_{j=0}^k \frac{x_1^j}{j!} \left( \partial_\nu^j v_1(\pi(x)) - \partial_\nu^j v_2(\pi(x)) \right)
  \end{align*}
  and squaring both sides and applying Cauchy--Schwarz inequality yields
  \begin{align*}
    v_1(x)^2 \leq (k+2) \left( v_2(x)^2 + \sum_{j=0}^k \frac{x_1^{2j}}{(j!)^2} \jmp{\partial_\nu^j v(\pi(x))}^2 \right)
    \text{.}
  \end{align*}
  Now, integration of both sides over $E_1$ leads to
  \begin{align*}
    \frac{1}{k+2} \int_{E_1} v_1(x)^2 \d{x}
    & \leq \int_{E_1} v_2(x)^2 \d{x} + \sum_{j=0}^k \int_{E_1} \frac{x_1^{2j}}{(j!)^2} \jmp{\partial_\nu^j v(\pi(x))}^2 \d{x}
    \\ & = \int_{E_1} v_2(x)^2 \d{x} + \sum_{j=0}^k \frac{h^{2j+1}}{(j!)^2 (2j+1)} \int_F \jmp{\partial_\nu^j v(\pi(x))}^2 \d{x}
    \text{.}
  \end{align*}
  Let $T_1 := [-1,0] \times [0,1]$ and $T_2 := [0,1] \times [0,1]$ be reference elements associated to $E_1$ and $E_2$, respectively. 
  As of the finite-dimensionality of $\cQ_k$, the norms $\norm{\cdot}_{L^2(T_1)}$ and $\norm{\cdot}_{L^2(T_2)}$ are equivalent over $\cQ_k$, and in particular there exists a constant $\hat{c} \in \R_{\geq 1}$ such that $\norm{w}_{L^2(T_1)}^2 \leq \hat{c} \norm{w}_{L^2(T_2)}^2$ holds true for all $w\in\cQ_k$.
  By applying this to the transformed function $\hat{v}_2(x_1,x_2) := v_2(h_1x_1,hx_2) \in \cQ_k$ one derives the estimate
  \begin{align*}
    \int_{E_1} v_2(x)^2 \d{x}
    & = h_1 h \int_{T_1} v_2(h_1x_1, h_2x_2)^2 \d{x}
    = h_1 h \norm{\hat{v}_2}_{L^2(T_1)}^2
    \\ & \leq \hat{c} h_1 h \norm{\hat{v}_2}_{L^2(T_2)}^2
    = \hat{c} \sigma \int_{E_2} v_2(x)^2\d{x}
    %~ = c_0 \norm{v}_{L^2(E_2)}^2
    \text{.} 
  \end{align*}
  Combination of this inequality with the previous one gives the desired statement with the $h$- and $\sigma$-independent constant $c = (k+2) \hat{c}$.
\end{proof}

\begin{lemma}
  \label{thm:aStabilization}
  Let for all $v,w \in H^2(\wt{\Omega})$
  \begin{align*}
    a_{\wt{\Omega}}(v,w) := \sum_{i=1}^n \langle \delta_i v, \delta_i w\rangle_{L^2(\wt{\Omega})}\text{.}
  \end{align*}
  There exists an $h$-independent constant $c_1 \in \R_{>0}$ such that for all $v \in \cS$
  \begin{align*}
    \norm{v}_{a_{\wt{\Omega}}} \leq c_1 \left( \norm{v}_{a_{\Omega_0}} + \norm{v}_{\aStab} \right)\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Let $E_0 \in \cT_{\Gamma}$ and $c_0 := c\sigma \geq 1$ the constant from \cref{thm:jumpLemma} where $\sigma$ coincides with the shape regularity constant given in \cref{asp:sigma}.
  The geometric assumption~\ref{asp:face} states the existence of $d'\leq d$ pairwise different elements $E_i \in \cT_0$ such that the last element is no boundary element, \ie $E_{d'} \in \cT_0 \setminus\cT_{\Gamma}$, and such that two consecutive elements are always connected by a joint boundary face, \ie $E_{i} \cap E_{i+1} \in \cF$.
  Up to $d$-fold iterative application of \cref{thm:jumpLemma} yields
  \begin{align*}
    \sum_{i=1}^n \norm{\delta_i v}_{L^2(E_0)}^2
    \leq  c_0^d \left( \sum_{i=1}^n \norm{\delta_i v}_{L^2(E_{d'})}^2 + n \sum_{i=1}^l \sum_{j=0}^k \frac{h^{2j+1}}{(j!)^2 (2j+1)} \norm{\jmp{\partial_\nu^j v}}_{L^2(E_{i-1}\cap E_i)}^2 \right)
    \text{.}
  \end{align*}
  When summing the above inequality over all elements $E \in \cT_\Gamma$ \cref{asp:face} also ensures that each interior element $E \in \cT_0\setminus\cT_\Gamma$ and each boundary face $F \in \cF$ is visited at most $4d^2$ times.
  Therefore holds for all $v \in \cS$
  \begin{align*}
    a_{\wt{\Omega}}(v,v)
    & = \sum_{E \in \cT_0} \sum_{i=1}^n \norm{\delta_i v}_{L^2(E)}^2
    \\ & = \sum_{E \in \cT_{\Gamma}} \sum_{i=1}^n \norm{\delta_i v}_{L^2(E)}^2 + \sum_{E \in \cT_0 \setminus \cT_\Gamma} \sum_{i=1}^n \norm{\delta_i v}_{L^2(E)}^2
    \\ & \leq (4c_0^dd^2+1) \sum_{E \in \cT_0\setminus\cT_\Gamma} \sum_{i=1}^n \norm{\delta_i v}_{L^2(E)}^2
      + 4c_0^dd^2n \sum_{F \in \cF} \sum_{j=0}^k \frac{h^{2j+1}}{(j!)^2 (2j+1)}  \norm{\jmp{\partial_\nu^j v }}_{L^2(F)}^2
      %+ \sum_{E \in \cT_0 \setminus \cT_\Gamma} \sum_{i=1}^n \norm{\delta_i v}_{L^2(E)}^2
    \\ & \leq (4c_0^dd^2+1) \left( \aOmega(v,v) + \aStab(v,v) \right)
    \text{.}
  \end{align*}
  In particular, one obtains with $c_1 := \sqrt{4c_0^dd^2+1}$ the inequality
  \begin{align*}
    \norm{v}_{a_{\wt{\Omega}}}
    & = \sqrt{a_{\wt{\Omega}}(v,v)}
    \leq \sqrt{4c_0^dd^2+1} \cdot \sqrt{\norm{v}_{\aOmega}^2 + \norm{v}_{\aStab}^2 }
    \leq c_1 \left( \norm{v}_{\aOmega} + \norm{v}_{\aStab} \right)\text{.}
    \qedhere
  \end{align*}
  %Summing over all $E \in \wt{\cT}$ \TK{and using the geometric assumptions} shows that the claimed statement holds for $c_1 = \sqrt{(1+\frac{4d(d+1)}{2})c_0^d n}$.
  %~ \snote{Assumes that we have a conforming grid of rectangles.}
\end{proof}

\begin{lemma}
  \label{thm:aNitControl}
  There exists an $h$-independent
  %\TK{cut-independent}
  constant $c_2 \in \R_{\geq 1}$ such that for all $v \in \cS$ holds
  \begin{align*}
    \abs{\aNit(v,v)} \leq c_2 \norm{v}_{H^2(\wt{\Omega})} \, \norm{v}_{\Gamma,h}
  \end{align*}
\end{lemma}

\begin{proof}
  Recall
  \begin{align*}
    \aNit(v,w) = \sum_{\substack{\abs{\alpha}\leq 3, \abs{\beta}\leq 1,\\ \abs{\alpha} + \abs{\beta} \leq 3}} \int_{\Gamma} c_{\alpha\beta}^\Gamma \, \partial^\alpha v \, \partial^\beta w \d{x}
  \end{align*}
  and
  \begin{align*}
    \norm{v}_{\Gamma,h}^2 = \sum_{i=0}^1 \norm{ h^{i-3/2} \wt{T}_i v}^2_{L^2(\Gamma)}
    = \sum_{\abs{\beta} \leq 1} \norm{h^{\abs{\beta}-3/2} \partial^\beta v}_{L^2(\Gamma)}^2
    \text{.}
  \end{align*}
  Let $c_{\infty} := \max_{\alpha,\beta} \Vert{c^\Gamma_{\alpha\beta}\Vert}_{\infty}$.
  Then Cauchy--Schwarz and the inverse inequalities from \cref{asp:inverse} lead to the desired  estimate
  \begin{align*}
    \abs{\aNit(v,v)}
    & \leq c_\infty \sum_{\substack{\abs{\alpha}\leq 3, \abs{\beta}\leq 1, \\ \abs{\alpha} + \abs{\beta} \leq 3}} \sum_{E \in \cT_{\Gamma}} \norm{h^{\max(0,\abs{\alpha}-3/2)} \partial^\alpha v}_{L^2(\Gamma\cap E)} \norm{h^{\min(0,3/2-\abs{\alpha})}\partial^\beta v}_{L^2(\Gamma\cap E)}
    \\ & \leq c_\infty \sum_{\substack{\abs{\alpha}\leq 3, \abs{\beta}\leq 1, \\ \abs{\alpha} + \abs{\beta} \leq 3}} \sum_{E \in \cT_{\Gamma}} c_{\text{inv}} \norm{v}_{H^2(E)} \norm{h^{\abs{\beta}-3/2} \partial^\beta v}_{L^2(\Gamma \cap E)}
    \\ & \leq 29c_\infty c_{\text{inv}} \norm{v}_{H^2(\wt{\Omega})} \norm{v}_{\Gamma,h}\text{.}
    \qedhere
  \end{align*}
\end{proof}

\begin{lemma}
  \label{thm:carstensResult}
  Let $H$ be a Hilbert space and $a_1, a_2\colon H \times H \rightarrow \R$  continuous symmetric bilinear forms.
  Suppose that $a_1$ has a finite dimensional kernel~$\ker a_1$ and that $a_1$ is coercive on its orthogonal complement~$(\ker a_1)^\perp$.
  If furthermore $a_2$ is positive semi-definite on $H$ and positive definite on $\ker a_1$, then $a_1 + a_2$ is coercive on $H$. 
\end{lemma}

\begin{proof}
  See \cite{Graeser15}.
\end{proof}

\begin{lemma}
  \label{thm:H2coercivity}
  There exists an $h$-independent
  %\TK{cut-independent}
  constant $c_3 \in \R_{\geq 1}$ such that for all functions $v \in H^2(\wt{\Omega})$ holds
  \begin{align*}
    \norm{v}_{H^2(\wt{\Omega})} \leq c_3 \left( \norm{v}_{a_{\wt{\Omega}}} + \norm{v}_{\Gamma} \right)
  \end{align*}
\end{lemma}

\begin{proof}
  By \cref{asp:aOmegaExt} the bilinear form $a_{\wt{\Omega}}$ is coercive on $H_{0}^2(\wt{\Omega})$ with an $h$-independent coercivity constant $\gamma \in \R_{>0}$, \ie
  \begin{align*}
    \gamma \norm{v}_{H^2(\wt{\Omega})}^2 \leq a_{\wt{\Omega}}(v,v)
  \end{align*}
  is true for all $v \in H_{0}^2(\wt{\Omega})$.
  From the definition of $a_{\wt{\Omega}}$
  %~ \snote{Make this more precise?}
  it is also apparent that its kernel is finite-dimensional with
  \begin{align*}
    \ker a_{\wt{\Omega}} \subseteq \operatorname{span}\left\{ 1, x_1, x_2 \right\}
  \end{align*}
  whereas the bilinear form inducing the norm $\norm{\cdot}_{\Gamma}$ 
  \begin{align*}
    a_{\Gamma}(v,w) := \langle v, w \rangle_{L^2(\Gamma)} + \langle \partial_\nu v, \partial_\nu w\rangle_{L^2(\Gamma)}
  \end{align*}
  is positive semi-definite on $H^2(\wt{\Omega})$ and strictly positive on $\ker a_{\wt{\Omega}}$.
  
  Altogether, this means that the conditions for \cref{thm:carstensResult} are met and therefore the bilinear form $a_{\wt{\Omega}} + a_\Gamma$ is coercive on $H^2(\wt{\Omega})$ with some coercivity constant~$\wt{\gamma} \in \R_{>0}$.
  Hence, 
  the desired estimate holds with $c_3 := \wt{\gamma}^{-1/2}$.
  
  
  %~ \TK{The following needs to go in the assumption section:}
  %~ We assume that $a_{\wt{\Omega}}$ is coercive on the space
  %~ \begin{align*}
    %~ \wt{U} = \left\{ v \in H^2(\Omega) \mid Tv = 0 \right\}\text{.}
  %~ \end{align*}
  %~ \snote{Is clear that the trace here is restricted to $\Gamma_0$?}
  %~ and that the coercivity constant can be bounded from below by a constant $\underline{c}$ that is \TK{cut-independent}.
  
  %~ With this assumption, and the right inverse $E$ of $T$ we obtain for all $v \in H^2(\wt{\Omega})$
  %~ \snote{Need to argue shortly that the norm of the right inverse is also cut-independent.}
  %~ \begin{align*}
    %~ \norm{v}_{H^2(\wt{\Omega})}
    %~ & \leq \norm{v - ETv}_{H^2(\wt{\Omega})} + \norm{ETv}_{H^2(\wt{\Omega})}
    %~ \\ & \leq \underline{c} \norm{v - ETv}_{a_{\wt{\Omega}}} + \norm{E} \, \norm{v}_{\Gamma_0}
    %~ \\ & \leq \underline{c} \norm{v}_{a_{\wt{\Omega}}} + \left(\underline{c}\norm{a}+1\right)\norm{E} \, \norm{v}_{\Gamma_0}
    %~ \text{.}
  %~ \end{align*}
\end{proof}

The previous results allow to conclude the following discrete ellipticity property for the bilinear form $a$ with respect to the triple-norm $\trinorm{\cdot}$:

\begin{proposition}\label{thm:trinormCoercivity}
  Suppose $\lambda \geq 12(c_1c_2c_3)^2$ where the $c_i$ are the constants from \cref{thm:aStabilization}, \cref{thm:aNitControl} and \cref{thm:H2coercivity}, respectively.
  Then for all $v \in \cS$
  \begin{align*}
    \frac{1}{2} \trinorm{v}^2 \leq a(v,v) \leq \frac{3}{2} \trinorm{v}^2\text{.}
  \end{align*}
\end{proposition}

\begin{proof}
  Application of \cref{thm:aNitControl}, then \cref{thm:H2coercivity}, and then \cref{thm:aStabilization} yields the estimate
  \begin{align*}
    \abs{\aNit(v,v)}
    & \leq c_2 \norm{v}_{H^2(\wt{\Omega})} \, \norm{v}_{\Gamma,h}
    \\ & \leq c_2 c_3 \left( \norm{v}_{a_{\wt{\Omega}}} + \norm{v}_{\Gamma} \right) \, \norm{v}_{\Gamma,h}
    \\ & \leq c_2 c_3 \left( c_1\left( \norm{v}_{a_{\Omega_0}} + \norm{v}_{\aStab} \right) + \norm{v}_{\Gamma}  \right) \, \norm{v}_{\Gamma,h}\text{.}
  \end{align*}
  By application of Young's inequality, $h \leq 1$, and from the assumption $\lambda \geq 12(c_1c_2c_3)^2$ one infers furthermore
  \begin{align*}
    \abs{\aNit(v,v)}
    & \leq \frac{1}{4} \norm{v}_{a_{\Omega_0}}^2 + \frac{1}{4} \norm{v}_{\aStab}^2 + 3(c_1c_2c_3)^2 \norm{v}_{\Gamma,h}^2
     \leq \frac{1}{4} \trinorm{v}^2
    \text{.}
  \end{align*}
  Plugging this into the definition of the bilinear form~$a$ readily gives
  \begin{align*}
    a(v,v)
    & = \aOmega(v,v) + \aPen(v,v) + \aStab(v,v) + \aNit(v,v)
    = \trinorm{v}^2 + \aNit(v,v)
    \leq \frac{3}{2} \trinorm{v}
  \end{align*}
  and analogously also
  \begin{align*}
    a(v,v) = \trinorm{v}^2 + \aNit(v,v) \geq \frac{1}{2} \trinorm{v}
  \end{align*}
  holds true.
  This completes the proof.
\end{proof}

A direct consequence of the ellipticity result \cref{thm:trinormCoercivity} is the existence and uniqueness of the discrete approximation $\uh$.

\begin{corollary}
  Suppose $\lambda \geq 12(c_1c_2c_3)^2$.
  There exists a unique finite element function~$\uh \in \cS_\ast$ such that
  \begin{align*}
    \forall{v \in \cS_\ast}\colon a(\uh,v) = \ell(v)\text{.}
  \end{align*}
\end{corollary}

\begin{proof}
  Apply Lax--Milgram's theorem to the bilinear form~$a$ over the discrete space~$\cS_\ast$ equipped with the norm~$\trinorm{\cdot}$.
\end{proof}

%~ \snote{Somewhere: Remark on cut-independency of constants... and of $\p$-dependency.}
