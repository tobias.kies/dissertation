\label{sec:cutFEMerror}

In the following, let $\Iu \in \cS_\ast$ be the approximation of the membrane function~$u$ in the sense of \cref{asp:approximation}, and let $\wt{u} \in H^r(\wt{\Omega})$ the extension of $u$ in the sense of \cref{asp:uExt}.
%~ Moreover, let $s := \min(k+1,r)$ the maximal order at which the solution function~$u$ may be approximated within the given $\cQ_k$ finite element space.

From now on it is assumed that $\lambda$ is large enough for the discrete ellipticity result \cref{thm:trinormCoercivity} to hold true.
All constants $c \in \R_{>0}$ in the results below are stated with the understanding that they are grid-independent.

\begin{lemma}\label{thm:dee1}
  There exists $c \in \R_{>0}$ such that for all $u \in H^2(\Omega_0)$
  \begin{align*}
    \norm{u}_{H^2(\Omega_0)} \leq c \norm{u}_{a_{\Omega_0} + \aPen}
    \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  The proof is the same as for \cref{thm:H2coercivity}.
\end{proof}


\begin{lemma}\label{thm:dee2}
  There exists $c \in \R_{>0}$ such that
  \begin{align*}
    \norm{u - \Iu}_{\aOmega} + \norm{u-\Iu}_{\aPen} \leq c \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Let $c_\infty := \sum_{i=1}^n \max_{\abs{\alpha},\abs{\beta} \leq 2} \Vert c^i_{\alpha\beta}\Vert_{L^\infty(\wt{\Omega})}$.
  Then the volume approximation inequality from \cref{asp:approximation} and other basic estimates yield
  \begin{align*}
    \norm{u-\Iu}_{\aOmega}^2
    & \leq \norm{\wt{u}-\Iu}_{a_{\wt{\Omega}}}^2
    \\ & = \sum_{i=1}^n \sum_{\abs{\alpha},\abs{\beta} \leq 2} \sum_{E \in \cT_0} \int_E c^i_{\alpha,\beta} \partial^\alpha(\wt{u}-\Iu) \, \partial^\beta(\wt{u}-\Iu) \, \mathrm{d}x
    \\ & \leq c_\infty \sum_{\abs{\alpha},\abs{\beta} \leq 2} \sum_{E \in \cT_0} \norm{\partial^\alpha(\wt{u}-\Iu)}_{L^2(E)} \, \Vert\partial^\beta(\wt{u}-\Iu)\Vert_{L^2(E)}
    \\ & \leq c' \sum_{E \in \wt{\cT}} \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(E)}^2
    \\ & = c' \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}^2
    %~ \text{.}
  \end{align*}
  where $c' \in \R_{>0}$ is a generic grid-independent constant.
  Similarly, for a suitable constant $c'' \in \R_{>0}$, the trace approximation formula gives
  \begin{align*}
    \norm{u-\Iu}_{\aPen}^2
    & = \norm{\wt{u}-\Iu}_{\aPen}^2
    \\ & = \lambda \sum_{\abs{\alpha} \leq 1} \sum_{E \in \cT_{\Gamma}} \norm{\partial^\alpha (\wt{u}-\Iu)}_{L^2(E\cap \Gamma)}^2
    \\ & \leq c'' \sum_{E \in \cT_{\Gamma}} \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(E)}^2
    \\ & = c'' \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}^2
    \text{.}
  \end{align*}
  Combining these two estimates yields the desired result.
\end{proof}


\begin{lemma}\label{thm:dee3}
  There exists $c \in \R_{>0}$ such that for all $v\in\cS$
  \begin{align*}
    \aNit(u-\Iu,v) + \aNit(v,u-\Iu) \leq c \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})} \, \trinorm{v}\text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  Let
  \begin{align*}
    c_\infty := \max_{\nastyidx} \Vert c^\Gamma_{\alpha\beta}\Vert_{L^\infty(\Gamma_0)}
    \text{.}
  \end{align*}
  With the trace approximation from \cref{asp:approximation}, the inequality $\abs{\beta} - \frac{3}{2} \leq \frac{3}{2} - \abs{\alpha}$ for the multi-indices occurring below, and the Cauchy-Schwarz inequality one has for appropriate generic constants $c',c'' \in \R_{>0}$ the chain of estimates
  \begin{align*}
    \aNit(\wt{u}-\Iu,v)
    & \leq c_{\infty} \sum_{E\in\cT_{\Gamma}} \sum_{\nastyidx} \norm{\partial^\alpha (\wt{u}-\Iu)}_{L^2(\Gamma\cap E)} \, \big\Vert\partial^\beta v\big\Vert_{L^2(\Gamma\cap E)}
    \\ & \leq c_\infty c' \sum_{E\in\cT_{\Gamma}} \sum_{\nastyidx} \norm{h^{\max\left(\abs{\alpha}-\frac{3}{2},0\right) + s - \abs{\alpha} - \frac{1}{2}} D^s\wt{u}}_{L^2(E)} \, \norm{h^{\min\left(\frac{3}{2}-\abs{\alpha},0\right)} \partial^\beta v}_{L^2(\Gamma\cap E)}
    \\ & \leq c_\infty c' \sum_{E\in\cT_{\Gamma}} \sum_{\nastyidx} \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(E)} \, \norm{h^{\abs{\beta}-\frac{3}{2}} \partial^\beta v}_{L^2(\Gamma_0\cap E)}
    \\ & \leq c'' \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})} \, \norm{v}_{\aPen}
    \text{.}
  \end{align*}
  Similarly, one infers with suitable generic constants $c',c''\in\R_{>0}$ from the inverse trace estimate in \cref{asp:inverse}, the trace approximation property in \cref{asp:approximation}, and the Cauchy-Schwarz inequality
  \begin{align*}
    \aNit(v,\wt{u}-\Iu)
    & \leq c_\infty \sum_{E \in \cT_{\Gamma}} \sum_{\nastyidx} \norm{\partial^\alpha v}_{L^2(\Gamma\cap E)} \, \norm{\partial^\beta (\wt{u}-\Iu)}_{L^2(\Gamma\cap E)}
    \\ & \leq c' \sum_{E \in \cT_{\Gamma}} \sum_{\nastyidx} \norm{h^{\min\left(0,\frac{3}{2}-\abs{\alpha}\right)} v}_{H^2(E)} \norm{h^{k-1-\abs{\beta}-\frac{1}{2}} D^{k+1}\wt{u}}_{L^2(E)}
    \\ & \leq c'' \sum_{E\in\cT_{\Gamma}} \norm{v}_{H^2(E)} \, \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(E)}
    \\ & \leq c'' \norm{v}_{H^2(\wt{\Omega})} \, \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}
    \text{.}
  \end{align*}
  The $H^2(\wt{\Omega})$ coercivity result from \cref{thm:H2coercivity} and the stabilization estimate in \cref{thm:aStabilization} imply
  \begin{align*}
    \norm{v}_{H^2(\wt{\Omega})}
    & \leq c_3\left( \norm{v}_{a_{\wt{\Omega}}} + \norm{v}_{\aPen} \right)
    \\ & \leq c_1c_3 \left( \norm{v}_{\aOmega} + \norm{v}_{\aPen} + \norm{v}_{\aStab}\right)
    \\ & \leq 3c_1c_3 \trinorm{v}
    \text{.}
  \end{align*}
  Finally, putting all of the above inequalities together proves the assertion.
\end{proof}


\begin{lemma}\label{thm:dee4}
  There exists $c \in \R_{>0}$ such that
  \begin{align*}
    \norm{\Iu}_{\aStab} \leq c \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}
    \text{.}
  \end{align*}
\end{lemma}
%~ \snote{Might be interesting to refine this a little further for the case that $4 \leq r < k+1$. Could be made a remark as well.}


\begin{proof}
  As of the regularity assumption~$\wt{u} \in H^{k+1}(\wt{\Omega})$, the jump term~$\llbracket\partial_\nu^j\wt{u} \rrbracket$ is well-defined for every order~$j \leq k$ and vanishes for every boundary element face~$F \in \cF$.
  Using this and the trace approximation error estimate for element faces in \cref{asp:approximation} one directly obtains for a suitable constant $c \in \R_{>0}$
  \begin{align*}
    \aStab(\Iu,\Iu)
    & = \aStab(\wt{u}-\Iu,\wt{u}-\Iu)
    \\ & = \sum_{F \in \cF} \sum_{j=2}^k \frac{1}{(j!)^2 (2j+1)} \norm{ h^{j-3/2} \jmp{\partial_\nu^j (\wt{u}-\Iu)}}_{L^2(F)}^2
    \\ & \leq \sum_{E \in \cT_0} \sum_{j=2}^k \frac{1}{(j!)^2 (2j+1)} \norm{ h^{j-3/2} \partial_\nu^j (\wt{u}-\Iu)|_E}_{L^2(\partial E)}^2
    \\ & \leq c \sum_{E\in\cT_0} \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(E)}^2
    \\ & = c \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}^2
    \text{.}
    \qedhere
  \end{align*}
\end{proof}


\begin{proposition}\label{thm:dee5}
  There exists $c \in \R_{>0}$ such that
  \begin{align*}
    \trinorm{\Iu - u_h} \leq c \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}
    \text{.}
  \end{align*}
\end{proposition}

\begin{proof}
  For the sake of a simpler notation, let $\Ie := u - \Iu$ be the interpolation error and $v := u_h - \Iu \in \cS_\ast$ be the error between the finite element approximation and the solution's interpolation.
  Consecutive use of the $\trinorm{\cdot}$-coercivity result in \cref{thm:trinormCoercivity}, the Galerkin orthogonality~\eqref{eq:galerkinOrthogonality} of the solution function~$u$ and the discrete approximation~$u_h$, the definition of the bilinear form~$a$, and the approximation error estimates \cref{thm:dee2}, \cref{thm:dee3} and \cref{thm:dee4} allows one to conclude for a sufficiently large grid-independent constant $c \in \R_{>0}$
  \begin{align*}
    \frac{1}{2} \trinorm{u_h - \Iu}^2
    & \leq a(u_h-\Iu,u_h-\Iu)
    \\ & = a(u-\Iu,u_h-\Iu)
    \\ & = \aOmega(\Ie,v) + \aPen(\Ie,v) + \aNit(\Ie,v) + \aNit(v,\Ie) + \aStab(\Iu, v)
    \\ & \leq c \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})} \left( \norm{v}_{\aOmega+\aPen} + \trinorm{v} + \norm{v}_{\aStab} \right)
    \\ & \leq 3c \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})} \, \trinorm{v}\text{,}
  \end{align*}
  which immediately implies the claimed statement.
\end{proof}


\begin{theorem}\label{thm:dee6}
  There exists
  %\TK{a cut-independent}
  a grid-independent constant $c \in \R_{>0}$ such that
  \begin{align*}
    \norm{u - u_h}_{H^2(\Omega_0)} \leq c \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega}}
    \text{.}
  \end{align*}
\end{theorem}

\begin{proof}
  In the following proof let $c \in \R_{>0}$ be a generic constant.
  The triangle inequality allows to split the discretization error into
  \begin{align*}
    \norm{u - u_h}_{H^2(\Omega_0)}
    & \leq \norm{u - \Iu}_{H^2(\Omega_0)} + \norm{\Iu - u_h}_{H^2(\Omega_0)}
    \text{.}
  \end{align*}
  From the volume approximation property in \cref{asp:approximation} the estimate
  \begin{align*}
    \norm{u - \Iu}_{H^2(\Omega_0)} \leq c \norm{h^{k-1} D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}
  \end{align*}
  is immediately apparent.
  From the coercivity estimate in \cref{thm:dee1} and the approximation property in \cref{thm:dee5} one infers the estimate
  \begin{align*}
    \norm{\Iu - u_h}_{H^2(\Omega_0)}
    & \leq c \trinorm{\Iu-u_h}
    \leq c \norm{h^{k-1}D^{k+1}\wt{u}}_{L^2(\wt{\Omega})}
    \text{.}
  \end{align*}
  Combination of all three estimates completes the proof.
\end{proof}
 
Altogether, the above results prove under the given assumptions the optimal asymptotic convergence behavior
\begin{align*}
  \norm{u - u_h}_{H^2(\Omega_0)} \in \mathcal{O}(h^{k-1}) \text{ for } h \to 0
\end{align*}
for quasi-uniform grids.

%~ \TK{To-do: Think about either proving or remarking on Aubin-Nitsche, Strang, condition number estimate.}

\begin{remark}
  Further important properties in general numerical applications that are related to the above results concern error estimates in the sense of Aubin-Nitsche and Strang as well as bounds on condition numbers for the resulting discrete systems.
  These aspects play a subordinate role in the remainder of this thesis and therefore their precise treatment is left open.
  Various results in these directions are readily obtained either via standard techniques or along the lines of the corresponding proofs presented in~\cite{BuHa12}.
\end{remark}

\begin{comment}
\section{Preliminaries}

In this section we introduce a fictitious domain stabilized Nitsche method for approximating the solution of Dirichlet problems that typically arise from minimization of the linearized elastic bending energy subject to curve constraints.

\TK{For the type of problem (and its solution) we make the following assumptions...}

\TK{Regarding the discretization we assume...}

%~ Let $\Omega \subseteq \R^2$ a bounded rectangular domain.
%~ In this section we develop a fititious domain stabilized Nitsche method for approximation of $u^\ast \in H^2(\Omega_0)$ which fulfills
%~ \begin{align*}
  %~ \forall{v \in \Uad\colon} \ a_{\Omega_0}(u^\ast,v) = \ell_0(v)
  %~ \text{.}
%~ \end{align*}
%~ \TK{To-do: Introduce all the necessary things.}

\TK{What is $\Gamma$, $\Uad$, $T_i$, $h$, $\cS$? What special shape do we assume in $a$?}

Denote the trace norm by
\begin{align*}
  \norm{u}_{\Gamma}^2 := \sum_{i=0}^1 \norm{T_i u}^2
\end{align*}
and define the weighted trace scalar product
\begin{align*}
  \aPen(u,v) := \sum_{i=0}^1 \langle h^{i-3/2} T_iu, h^{i-3/2} T_iv \rangle_{L^2(\Gamma)}
  \text{.}
\end{align*}

\TK{Define $\aNit$}

\TK{Define $\aStab$}

Finally, let for $u,v \in \cS$
\begin{align*}
  a(u,v)  = a_{\Omega_0}(u,v) + \aPen(u,v) + \aNit(u,v) + \aNit(v,u) + \aStab(u,v)
\end{align*}
and
\begin{align*}
  \ell(v) = \ell_0(v) + \aNit(v,u^\ast) + \aPen(u^\ast,v)\text{.}
  \text{.}
\end{align*}

The finite-element approximation $u_h$ of $u^\ast$ is given by the solution of the variational equation
\begin{align*}
  \forall{v \in \cS\colon} \ a(u_h,v) = \ell(v)
  \text{.}
\end{align*}


\section{Stabilization property}

\begin{lemma}
  There exists a \TK{cut-independent} $c_3 \in \R_{\geq 1}$ such that for all $u \in \cS$ holds
  \begin{align*}
    \norm{u}_{a_{\wt{\Omega}}} \leq c_3 \left( \norm{u}_{a_{\Omega_0}} + \norm{u}_{\aStab} \right)
  \end{align*}
\end{lemma}

\begin{proof}
  \TK{to-do}
\end{proof}



\section{Existence and uniqueness}

\begin{lemma}
  There exists a \TK{cut-independent} $c_1 \in \R_{\geq 1}$ such that for all $u \in \cS$ holds
  \begin{align*}
    \abs{\aNit(u,u)} \leq c_1 \norm{u}_{H^2(\wt{\Omega})} \, \norm{u}_{\aStab}
  \end{align*}
\end{lemma}

\begin{proof}
  \TK{to-do}
\end{proof}


\begin{lemma}
  There exists a \TK{cut-independent} $c_2 \in \R_{\geq 1}$ such that for all $u \in H^2(\wt{\Omega})$
  \begin{align*}
    \norm{u}_{H^2(\Omega)} \leq c_2 \left( \norm{u}_{a_{\wt{\Omega}}} + \norm{u}_{\Gamma} \right)
  \end{align*}
\end{lemma}

\begin{proof}
  \TK{to-do}
\end{proof}


\begin{lemma}
  Let $\lambda \geq 12(c_1c_2c_3)^2$.
  Then for all $u \in \cS$ holds
  \begin{align*}
   \frac{1}{2} \trinorm{u}^2 \leq a(u,u) \leq \frac{3}{2} \trinorm{u}^2
   \text{.}
  \end{align*}
\end{lemma}

\begin{proof}
  \TK{The previous results, Young's inequality and the assumption $h \leq 1$ yield}
  \begin{align*}
    \abs{\aNit(u,u)}
    & \leq c_1 \norm{u}_{H^2(\wt{\Omega}))} \, \norm{u}_{\aStab}
    \\ & \leq c_1 c_2 \left( \norm{u}_{a_{\wt{\Omega}}} + \norm{u}_{\Gamma} \right) \, \norm{u}_{\aStab}
    \\ & \leq c_1 c_2 \left( c_3\left( \norm{u}_{a_{\Omega_0}} + \norm{u}_{\aStab} \right) + \norm{u}_{\Gamma}  \right) \, \norm{u}_{\aStab}
    \\ & \leq \frac{1}{4} \norm{u}_{a_{\Omega_0}}^2 + \frac{1}{4} \norm{u}_{\aStab}^2 + 3(c_1c_2c_3)^2 \norm{u}_{\aStab}^2
    \\ & \leq \frac{1}{4} \trinorm{u}^2
    \text{,}
  \end{align*}
  which immediately implies the assertion by virtue of the definition of $a$.
\end{proof}


\begin{remark}
  \TK{Obviously, the bound on $\lambda$ is not very tight.}
\end{remark}



\section{Approximation error estimates}
\TK{Mostly $H^2$ error, maybe also Strang- and Aubin-type results.}

\TK{Maybe: Also condition number}
\end{comment}

%~ In this section we consider a PDE posed over a domain $\Omega_0 \subseteq \Omega$ \TK{such that the Dirichlet boundary is also contained in $\partial\Omega_0$}.
%~ Let $\cA_i\colon H^2(\Omega) \rightarrow L^2(\Omega)$, $i = 1,\dots,n$ some differential operators and let for $\Omega' \subseteq \Omega$
%~ \begin{align*}
  %~ a_{\Omega'}(u,v) := \sum_{i=1}^n \langle \cA_i u|_{\Omega'}, \cA_i v|_{\Omega'} \rangle_{L^2(\Omega')}
  %~ \text{.}
%~ \end{align*}
%~ We assume that the PDE can be described by the variational equality
%~ \begin{align*}
  %~ \forall{v \in \Uad^0\colon} \ a_{\Omega_0}(u^\ast,v) = \ell_0(v)
%~ \end{align*}
%~ where $\ell\in H^2(\Omega_0)'$.




%~ In this section, suppose that we want to solve an elliptic fourth order PDE on $\Omega_0 := \Omega(\p)$ subject to Dirichlet boundary conditions, \ie
%~ \begin{align*}
  %~ Lu & = f \qquad\text{ on $\Omega_0$}
  %~ \\ Tu & = g \qquad\text{ on $\partial\Omega_0$.}
%~ \end{align*}
%~ Let $a_0\colon H^2(\Omega_0) \times H^2(\Omega_0) \rightarrow \R$ the bilinear form that is induced by $L$, \ie $a_0(u,v) = \langle Lu, v\rangle_{L^2(\Omega_0)}$ for all $u,v \in H_0^2(\Omega_0)$.
%~ The goal is to approximate the unique weak solution $u^\ast \in \Uad = \{v \in H^2(\Omega_0) \mid Tv = g\}$ of this PDE, which is characterized by the variational equation
%~ \begin{align*}
  %~ \forall{v \in \Uad^0\colon} \ a_0(u^\ast,v) = \ell_0(v)
%~ \end{align*}
%~ where $\Uad^0 := \{v \in H^2(\Omega_0) \mid Tv = 0\}$ and $\ell_0 \in H^2(\Omega_0)'$, and $a_0$ is elliptic on $H_0^2(\Omega_0)$.

%~ In the following we consider a discretization that lives on a grid $\cT$ that is not matched to the domain $\Omega_0$ but instead to the bulk domain $\Omega$.
%~ We assume that $\cT$ is composed of quadrilateral \snote{axis-aligned} elements only, and that $\Omega = \bigcup_{E \in \cT} E$.
%~ Since we may have to solve this kind of PDE many times on only slightly different $\Omega_0$ we save computational effort with this approach because we do not need to re-mesh after every step.

%~ We extend $a_0$ and $\ell_0$ by zero to a bilinear form on $H^2(\Omega)$ and a linear form on $H^2(\Omega)'$, respectively.
%~ We furthermore require 
