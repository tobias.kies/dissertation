Since only the approximation of $u(\p)$ for one fixed $\p \in \cD^\circ$ is considered here, the configuration parameter~$\p$ is dropped entirely for the remainder of this chapter.
For notational clarity $\Omega_0 := \Omega(\p)$ is defined and $\Omega \supseteq \Omega_0$ denotes a \emph{bulk domain} which does not necessarily have to coincide with the set $\Omega$ from the parameterization domain in \cref{chap:model}.
Similarly, also $\E_0 := \E(\p,\cdot)$ and $\Uad := \Uad(\p)$ are defined.
%~ In particular, 
All estimates in the subsequent sections may have an implicit dependence on the configuration parameter~$\p$ without further notice.

For now, the attention is on curve constraints.
Departing from previous chapters, the trace operator $T$ in the following sections is defined to incorporate not only the $\p$-dependent but also the $\p$-independent Dirichlet boundary conditions.
%~ In the following sections, not only the $\p$-dependent but also the $\p$-independent Dirichlet boundary conditions are incorporated into the trace operator $T$.
Hence, let the set $\Gamma \subseteq \partial\Omega_0$ denote the portion of the boundary~$\partial\Omega_0$ on which Dirichlet boundary conditions are prescribed, and extend the constraint right hand side $g$ to all of $\Gamma$ by zero.
It is assumed that $\Gamma$ has a non-trivial one-dimensional measure.

Recall the definition of the admissible set
\begin{align*}
  \Uad := \left\{ v \in H_\ast^2(\Omega_0) \mid Tv = g \right\}\text{.}
\end{align*}
Here, $T$ is the trace operator on the Dirichlet boundary~$\Gamma$, \ie
\begin{align*}
  T\colon H^2(\Omega_0) \rightarrow H^{3/2}(\Gamma) \times H^{1/2}(\Gamma)
  ,\qquad v \mapsto (v|_{\Gamma}, \partial_\nu v|_{\Gamma})
  \text{.}
\end{align*}
Furthermore, $H_\ast^2(\Omega_0) \subseteq H^2(\Omega_0)$ is a closed linear subspace which  possibly encodes periodic or homogeneous natural boundary conditions,
and the function $g \in \operatorname{range}(T)$ is the Dirichlet constraint right hand side.

As before, the homogeneous admissible set is analogously defined by
\begin{align*}
  \Uad^0 := \left\{ v \in H_\ast^2(\Omega_0) \mid Tv = 0 \right\}\text{.}
\end{align*}
Finally, for later use it is also convenient to define the \emph{extended trace operator}
\begin{align*}
  \wt{T}\colon H^2(\Omega_0) \rightarrow H^{3/2}(\Gamma) \times H^{1/2}(\Gamma)^2
  ,\qquad v \mapsto (v|_{\Gamma}, \nabla v|_{\Gamma})
  \text{.}
\end{align*}

%~ \subsection*{Structure of the membrane energy minimization problem}
\subsection*{Structure of the membrane energy}

The optimal membrane shape is determined by the function $u \in \Uad$ which solves the energy minimization problem
\begin{align*}
  \min_{v \in \Uad} \E_0(v)\text{.}
\end{align*}
As mentioned earlier, in this chapter the energy $\E_0$ is assumed to be quadratic and elliptic, \ie up to a $v$-independent constant holds
\begin{align*}
  \E_0(v) = \frac{1}{2} a_{\Omega_0}(v,v) - \ell_0(v)
\end{align*}
where $a_{\Omega_0}$ is an elliptic bilinear form on
%$H_\ast(\Omega_0)$
%~ $\Uad$
the linear subspace $\Uad^0$.
and $\ell_0 \in H^2(\Omega_0)'$ is a bounded linear form.
Moreover, it is assumed that the bilinear form $a_{\Omega_0}$ has a special structure:

\begin{asp}{asp:aOmega}
  Suppose
  \begin{align*}
    a_{\Omega_0}(v,w) = \sum_{i=1}^n \langle \delta_i v, \delta_i w\rangle_{L^2(\Omega_0)}
  \end{align*}
  where the $\delta_i$ define differential operators
  \begin{align*}
    \delta_i v := \sum_{\abs{\alpha}\leq 2} c^i_\alpha \partial^\alpha v
  \end{align*}
  with bounded coefficient functions $c^i_\alpha \in C^2(\Omega)$.%\snote{sufficiently smooth?}
\end{asp}

\noindent It is remarked that the quadratic parts in the Monge-gauge energy from \cref{thm:linearizationMongeGauge} as well as those in the linearized energy for tubular reference manifolds from \cref{thm:linearizationTube} are of this form.

\subsection*{Unfitted grid of quadrilaterals}

The upcoming discretization works with an unfitted grid on an arbitrarily simple background domain.
Therefore, it is assumed that the bulk domain~$\Omega$ is rectangular with its border lines parallel to the coordinate axes.
The grid~$\cT$ itself is assumed to have no hanging nodes
%\snote{Other word for ``no hanging nodes''?}
and to consist out of quadrilateral elements $E$ such that $\Omega = \bigcup_{E \in \cT} E$.
For simplicity of notation the elements $E$ are assumed to be closed sets with pairwise disjoint interiors.

The set of \emph{elements that intersect the domain $\Omega_0$ non-trivially} is denoted by
\begin{align*}
  \cT_0 := \left\{ E \in \cT \mid E^\circ \cap \Omega_0 \neq \emptyset\right\}
\end{align*}
where $E^\circ$ denotes the interior of $E$.
This again induces the \emph{grid-dependent bulk domain}
\begin{align*}
  \wt{\Omega} := \bigcup_{E \in \cT_0} E\text{.}
\end{align*}
Similarly, the set of \emph{Dirichlet boundary elements} is defined as
\begin{align*}
  \cT_{\Gamma} := \left\{ E \in \cT \mid E^\circ \cap \Gamma \neq \emptyset \right\}
  \text{,}
\end{align*}
which induces the \emph{grid-dependent neighborhood of the Dirichlet boundary}
\begin{align*}
  \Omega_{\Gamma} := \bigcup_{E \in \cT_{\Gamma}} E
  \text{.}
\end{align*}
Moreover, the discretization also requires the set of \emph{boundary faces}
\begin{align*}
  \cF := \left\{ E_1 \cap E_2 \mid E_1 \in \cT_{\Gamma}, \ E_2 \in \cT_0, \ E_1 \neq E_2, \ \abs{E_1 \cap E_2}_1 > 0\right\}
\end{align*}
where $\abs{E_1\cap E_2}_1$ is the one-dimensional measure of the set $E_1\cap E_2$.
For these faces a geometric assumption is made which ensures a grid-independent upper bound on the connectivity of boundary elements with non-boundary elements:

\begin{asp}{asp:face}
  Suppose there exists an $h$-independent $d \in \N$ with the following property: For all boundary elements $E_0 \in \cT_\Gamma$ exist $d' \leq d$ elements $E_1,\dots,E_{d'} \in \cT_0$ with $E_i \cap E_{i+1} \in \cF$ and where $E_i \in \cT_\Gamma$ for $i < d'$ and $E_{d'} \in \cT_0 \setminus \cT_{\Gamma}$.
\end{asp}

\noindent Lastly, the \emph{grid size function $h\colon \Omega \rightarrow \R$ with respect to the grid $\cT$} is defined almost-everywhere on $\Omega$ by the following rule:
\begin{align*}
  h(x)
  =
  \begin{cases}
    \operatorname{diam}(E)/\sqrt{2}, & \qquad \text{if $x \in E^\circ$ for an $E \in \cT$,} \\
    \operatorname{diam}(E_1\cap E_2), & \qquad \text{if $x \in E_1 \cap E_2$ for $E_1, E_2 \in \cT$.}
  \end{cases}
\end{align*}
Here, $\operatorname{diam}(A) := \sup_{x,y \in A} \norm{x-y}$ denotes the diameter of a set $A$.
This definition is well-posed everywhere except in those interface points $x$ where also an $E_3 \in \cT$ exists such that $x \in E_1 \cap E_2 \cap E_3$.
The function $h$ shall remain undefined in those points.
For later convenience it is assumed that $h \leq 1$ holds uniformly.
Also the weight $\sqrt{2}$ is due to reasons of convenience.

\noindent Finally, the following non-degeneracy condition is imposed:

\begin{asp}{asp:sigma}
  Suppose $\sigma \in \R_{\geq 1}$ such that for all adjacent elements $E_1, E_2 \in \cT_0$ the inequality $\frac{\abs{E_1}}{\abs{E_2}} \leq \sigma$ holds $h$-independently where $\abs{E_i}$ is the area of $E_i$.
\end{asp}

\noindent\Cref{fig:grid} shows an illustration of a typical example grid for a square domain with a circular cut-out.
More specifically,
%it depicts a discretization of the domain
a discretization of
$\Omega = \{ x \in [-1.6,1.6]^2 \mid \norm{x} \geq 1\}$ by uniform squares with side length~$0.4$ is depicted.
In particular, the grid size function is constant with $h\equiv 0.4$, and $\sigma = 1$.
All quadratic squares together, \ie including the white and gray ones, define the set~$\cT_0$, whose union in turn spans the domain~$\wt{\Omega}$.
The boundary grid~$\cT_\Gamma$ corresponds to the union of all gray squares, the union of which yields the discrete boundary domain~$\wt{\Omega}_\Gamma$.
The boundary faces~$\cF$ are given by the red element faces.
\Cref{asp:face} holds with $d=2$.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{gridDiagram.pdf}
  \end{center}
  \caption{Unfitted uniform grid of quadrilaterals. Boundary elements are colored in gray and boundary interfaces are marked in red.}
  \label{fig:grid}
\end{figure}
%~ \snote{To-do: Forgot to mention $\sigma$-property (non-degeneracy of grid elements)}

\subsection*{Piecewise $\cQ_k$ finite element space}

The base finite element space for the discretization is assumed to be given by
\begin{align*}
  \cS := \left\{ v \in C^{(k-1)/2}(\wt{\Omega}) \mid \forall{E \in \cT_0}\colon \ v|_E \in \cQ_{k} \right\} %\subseteq H^{k+1}(\wt{\Omega})
\end{align*}
with an odd $k\in\N_{\geq 3}$.
Here, $\cQ_k$
%is the set of \TK[better name?]{tensor polynomials} of up to order $k$ in $\R^2$, \ie
is a subset of the polynomials up to order $2k$ in $\R^2$ which is defined by
\begin{align*}
  \cQ_k := \left\{ \sum_{i,j=0}^k a_{ij} x_1^i x_2^j \ \bigg\vert\  a \in \R^{k\times k} \right\}
  \text{.}
\end{align*}
Note that $\cS$ indeed is non-empty and defines a conforming finite element space in $H^{2}(\wt{\Omega})$.
A local basis of $\cS$ is readily constructed via the solution of suitable Hermite interpolation problems.
The special and most simple case $k=3$ corresponds to the space spanned by Bogner--Fox--Schmit finite elements \cite{BFS65}.
%\cite[Section 2.2]{Ciarlet78}).

The finite element space is assumed to fulfill the typical approximation properties:

\begin{asp}{asp:approximation}
  There exists a grid-independent constant $c \in \R_{>0}$ such that for all $s \in [2,k+1]$ and all $v \in H^s(\wt{\Omega}) \cap H_\ast^2(\wt{\Omega})$ exists an approximation $\overline{v} \in \cS_\ast$ such that the \emph{volume approximation property}
  \begin{align*}
    \forall{E \in \cT_0} \ \forall{\abs{\alpha} \leq s}\colon\ \norm{\partial^\alpha (v - \overline{v})}_{L^2(E)} \leq c \norm{h^{s-\abs{\alpha}} v}_{L^2(E)}
  \end{align*}
  and the \emph{trace approximation property} for the boundary $\Gamma$
  \begin{align*}
    \forall{E \in \cT_{\Gamma}} \ \forall{\abs{\alpha} \leq s-\tfrac{1}{2}}\colon \ \norm{\partial^\alpha (v - \overline{v})}_{L^2(\Gamma \cap E)} \leq c \norm{h^{s-1/2-\abs{\alpha}} v}_{L^2(E)}
  \end{align*}
  as well as for the element faces
  \begin{align*}
    \forall{E \in \cT_{\Gamma}} \ \forall{\abs{\alpha} \leq s-\tfrac{1}{2}}\colon \ \norm{\partial^\alpha (v - \overline{v})}_{L^2(\partial E)} \leq c \norm{h^{s-1/2-\abs{\alpha}} v}_{L^2(E)}
  \end{align*}
  hold.
\end{asp}

\noindent Also the inverse inequalities are required:

\begin{asp}{asp:inverse}
  There exists a grid-independent constant $c_{\text{inv}} \in \R_{\geq 1}$ such that for all $v \in \cS_\ast$ the \emph{volume inverse inequality}
  \begin{align*}
    \forall{E \in \cT_0} \ \forall{\abs{\alpha} \leq k} \ \forall{\beta \leq \alpha}\colon \ \norm{\partial^\alpha v}_{L^2(E)} \leq c_{\text{inv}} \norm{h^{\abs{\beta}-\abs{\alpha}} \partial^\beta v}_{L^2(E)}
  \end{align*}
  and the \emph{trace inverse inequality}
  \begin{align*}
    \forall{E \in \cT_\Gamma} \ \forall{\abs{\alpha} \leq k} \ \forall{\beta \leq \alpha}\colon \  \norm{\partial^\alpha v}_{L^2(E \cap \Gamma)} \leq c_{\text{inv}} \norm{h^{\abs{\beta}-\abs{\alpha}-1/2} \partial^\beta v}_{L^2(E)}
  \end{align*}
  are fulfilled.
  Here the notation $\beta \leq \alpha$
  %($\beta < \alpha$)
  means that the multi-index $\beta$ is a
  %(strict)
  subset of the multi-index $\alpha$.
\end{asp}

\noindent Under slight assumptions on the interface $\Gamma$, both \cref{asp:approximation} and \cref{asp:inverse} are usually fulfilled,
%if for example only Dirichlet and periodic boundary conditions are present,
see also \cite{Ciarlet78} for details.


\subsection*{Regularity, bulk and extension assumptions}

The existence of a unique membrane function~$u \in H^2(\Omega_0)$ is readily established by virtue of the well-known Lax--Milgram theorem.
Additional regularity in $u$ is assumed
in order to prove a-priori discretization error estimates.
%, additional regularity in $u$ is assumed.
%~ property $u \in H^r(\Omega_0)$ for some $r \geq 2$ is assumed.
%~ Additionally, the discretization scheme and the proofs for the error bounds require $H^4$-regularity in a grid-dependent neighborhood of the Dirichlet boundary $\Gamma$.
%~ Therefore, also the regularity $u \in H^4(\Omega_0 \cap \Omega_{\Gamma})$ is postulated.
\begin{asp}{asp:regularity}
  Suppose $u \in H^{k+1}(\Omega_0)$ where $k \in \N$ is the order of the $\cQ_k$ finite element space.
\end{asp}

\noindent
Note that in many cases this assumption is fulfilled naturally in view of elliptic regularity theory (cf.~\cite[Chapter~7]{Grisvard85} and \cite{BlumRannacher80,GaGrSw10}) if the boundary data is sufficiently smooth.

The following assumption extends the coercivity of $\aOmega$ to the domain $\wt{\Omega}$:
\begin{asp}{asp:aOmegaExt}
  Suppose that the bilinear form
  \begin{align*}
    a_{\wt{\Omega}}(v,w) := \sum_{i=1}^n \langle \delta_i v, \delta_i w\rangle_{L^2(\wt{\Omega})}
  \end{align*}
  is coercive on the space $H_0^2(\wt{\Omega})$
  %~ \begin{align*}
    %~ H_{0,\Gamma}(\wt{\Omega}) := \left\{v \in H^2(\wt{\Omega}) \mit Tv = 0\right\}
  %~ \end{align*}
  with a grid-independent coercivity constant $\gamma \in \R_{>0}$.
\end{asp}

\noindent Furthermore, since a bulk discretization is considered, it is also assumed that the solution function~$u$ admits a suitable extension:

\begin{asp}{asp:uExt}
  Suppose there exists a grid-independent constant $c \in \R_{>0}$ and an extension $\wt{u} \in H^{k+1}(\wt{\Omega})$ such that $\wt{u}|_{\Omega_0} = u$ and
  \begin{align*}
    \forall{\abs{\alpha} \leq k+1}\colon \ \norm{\partial^\alpha \wt{u}}_{L^2(\wt{\Omega})} \leq c \norm{\partial^\alpha u}_{L^2(\Omega_0)}
    \text{.}
  \end{align*}
\end{asp}


\begin{comment}
  We assume that there exists an affine subspace $U_{\Omega} \subseteq H^2(\Omega)$ such that for any $v \in \Uad$ there exists a $\wt{v} \in U_{\Omega}$ such that $\wt{v}|_{\Omega_0} = v$, and such that furthermore $a_{\Omega}$ is elliptic over $U_{\Omega}$.
  
  \TK{We have a certain way do deal with multi-indices...}
  
  \TK{Given a non-negative bilinear form $\hat{a}$, we define $\norm{v}_{\hat{a}} := \sqrt{\hat{a}(v,v)}$.}
\end{comment}

\begin{comment}
\subsubsection{Grid and finite element space}  
  Concerning the finite element space, we assume that a finite-dimensional subspace $\cS \subseteq H^2(\wt{\Omega})$ and $k \in \N$ are given such that for all $E \in \wt{\cT}$ and $v \in \cS$ holds $v|_{E} = \Q_k$.
  We make the simplifying assumption that $k+1 \leq r$.
  
  Many of the bilinear forms we will use piecewise.
  To this end, we introduce the space of $H^2$ functions that are piecewise in $H^{k+1}$:
  \begin{align*}
    \Hpw = \left\{ v \in H^2(\wt{\Omega}) \mid v|_E \in H^{k+1}(\Omega') \text{ for all $E \in \wt{\cT}$} \right\}
  \end{align*}
  
\end{comment}
