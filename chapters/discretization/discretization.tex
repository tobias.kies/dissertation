\label{sec:fictitiousNitscheDiscretization}

The discretization scheme requires three further ingredients
besides the bilinear form $a_{\Omega_0}$ and the linear operator $\ell_0$:
%~ the discretization scheme requires three further ingredients:
A \emph{penalty term}, a \emph{Nitsche term}, and a so-called \emph{stabilization term}  augment the elastic energy~$\E_0$.

\subsubsection*{Penalty term}
  As the finite element space $\cS_\ast$ does not incorporate the constraints~$Tv = g$ explicitly they are enforced approximately via a penalty term.
  For later convenience this is done with help of the extended trace~$\wt{T}v = (v|_{\Gamma}, \partial_\nu v|_{\Gamma})$.
  In this sense, note that the condition $Tv = g$ is equivalent to $\wt{T}v = (g_0, g_1\nu + \partial_\tau g_0 \tau) =: \wt{g}$ where $\tau$ is an arbitrary unit tangent field on the Dirichlet boundary $\Gamma$.
  The scheme uses a weighted $L^2$-penalization of this constraint,
  \begin{align*}
    \frac{\lambda}{2} \left(h^{-3} \norm{u - \wt{g}_0}^2_{L^2(\Gamma)} + h^{-1} \norm{\nabla u - \wt{g}_1}^2_{L^2(\Gamma)} \right)\text{,}
  \end{align*}
  where $\lambda \in \R_{>0}$ is a ``large enough'' constant that is specified in greater detail later on.
  Taking the first variation of this quadratic energy term naturally leads to the symmetric positive semi-definite bilinear form
  \begin{align*}
    \aPen(v,w) := \lambda \sum_{i=0}^1 \langle h^{i-3/2} \,\wt{T}_i v, h^{i-3/2} \,\wt{T}_i w \rangle_{L^2(\Gamma)}
  \end{align*}
  and the linear functional
  \begin{align*}
    \lPen(v) & := \lambda \sum_{i=0}^1 \langle h^{i-3/2} \,\wt{g}_i, h^{i-3/2} \,\wt{T}_i v \rangle_{L^2(\Gamma)}
  \end{align*}
  where the components of the extended trace operator are indexed by $\wt{T} = (\wt{T}_0, \wt{T}_1)$.


\subsubsection*{Nitsche term}
  The Nitsche term ensures consistency of a minimizer of the augmented elastic energy.
  This term is essentially obtained by modifying the bilinear form $\aOmega$ by a Nitsche term~$\aNit$ such that the optimal membrane function~$u$ fulfills the characterizing variational equation over the larger space $H_\ast^2(\Omega_0) \supseteq \Uad^0$, \ie
  \begin{align}
    \label{eq:nitscheConsistency}
    \forall{v \in H_\ast^2(\Omega_0)}\colon \aOmega(u,v) + \aNit(u,v) = \ell_0(v)\text{.}
  \end{align}
  %~ This property is not trivially fulfilled because $\aOmega(u,v) = \ell_0(v)$ is only known for $v \in \Uad^0$, but in general not for $v \in H_\ast^2(\Omega_0)$.
  It is possible to derive a boundary representation of $\aNit$ by transitioning to the strong formulation for the membrane function~$u$.
  To this end,
  %~ \snote{This requires $u\in H^4$. Makes this clear earlier!}
  %assume for now that $u \in H^4(\Omega_0)$ holds true and
  let $c^i_{\alpha\beta} := c_\alpha^i \cdot c_\beta^i$ where the~$c^i_\alpha$ are the coefficients in the definitions of the differential operators~$\delta_i$ as stated in \cref{asp:aOmega}.
  Then
  \begin{align*}
    a_{\Omega_0}(v,w)
    = \sum_{i=1}^n \langle \delta_i v, \delta_i w\rangle_{L^2(\Omega_0)}
    = \sum_{i=1}^n \sum_{\abs{\alpha}, \abs{\beta} \leq 2} \int_{\Omega_0} c_{\alpha\beta}^i \partial^\alpha v \, \partial^\beta w \d{x}
  \end{align*}
  and integration by parts yields for $v \in H^2_\ast(\Omega_0)$
  \begin{align*}
    a_{\Omega_0}(u,v)
    & = \sum_{i=1}^n \sum_{\abs{\alpha}, \abs{\beta} \leq 2} (-1)^{\abs{\beta}} \int_{\Omega_0} \partial^\beta \left(c^i_{\alpha\beta} \partial^\alpha u \right) v \d{x}
    \\ & \qquad + \sum_{i=1}^n \sum_{\abs{\alpha}, \abs{\beta} \leq 2} \sum_{j=1}^{\abs{\beta}} (-1)^{j+1} \int_{\Gamma} \partial^{\underline{\beta}^{j-1}}\left(c_{\alpha\beta}^i \partial^\alpha u\right) \, \partial^{\overline{\beta}^{j+1}} v \, \nu_{\beta_j} \d{x}\text{.}
  \end{align*}
  where $\underline{\beta}^j = (\beta_1,\dots,\beta_j)$ and $\overline{\beta}^j = (\beta_j,\dots,\beta_{\abs{\beta}})$.
  Here also the periodic or natural boundary conditions on $\partial\Omega \setminus \Gamma$ play an important role in order to reduce the occurring boundary integral to just the Dirichlet boundary~$\Gamma$.
  Upon collecting terms and defining coefficient functions~$c^\Gamma_{\alpha\beta}$ such that
  \begin{align*}
    \sum_{\substack{\abs{\alpha} \leq 3,\abs{\beta}\leq 1, \\ \abs{\alpha}+\abs{\beta}\leq 3}}
      \int_{\Gamma} c^\Gamma_{\alpha\beta} \partial^\alpha u \partial^\beta v \d{x}
    = \sum_{i=1}^n \sum_{\abs{\alpha}, \abs{\beta} \leq 2} \sum_{j=1}^{\abs{\beta}}
      (-1)^{j} \int_{\Gamma} \partial^{\underline{\beta}^{j-1}}\left(c_{\alpha\beta}^i \partial^\alpha u\right) \, \partial^{\overline{\beta}^{j+1}} v \, \nu_{\beta_j} \d{x}
  \end{align*}
  holds, the Nitsche bilinear form is given by
  \begin{align*}
    \aNit(v,w) := \sum_{\substack{\abs{\alpha} \leq 3,\abs{\beta}\leq 1, \\ \abs{\alpha}+\abs{\beta}\leq 3}} 
      \int_{\Gamma} c^\Gamma_{\alpha\beta} \partial^\alpha v \partial^\beta w \d{x}\text{.}
  \end{align*}
  Based on the previous computations it is readily verified that the consistency property~\eqref{eq:nitscheConsistency} is indeed fulfilled.
  %~ \snote{TODO MAKE SURE THAT PARTIAL INTEGRATION WORKS ALSO WITH $H_\ast$ in the assumed generality!!}
  
  The actual discretization requires also the linear functional that is induced by the Nitsche bilinear form applied to the optimal membrane~$u$.
  For $v \in H^4(\Omega_0)$ it is defined by
  \begin{align*}
    \lNit(v) := \aNit(v,u)
    = \sum_{\substack{\abs{\alpha} \leq 3,\abs{\beta}\leq 1, \\ \abs{\alpha}+\abs{\beta}\leq 3}} \int_{\Gamma} c^\Gamma_{\alpha\beta} \, \partial^\alpha v \, g_{\abs{\beta}} \d{x}\text{.}
  \end{align*}
  %~ One observes that $\aNit$ is in general non-symmetric and indefinite.
  %~ \snote{Extend these definitions to a large space as needed.}
  %~ Note that these definitions naturally extend to $\Hpw$.
  
  %~ \snote{Well-defined also for non-superregular $u$ -- but reasonable?}

  
\subsubsection*{Stabilization term}
  The stabilization term is sometimes also referred to as ghost penalty term.
  It is purely quadratic and is defined on the discrete space $\cS$ of piecewise $\cQ_k$-elements by
  \begin{align*}
    \aStab(v,w) = \sum_{F\in\cF}\sum_{j=2}^k \frac{1}{(j!)^2(2j+1)} \left\langle h^{j-3/2}\jmp{\partial_{\nu}^j v}, h^{j-3/2}\jmp{\partial_{\nu}^j w}\right\rangle_{L^2(F)}
    \text{.}
  \end{align*}
  Here, $\nu$ is a unit normal field on the respective face $F = E_1 \cap E_2$.
  Moreover, with the restrictions~$v_i := v|_{E_i}$, the expressions
  \begin{align*}
    \jmp{\partial_\nu^j v} := \sum_{\abs{\alpha} = j} \left(\partial^\alpha v_1 - \partial^\alpha v_2\right) \nu^\alpha
  \end{align*}
  denote the jumps of the normal derivatives along the edge $F$.
  The terms in $\aStab$ are independent of the orientation of $\nu$ due to their quadratic appearance.
  
  
\subsubsection*{The full discretization scheme}
  For all $v, w \in \cS$ let
  \begin{align}
    \label{eq:cutFEMa}
    a(v,w)  & := a_{\Omega_0}(v,w) + \aPen(v,w) + \aNit(v,w) + \aNit(v,w) + \aStab(v,w)
  \end{align}
  and
  \begin{align}
    \label{eq:cutFEMell}
    \ell(v) & := \ell_0(v) + \lPen(v) + \lNit(v)
    \text{.}
  \end{align}
  The finite element approximation $\uh \in \cS_\ast$ of the membrane function~$u$ is defined as the solution of the minimization problem
  \begin{align*}
    \min_{v\in\cS_\ast} \frac{1}{2} a(v,v) - \ell(v)
  \end{align*}
  or, equivalently, as the solution of the variational equation
  \begin{align*}
    \forall{v \in \cS_\ast\colon} \ a(\uh,v) = \ell(v)\text{.}
  \end{align*}
  Of crucial importance for the later error estimates is the fact that the continuous solution function~$u$ fulfills the consistency property
  \begin{align}
    \label{eq:consistency}
    \forall{v \in H_\ast^2(\Omega_0)\colon} \ a(u,v) = \ell(v)
    \text{.}
  \end{align}
  This is a direct consequence of the construction of the discretization scheme, and in particular relies on the definition of the Nitsche term $\aNit$.
  An immediate benefit of this variational equality is that the discrete approximation~$u_h$ preserves a Galerkin orthogonality property with respect to the bilinear form~$a$ over the space $\cS_\ast$, \ie it holds
  \begin{align}
    \label{eq:galerkinOrthogonality}
    \forall{v \in \cS_\ast}\colon \ a(u,v) = a(u_h,v)\text{.}
  \end{align}
  In Appendix~\ref{sec:appendixDiscretizations} this discretization scheme is motivated from a more pragmatic viewpoint
  where it is put in the context of the better known Galerkin, penalty and Nitsche schemes.
  The subsequent sections, however, are independent of those considerations.
