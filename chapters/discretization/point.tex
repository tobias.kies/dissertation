\label{sec:point}

This section derives a simple discretization scheme for point value constraints when the membrane parameterization domain $\Omega_0$ is discretized by a matched grid but the evaluation points do not necessarily coincide with nodes in the grid.
No new grid types or finite element spaces are introduced in the following paragraphs for the sake of a briefer notation.
Instead, it is simply assumed that those are given as before and that the domain $\Omega_0$ is rectangular and resolved exactly.
However, the technique described below readily generalizes to rather arbitrary finite element schemes and discrete spaces.

Let the membrane--particle interaction be described by point value constraints and denote by $N \in \N$ the total number of evaluation points $x_i \in \Omega_0$.
The membrane function~$u \in H_{\ast}^2(\Omega_0)$ is then characterized by the solution of the constrained minimization problem
\begin{align*}
  \min_{v \in H_\ast^2(\Omega_0)} & \ \frac{1}{2} \aOmega(v,v) - \ell_0(v), \qquad Tv = g
\end{align*}
where $T\colon H^2(\Omega_0) \rightarrow \R^N$ is the evaluation operator in the points~$(x_i)_{i=1,\dots,n}$ and $g \in \R^N$ is the vector of corresponding point values.

Given the conforming subspace $\cS_\ast \subseteq H_\ast^2(\Omega_0)$, the discrete approximation~$u_h \in \cS_\ast$ of the solution function~$u$ is analogously defined as the solution of the problem
\begin{align*}
  \min_{v \in \cS_\ast} & \ \frac{1}{2}\aOmega(v,v) - \ell_0(v), \qquad Tv = g
\end{align*}
or, equivalently due to the ellipticity of $\aOmega$, by the variational equation
\begin{align*}
  \forall{v \in \cS_\ast\colon} \ \aOmega(u_h,v) = \ell_0(v)\text{.}
\end{align*}

Let $M \in \N$ be the dimension of $\cS_\ast$ and let $(\psi_i)_{i=1,\dots,M}$ be a nodal basis of $\cS_\ast$.
Then the discrete approximation~$u_h$ is expressed via a coefficient vector~$\du \in \R^M$ as
\begin{align*}
  u_h = \sum_{i=1}^M \du_i \psi_i\text{.}
\end{align*}
In the spirit of the standard Galerkin method, define discretizations $A \in \R^{M\times M}$, $b \in \R^M$, $E \in \R^{N\times M}$ of the operators $\aOmega$, $\ell_0$ and $T$ by
\begin{align*}
  A_{ij} := \aOmega(\psi_i,\psi_j)
  ,\qquad b_i := \ell_0(\psi_i)
  ,\qquad E_{ij} := \psi_j(x_i)
  \text{.}
\end{align*}
Assuming that $E$ has full rank $N \leq M$,
the coefficient vector~$\du$ is uniquely determined by the solution of the finite-dimensional quadratic optimization problem
\begin{align}
  \label{eq:discOptimPoint}
  \min_{\dv \in \R^M} \frac{1}{2} \dv^TA\dv - b^T\dv, \qquad E\dv = g
\end{align}
or, equivalently, as the solution of the linear system of equations
\begin{align*}
  \begin{pmatrix}
    A & E^T \\ E & 0
  \end{pmatrix}
  \begin{pmatrix}
    u \\ \lambda
  \end{pmatrix}
  =
  \begin{pmatrix}
    b \\ g
  \end{pmatrix}
  %~ A\du & = b, \qquad E\textbf{u} = g\text{.}
\end{align*}
where $\lambda \in \R^N$.
Because in general the constraints are low-dimensional in comparison to the full finite element space, \ie $N \ll M$, and because the evaluation property is local, \ie the matrix $E$ is sparse, it is possible to avoid the reformulation of this equation as saddle point problem and instead one may incorporate the constraints directly into the finite element space $\cS_\ast$ in order to obtain a sparse conforming discretization.

To this end, assume without loss of generality that the basis indices are sorted such that the evaluation matrix~$E$ and the right hand side~$g$ take the block form
\begin{align*}
  E = \begin{pmatrix}
        E_1 &        & 0    & 0      & \cdots & 0 \\
            & \ddots &      & \vdots &        & \vdots \\
        0   &        & E_l  & 0      & \cdots & 0
      \end{pmatrix}
  ,\qquad g = \begin{pmatrix} g_1 \\ \vdots \\ g_l \end{pmatrix}
\end{align*}
where $E_i \in \R^{n_i \times m_i}$ and $g_i \in \R^{n_i}$.
Note that as of $N \ll M$ and due to the the local support of the basis functions $\psi_i$, also $\sum_{i=1}^l m_i \ll M$ holds true.

For each $i\in\{1,\dots,l\}$, let
\begin{align}
  \label{eq:qrLocal}
  E_i^T = Q^{(i)} R^{(i)}
\end{align}
a $QR$-decomposition of the transposed block~$E_i^T \in \R^{m_i \times n_i}$ with an orthogonal matrix~$Q^{(i)} \in \R^{m_i \times m_i}$ and an upper triangular matrix~$R^{(i)} \in \R^{m_i \times n_i}$.

The full rank assumption on~$E$ implies that the blocks~$E_i$ also have full rank~$n_i \leq m_i$ such that the $R$-matrices take the block form
\begin{align*}
  R^{(i)} = \mat{\wt{R}^{(i)} \\ 0}
\end{align*}
with invertible upper triangular matrices $\wt{R}^{(i)} \in \R^{n_i \times n_i}$.
Let $Q^{(i)}_{j}$ denote the columns of the matrix $Q^{(i)}$ and split it into the matrices
\begin{align*}
  \hat{Q}_i := \mat{Q_1^{(i)} & \cdots & Q_{n_i}^{(i)}} \in \R^{m_i \times n_i}
  ,\qquad \wt{Q}_i = \mat{Q_{n_i+1}^{(i)} & \cdots & Q_{m_i}^{(i)}} \in \R^{m_i \times (m_i-n_i)}
  \text{.}
\end{align*}
Since
\begin{align*}
   E_i\dv_i = g_i
   & \ \Longleftrightarrow \ \mat{ \big(\wt{R}^{(i)}\big)^T & 0} \big(Q^{(i)}\big)^T\dv_i = g_i
   \\ & \ \Longleftrightarrow \ \mat{ I & 0} \big(Q^{(i)}\big)^T\dv_i = \big(\wt{R}^{(i)}\big)^{-1}g_i
\end{align*}
one has
\begin{align*}
  E_i\dv_i = g_i \ \Longleftrightarrow \ \exists{c \in \R^{m_i-n_i}\colon} \ \dv_i = \hat{Q}_i\big(\wt{R}^{(i)}\big)^{-T}g_i + \wt{Q}_i c
  \text{.}
\end{align*}
In particular, a feasible solution vector $\du^0 \in \R^M$ with $\sum_{i=1}^M \du^0_i \psi_i \in \Uad$ is given by
\begin{align}
  \label{eq:du0formula}
  \du^0 := \begin{pmatrix}
            \hat{Q}_1 \big(\wt{R}^{(1)}\big)^{-T}g_1 \\
            \vdots \\
            \hat{Q}_l \big(\wt{R}^{(l)}\big)^{-T}g_l
          \end{pmatrix}
  \text{,}
\end{align}
and the kernel of $E$ is spanned by the columns of
\begin{align}
  \label{eq:Qtilda}
  \wt{Q} :=
    \begin{pmatrix}
      \wt{Q}_1 &        & 0         & 0       \\
               & \ddots &           & \vdots  \\
      0        &        & \wt{Q}_l  & 0       \\
      0        & \cdots & 0         & I
    \end{pmatrix}
    \in \R^{M \times (M-N)}
    \text{.}
\end{align}
Therefore,
%~ with
%~ \begin{align*}
  %~ \wt{A} := \wt{Q}^T A \wt{Q}
  %~ , \qquad \wt{b} := \wt{Q}^Tb
%~ \end{align*}
the optimization problem~\eqref{eq:discOptimPoint} is equivalent to the reduced problem
\begin{align*}
  %~ \label{eq:discOptimPointTransformed}
  \min_{\dv \in \R^{M-N}} \frac{1}{2} (\wt{Q}\dv + \du^0)^T {A} (\wt{Q}\dv+\du^0) - {b}^T(\wt{Q}\dv+\du^0)
  \text{.}
\end{align*}
Analogously to the previous formulations, the minimizer $\wt{\du} \in \R^{M-N}$ of this problem is equivalently given by the solution of the linear system
\begin{align}
  \label{eq:reducedLinearSystem}
  \wt{A}\wt{\du} = \wt{b}
\end{align}
where
\begin{align*}
  \wt{A} := \wt{Q}^T A \wt{Q}
  , \qquad \wt{b} := \wt{Q}^T\left(b - A\du^0\right)
  \text{.}
\end{align*}
The discrete solution vector of the original problem~$\du$ is related to the vector~$\wt{\du}$ via $\du = \wt{Q}\wt{\du} + \du^0$.

It is readily verified that the matrix~$\wt{A}$ is again symmetric positive definite.
Moreover, it is also sparse as of the sparsity of $A$ and $E$, and the assembling effort is comparable to the one for the original system matrix~$A$.
Hence, computation of $\du$ by solving the system~\eqref{eq:reducedLinearSystem} is numerically feasible and admits the use of typical solvers for symmetric positive definite matrices.

Since one is dealing with an equivalent reformulation of the original Galerkin scheme, the well-known C\'ea-Lemma is still applicable and yields the estimate
\begin{align*}
  \norm{u - u_h}_{H^2(\Omega_0)} \leq c \inf_{v \in \cS_\ast \cap \Uad} \norm{u - v}_{H^2(\Omega_0)} \leq c \norm{u - \Iu}_{H^2(\Omega_0)}
\end{align*}
for an $h$-independent constant $c \in \R_{>0}$ and an approximation function~$\Iu \in \cS_\ast \cap \Uad$ of the solution~$u$.
Therefore, given the regularity assumption $u \in H^r(\Omega_0)$ with $r \in [2,k+1]$, one expects for quasi-uniform grids the asymptotic convergence behavior
\begin{align*}
  \norm{u - u_h}_{H^2(\Omega_0)} \in \mathcal{O}(h^{r-2}) \text{ for } h \to 0\text{.}
\end{align*}

\begin{comment}
We consider the problem
\begin{align*}
  \min_{u \in \Uad(\p)} \frac{1}{2}a(u,u) - \ell(u)
\end{align*}
where $a$ is a symmetric elliptic bilinear form on $\Uad(\p)$ and $\ell \in H^2(\Omega)'$.
Existence and uniqueness of a solution $u^\ast = u^\ast(\p)$ is readily established by Lax--Milgram's theorem and the solution is characterized by the variational equation
\begin{align*}
  \forall{v \in \Uad^0(\p)\colon} \ a(u^\ast,v) = \ell(v)\text{.}
\end{align*}

Let $\S \subseteq H^2(\Omega)$ be a conforming finite element space and $\Sad \subseteq \Uad(\p)$ as well as $\Sad^0 \subseteq \Uad^0(\p)$ be conforming subspaces.
The Galerkin-approximation $u_h$ of $u^\ast$ with respect to $\S$ is given by the solution of
\begin{align*}
  \forall{v_h \in \Sad^0\colon} \ a(u_h,v_h) = \ell(v_h)\text{.}
\end{align*}
Equivalently, we may write $u_h = \sum_{i \in \cN} \overline{u}_i \varphi_i$ where $(\varphi_i)_{i\in\cN}$ is a basis of $\cS$ and determine the vector of coefficients $\overline{u}$ through solving the linear system
\begin{align*}
  C^TAC\overline{u}_0 = C^T(b-A\overline{u}_g)
\end{align*}
where $\overline{u}_g \in \Sad$ and $\overline{u} = \overline{u}_0 + \overline{u}_g$.
Here $A_{ij} = a(\varphi_i,\varphi_j)$, $b_i = \ell(\varphi_i)$, and $(\psi_j)_{j \in \cN_0}$ is a basis of $\Sad$ such that $\psi_j = \sum_{i\in\cN} C_{ij} \varphi_i$.
From the well-known C\'ea-Lemma we know that
\begin{align*}
  \norm{u^\ast-u_h}_{H^2(\Omega)} \leq C \inf_{v_h \in \Sad} \norm{u^\ast - v_h}_{H^2(\Omega)}
\end{align*}
where $C$ \TK{depends on the ellipticity constants of $a$, which are $\p$-independent here}.

\TK{To-do: Do we mention Bogner-Fox-Schmit somewhere?}
\TK{Also, what about a more precise error estimate, \ie for example when the point constraints are matched to the grid? And that it is actually pretty simple co construct these subspaces (which generally might be rather difficult and lead to sparse system matrices. Also important in our case that such a conforming discretization $\Sad$ is actually always possible, except for too coarse grids.}
\end{comment}
