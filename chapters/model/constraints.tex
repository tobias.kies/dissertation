\label{sec:modelConstraints}

Membrane--particle couplings
%in hybrid models are generally
are expressed via geometric constraints for the membrane surface which hold along an interface between membrane and particle.
This concept leads to a wealth of possibilities for the modeling of such couplings and reflects
%~ nicely
on the diversity of membrane--particle interactions in biology.
This view is in particular appropriate if the particles can be considered to be rigid and if such local deformations are enforced strongly.
%~ The consequential wealth of possibilities to model such couplings reflects on the diversity of interactions between biomembranes and various types of particles in biology.
Within the variational framework, the works \cite{ElGrHoKoWo16} and \cite{Hobbs16} give an overview of established and novel types of couplings for flat and spherical reference surfaces.

This section introduces two of those constraint types, namely \emph{point value constraints} and \emph{curve constraints}.
In comparison to the above-mentioned works an additional effort is made to put those constraints into a fully geometric perspective, which de-couples them from the specific choice of reference surface.
In order to relate the geometric particle objects and their constraints to a membrane surface, a formalism is introduced based on local projections.
Within this setting, particle degrees of freedom are then modeled through the direct application of a \emph{transformation function} to the geometric particle object.
For ease of presentation only one single particle is considered at first.

%~ This section begins by introducing various popular or potentially relevant constraint types, namely point value, curve, and averaged curve constraints.
%~ This section begins by introducing two popular or potentially relevant constraint types, namely point value and curve constraints.
%~ Those constraints are each initially stated in a general geometric setting and then reformulated to constraints for perturbation functions within the setting of parameterized representations.
%~ For ease of presentation, at first always only one single particle is considered.
%~ Afterwards also the relevant notation for position-dependent constraints and multiple particles is established.

\subsection{Point value constraints}
  \label{sec:pointValueConstraints}
  %This may for example be a reasonable model when the particle is a protein with very specific binding residues; an illustration of this is shown in 
  %~ An example application of point value constraints is the modeling of BAR-domain proteins coupling to biomembranes.
  %~ There the essential idea is that BAR-domain proteins have a small number of residues to which the membrane is expected to connect preferentially and which are of moderate size in comparison to the overall particle.
  As the name suggests, point value constraints prescribe the membrane shape in a finite number of points.
  An example application of such constraints is the modeling of filament-induced deformations of membranes.
  There the filament is essentially considered to be a rigid rod whose end is attached to the membrane.
  Thus, given a fixed position of the rod and assuming that its end is sufficiently small, it is justified to model such an interaction by simply prescribing the membrane position at a single point.
  Another slightly different example application is the modeling of BAR-domain proteins binding to biomembranes, where the protein is effectively rigid and where there is a small number of atoms which are likely to connect to the membrane.
  See especially \cref{sec:appFCHo2} later for further details on this in the context of a concrete application to the aggregation of FCHo2~F-BAR~domains.
  Both concepts are illustrated graphically in \cref{fig:pointValueConstraints}.
  
  \begin{figure}
    \begin{center}
      \hspace{1cm}
      \includegraphics[height=4cm,trim={0.1mm 3mm 0 3.5mm},clip]{02_filament.pdf}
      \hspace{1cm}
      \includegraphics[height=3cm,trim={0 3mm 0 3mm},clip]{03_BAR_domain.pdf}
    \end{center}
    \caption{Schematics of particle--membrane interactions modeled by point value constraints. Blue curves indicate membrane shape and red dots are constraint points. Left: Filament pushing against a membrane. Right: Residues of a BAR-protein binding to a membrane.
    %~ \TK{(TODO: Redo the filament picture; enhance constraint pictures in general.)}
    }
    \label{fig:pointValueConstraints}
  \end{figure}
  
  Mathematically, these situations are each described by a finite set $\cG \subseteq \R^3$ of discrete points that prescribe those areas of the particle to which the biomembrane binds.
  Point value constraints then require the surface~$\cM$ to contain all the points in $\cG$, which simply is stated as the geometric inclusion condition
  \begin{align*}
    \cG \subseteq \cM\text{.}
  \end{align*}
  %~ Another application of point value constraints is discussed in \cite{Hobbs16} where they are used to model the behavior of filaments pushing against a membrane.
  %~ \TK{TODO ADD FIGURES}
  Note that point value constraints are not to be confused with \emph{point curvature constraints}, which are treated to a greater extent for example in \cite{Hobbs16}.

  \subsubsection{Parameterization over reference set}
    Suppose a reference surface $\cM_0$ with a unit normal field~$\nu_0$ and a parameterization function~$\varphi\colon \Omega \rightarrow \cM_0$.
    Given a perturbation functions $u\colon \Omega \rightarrow \R$, which in turn induces a perturbed surface
    \begin{align*}
      \cM(u) := \left\{ \varphi(x) + u(x)\nu_0(\varphi(x)) \mid x \in \Omega \right\}\text{,}
    \end{align*}
    the condition $\cG\subseteq\cM(u)$ may be expressed in terms of $u$ alone.
    To this end, the \emph{local projection function}~$\pi\colon \R^3 \rightarrow \cM_0$ and the \emph{local distance function}~$d\colon \R^3 \rightarrow \R$ are introduced on $\cM_0$.
    For all $x$ in a sufficiently small neighborhood of $\cM_0$ they are uniquely defined by the property
    \begin{align*}
      x = \pi(x) + d(x)\,\nu_0(\pi(x))
      \text{.}
    \end{align*}
    %~ for all $x$ in a sufficiently small neighborhood of $\cM_0$.
    One then further arranges the points in $\cG$ as $\cG = \{x_1,\dots,x_N\}$ and defines
    \begin{align*}
      \wt{x}_i := \varphi^{-1}\left(\pi(x_i)\right)
      ,\qquad g_i := d(x_i)\text{.}
    \end{align*}
    The inclusion $\cG\subseteq\cM(u)$ is equivalently stated as
    \begin{align*}
      \forall{i\in\{1,\dots,N\}\colon}\ u(\wt{x}_i) = g_i\text{,}
    \end{align*}
    or even shorter with $\Gamma := \{\wt{x}_1,\dots,\wt{x}_N\}$ and $g:=(g_1,\dots,g_N) \in \R^N$ as
    \begin{equation*}
      u|_{\Gamma} = g\text{.}
    \end{equation*}
    It quickly becomes evident that this representation need not be well-defined for general point sets~$\cG$.
    Problems occur in particular when $\pi(x_i) = \pi(x_j)$ holds for $i\neq j$ or when an $x_i$ is outside the domain of definition of $\pi$ or $d$.
    Therefore, well-definedness is imposed as an assumption, which is reasonable if the particle described by $\cG$ has an appropriate orientation relative to the reference membrane and induces only a small deformation.


\subsection{Curve constraints}
  \label{sec:curveConstraints}
  Curve constraints are typically used when the interface between particle and membrane forms a simple closed curve $\cG \subseteq \R^3$.
  An example of this are transmembrane proteins with a so-called hydrophobic belt that forms the interface to which the membrane's lipids are likely to attach.
  Usually it is assumed that the structure of the belt furthermore prescribes a fixed slope for the membrane along the interface curve~$\cG$.
  Mathematically, the inclusion condition is directly stated as
  \begin{align*}
    \cG \subseteq \cM\text{.}
  \end{align*}
  The slope-condition is interpreted such that for every interface point~$x \in \cG$ a two-dimensional vector space $\wt{T}_x\cG \subseteq \R^3$ must be given which also contains the tangent space of $\cG$, \ie $T_x\cG \subseteq \wt{T}_x\cG$ holds.
  The slope-constraint is then stated by prescribing the tangent spaces of $\cM$ along the interface $\cG$ as
  \begin{align*}
    \forall{x \in \cG\colon} \ T_x\cM  = \wt{T}_x\cG
    \text{.}
  \end{align*}
  Further examples for the application of curve constraints are the modeling of scaffolding particles or wrapped nanoparticles.
  While for the case of transmembrane proteins the membrane area ``inside'' the curve $\cG$ is not present at all, these two other models would at first prescribe the membrane shape to be fixed on the inside.
  However, due to the fixed shape the total bending energy in that area remains constant over all feasible membrane shapes and is therefore negligible during energy minimization.
  Hence, scaffolding and wrapping interactions may equivalently be modeled by curve constraints.
  An illustration of these concepts is sketched in \cref{fig:curveConstraints}.

  \begin{figure}
      \includegraphics[width=0.3\textwidth,height=2cm]{04_inclusion.pdf}
      \hfil
      \includegraphics[width=0.3\textwidth,height=2cm,trim={0 1mm 0 1mm},clip]{05_nano_particle.pdf}
      \hfil
      \includegraphics[width=0.3\textwidth,height=2cm]{06_scaffold.pdf}
    \caption{Schematics of objects interacting with a lipid bilayer leading to curve constraints. Blue curves indicate membrane shape, red dots are constraint points, and red lines show slope constraints. Left: Membrane inclusion. Middle: Partially wrapped nanoparticle. Right: Membrane scaffold.}
    \label{fig:curveConstraints}
  \end{figure}

  \begin{comment}
  Such a description is often used to model the interaction of transmembrane proteins or nano-particles with the membrane, but
  it can also be regarded as a type of coarse-graining of point curvature constraints.
  Note that in those cases there is a part ``inside'' the curve $\cG$ where the membrane either is not present at all due to displacement, or its shape is determined by adhesion to the particle.
  Since there consequently is no or only a constant energy contribution from that inside part, it may be entirely neglected during minimization over the space of feasible membranes.
  In particular, and with regard to a perturbation approach with a reference surface $\cM_0$ and parameterization over a reference set $\Omega$, this implies that only functions over a reduced domain $\hat{\Omega} \subseteq \Omega$ need to be considered.
  See \TK{TODO FIGURE} for an illustration.
  \end{comment}
  
  \subsubsection{Parameterization over reference set}
    As before, suppose a reference surface~$\cM_0$ is given with a parameterization~$\varphi\colon\Omega\rightarrow\cM_0$ and the local projection and distance functions $\pi$ and $d$, respectively.
    For notational convenience the interface~$\cG$ is extended locally to a hypersurface~$\wt{\cG} \supseteq \cG$ such that the equality $T_x\wt{\cG} = \wt{T}_x\cG$ is true for all $x \in \cG$.
    Given $\wt{\cG}$, the \emph{inverse local projection~$\copi$ with respect to~$\wt{\cG}$} is characterized by the property
    \begin{align*}
      \copi(x) = x + d(\copi(x)) \nu_0(x) \in \wt{\cG}
      %~ \text{.}
    \end{align*}
    for $x$ in a sufficiently small neighborhood of $\pi(\cG)$.
    %~ for $x \in \cM_0$ and is assumed to be well-defined in a neighborhood of $\pi(\cG)$.
    With a unit normal vector field~$\nu$ on the parameterized interface~$\Gamma := \varphi^{-1}(\pi(\cG))$ and
    \begin{align*}
      %~ \Gamma :=  \varphi^{-1}(\pi(\cG))
      %~ , \qquad
      g_1 := d \circ \copi \circ \varphi
      , \qquad g_2 := \nabla g_1 \cdot \nu
    \end{align*}
    the geometric curve constraints are equivalently expressed in terms of $u$ by
    \begin{align*}
      u|_{\Gamma} = g_1, \qquad \partial_\nu u|_{\Gamma} = g_2\text{.}
    \end{align*}
    As for point value constraints, such a formulation needs not exist in general, especially when $\pi$ is not injective on $\cG$.
    Therefore, well-definedness is again an assumption, which is plausible in the case of small deformations and compatible particle orientations.

    \begin{comment}
    \myhrule
    Again, assume existence of a set $\wt{\Gamma} \subseteq \cM_0$ and a function $\wt{\gamma}\colon \wt{\Gamma} \rightarrow \R$ such that the map $\wt{\Gamma} \ni x \rightarrow x + \wt{\gamma}(x)\nu_0(x) \in \cG$ is bijective.
    Let $\Gamma := \varphi^{-1}(\wt{\Gamma})$ and $\gamma := \wt{\gamma} \circ \varphi$.
    Denote by $B$ the interior of $\Gamma$ and define $\Omega_B := \Omega \setminus B$.
    As remarked above, the area $B$ is to be neglected for perturbations and therefore only perturbation functions $u\colon \Omega_B \rightarrow \R$ are considered.
    The condition $\cG\subseteq \cM(u)$ is immediately expressed in terms of $u$ as $u|_{\Gamma} = \gamma$.
    
    \snote{Order the following in a nicer way; state the assumptions directly after definitions. Then do conclusions.}
    Concerning the tangent condition, suppose that $\wt{T}_x\cG = \im(A(\phi^{-1}(x)))$ where $A(\phi^{-1}(x)) \in \mathbb{R}^{3\times 2}$.
    We make the assumption that $\gamma$ can be extended and is differentiable in a neighborhood of $\Gamma$ and that
    \begin{align*}
      \wt{T}_x\cG = \spn\left\{ \left(D\varphi + \gamma D\wt{\nu}_0 + \wt{\nu}_0 \otimes \nabla \gamma\right) \tau_\Gamma , \left(D\varphi + \gamma D\wt{\nu}_0\right)\nu_{\Gamma} + \wt{\nu}_0 s \right\}
    \end{align*}
    for some $s\in\R$, where $\tau_{\Gamma}$ and $\nu_{\Gamma}$ are tangent and normal vectors of $\Gamma$ in $\R^2$, respectively, and which means that the tangent space $\wt{T}_x\cG$ may not be parallel to the line that is spanned by $\wt{\nu}_0$.\snote{True?}
    From the condition $\cG \subseteq \cM(u)$ we already know $D\phi\tau_{\Gamma} = \left(D\varphi + \gamma D\wt{\nu}_0 + \wt{\nu}_0 \otimes \nabla \gamma\right) \tau_\Gamma \in T_x\cM(u)$, therefore equality of the tangent spaces follows from the constraint $\partial_{\nu_\Gamma} u = s$.

    In order to obtain a formula for $s$, assume that $C := D\varphi + \gamma D\wt{\nu}_0$ has full rank.\snote{When would this fail?}
    Then, by assumption, for some $\mu \in \R^2$ and an invertible $X \in \R^{2\times 2}$ holds
    \begin{align*}
      A & = (C + \wt{\nu}_0 \otimes \mu)X
    \end{align*}
    where $\mu_2 = s$.
    From regularity of $C^TC$ and $C^T\wt{\nu}_0 = 0$ we infer
    \begin{align*}
      C^TA = C^TCX \ \Longrightarrow \ X = (C^TC)^{-1} C^TA
    \end{align*}
    and therefore also
    \begin{align*}
      s = e_2^T\mu & = e_2^T(C^T + \mu \otimes \wt{\nu}_0)\wt{\nu}_0
        = e_2^TX^{-T}A^T\wt{\nu}_0
        = e_2^T(C^TC)(A^TC)^{-1}A^T\wt{\nu}_0
        \text{.}
    \end{align*}
    Altogether, the constraints read
    \begin{align*}
      u|_{\Gamma} & = \gamma
      \\ \partial_{\nu_\Gamma} u|_{\Gamma} & = e_2^T(C^TC)(A^TC)^{-1}A^T\wt{\nu}_0
      \text{.}
    \end{align*}
    Note again that the above assumptions do not always hold, but that they are fulfilled if the particle only induces a ``small'' perturbation relative to the reference surface $\cM_0$.
    In that case surfaces that fulfill the constraint can again be represented as perturbations of $\cM_0$.
    \end{comment}


\subsubsection{Out of scope: Averaged curve constraints}

  Within the Monge-gauge setting, averaged curve constraints have been introduced in \cite{ElGrHoKoWo16} as a simplifying approximation of classical curve constraints with a natural connection to so-called point curvature constraints.
  %~ \snote{Is this true? Or have they been used somewhere else already?}
  As such they also might play a future role in linking discrete--continuum curve constraint based models and fully continuum particle--membrane models.
  Besides their analytic properties, it is also conceivable that averaged constraints can be a direct consequence from physical modeling steps.
  
  The curve constraints are averaged with respect to a reference manifold~$\cM_0$.
  Here they are immediately stated in their parameterized formulation in order to avoid additional notation.
  Using the previously established notation for curve constraints, the averaged curve constraints read
  \begin{align*}
    \int_{\Gamma} u(x) \d{x} = \int_{\Gamma} g_1(x) \d{x}, \qquad \int_{\Gamma} \partial_\nu u(x) \d{x} = \int_{\Gamma} g_2(x) \d{x}\text{.}
  \end{align*}
  This type of constraint is different from the previous ones as it is not directly expressed in terms of a subset evaluation or the trace of $u$, but rather applies a further linear operator to the point evaluation or trace operator.
  The theory developed in the subsequent chapters applies only to pure evaluation and trace type constraints and therefore averaged constraints are excluded from the following model framework.
  However, as these constraints are at least closely related to the pure trace operator, they can be considered a prototypical reference model
  %and outlook
  for future extensions.
  
  \begin{comment}
  \begin{remark}[Hierarchy of couplings and average curve constraints]
    \TK{TODO REMARK: Could mention here average constraints and how they would break the Gauss--Bonnet assumption. Also the hierarchical structure could be interesting.}
    
    \TK{TODO ALSO: Generalization to $n$ particles is clear. -- maybe note that somewhere else}
  \end{remark}

  \snote{Remark on the hierarchy of couplings?}
  \end{comment}
%~ \subsection{Average curve constraints}
  %~ Average constraints add another level of coarsification by only requiring that constraints are fulfilled \emph{on average}.
  
  %~ \TK{TODO Check what exactly was meant by average constraints.}
  
  %~ \TK{TODO: Mini-Sentence that says what average constraints look like in the parameterized setting.}

\subsection{Parametric particle configurations}
  \label{sec:parametricConfigurations}
  Besides a description of the membrane energy and the membrane--particle coupling, a fully functional model for membrane--particle interactions also needs to describe the degrees of freedom associated to each particle.
  The arguably most intuitive choice for particle degrees of freedom is to enable each particle to take various positions and orientations within space.
  However, in some other relevant situations additional parameterizations of the particle state may come into play, for example by incorporating a parameter for a particle's size or for the angle at which the membrane connects to the particle.
  
  In order to reflect the resulting flexibility in particle modeling, it makes sense to treat interaction models in a framework that is largely able to cover such possibilities as well.
  To this end, the general requirement in this thesis is that there exists a smooth \emph{particle transformation function}
  \begin{align*}
    \Psi\colon \R^k \times \R^3 & \rightarrow \R^3, \qquad (p,x) \mapsto \Psi(p;x)
  \end{align*}
  such that every $\Psi(p;\cdot)$ is a bijection which maps a reference particle set $\cG_0$ (or analogously a reference hypersurface $\wt{\cG}_0$ for curve constraints) to a parameterized counterpart
  \begin{align*}
    \cG(p) := \Psi(p;\cG_0)\text{.}
  \end{align*}
  Here $p \in \R^k$ denotes the \emph{particle state}.
  Correspondingly, the parameterization of particles also induces $p$-dependent constraints, which in addition depend on the chosen membrane--particle coupling.
  Within the parameterized setting for curve constraints this furthermore implies $p$-dependent reference domains $\Omega(p) \subseteq \Omega$ since ``inside'' areas of the particle are excluded from the computation.
  %~ While also discrete state variables could be considered at this point, t
  %~ Note that in principle discrete state variables could be considered as well, which however is not considered in this thesis.
  
  The extension of this concept to a finite and fixed number of $N \in \N$ particles is straight-forward.
  In that context, a \emph{particle configuration} $\p = (\p_i)_{i=1,\dots,N} \in (\R^k)^N$ is a collection of $N$ particle states.
  In general, not all particle configurations are admissible, either due to physical restrictions or limits in the model.
  Therefore, feasible configurations are limited to a \emph{configuration space}~$\cD \subseteq (\R^k)^N$.
  This leads to a $\p$-dependent set of constraints and a \emph{parametric reference domain}~$\Omega(\p) := \bigcap_{i=1}^N \Omega(\p_i)$, if applicable.

  \begin{example}[Variable positions and orientations]
    \label{thm:variablePositionOrientation}
    As mentioned above, a popular choice for the particle transformation function would allow particles to move and rotate freely in space.
    One way of modeling this is given by
    \begin{align*}
      \Psi\colon \R^6 \times \R^3 & \longrightarrow \R^3
      \\ (p,x) & \longmapsto R_3(p_6)R_2(p_5)R_1(p_4) x + \mat{p_1\\p_2\\p_3}
      \text{.}
    \end{align*}
    Here the $R_i$ are the rotation matrices around the $x_i$-axes, \ie
    \begin{align*}
      R_i(\alpha)x := (e_i \otimes e_i)x + \cos(\alpha) (e_i \times x) \times e_i + \sin(\alpha) (e_i\times x)
    \end{align*}
    where $e_i$ is the $i$-th unit vector.
    Hence, the parameters $p_1$, $p_2$ and $p_3$ determine a particle's translation in space, and $p_4$, $p_5$ and $p_6$ set the orientation.
    Note that the order of the rotation matrices matters and that this choice works best if the base state $\cG_0$ is centered around the origin as much as possible.
  \end{example}

  \begin{comment}
  Up to now we only considered a particle with fixed position and orientation in space.
  As a particle is in principle free to move in space and its optimal location is not known a priori, it is necessary to introduce a position-dependence of the coupling constraints.
  To this end, suppose that a reference set $\cG_0 \subseteq\R^3$ is given, which may either be a set of points or a curve in the sense of point value constraints or curve constraints, respectively.
  In order to parameterize the particle position we introduce the map
  \begin{align*}
    \Psi\colon \R^6 \times \R^3 & \longrightarrow \R^3
    \\ (p,x) & \longmapsto R_3(p_6)R_2(p_5)R_1(p_4) \left( x + \mat{p_1\\p_2\\p_3} \right)
  \end{align*}
  where the matrices defined by
  \begin{align*}
    R_i(\alpha)x := (e_i \otimes e_i)x + \cos(\alpha) (e_i \times x) \times e_i + \sin(\alpha) (e_i\times x)
  \end{align*}
  \snote{Choose nicer representation.}
  are the counter-clockwise rotation matrices around the $x_i$-axes by angle $\alpha$.
  For later notational convenience we define the \emph{composed rotation matrix}
  \begin{align*}
    R(p) := R_3(p_6)R_2(p_5)R_1(p_4)\text{.}
  \end{align*}
  The $\Psi(p)$ describe arbitrary translations and rotations and are able to describe arbitrary positions and rotations of objects in $\R^3$.
  Given $p \in \R^6$, the parameterized reference set is defined by
  \begin{align*}
    \cG(p) := \Psi(p;\cG_0)
    \text{,}
  \end{align*}
  which immediately leads to the $p$-dependent constraint
  \begin{align*}
    \cG(p) \subseteq \cM
  \end{align*}
  and, if $\cG_0$ is a curve, also to
  \begin{align*}
    T_{\Psi(p;x)}\cM  = R(p)\wt{T}_x\cG_0, \qquad x \in \cG_0
    \text{.}
  \end{align*}
  The parameterization of these constraints with respect to a reference domain is done analogously to the $p$-independent case.
  As before, the assumptions that necessarily have to hold for a well-defined parameterization are not trivially fulfilled and consequently a parameterization is in general not possible for all $p \in \R^6$.
  If the deformation induced by $\cG_0$ is not too large, there is a range of translations and orientations $p$ for which the parameterization is possible.
  \end{comment}
  
  \begin{comment}
  Suppose a reference set $\cG_0 \subseteq\R^3$, which may either be a set of points or a curve (in the sense of curve constraints) and suppose $\Gamma_0 \subseteq\R^2$ and $\gamma_0\colon \Gamma_0 \rightarrow \R$ such that $(\id,\gamma_0)(\Gamma_0) = \cG_0$.
  We parameterize the particle's position over $p \in \R^6$ using the translation--rotation function that is defined by
  \begin{align*}
    \Psi(p;x) = R_3(p_6)R_2(p_5)R_1(p_4)\left( x + \mat{p_1 & p_2 & p_3}^T \right)
  \end{align*}
  where $R_i(\alpha)$ is the counter-clockwise rotation matrix around the $x_i$-axis by the angle $\alpha$.
  The $p$-dependent particle position is then given by
  \begin{align*}
    \cG(p) := \Psi(p;\cG_0)\text{.}
  \end{align*}
  Using local coordinates $(\pi,d)$\snote{todo:introduce}, we have
  \begin{align*}
    \Gamma(p) = \pi \circ \Psi(p) \circ (\id,\gamma_0) (\Gamma_0)\text{,}
  \end{align*}
  and under the assumption that there exists a well-defined function $\wt{\gamma}(p) \colon \pi(\cG) \rightarrow \R$ such that $\wt{\gamma}(p;\pi(x)) = d(x)$ for all $x \in \cG(p)$ we have $\gamma(p) = \wt{\gamma}(p) \circ \varphi|_{\Gamma(p)}$.

  Similarly, if $\cG_0$ is a curve and therefore also a reference extended tangent bundle $\wt{T}\cG_0$ is given, we use the map
  \begin{align*}
    \Psi'(p;x) = R_3(p_6)R_2(p_5)R_1(p_4)
  \end{align*}
  to define
  \begin{align*}
    \wt{T}\cG(p) := \Psi'(p; \wt{T}\cG_0)\text{.}
  \end{align*}
  \end{comment}
