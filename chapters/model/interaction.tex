Up to this point, various ways to model the membrane energy and membrane--particle interactions have been discussed, including variable particle configurations.
In summary, this motivates a framework for \emph{parameterized models for membrane-mediated particle interactions} where the following \emph{model decisions} are required:
\begin{itemize}
  \item
    A \emph{reference surface}~$\cM_0$ together with a \emph{parameterization}~$\varphi\colon \Omega \rightarrow \cM_0$ over a \emph{parameterization domain}~$\Omega\subseteq\R^2$.
  
  \item
    A \emph{parameterized elastic energy}~$\E(\Omega)$. In the context of this thesis the energy is assumed to be of the form
    \begin{align}
      \label{eq:energyForm}
      \E(\Omega;u) := \int_{\Omega} \rho(x, u(x), Du(x), D^2u(x)) \d{x}
    \end{align}
    with an integrand~$\rho\colon \Omega \times \R \times \R^2 \times \R^{2\times 2} \rightarrow \R$.
    Further assumptions on~$\rho$ are stated where necessary.
    
  \item
    A \emph{reference space of admissible membranes}~$H^2_\ast(\Omega) \subseteq H^2(\Omega)$ which incorporates particle-independent Dirichlet constraints or periodic boundary conditions.
    %~ In this work it is assumed that $H_\ast(\Omega) \subseteq H^2(\Omega)$ holds.
    
  \item
    A \emph{reference particle geometry}~$\cG_0$ (and possibly also $\wt{\cG}_0$) in the sense of point value constraints in \cref{sec:pointValueConstraints} or curve constraints in \cref{sec:curveConstraints}.
    
  \item
    A \emph{particle transformation}~$\Psi\colon \R^k \times \R^3 \rightarrow \R^3$ in the sense of \cref{sec:parametricConfigurations}.
    
  \item
    A fixed number $N \in \N$ of particles together with a \emph{configuration space}~$\cD \subseteq \R^{N \times k}$.
\end{itemize}
The entirety of these decisions defines a particle--membrane interaction model.
Given a \emph{particle configuration}~$\p \in \cD$ they induce the following quantities:
\begin{itemize}
  \item
    \emph{Parametric particles}~$\cG(\p_i) := \Psi(\p_i,\cG_0)$, and also $\wt{\cG}(\p_i) := \Psi(\p_i,\wt{\cG}_0)$ in the case of curve constraints.

  \item
    \emph{Parametric interfaces}~$\Gamma_i(\p_i) := \varphi^{-1}(\pi(\cG(\p_i)))$ together with the joint interface $\Gamma(\p) := \bigcup_{i=1}^N \Gamma_i(\p_i)$.
    These implicitly rely on the well-definedness of the local projection and distance functions, see also \cref{sec:modelConstraints}.
    It is assumed that the $\Gamma_i(\p_i)$ are pairwise disjoint.
    
  \item
    In case of curve constraints the \emph{parametric domain}~$\Omega(\p) := \Omega \setminus \bigcup_{i=1}^N B_i(\p_i)$ where $B_i(\p_i)$ denotes the interior of the curve $\Gamma_i(\p_i)$.
    For simplicity of notation, let $\Omega(\p) := \Omega$ in the case of point value constraints.
    
  \item
    \emph{Parametric trace operator}~$T(\p)$. In case of point value constraints this is given by the canonical point evaluation operator
    \begin{align*}
      T(\p)\colon H^2(\Omega(\p)) & \rightarrow \R^{\abs{\Gamma(\p)}}
      ,\quad u \mapsto \left(u(x)\right)_{x \in \Gamma(\p)}
      \text{,}
    \end{align*}
    which is well-defined in view of the Sobolev embedding $H^2(\Omega(\p)) \rightarrow C(\Omega)$, see \eg
    %~ \cite{Evans10}.
    \cite[Theorem 5.4]{Adams75}.
    In case of curve constraints the operator corresponds to the classical trace operator
    \begin{align*}
      T(\p)\colon H^2(\Omega(\p)) & \rightarrow H^{3/2}(\Gamma(\p)) \times H^{1/2}(\Gamma(\p))
      , \quad u \mapsto (u|_{\Gamma(\p)}, \partial_\nu u|_{\Gamma(\p)})
    \end{align*}
    where $\nu$ is the outer unit normal vector field on $\Gamma(\p)$.
    See \eg~\cite[Chapter~1.5]{Grisvard85} for detailed definitions and results on well-definedness for sufficiently smooth interfaces~$\Gamma(\p)$.
    
  \item
    \emph{Parametric constraints}~$g_i(\p)$.
    For point value constraints one has $g_i(\p) \in \R^{\abs{\Gamma_i(\p_i)}}$ as defined in \cref{sec:pointValueConstraints}.
    For curve constraints the definition is given in \cref{sec:curveConstraints} and regularity $g_i(\p) \in H^{3/2}(\Gamma_i(\p_i)) \times H^{1/2}(\Gamma_i(\p_i))$ is assumed.
    The associated \emph{joint constraints} are defined by
    \begin{align*}
      g(\p) := (g_i(\p_i))_{i=1,\dots,N} \in \R^{\abs{\Gamma(\p)}}
    \end{align*}
    or
    \begin{align*}
      g(\p;x) := g_i(\p;x) \ \text{ for } x \in \Gamma_i(\p_i)
      \text{,}
    \end{align*}
    respectively.
    These quantities rely on the local projection and distance functions.
    
  \item
    \emph{Parametric set of admissible membranes}~$\Uad(\p)$, defined by
    \begin{align*}
      \Uad(\p) := \left\{ v \in H_\ast^2(\Omega(\p)) \mid T(\p;v) = g(\p) \right\}
      \text{,}
    \end{align*}
    and the corresponding \emph{homogeneous set of admissible membranes}~$\Uad^0(\p)$, given by
    \begin{align*}
      \Uad^0(\p) := \left\{ v \in H_\ast^2(\Omega(\p)) \mid T(\p;v) = 0 \right\}
      \text{.}
    \end{align*}
    Here $H^2_\ast(\Omega(\p))$ is to be understood as the image of the canonical restriction operator from $\Omega$ to $\Omega(\p)$ over the space $H^2_\ast(\Omega)$.
    
  \item
    \emph{Parametric elastic energy}~$\E(\p)$, given by
    \begin{align*}
      \E(\p,u) := \E(\Omega(\p),u) = \int_{\Omega(\p)} \rho(x,u(x),Du(x),D^2u(x)) \d{x}
    \end{align*}
    for all $u \in H^2(\Omega(\p))$.
    
  \item
    For further notational convenience it is assumed that the parametric energy~$\E(\p)$ has a unique minimizer over the set of admissible membranes~$\Uad(\p)$.
    Correspondingly, the \emph{(optimal) stationary membrane}~$u(\p)$ is defined by
    \begin{align*}
      u(\p) := \argmin_{v \in \Uad(\p)} \E(\p;v)
      \text{.}
    \end{align*}
\end{itemize}
In the special case that $\E(\p)$ induces an elliptic linear operator, the unique existence of the stationary membrane need not be assumed and is a direct consequence of the well-known theorem of Lax--Milgram instead:
 %~ \TK{REF?}.
\begin{theorem}[Unique optimal membrane shapes for quadratic elliptic energies]
  \ \newline Let $\p \in \cD$ and suppose a non-empty admissible set $\Uad(\p)$.
  Suppose furthermore that the first variation $\delta \E(\p;u)$ of $\E(\p;\cdot)$ is linear in $u$, and that the second variation $\delta^2\E(\p)$ is elliptic over $\Uad^0(\p)$.
  Then $u(\p)$ is well-defined.
\end{theorem}
Putting everything together,
%~ the main object of interest,
the \emph{interaction potential~$\EI$} is defined as
\begin{align*}
  \EI\colon \cD & \rightarrow \R
  ,\qquad \p \mapsto \E(\p;u(\p)) = \inf_{v \in \Uad(\p)} \E(\p;v)
  \text{,}
\end{align*}
and an element $\p^\ast \in \cD$ is called an \emph{optimal configuration} if and only if
\begin{align*}
  \p^\ast \in \argmin_{\p \in \cD} \EI(\p)
\end{align*}
holds true.
For the remainder of this thesis, existence of at least one optimal configuration is postulated.
%While it is relatively simple in many cases to prove existence of at least one optimal configuration for suitable choices of $\cD$ and $\E$, the existence of such a configuration is simply postulated for the remainder of this thesis for the sake of generality.
Note however
%~ It is explicitly emphasized
that uniqueness need not hold and is even ruled out in many cases due to symmetries in the configuration space.

\begin{comment}
So far, various ways to model the membrane energy and the membrane--particle interactions have been presented, including variable particle configurations.
In combination with the variational framework from \cite{ElGrHoKoWo16}, this motivates the following mathematical description of discrete--continuum models for particle--membrane interactions in a parameterized formulation:

Suppose $N \in \N$ particles with $k \in \N$ state variables each and the configuration space~$\cD \subseteq (\R^k)^N$.
For notational simplicity all particles are supposed to be of equal type.
Given a configuration $\p \in \cD$, the particle interfaces are defined by
\begin{align*}
  \Gamma_i(\p_i) := \varphi^{-1}(\pi(\cG(\p_i))),
  %\qquad g_i := d(\cG(\p_i)),
  \qquad i = 1,\dots,N\text{.}
\end{align*}
%~ and curve-based constraints induce curve interfaces
%~ \begin{align*}
  %~ \Gamma_i(\p_i) := \varphi^{-1}(\pi(\cG(\p_i))),
  %~ %\qquad g_i^1 := d\circ \copi_i\circ\varphi,\qquad g_i^2 := \nabla g_1 \cdot \nu
  %~ \qquad i = 1,\dots,N\text{.}
%~ \end{align*}
It is assumed that the $\Gamma_i(\p_i)$ are pairwise disjoint for every $\p \in \cD$
and the overall joint interface is denoted by $\Gamma(\p) := \bigcup_{i=1}^N \Gamma_i(\p_i)$.
As before, $\Omega(\p) \subseteq \Omega$ is the parameterization domain induced by the $\Gamma_i$.

If point constraints are used for the particles, then
\begin{align*}
  T(\p)\colon H^2(\Omega(\p)) & \longrightarrow \R^{\abs{\Gamma(\p)}}
  \\ u & \longmapsto (u(x))_{x \in \Gamma(\p)}
\end{align*}
is defined to be the point evaluation operator.
Else, in case of curve
%type
constraints, 
\begin{align*}
  T(\p)\colon H^2(\Omega(\p)) & \longrightarrow H^{3/2}(\partial\Omega(\p)) \times H^{1/2}(\partial\Omega(\p))
  \\ u & \longmapsto (u|_{\Gamma(\p)}, \partial_\nu u|_{\Gamma(\p)})
\end{align*}
is defined as trace operator where $\nu$ is the outer unit normal with respect to $\Omega(\p)$.
\snote{TODO Well-definedness by Sobolev embedding or trace definition}

The full parameterized constraints are stated as
\begin{align*}
  %~ G(\p)T(\p;u)  & = g(\p)
  T(\p;u)  & = g(\p)\text{.}
\end{align*}
%~ where $G(\p)$ is a linear bounded operator over the range of $T(\p)$ and $g(\p) \in \operatorname{range}(G(\p))$.
\snote{NEED TO DEFINE $g$ still. Or give examples.}
\snote{Smoothness assumptions on $G$ and $g$?}
\snote{TODO CONSTRAINTS ($g$ and $G$)}
%~ Here $\copi_i$ is the inverse local projection with respect to $\wt{\cG}(\p_i)$ and $\nu$ is the outer unit normal on $\partial\Omega(\p)$.
%~ The joint constraints right hand side for point constraints is given by $g := (g_i)_{i=1,\dots,N}$.
%~ For curve type constraints it is given by $g(x) = (g_i^1(x), g_i^2(x))$ for $x \in \Gamma_i(x)$.
%~ \TK{Under smoothness assumptions} this yields an element $g \in H^{3/2}(\Gamma) \times H^{1/2}(\Gamma)$.

Suppose a closed linear subspace $\hat{H} \subseteq H^2(\Omega)$ which encodes $\p$-independent constraints in the model.
The set of admissible membrane shapes at the configuration $\p$ is
\begin{align*}
  %~ \Uad(\p) := \left\{ u \in \hat{H} \cap H^2(\Omega(\p)) \mid G(\p)T(\p;u) = g(\p)\right\}
  \Uad(\p) := \left\{ u \in \hat{H} \cap H^2(\Omega(\p)) \mid T(\p;u) = g(\p)\right\}\text{.}
\end{align*}
where $\hat{H}\cap H^2(\Omega(\p))$ has to be understood as a subset of $H^2(\Omega(\p))$ by virtue of the canonical restriction operator.

The membrane energy is assumed to be a continuous functional
\begin{align*}
  \E(\p)\colon \hat{H} \cap H^2(\Omega(\p)) & \longrightarrow \R
  ,\quad u \longmapsto \int_{\Omega(\p)} \rho\left(x,u(x),\nabla u(x), D^2 u(x)\right) \d{x}
\end{align*}
where $\rho$ is a model specific integrand.
\snote{Could be even more general energies... with purely $p$ dependent parts or curve integrals}

The upcoming parts assume for convenience that there exists a unique optimal membrane shape
\begin{align*}
  u(\p) := \argmin_{v \in \Uad(\p)} \E(\p,v)
\end{align*}
for each $\p \in \cD$.
In the special case that $\E(\p)$ induces an elliptic linear operator for every $\p$, this property is a direct consequence of the well-known theorem of Lax--Milgram:

\begin{theorem}[Unique optimal membrane shapes of quadratic elliptic energies]
  Let $\p \in \cD$.
  Suppose that the first variation $\delta \E(\p)$ of $\E(\p)$ is linear in $u$, and that the second variation $\delta^2\E(\p)$ is elliptic over the linear space
  \begin{align*}
    \left\{ u \in \hat{H} \cap H^2(\Omega(\p)) \mid T(\p;u) = 0\right\} \subseteq H^2(\Omega(\p)) \text{.}
  \end{align*}
  If $\Uad(\p)$ is nonempty, then $u(\p)$ is well-defined.
\end{theorem}

The interaction potential with respect to $\E$ under the given constraints is defined by
\begin{align*}
  \EI\colon \cD & \longrightarrow \R
  ,\qquad \p \longmapsto \E(\p,u(\p)) = \inf_{u \in \Uad(\p)} \E(\p;u)\text{,}
\end{align*}
and optimal particle configurations $\p^\ast \in \cD$ are characterized by
\begin{align*}
  \p^\ast \in \argmin_{\p\in\Lambda} \EI(\p)\text{.}
\end{align*}
While existence of an optimal configuration is readily obtained under suitable assumptions, a minimizer of $\EI$ is in general not unique.

\TK{TODO: State that this is the very important item. Motivate simple gradient type approaches. State that optimal membrane shapes are obtained from solving a PDE.}
\end{comment}



\begin{comment}
The interaction potential plays an important role for the investigation of membrane-mediated particle interactions as it describes the minimal membrane energy that is achievable under the constraints induced by a given particle configuration.

In order to make this idea rigorous, we first introduce some notation.
In the following we only consider perturbations of a reference surface $\cM_0 \subseteq \R^3$ which again is parameterized over a domain $\Omega \subseteq \R^2$.
Also, for simplicity of notation, we restrict ourselves to curve constraints and point constraints.
The extension to average constraints is straight-forward.


Let $N \in \mathbb{N}$ particles be given as well as a \emph{configuration space $\cD \subseteq \R^{N\times 6}$} that describes the set of admissible combinations of particle positions and orientations.
Given a fixed \emph{particle configuration} $\p = (\p^i)_{i=1}^N \in \cD$, let $\Omega(\p) \subseteq \Omega$ denote the associated \emph{reference domain} and let $\E(\p)\colon \hat{H}(\p) \rightarrow \R$ a not necessarily linearized \emph{$\Omega(\p)$-parameterized energy} where $\hat{H}(\p) \subseteq H^2(\Omega(\p))$ is a linear subspace incorporating problem-dependent boundary conditions or other constraints that are not induced by particles.

For each $i\in\{1,\dots,N\}$, let $\Gamma_i(\p^i) \subseteq \Omega(\p)$ the \emph{parameterization domain} of the interface of the $i$-th particle with the membrane.
If point constraints are considered, then $\Gamma_i(\p^i) = \{x^i_1,\dots,x^i_{m_i}\}$ and we define the \emph{point evaluation operator}
\begin{align*}
  T^i(\p)\colon H^2(\Omega(\p)) & \longrightarrow \R^{m_i}
  \\ u & \longmapsto (u(x^i_j))_{j = 1,\dots,m_i}
\end{align*}
and let $g^i(\p) \in \R^{m_i}$ the parameterized constraint right hand side.
Note that those $T^i(\p)$ are well-defined as of the Sobolev-embedding $H^2(\Omega(\p)) \rightarrow C^0(\Omega(\p))$.
Similarly, in case of curve constraints, we define the \emph{trace operator}
\begin{align*}
  T^i(\p)\colon H^2(\Omega(\p)) & \longrightarrow H^{3/2}(\Gamma_i(\p^i)) \times H^{1/2}(\Gamma_i(\p^i))
  \\ u & \longmapsto \left(u|_{\Gamma_i(\p^i)}, \partial_{\nu} u|_{\Gamma_i(\p^i)}\right)
\end{align*}
and let $g^i(\p) \in H^{3/2}(\Gamma_i(\p^i)) \times H^{1/2}(\Gamma_i(\p^i))$ the parameterized constraint right hand side.
Here $\nu$ denotes the outer unit normal on $\partial\Omega(\p)$.
\TK[Maybe reference somewhere trace operators]{Under suitable smoothness assumptions on $\Gamma_i$}, the operators $T^i(\p)$ are again well-defined and bounded.

Defining $T(\p) := (T^i(\p))_{i=1,\dots,N}$ and $g(\p) := (g^i(\p))_{i=1,\dots,N}$
allows us to define the \emph{set of feasible membranes} by
\begin{align*}
  \Uad(\p) = \left\{u \in \hat{H}(\p) \mid T(\p) = g(\p) \right\}\text{.}
\end{align*}
The interaction potential with respect to $\E$ under the given constraints is defined by
\begin{align*}
  \EI\colon \cD & \longrightarrow \R \cup \{\infty\}
    \\ \p & \longmapsto \E(\p,u(\p)) = \inf_{u \in \Uad(\p)} \E(\p;u)\text{.}
\end{align*}
%where the convention $\inf \emptyset = \infty$.

In general, the interaction potential under the given assumptions needs not be well-defined and hence this property has to be established separately in dependence of $\Uad$ and $\E$.
As we are going to focus on quadratic elastic energies $\E$ mainly, the following result is sufficient for our purposes.
\begin{theorem}
  Let $\delta \E(\p)$ linear and $\delta^2 \E(\p)$ elliptic on $\Uad(\p)$ for every $\p \in \cD$.
  If $\Uad(\p)$ is nonempty, then there exists a unique $u(\p) \in \Uad(\p)$ such that $\E(\p; u(\p)) = \inf_{u \in \Uad(\p)} \E(\p;u)$.
  In particular, the interaction potential $\EI$ is well-defined.
\end{theorem}

\begin{proof}
  This statement is an immediate consequence of Lax--Milgram's theorem and the fact that the $\Uad(\p)$ are closed affine linear subspaces as of the linearity and boundedness of $T(\p)$.
\end{proof}
\end{comment}

\begin{comment}
Now, as the membrane--particle system follows an energy minimization principle, we can expect -- in absence of thermal fluctuations -- that for a fixed particle configuration the membrane in long-term takes the shape of a $\cM(u(\p))$ where $u(\p)$ minimizes the elastic energy $\E(\p)$.
However, as both the membrane shape and the particles are free in space, it is energetically most favorable for the particles to take a configuration $\p^\ast$ such that $\E(\p^\ast)$ is minimal, while the membrane takes the associated shape $\cM(u(\p^\ast))$.
Such configurations are of special interest as they are expected to be the most stable with respect to thermal (and also other) perturbations.
Altogether, this leads us to:
\begin{problem}
  \label{prob:stationaryConfiguration}
  Find $\p^\ast \in \cD$ such that
  \begin{align*}
    \EI(\p^\ast) = \inf_{\p\in\cD} \EI(\p)\text{.}
  \end{align*}
\end{problem}
Due to nonlinearities and possible symmetries we can not expect that $\EI$ has a unique global minimizer.
However, if $\cD$ is compact and $\EI$ is continuous on $\cD$, then existence of a global minimizer is readily established.

As finding global minima of such a nonlinear function poses a rather challenging problem, we restrict ourselves in the following to the computation of \emph{local minima} of $\EI$, which we also call \emph{stationary particle configurations}.
A typical approach to find such local minima is to employ gradient-type optimization methods, and
Therefore our next step is to investigate differentiability properties of $\EI$.

\TK{Motivate gradient method here.}
%~ The 
\end{comment}

  
  
\subsection{Further remarks}
\label{sec:modelRemarks}


\paragraph{Choice of reference surface}
Typical choices of reference surfaces are minimizers of the elastic energy in the absence of particles, possibly subject to particle-independent constraints.
%~ A suitable type of reference surfaces is usually given by minimizers of the elastic energy in the absence of particle, but possibly subject to particle-independent constraints such as for example volume constraints.
In the presence of particles, it is particularly plausible that the optimal perturbation function for such a stationary reference surface fulfills the smallness assumptions required for parameterization, at least as long as the particle count remains reasonably bounded and as long as the particles themselves induce small deformations only.
 %~ such a stationary reference surface, the optimal perturbation functions in the presence of particles is particularly plausible to fulfill the smallness assumptions required for parameterization, at least as long as the particle count remains reasonably bounded and the particles themselves induce a small deformation.
In some situations, also other reference surfaces can be appropriate choices,
for example
%~ This is may be the case
if the entirety of particles induces a strong deformation of the ``natural'' membrane state.
Compare also \cite{Hobbs16} for related viewpoints.

\paragraph{Neglecting the Gauss curvature term}
In view of the Gauss--Bonnet theorem~\eqref{eq:GaussBonnet} and the comments from the introductory part of the chapter, it is justified to drop the Gauss curvature term if the parametric particle interfaces~$\wt{\cG}(\p)$ remain constant with respect to the parameter~$\p$ up to orthonormal transformations.
Hence, the negligibility in this case depends on the choice of the transformation function~$\Psi$ and is in particular given for spatial transformations as defined in \cref{thm:variablePositionOrientation}.
It is furthermore also required that the $\p$-independent constraints in $H_\ast^2(\Omega)$ keep the values and normals along the boundary~$\partial\Omega$ either constant or make them subject to periodic boundary conditions.
In more general cases the negligibility condition demands a closer inspection of the model.

\begin{comment}
\paragraph{Practicality of parameterizing}
Explicitly parameterizing over a reference domain~$\Omega$ is a convenient way of making geometric PDEs accessible to relatively simple discretization schemes, at least as long as also the geometries stay simple.
Once the geometries become too complicated, one should rather refer to parametric methods as for example introduced and applied in \cite{DzEl13,Hobbs16}.
\end{comment}

\paragraph{Validity of linearizing}
Loosely speaking, minimization of the linearized elastic energy~$\ELin$ yields a good approximation of the nonlinear energy's minimizer~$u$ if the associated optimal surface~$\cM(u)$ is a ``small'' perturbation of $\cM_0$.
This in particular imposes a smallness assumption on the perturbation function~$u$ as well as its derivatives.
A quantitative error analysis of the linearization error is possible by analyzing the corresponding error term of the Taylor expansion.
While such an analysis could give valuable insight into the validity of linearized interaction models in practical applications, it exceeds the scope of this work and is therefore left open for further investigations.

\paragraph{Beyond pure membrane-mediated interactions}
Within the presented framework the interaction between particles is purely mediated by the membrane's elastic energy.
This is readily extended by direct particle--particle interactions which are described by an additional potential $\E_{\text{p--p}}\colon \cD \rightarrow \R$.
To this end, the definition of the interaction potential is simply augmented to
\begin{align*}
  \EI(\p) := \E(\p,u(\p)) + \E_{\text{p--p}}(\p)
  \text{.}
\end{align*}
Once differentiability for the classical interaction potential is shown, the differentiability of the augmented potential becomes directly accessible through smoothness properties of $\E_{\text{p--p}}$.

In addition, the differentiability results from the subsequent chapters generalize to some extent to a larger class of energies~$\E$, which are not necessarily of the form~\eqref{eq:energyForm} and contain so-called soft particle--membrane interactions, such as penalty curve constraints~(cf.~\cite{GrKi17}).
%~ For example, certain types of soft particle--membrane interactions may in some cases be incorporated, such as penalty curve constraints (cf.~\cite{GrKi17}).
For the sake of simplicity, further details in this direction are omitted, but it is noted that corresponding results are obtainable by essentially the same proofs as given in this work.
%and left to the interested reader.

\paragraph{Including further constraints}
In this thesis the $\p$-independent constraints are restricted to Dirichlet and periodic boundary conditions.
In view of the remarks on the choice of the reference surface and with the development of more descriptive models in mind, it may also be relevant to incorporate further constraints such as for example volume and area constraints.
Within the context of particle--membrane interactions corresponding techniques for such constraints have been applied successfully in \cite{Hobbs16}.
However, the extension of the differentiability results below to volume and area constrained membranes is not immediate and remains an open topic.
%~ \TK{(Beyond Dirichlet/periodic TODO - at the end (volume/surface/inequality constraints; what about penalty terms?)}
%~ \TK{TO-do: Mention somewhere for the model that it also extends further (to for example penalty type models)}
%~ \TK{In principle, the energies may also be augmented by surface or volume constraints, as is for example shown in \cite{Hobbs16}.
%~ This may in particular be of physical relevance for tubular domains and might even improve the model quality by moving the reference surface closer to being a critical membrane state.}

\paragraph{Spontaneous curvature as a source of irregularity}
Closer inspection of \cref{thm:linearizationMongeGauge,thm:linearizationTube} shows that -- at least for these examples -- the linear term in the quadratic energy $\ELin$ contains derivatives of $u$ of order one or higher if and only if the spontaneous curvature~$c_0$ is not matched with the mean curvature~$H_0$ of the reference surface~$\cM_0$.
In view of standard elliptic regularity theory (cf.~\cite[Chapter~7]{Grisvard85} and \cite{BlumRannacher80,GaGrSw10}), this potentially implies a loss of regularity in the stationary membrane~$u(\p)$ unless~$c_0$ and $H_0$ are sufficiently smooth.
Such a loss of regularity would in particular have an impact on numerical approximation schemes, which either might suffer from inefficiency due to non-optimal rates of convergence or
%~ which would
have to account for irregularities in $c_0$ and $H_0$ explicitly.
A closer treatment of these aspects is omitted in this thesis and left to special applications. 

\paragraph{Choice of configuration space}
Ideally, one would like to include all those particle configurations which do not yield intersecting particle interfaces.
However, such a general choice is often not feasible within the parameterized model framework as it might lead to incompatibilities with the local projection and distance functions which are required for the definition of the parametric interfaces.
Hence, a purely pragmatic choice is to limit the configuration space $\cD$ to a subset of
\begin{align*}
  \cD_{\max} := \left\{ \p \in \R^{N\times k} \mid \text{$\Gamma(\p)$ and $g(\p)$ are well-defined}\right\}
  \text{.}
\end{align*}
In many cases it is reasonable to choose a strict and possibly closed subset $\cD \subsetneq \cD_{\max}$. %in order to avoid for example instabilities within numerical schemes.
Such a step is especially justified if the interaction potential fulfills $\EI(\p) \to \infty$ for $\p \to \partial \cD_{\max}$, but it is also more generally valid because the existence of an optimal particle configuration on the boundary~$\partial\cD_{\max}$ would indicate a wrong model choice at some point of the framework.
