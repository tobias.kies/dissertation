At the core of the model lies the description of the lipid membrane as a sufficiently smooth surface~$\cM \subseteq \R^3$, which is depicted in \cref{fig:hypersurface}.
\begin{figure}
  \begin{center}
    \includegraphics[height=3cm,trim={0 3mm 0 4mm},clip]{01_lipid_bilayer_to_hypersurface.pdf}
  \end{center}
  \caption{
  The biological membrane is a bilayer formed by lipids with hydrophobic tails and hydrophilic heads (rods with spheres).
  %~ illustrated here as rods with spheres.
  %~ The model approximates the membrane 
  It is approximated
  by
  an infinitely thin
  %~ a
  single sheet along its center line
  %~ , illustrated by the continuous blue line~$\cM$.
  (continuous blue line~$\cM$).
  }
  \label{fig:hypersurface}
\end{figure}
The energy associated to this surface~$\cM$ is given in terms of its \emph{principal curvatures} $\kappa_1$, $\kappa_2$ by the \emph{Canham--Helfrich energy with surface tension}
%\snote{Reminder: Make sure to reference Canham/Helfrich in the introduction.}
\begin{align*}
  \ECH(\cM) := \int_{\cM} \frac{1}{2} \kappa (H-c_0)^2 + \kappa_G K + \sigma \d{\HM}
  \text{.}
\end{align*}
Here, $H := \kappa_1 + \kappa_2$ is the \emph{mean curvature}, $K := \kappa_1 \kappa_2$ is the \emph{Gaussian curvature}, $c_0\colon \cM \rightarrow \R$ is the \emph{spontaneous curvature}, $\kappa \in \R_{>0}$ and $\kappa_G \in \R$ are \emph{bending rigidities}, and $\sigma \in \R_{\geq 0}$ is the \emph{surface tension}.
The integration is carried out with respect to the \emph{Hausdorff measure} $\HM$.

Stationary membranes are determined by minimization of this energy.
Therefore, the direct use of the Canham--Helfrich energy in the interaction model implies that its associated Lagrange equation needs to be solved
over a not yet defined set of admissible membranes.
In the special case $c_0 = \kappa_G = \sigma = 0$ the above energy is reduced to the \emph{Willmore energy} and the corresponding Lagrange equation reads
\begin{align*}
  \Delta_{\cM}H + 2H(H^2-K) = 0\text{.}
\end{align*}
Analogous formulations are known for the general case (cf.~\cite{DoNo12}).
This defines a geometric fourth order partial differential equation, which is highly nonlinear in nature.
The analytical and numerical analysis of such a problem pose a considerable challenge and the repeated numerical solution of this PDE, which is required during the numerical optimization of particle configurations, demands a substantial amount of computational power (cf.~\cite{Dziuk08,BGN08}).
%~ \snote{Maybe put some references here.}
%\TK[Should we put references here? Saying that there are numerical methods but almost no numerical analysis.]{}
In order to reduce complexity, it is desirable to incorporate further assumptions and approximations into the membrane model as far as reasonably possible.
%~ It is worth noting that this energy is closely related to the much-studied \emph{Willmore energy}
%~ \begin{align*}
  %~ \EWill(\cM) = \frac{1}{2} \int_{\cM} H^2\d{\HM}
%~ \end{align*}
%~ whose stationary solutions $\cM$ fulfill the Lagrange equation
%~ \begin{align*}
  %~ \Deltasurf H + 2 H(H^2-K) = 0\text{,}
%~ \end{align*}
%~ where $\Deltasurf$ denotes the Laplace--Beltrami operator on $\cM$.
%~ This corresponds to a highly nonlinear fourth order geometric partial differential equation, the analysis and numerical solution of which impose a considerable challenge.
%~ \snote{To-do: Add references here to back this up or reformulate last sentence.}
%~ Analogously, the same holds true for the above Canham--Helfrich energy with surface tension.
%~ \hrule

Arguably, the most commonly used simplification relies on the famous \emph{Gauss--Bonnet theorem}, which states
\begin{align}
  \label{eq:GaussBonnet}
  \int_{\cM} K \d{\HM} + \int_{\partial\cM} k_g \d{\mathbb{H}(\partial\cM)} = 2\pi_{\chi}(\cM)\text{.}
\end{align}
Here $k_g$ denotes the \emph{geodesic curvature} of $\partial\cM$ and $\pi_{\chi}(\cM)$ is the \emph{Euler characteristic} of the hypersurface~$\cM$.
The equality reveals the negligibility of the Gaussian curvature term during minimization of the elastic energy~$\ECH$ as long as the integral over the geodesic curvature~$k_g$ on the boundary~$\partial\cM$ remains constant within a given set of admissible membranes.
This property is true especially if the shape of $\partial\cM$ is fixed and if also the unit normals along $\partial\cM$ remain constant.
%and the unit normals of the various connected components of $\partial\cM$
%and the unit normal vectors of $\cM$ on those components
%remain constant up to translation and rotation.
%, where the same translation and rotation has to be applied to both the connected components and the unit normal vectors.
%\snote{Is this formulation understandable enough?}
%~ Indeed, it will turn out that this condition is met for all the example problems that we are going to consider later on in this work.
In the majority of particle interaction models considered in the literature, this assumption is generally fulfilled,
%In the particle interaction models most commonly considered in literature this is indeed the case,
and henceforth the Gaussian curvature usually is rightfully neglected.
However, in this work this simplification is \emph{not} to be applied just yet because the negligibility
%in general
depends on the precise particle--membrane interaction model and other model decisions concerning every particle's degrees of freedom which are still to be introduced.
%Nonetheless, the analysis in the remainder of this section is carried without this simplification as the treatment of non-vanishing Gaussian curvature terms may be of interest for future research.
Therefore the further discussion of this issue is postponed until the end of this chapter in \cref{sec:modelRemarks}.

Two further simplifications are also possible under the assumption that stationary membranes are perturbations of an \emph{a priori} known reference surface~$\cM_0$.
%it is justifiable to apply two further simplification steps to this problem.
The first simplification technique makes use of this situation by transforming the minimization problem via a parameterization of the reference surface $\cM_0$ onto some parameterization domain~$\Omega$.
This reduces the problem of computing stationary membranes, which usually corresponds to an energy minimization problem over a class of hypersurfaces, to a scalar non-linear PDE over the single fixed set~$\Omega$.
The second technique imposes an additional smallness assumption on the perturbations of the reference surface~$\cM_0$, which vindicates the approximation of the elastic energy~$\ECH$ in terms of a second order Taylor expansion.
Hence, a stationary membrane shape is approximated by minimizers of a quadratic energy, which in turn corresponds to solving a linear PDE over the fixed domain~$\Omega$.


\subsection{Parameterization over a reference domain}
  This section considers a parameterized bounded reference surface~$\cM_0$.
  For the sake of simplicity, suppose existence of an almost-global parameterization, \ie a single bounded parameterization domain~$\Omega \subseteq \R^2$ and a map $\varphi\colon\Omega \rightarrow \cM_0$ which is a bijection up to a relative null set of~$\cM_0$.
  It is furthermore assumed that $\varphi$
  defines a \mbox{$2$-diffeomorphism}.
  % which is assumed to be a two-diffeomorphism as well.
  Given a unit normal field~$\nu_0$ on $\cM_0$ and a perturbation function~$u \in C^2(\Omega)$, the \emph{perturbation of $\cM_0$ by $u$ (in normal direction~$\nu_0$)} is defined as
  \begin{align*}
    \cM := \cM(u)  := \left\{ \varphi(x) + u(x)\nu_0(\varphi(x)) \mid x \in \Omega \right\}\text{.}
  \end{align*}
  The following paragraphs implicitly assume that $\norm{u}_\infty$ is sufficiently small and, if applicable, fulfills suitable
  %~ periodic
  boundary conditions on $\Omega$ such that the perturbation~$\cM$ is again a surface without self-intersections and which is smooth enough for the energy~$\ECH(\cM)$ to be well-defined.
  
  By standard means of
  %~ elementary
  differential geometry (as introduced \eg in \cite{Willmore13}) the elastic energy $\ECH(\cM)$ is expressable exclusively in terms of the perturbation function~$u$ and derivatives thereof as well as geometric quantities of the reference surface~$\cM_0$, but without relying explicitly on quantities of the perturbed surface~$\cM$.
  % without the direct use ofquantities from $\cM$.
  %~ To this end, we need some geometric quantities.
  %\snote{To-do: Make a short reference to a book on differential geometry where the following quantities are all introduced. (Or even earlier when principal curvatures are mentioned for the first time.)}
  
  In order to carry out this transformation explicitly one defines a parameterization of the perturbed surface~$\cM$ by
  \begin{align*}
    \phi(x) := \varphi(x) + u(x)\nu_0(\varphi(x))
    \text{.}
  \end{align*}
  In a slight abuse of notation the partial derivatives of $\phi$ are abbreviated by
  \begin{align*}
    \phi_i := \partial_i \phi
    ,\qquad \phi_{ij} := \partial_{ij} \phi
    \text{,}
  \end{align*}
  where it is noted that the following steps entirely avoid any explicit indexing of components of $\phi$ in order to rule out any possible ambiguity with this notation.
  The tangent space of the perturbed surface~$\cM$ in a point~$\phi(x)$ is given by the image of the Jacobian
  \begin{align*}
    D\phi(x)
    %~ = \mat{\partial_1 \phi(x), & \partial_2 \phi(x)}
    = \mat{\phi_1(x), &\phi_2(x)}
  \end{align*}
  and hence a normal field $\nu$ on $\cM$ is defined by
  \begin{align*}
    \wt{\nu}  := \phi_1 \times \phi_2
    ,\qquad Q := \norm{\wt{\nu}}
    ,\qquad \nu := \frac{\wt{\nu}}{Q}\text{.}
  \end{align*}
  The \emph{first fundamental} form, also known as \emph{metric tensor}, is given by the symmetric matrix
  \begin{align*}
    \I := D\phi^T D\phi = \mat{\norm{\phi_1}^2 & \phi_1 \cdot \phi_2 \\ \phi_1 \cdot \phi_2 & \norm{\phi_2}^2}\text{.}
  \end{align*}
  It holds
  \begin{align*}
    \det(\I) = \norm{\phi_1}^2 \norm{\phi_2}^2 - (\phi_1 \cdot \phi_2)^2 = Q^2
  \end{align*}
  and therefore
  \begin{align*}
    \I^{-1}
    = \frac{1}{Q^2} \mat{ \I_{22} & -\I_{12} \\ -\I_{21} & \I_{11} }
    = \frac{1}{Q^2} \mat{ \norm{\phi_2}^2 & - \phi_1\cdot\phi_2 \\ -\phi_1\cdot\phi_2 & \norm{\phi_1}^2 }\text{.}
  \end{align*}
  The \emph{second fundamental form} is defined as
  \begin{align*}
    \II
    := \mat{\partial_{11}\phi \cdot \nu & \partial_{12}\phi \cdot \nu \\ \partial_{21}\phi \cdot \nu & \partial_{22}\phi \cdot \nu}
    = \frac{1}{Q}\mat{\partial_{11}\phi \cdot \wt{\nu} & \partial_{12}\phi \cdot \wt{\nu} \\ \partial_{21}\phi \cdot \wt{\nu} & \partial_{22}\phi \cdot \wt{\nu}}
  \end{align*}
  and the \emph{shape operator $S := \II \cdot \I^{-1}$} takes the form
  \begin{align*}
    S %:= \II \cdot \I^{-1}
    = \frac{1}{Q^3} \mat{
        \norm{\phi_2}^2 (\phi_{11}\cdot \wt{\nu}) - (\phi_1\cdot\phi_2)(\phi_{12}\cdot\wt{\nu}),
        & \norm{\phi_1}^2(\phi_{12}\cdot\wt{\nu}) - (\phi_1\cdot\phi_2)(\phi_{11}\cdot\wt{\nu})
        \\ \norm{\phi_2}^2(\phi_{21}\cdot\wt{\nu}) - (\phi_1\cdot\phi_2)(\phi_{22}\cdot\wt{\nu}),
        & \norm{\phi_1}^2(\phi_{22}\cdot\wt{\nu}) - (\phi_1\cdot\phi_2)(\phi_{21}\cdot\wt{\nu})
      }
      \text{.}
  \end{align*}
  The eigenvalues of the shape operator define the \emph{principal curvatures} $\kappa_1$ and $\kappa_2$.
  Consequently, the \emph{mean curvature} is
  \begin{align*}
    H = \Tr(S)
    = \frac{1}{Q^3} \left( \norm{\phi_1}^2 (\phi_{22}\cdot \wt{\nu}) + \norm{\phi_2}^2 (\phi_{11}\cdot \wt{\nu}) - 2(\phi_1\cdot\phi_2)(\phi_{12}\cdot\wt{\nu})\right)
  \end{align*}
  and the \emph{Gaussian curvature} is
  \begin{align*}
    K = \det(S) = \frac{1}{Q^4} \left( (\phi_{11}\cdot\wt{\nu})(\phi_{22}\cdot\wt{\nu}) - (\phi_{12}\cdot\wt{\nu})^2\right)\text{.}
  \end{align*}
  Finally, application of the transformation formula to $\ECH$ with the diffeomorphism~$\phi$ yields the \emph{parameterized elastic energy}
  %~ \begin{align*}
    %~ \ECH(\cM) & = \int_{\Omega} \frac{\kappa}{2Q^8} \left( \norm{\phi_1}^2 (\phi_{22}\cdot \wt{\nu}) + \norm{\phi_2}^2 (\phi_{11}\cdot \wt{\nu}) - 2(\phi_1\cdot\phi_2)(\phi_{12}\cdot\wt{\nu}) - Q^3c_0\right)^2\d{x}
      %~ \\ & \qquad + \int_{\Omega} \frac{\kappa_G}{Q^3} \left( (\phi_{11}\cdot\wt{\nu})(\phi_{22}\cdot\wt{\nu}) - (\phi_{12}\cdot\wt{\nu})^2\right) + \sigma Q \d{x}\text{.}
  %~ \end{align*}
  \begin{align*}
    \ECH(\cM) & = \int_{\Omega} Q \left( \frac{\kappa}{2} (H-c_0)^2 + \kappa_G K + \sigma \right) \d{x}\text{.}
  \end{align*}
  Similar to the original formulation, the minimization of this energy is in general equivalent to solving a highly non-linear fourth order PDE.
  However, due to the restriction to perturbations of $\cM_0$, this PDE is now stated for a scalar function~$u$ over a single fixed reference domain~$\Omega$ instead of for a class of hypersurfaces.
  
  In the context of particle--membrane interaction models flat, tubular and spherical geometries are especially of interest.
  %~ Reference geometries that are of particular interest with respect to particle--membrane interactions are flat, tubular, and spherical surfaces.
  The examples below state the parameterized mean and Gaussian curvature as well as the volume element $Q$ for the corresponding reference surfaces. % by applying the above formulas.
  This immediately yields the respective parameterized elastic energy by substitution of those quantities into the formula above.
  Although the main focus of attention in this thesis is on the linearized models presented in the next section, such non-linear parameterized formulations have a potential value in future applications for the modeling of particle interactions with locally strong deformations.
  
  While generally relatively little is known about the regularity of solutions of the associated non-linear PDEs and about the approximation properties of related discretization schemes, there are, at least to an extent, such results for the closely related minimization problem of the Willmore functional in the graph case, see~\cite{DeKaSc15,DeGrRo15}.
  Those works give rise to the expectation that similar properties might be derivable for  other related formulations as well, thus potentially making these problems numerically more tractable.
  
  \begin{example}[Euclidean plane]
    \label{thm:WillmoreGraphCase}
    Suppose $\Omega\subseteq\R^2$ and define a flat reference surface by $\cM_0 := \Omega \times \{0\}$, admitting the canonical parameterization $\varphi\colon\Omega\rightarrow\cM_0,\,x \mapsto (x,0)$.
    For notational convenience, denote the partial derivatives of $u$ by $u_i := \partial_i u$ and $u_{ij} := \partial_{ij} u$.
    It holds
    \begin{align*}
      \phi(x) = \mat{x_1 \\ x_2 \\ u(x)}
      ,\quad \phi_1(x) = \mat{1 \\ 0 \\ u_1(x)}
      ,\quad \phi_2(x) = \mat{0 \\ 1 \\ u_2(x)}
      ,\quad \wt{\nu}(x) = \mat{-u_1(x) \\ -u_2(x) \\ 1}
    \end{align*}
    and
    \begin{align*}
      Q(x) = \sqrt{1+u_1(x)^2 + u_2(x)^2}
      ,\qquad \phi_{ij}(x) \cdot \wt{\nu}(x) = u_{ij}(x)
      \text{.}
    \end{align*}
    Therefore,
    \begin{align*}
      H & = \frac{u_{22}(1+u_1^2) + u_{11}(1+u_2^2) - 2 u_1u_2 u_{12}}{(1+u_1^2+u_2^2)^{3/2}}
      ,\qquad K = \frac{u_{11}u_{22} - u_{12}^2}{(1+u_1^2+u_2^2)^2}\text{ .}
    \end{align*}
  \end{example}
    
  \begin{example}[Tube]
    \label{thm:tubeGraphCase}
    Let $L \in \R_{>0}$, $r \in \R_{>0}$ and suppose a tube of length $L$ and radius $r$, given by
    \begin{align*}
      \cM_0 := \{ x \in \R^3 \mid x_1 \in [0,L], \ x_2^2+x_3^2 = r \}\text{.}
    \end{align*}
    A global parameterization of the cylinder~$\cM_0$ up to a null set over the reference domain~$\Omega = [0,L] \times [0,2\pi)$ %]$
    is given by the cylindrical coordinates
    \begin{align*}
      \varphi(x,y) = \mat{x \\ r\cos(y) \\ r\sin(y)}
      ,\qquad \nu_0(x,y) = \mat{0 \\ \cos(y) \\ \sin(y)}\text{.}
    \end{align*}
    With the shorthands $c := \cos(y)$, $s:=\sin(y)$ one has
    \begin{align*}
      \phi = \mat{x \\ (u+r)c \\ (u+r)s}
      ,\ \ \phi_1 = \mat{1 \\ u_1c \\ u_1s}
      ,\ \ \phi_2 = \mat{0 \\ u_2c - (u+r)s \\ u_2s +(u+r)c}
      ,\ \ \wt{\nu} = \mat{u_1(u+r) \\ -u_2s -(u+r)c \\ u_2c - (u+r)s}
    \end{align*}
    with
    \begin{align*}
      \norm{\phi_1}^2 = 1 + u_1^2
      ,\quad\norm{\phi_2}^2 = (u+r)^2 + u_2^2
      ,\quad\phi_1\cdot\phi_2 = u_1 u_2
    \end{align*}
    as well as
    \begin{align*}
      Q & = \sqrt{(1+u_1^2)(u+r)^2 + u_2^2}
      \\ \phi_{11}\cdot\wt{\nu} & = -u_{11}(u+r)
      \\ \phi_{12}\cdot\wt{\nu} & = u_1 u_2 - (u+r) u_{12}
      \\ \phi_{22}\cdot\wt{\nu} & = (u+r)^2 - (u+r)u_{22} + 2u_2^2\,\text{.}
    \end{align*}
    This yields
    \begin{align*}
      H & = \frac{1}{Q^3} \left( -(u+r)^3 u_{11} + (u+r)^2 (1+u_1^2) \right.
      \\ & \qquad\qquad \left. + (u+r)\left(2u_1 u_2 u_{12}-u_{22}(1+u_1^2)-u_2^2 u_{11}\right) + 2u_2^2 \right)
    \end{align*}
    and
    \begin{align*}
      K & = \frac{1}{Q^4} \left( -(u+r)^3 u_{11} + (u+r)^2 (u_{11}u_{22} - u_{12}^2) \right.
        \\ & \qquad\qquad + \left. 2(u+r)(u_1 u_2 u_{12} - u_2^2 u_{11}) - u_1^2 u_2^2 \right)\text{.}
    \end{align*}
    Note that here the perturbation function $u$ is required to fulfill suitable periodic boundary conditions along the boundary $[0,L] \times \{0,2\pi\}$ in order to compensate for the fact that the function~$\varphi$ is not a global parameterization in the classical sense.
    This ensures $u \circ \varphi^{-1} \in C^2(\cM_0)$.
  \end{example}
    
  \begin{example}[Sphere]
    \label{thm:sphereGraphCase}
    Suppose $r \in \R_{>0}$ and an $r$-sphere
    \begin{align*}
      \cM_0 := \left\{ x \in \R^3 \mid \norm{x} = r \right\}
    \end{align*}
    and parameterize this surface up to a null set over the reference domain~$\Omega = [0,2\pi) \times [0,\pi]$ %]$
    by the spherical coordinates
    \begin{align*}
      \varphi(x,y) = r \mat{\cos(x)\sin(y) \\ \sin(x)\sin(y) \\ \cos(y)}
      ,\qquad\nu_0(x,y) = \mat{\cos(x)\sin(y) \\ \sin(x)\sin(y) \\ \cos(y)}
      \text{.}
    \end{align*}
    Using the shorthands $\cx = \cos(x), \sx = \sin(x), \cy = \cos(y), \sy = \sin(y)$, this leads to
    \begin{align*}
      \phi & = (r+u)\mat{\cos(x)\sin(y) \\ \sin(x)\sin(y) \\ \cos(y)}
      , & \phi_1 & = \mat{\sy\left( \cx\ux - (u+r)\sx \right) \\ \sy\left(\sx\ux + (u+r)\cx \right) \\ \cy \ux}
      ,\\\phi_2 & = \mat{\cx\left( \sy\uy + (u+r)\cy \right) \\ \sx\left( \sy\uy + (u+r)\cy\right) \\ \cy\uy - (u+r)\sy}
      , & \wt{\nu} & = (u+r)\mat{-(u+r)\cx\sy^2 - \ux\sx + \uy\cx\cy\sy \\ -(u+r)\sx\sy^2 + \ux\cx + \uy\sx\cy\sy \\ -\sy\left((u+r)\cy + \uy\sy\right) }
    \end{align*}
    and
    \begin{align*}
      \norm{\phi_1}^2 = (u+r)^2\sy^2+\ux^2
      ,\qquad\norm{\phi_2}^2 = (u+r)^2 + \uy^2
      ,\qquad\phi_1\cdot\phi_2 = \ux\uy
    \end{align*}
    as well as
    \begin{align*}
      Q & = \abs{u+r} \sqrt{ \left( (u+r)^2 + \uy^2\right)\sy^2 + \ux^2}
      \\ \phi_{11}\cdot\wt{\nu} & = (u+r) \left( \sy^3 (u+r)^2 +\sy\left(2\ux^2-(u+r)\uxx\right) - \uy \cy\sy^2 (u+r) \right)
      \\ \phi_{12}\cdot\wt{\nu} & = (u+r) \left( (u+r)\left(\ux\cy-\uxy\sy\right) + 2\ux\uy\sy\right)
      \\ \phi_{22}\cdot\wt{\nu} & = \sy (u+r) \left( (u+r)^2 - \uyy(u+r) + 2\uy^2 \right)\text{.}
    \end{align*}
    Altogether,
    \begin{align*}
      H & = \frac{(u+r)^2}{Q^3} \bigg( 2\sy^3(u+r)^3 - \sy(u+r)^2 \left(\uxx +\uyy\sy^2 +\cy\sy\uy\right) + 3\sy(u+r)\left(\ux^2+\sy^2\uy^2\right)
      \\ & \qquad + \sy\left(\uxx\uy^2+\uyy\ux^2-2\ux\uy\uxy\right) + \cy\uy\left(2\ux^2+\sy^2\uy^2\right)\bigg)
    \end{align*}
    and
    \begin{align*}
      K & = \frac{(u+r)^3}{Q^4} \bigg(
       (u+r)^3 \sy^4
       -(u+r)^2 \sy^2 \left(\uxx + \sy\cy\uy + \uyy\sy^2 \right)
       \\ & \qquad +(u+r)\sy^2 \left( \uxx\uyy - \uxy^2 + 2\ux^2 + 2\uy^2\sy^2 + \sy\cy\uy\uyy \right)
       -(u+r)\cy^2\ux^2
       \\ & \qquad -2\sy^2\left( \uxx\uy^2 - 2\ux\uy\uxy + \uyy\ux^2 \right)
       +2\sy\cy\left( (u+r)\ux\uxy - 2\ux^2\uy - \uy^3\sy^2\right)
       \bigg)
       \text{.}
    \end{align*}
    As for the cylinder, the perturbation function is required to fulfill suitable periodic boundary conditions on the boundary~$\partial\Omega$ in order to ensure smoothness of the perturbation surface~$\cM(u)$.
  \end{example}
  
  %~ \begin{remark}
    %~ Technically, the above examples for the sphere and tube do not use global charts $\varphi$ as it was required earlier.
    %~ However, the images of the given parameterizations coincide with $\cM_0$ up to a null set and therefore the value of the energy function stays the same when the integral is transformed using those $\varphi$.
  %~ \end{remark}
  
  %~ \snote{Might want to add a remark on generalization to families of parameterizations. (When no single parameterization is possible.)}
  
  
  \begin{comment}
  --- The following is not quite right yet. ---
  \begin{remark}
    The above considerations easily generalize to the setting where not only a single parameterization is given but a (finite) family of parameterizations \mbox{$\varphi_j\colon \Omega_j \rightarrow \cM_0$}, $j\in J$ instead that covers all of $\cM_0$:
    Without loss of generality we may assume that the closed sets $\overline{\Omega_j}$ are pairwise disjoint.
    Let $\Omega := \bigcup_{j\in J} \Omega_j$ and extend $\varphi_j$ to $\Omega$ by zero.
    Given a partition of unity $(\rho_j)_{j\in J}$ with $\operatorname{supp}\rho_j = \operatorname{supp}\varphi_j$ and $u \in C^2(\Omega)$, the perturbed surface is defined by
    \begin{align*}
      \cM = \left\{ \textstyle\sum_{j\in J} \rho_j(x) (\varphi_j(x) + u(x) \nu_0(x)) \mid x \in \Omega \right\}
    \end{align*}
    The quantities $H_j$, $K_j$ and $Q_j$ are then defined with respect to the $\varphi_j$. the parameterized energy is given by
    \TK{TODO},
    and under mild assumptions it can be shown that this leads to a reasonable PDE formulation.
    \snote{(However, at this point one should slowly start thinking about surface finite element methods.)}
  \end{remark}
  \end{comment}
    
\subsection{Geometric linearization}
  \label{sec:geometricLinearization}
  Next the linearization of the elastic bending energy $\ECH$ via a second order Taylor expansion is considered.
  Such an approximation simplifies the computation of a stationary membrane shape to the solution of a linear fourth order partial differential equation.
  
  Let $\cM_0$ again be a sufficiently smooth reference surface and $\nu_0$ a unit normal field on $\cM_0$.
  Given a perturbation function~$u\in C^2(\cM_0)$, a family of perturbed surfaces is defined by
  \begin{align*}
    \cM(t) := \left\{ x + tu(x)\nu_0(x) \mid x \in \cM_0 \right\}\text{.}
  \end{align*}
  Again, assuming that $\norm{u}_{L^\infty(\cM_0)}$ is small enough, $\cM(t)$ is well-defined for $t \in [-1,1]$.
  With
  \begin{align*}
    J(t) := \ECH(\cM(t))
  \end{align*}
  the linearization of $\ECH$ in $u$ and in normal direction $\nu_0$ is given by
  \begin{align*}
    \ELin(u) := J(0) + J'(0) + \frac{1}{2} J''(0) \approx J(1) = \ECH(\cM(u))\text{.}
  \end{align*}
  Similar to the case of parameterized energies, it is possible to state the linearized energy exclusively in terms of the perturbation function~$u$ and its derivatives and in terms of geometric quantities of the reference surface~$\cM_0$ without relying on the $\cM(t)$ explicitly.
  
  An extensive computation of the linearization terms  is carried out in Appendix~\ref{sec:appendixLinearization}.
  For the reader's convenience the most important results are restated below.
  There, $H_0$ and $K_0$ denote the mean and Gaussian curvature on $\cM_0$, respectively, and $\cH_0$ and $\hat\cH_0 := \cH_0 + \nu_0 \otimes \nu_0$ are the extended Weingarten map and the regularized extended Weingarten map, respectively.
  Furthermore, $\gsurfn{}$ and $\Dsurfn^2$ are the surface gradient and surface Hessian, respectively.
  Finally, $\dmat$ denotes the so-called material derivative on the space--time manifold $\cM_T := \bigcup_{t \in [-1,1]} \{t\} \times \cM(t)$.
  See Appendix~\ref{sec:appendixLinearization} for precise definitions of the involved quantities.
  
  % The following part is somewhat hacky.
  % Follows an idea from https://tex.stackexchange.com/questions/51286/recalling-a-theorem after restatable did not yield the desired results.
  
  \newcommand{\dmatH}{
    It holds
    \begin{align*}
      \dmat{H}|_{t=0} & = -u\norm{\cH_0}_F^2 - \Deltasurfn{u}
      %~ \\ \dmat^2{H}|_{t=0} & = 2u^2\Tr(\cH_0^3) + 3u\cH_0 : \Dsurfn^2{u} - \norm{\gsurfn{u}}^2H_0 + u\Deltasurfn\nu_0 \cdot \gsurfn{u}
      \\ \dmat^2{H}|_{t=0} & = 2u^2\Tr(\cH_0^3) - \gsurfn{u}^T\cH_0\gsurfn{u} + 4u\cH_0 : \Dsurfn^2{u} 
      \\ & \qquad - \norm{\gsurfn{u}}^2H_0 + 2u\Deltasurfn\nu_0 \cdot \gsurfn{u}
    \end{align*}
    where $\Deltasurfn$ is the Laplace-Beltrami operator on $\cM_0$ and $\norm{\cdot}_F$ denotes the Frobenius norm with $:$ as the inducing matrix scalar product.
  }
  \begingroup
  \def\thetheorem{\ref{thm:dmatH}}
  \begin{proposition}
    \dmatH
  \end{proposition}
  \addtocounter{theorem}{-1}
  \endgroup


  \newcommand{\dmatK}{  
    Let $\adj(\hat\cH)$ denote the adjugate matrix of $\hat\cH$.
    It holds
    \begin{align*}
      \dmat{K}|_{t=0} & = \Tr(\adj(\hat\cH)\dmat\hat\cH)|_{t=0}
      \\ \dmat^2{K}|_{t=0} & = \Tr(\dmat\adj(\hat\cH)\,\dmat\hat\cH)|_{t=0} + \Tr(\adj(\hat\cH)\dmat^2\hat\cH)|_{t=0}
    \end{align*}
    where expressions for $\dmat\hat\cH|_{t=0}$ and $\dmat^2\hat\cH|_{t=0}$ are given in \cref{thm:dmatRegularizedExtendedWeingarten}.
    If furthermore $K(0) \neq 0$, let $A := \hat\cH^{-1}\dmat\cH^{-1}|_{t=0}$.
    Then
    \begin{align*}
      \dmat{K}|_{t=0} & = K \Tr(A)
      \\ \dmat^2{K}|_{t=0} & = K \left( \Tr\left(A\right)^2 - \norm{A}_F^2 + \Tr\left(\hat{\cH}^{-1}\dmat^2\hat\cH\right)|_{t=0} \right)
      \text{.}
    \end{align*}
  }
  \begingroup
  \def\thetheorem{\ref{thm:dmatK}}
  \begin{proposition}
    \dmatK
  \end{proposition}
  \addtocounter{theorem}{-1}
  \endgroup


  \newcommand{\derivativeIntC}{
    It holds
    \begin{align*}
      \dtf{\int_{\cM(t)} 1 \dH} = \int_{\cM_0} uH_0 \dH
    \end{align*}
    and
    \begin{align*}
      %\dtfn{\int_{\cM(t)} 1 \dH}{2} = \int_{\cM_0} \norm{\gsurfn{u}}^2 \dH
      \dtfn{\int_{\cM(t)} 1 \dH}{2} = \int_{\cM_0} \norm{\gsurfn{u}}^2 + 2u^2 K_0\dH
      \text{.}
    \end{align*}
  }
  \begingroup
  \def\thetheorem{\ref{thm:derivativeInt1}}
  \begin{theorem}
    \derivativeIntC
  \end{theorem}
  \addtocounter{theorem}{-1}
  \endgroup


  \newcommand{\derivativeIntH}{
    It holds
    \begin{align*}
      \dtf{ \frac{1}{2}\int_{\cM(t)} (H-c_0)^2 \dH}
      & = \int_{\cM_0} -(H_0-c_0) \left(u\norm{\cH_0}_F^2 + \Deltasurfn{u} + \dmat c_0\right)
      \\ & \qquad \qquad + \frac{1}{2} uH_0(H_0-c_0)^2 \dH
    \end{align*}
    and
    %~ \begin{align*}
      %~ \dtfn{ \frac{1}{2}\int_{\cM(t)} (H-c_0)^2 \d{x}}{2}
      %~ & = \int_{\cM_0} \left(u\norm{\cH_0}_F^2 + \Deltasurfn{u} + \dmat c_0\right)^2 \d{x}
          %~ \\ & \qquad + \int_{\cM_0} (H_0-c_0)( \dmat^2 H|_{t=0} - \dmat^2c_0 )\d{x}
          %~ \\ & \qquad + \int_{\cM_0} 2 uH_0(H_0-c_0)\left(u\norm{\cH_0}_F^2 + \Deltasurfn{u} + \dmat c_0\right) \d{x}
          %~ \\ & \qquad + \int_{\cM_0} \frac{1}{2} (H_0-c_0)^2 \norm{\gsurfn{u}}^2 \d{x}
    %~ \end{align*}
    \begin{align*}
      \dtfn{ \frac{1}{2}\int_{\cM(t)} (H-c_0)^2 \dH}{2}
      & = \int_{\cM_0} \left(u\norm{\cH_0}_F^2 + \Deltasurfn{u} + \dmat c_0|_{t=0}\right)^2 \dH
          \\ & \qquad + \int_{\cM_0} (H_0-c_0)( \dmat^2 H|_{t=0} - \dmat^2c_0|_{t=0} )\dH
          \\ & \qquad - \int_{\cM_0} 2 uH_0(H_0-c_0)\left(u\norm{\cH_0}_F^2 + \Deltasurfn{u} + \dmat c_0|_{t=0}\right) \dH
          \\ & \qquad + \int_{\cM_0} \frac{1}{2} (H_0-c_0)^2 \left(\norm{\gsurfn{u}}^2 + u^2 K_0\right) \dH
    \end{align*}
    where $\dmat^2 H|_{t=0}$ is as in \cref{thm:dmatH}.
  }
  \begingroup
  \def\thetheorem{\ref{thm:derivativeIntH}}
  \begin{theorem}
    \derivativeIntH
  \end{theorem}
  \addtocounter{theorem}{-1}
  \endgroup


  \newcommand{\derivativeIntK}{
    %~ Let $A := \Dsurfn^2{u} + u\cH_0^2$.
    It holds
    %~ \begin{align*}
      %~ \dtf{\int_{\cM(t)} K \d{x}}
      %~ & = \int_{\cM_0} -\kappa_2\,\mu_1^T A \mu_1 -\kappa_1\,\mu_2^T A \mu_2  + u K H_0 \d{x}
    %~ \end{align*}
    \begin{align*}
      \dtf{\int_{\cM(t)} K \dH}
      & = \int_{\cM_0} \dmat{K}|_{t=0}  + u K_0 H_0 \dH
    \end{align*}
    and
    %~ \begin{align*}
      %~ \dtfn{\int_{\cM(t)} K \d{x}}{2}
      %~ & = \int_{\cM_0} \dmat^2{K}|_{t=0} - 2 u\cH_0 \left( \kappa_2\,\mu_1^T A \mu_1 +\kappa_1\,\mu_2^T A \mu_2 \right) + K \norm{\gsurfn{u}}^2 \d{x}
    %~ \end{align*}
    \begin{align*}
      \dtfn{\int_{\cM(t)} K \dH}{2}
      & = \int_{\cM_0} \dmat^2{K}|_{t=0} + 2 u H_0 \dmat{K}|_{t=0} + K_0 \left(\norm{\gsurfn{u}}^2 + u^2 K_0 \right)\dH
    \end{align*}
    where $\dmat{K}|_{t=0}$ and $\dmat^2{K}|_{t=0}$ are given in \cref{thm:dmatK}.
  }
  \begingroup
  \def\thetheorem{\ref{thm:derivativeIntK}}
  \begin{theorem}
    \derivativeIntK
  \end{theorem}
  \addtocounter{theorem}{-1}
  \endgroup
  
  
  \subsubsection{Examples of linearized bending energies}
  Combining the results from \cref{thm:derivativeInt1,thm:derivativeIntH,thm:derivativeIntK} yields the linearized bending energy~$\ELin$.
  For the special cases of flat and tubular reference surfaces the explicit formulas of the linearized energy are shown below where the energy is both linearized and parameterized over a suitable reference domain.
  In those examples the spontaneous curvature~$c_0(t)$ is assumed to have a negligible dependence on the perturbation function~$u$, which implies a vanishing material derivative~$\dmat{c_0} = 0$.
  Furthermore, $u$ is directly expressed in local coordinates, \ie as a function~$u\colon \Omega \rightarrow \R$.
  
  \begin{example}[Euclidean plane]
    \label{thm:linearizationMongeGauge}
    Suppose a flat surface $\cM_0 = \Omega \times \{0\}$.
    In that case $\nu_0$ and $\hat{\cH}_0|_{t=0}$ are constant and $\cH_0$, $K_0$ and $H_0$ vanish.
    As a consequence, also $\adj{\hat{\cH}}|_{t=0} = 0$ holds and the derivative $\dmat{K}|_{t=0}$ vanishes as well.
    Explicit computation reveals
    \begin{align*}
      \dmat^2{K}|_{t=0}
      & = \left.\dmat\adj(\hat{\cH}) : \dmat\hat{\cH} \right|_{t=0}
      \\ & = \mat{-u_{22} & u_{12} & 0 \\ u_{12} & -u_{11} & 0 \\ 0 & 0 & 0} : \mat{u_{11} & u_{12} & -u_1 \\ u_{12} & u_{22} & -u_2 \\ -u_1 & -u_2 & 0}
      \\ & = 2u_{12}^2 - 2 u_{11} u_{22}
      \text{.}
    \end{align*}
    %is obtained from \cref{thm:dmatRegularizedExtendedWeingarten} and \cref{thm:dmatK}.
    %~ Also, we have $\kappa_1 = \kappa_2$ on $\Omega$ and we may choose the $\mu_i$ to be the Euclidean unit vectors in $\R^2$. -- NO! You may not choose $\mu_i$ arbitrarily. They have to be compatible with u.
    Moreover, the surface derivatives simplify to the standard derivatives in two dimensions, \ie $\gsurfn$ and $\Deltasurfn$ are identified with $\nabla$ and $\Delta$ on $\R^2$.
    Combining the above results yields, up to a $u$-independent constant, the linearized energy
    \begin{align*}
      \ELin(u) & = \frac{1}{2} \int_{\Omega} \kappa (\Delta u)^2 + 2\kappa_G \left(u_{12}^2 - u_{11} u_{22}\right) +  \left(\sigma + \frac{\kappa c_0^2}{2} \right) \norm{\nabla u}^2 \d{x}
        + \int_{\Omega} \kappa \: c_0 \: \Delta u \d{x}
        %~ + C
        \text{.}
      %~ \int_{\Omega} \frac{\kappa}{2} (\Delta u)^2 + \kappa_G \TK{TODO} + \frac{\sigma}{2} \norm{\nabla u}^2 \d{x} + \text{const}
    \end{align*}
    %~ where $C$ is a $u$-independent constant.
    In case of vanishing spontaneous curvature $c_0 = 0$ and by neglecting the Gaussian curvature term, the well-known \emph{Canham--Helfrich energy in Monge-gauge} is recovered:
    \begin{align*}
      \ELin(u) = \frac{1}{2}\int_{\Omega} \kappa (\Delta u)^2 + \sigma \norm{\nabla u}^2 \d{x}\text{.}
    \end{align*}
  \end{example}
  
  
  \begin{example}[Tube]
    \label{thm:linearizationTube}
    Let $L, r \in \R_{>0}$ and suppose a tube of length $L$ and radius $r$
    \begin{align*}
      \cM_0 = \left\{ x \in \R^3 \mid x_1 \in [0,L], \ x_2^2 + x_3^2 = r^2 \right\}
    \end{align*}
    equipped with the cylindrical coordinates $\varphi(x,y) = (x, r\cos(y), r\sin(y))^T$ over the parameterization domain~$\Omega := [0,L] \times [0,2\pi]$.
    Moreover, for the sake of simplicity it is assumed that Gaussian curvature is negligible, \ie $\kappa_G = 0$.
    %~ \snote{Lift this?}
    Using the shorthands $c := \cos(y)$ and $s := \sin(y)$ the quantities of interest are given in local coordinates by
    \begin{align*}
      \wt{\nu}_0 & = \mat{0 \\ c \\ s}
      ,\qquad \wt{\cH}_0 = \frac{1}{r} \mat{0 & 0 & 0 \\ 0 & s^2 & -cs \\ 0 & - cs & c^2}\text{,}
    \end{align*}
    which further implies $H_0 = \frac{1}{r}$, $\norm{\cH_0}_F^2 = \frac{1}{r^2}$ and $K_0 = 0$.
    The surface gradient and Hessian in local coordinates are
    \begin{align*}
      \gsurfn u & = \frac{1}{r} \mat{r{u}_1 \\ -s{u}_2 \\ c{u}_2}
      ,\qquad \Dsurfn^2 u = \frac{1}{r^2} \mat{r^2 u_{11} & -ru_{12}s & r u_{12} c \\ -r u_{12} s & u_{22} s^2 + u_2 c s & u_2 s^2 - u_{22} cs \\ r u_{12} c & - u_2c^2 - u_{22}cs & u_{22} c^2 - u_2 cs}
      \text{.}
    \end{align*}
    %~ For notational convenience replace $\wt{u}$ in the following formula by $u$ and define $c := \frac{1}{r} - c_0$.
    Define $c := \frac{1}{r} - c_0$.
    Using the constant area element~$Q \equiv r$
    the parameterized linearized energy then reads, again up to a $u$-independent constant,
    \begin{align*}
      \ELin(u) & = \frac{r\kappa}{2} \int_{\Omega} \left(u_{11} + \frac{u+u_{22}}{r^2} \right)^2
      + c \left(\frac{2u^2}{r^3} - \frac{u_2^2}{r^3} + \frac{4u u_{22}}{r^3} - \frac{1}{r} \left(u_1^2 + \frac{u_2^2}{2} \right) \right) \d{x}
      %~ \\ & \qquad + \frac{r\kappa_G}{2} \int_{\Omega} TODO \d{x}
      \\ & \qquad + \frac{r\kappa}{2} \int_{\Omega} -\frac{2uc}{r} \left( u_{11} + \frac{u+u_{22}}{r^2} \right) + \frac{c^2}{2} \left(u_1^2 + \frac{u_2^2}{r^2} \right) \d{x}
      \\ & \qquad + \frac{r\sigma}{2} \int_{\Omega} \left(u_1^2 + \frac{u_2^2}{r^2} \right) \d{x}
      \\ & \qquad + r\kappa \int_{\Omega} -c\left(u_{11} + \frac{u+u_{22}}{r^2} \right) + \frac{uc^2}{2r} \d{x}
      %~ \\ & \qquad + \frac{r\kappa_G}{2} \int_{\Omega} TODO \d{x}
      \\ & \qquad + r\sigma \int_{\Omega} \frac{u}{r} \d{x} % + C
      \text{.}
    \end{align*}
    As in \cref{thm:tubeGraphCase} for the parameterization over tubular surfaces it is again assumed that $u$ fulfills appropriate periodic boundary conditions along the boundary $[0,L] \times \{0,2\pi\}$.
    %Assume vanishing Gaussian curvature $\kappa_g = 0$ and assume that the spontaneous curvature is given by $c_0 = H_0 = \frac{1}{r}$.
    %~ Then the linearized energy simplifies to 
    %~ \begin{align*}
      %~ \ELin(u) = \int_{\cM_0} \frac{\kappa}{2} \left( \Deltasurfn u + u \norm{\cH_0}_F^2 \right)^2 + \frac{\sigma}{2} \norm{\gsurfn u}^2 \HMn + \text{const}
      %~ \text{.}
    %~ \end{align*}
    %~ Using the parameterization
    %~ \begin{align*}
      %~ \varphi(x_1,x_2) = \mat{x_1 \\ r\cos(x_2) \\ r\sin(x_2)}
    %~ \end{align*}
    %~ and $\wt{u} := u \circ \varphi$
    %~ we can express the surface derivatives in local coordinates by
    %~ \begin{align*}
      %~ \gsurfn u & = \mat{\partial_1 \wt{u} \\ -\frac{\sin(x_2)}{r} \partial_2\wt{u} \\ -\frac{\cos(x_2)}{r} \partial_2\wt{u}}
      %~ ,\qquad \Deltasurfn u = \partial_{11} \wt{u} + \frac{\partial_{22}\wt{u}}{r^2}
    %~ \end{align*}
    %~ and the extended Weingarten map is given by
    %~ \begin{align*}
      %~ \cH_0 = \frac{1}{r}\mat{0 & 0 & 0 \\ 0 & \sin(x_2)^2 & -\sin(x_2)\cos(x_2) \\ 0 & -\sin(x_2)\cos(x_2) & \cos(x_2)^2}\text{,}
    %~ \end{align*}
    %~ hence $\norm{\cH_0}_F^2 = \frac{1}{r^2}$.
    %~ As of $\det\left(D\varphi^TD\varphi\right)^{1/2} = r$, the energy parameterized over the domain $\Omega = [0,L] \times [0,2\pi]$ is
    %~ \begin{align*}
      %~ \ELin(u)
      %~ & = \frac{1}{2} \int_{\Omega} \frac{\kappa}{r^3} \left( r^2\partial_{11} \wt{u} + \partial_{22}\wt{u} + \wt{u} \right)^2 + \frac{\sigma}{r} \left( r^2(\partial_1\wt{u})^2 + (\partial_2\wt{u})^2 \right) \d{x}\text{.}
    %~ \end{align*}
  \end{example}
  
  %~ \begin{example}[Sphere]
    %~ Let $r \in \R_{>0}$ and suppose
    %~ \begin{align*}
      %~ \cM_0 = \left\{ x \in \R^3 \mid \norm{x} = r \right\}
    %~ \end{align*}
    %~ is the sphere of radius $r$.
    %~ We assume that the spontaneous curvature coincides with the mean curvature on $\cM_0$, \ie $c_0 = H_0 \equiv \frac{2}{r}$, and that $\kappa_g = 0$.
    %~ \TK{To-do: Rest, basically as for tube.}
  %~ \end{example}
