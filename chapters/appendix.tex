\stepcounter{chapter}
\setcounter{secnumdepth}{2}
\renewcommand{\thesection}{\Alph{section}}
%~ \renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}} % For fancy header
\section{Matrix calculus: Differentiation of determinants and inverse matrices}
  In what follows, let $\cB \subseteq \mathbb{R}^d$ an open domain, $A \in C^1(\cB, \R^{n\times n})$ and $x \in \cB$.
  The symbol~$\partial_i$ denotes the partial derivative with respect to the $i$-th component.

  \begin{lemma}\label{thm:matrixInverseDerivative}
    If $A(x)$ is regular, then
    \begin{align*}
      \partial_i \left( A(x)^{-1} \right) = -A(x)^{-1} \left(\partial_i A(x)\right) A(x)^{-1}\text{.}
    \end{align*}
  \end{lemma}
  
  \begin{proof}
    \cite[Chapter 8]{Magnus88}
  \end{proof}
  
  
  \begin{lemma}[Jacobi's formula]\label{thm:matrixDeterminantDerivative}
    It holds
    \begin{align*}
      \partial_i \det(A(x)) = \Tr\left( \adj(A(x)) \partial_i A(x) \right)\text{.}
    \end{align*}
    If $A(x)$ is regular, then
    \begin{align*}
      \partial_i \det(A(x)) = \det(A(x)) \Tr\left( A(x)^{-1} \partial_i A(x) \right)\text{.}
    \end{align*}
  \end{lemma}
  
  \begin{proof}
    \cite[Chapter 8]{Magnus88}
  \end{proof}
  
  
  \begin{comment}
  \begin{lemma}
    Let $A(x)$ be symmetric and let $\lambda_0 \in \R$ a simple eigenvalue of $A(x)$ with normalized eigenvector $v_0 \in \R^n$.
    Then there exists an open neighborhood $\cB' \subseteq \cB$ of $x$ and unique functions $\lambda\colon \cB' \rightarrow \R$, $v\colon\cB' \rightarrow \R^n$ such that $\lambda(x) = \lambda_0$, $v(x) = v_0$ and $A(y)v(y) = \lambda(y)v(y)$ for all $y \in \cB'$.
    Furthermore, it holds
    \begin{align*}
      \partial_i \lambda(x) & = v_0^T \left(\partial_i A(x)\right) v_0
      \\ \partial_i v(x) & = (\lambda_0 - \id)^+ \left(\partial_i A(x)\right)v_0
    \end{align*}
    where $(\cdot)^+$ denotes the Moore--Penrose pseudoinverse.
  \end{lemma}
  
  \begin{proof}
    See \cite[Section 12]{Andrew93}.
  \end{proof}
  \end{comment}
  
  
  \begin{comment}
  \begin{lemma}\label{thm:matrixEigenDerivative}
    Let $A$ be symmetric, $\lambda \in C^1(\cB,\R)$ and $v\in C^1(\cB,\R^n)$ such that $A(y)v(y) = \lambda(y) v(y)$ for all $y\in\cB$.
    Then
    \begin{align*}
      \partial_i \lambda(x) & = v_0^T \left(\partial_i A(x)\right) v_0
      \\ \partial_i v(x) & = (\lambda_0 - \id)^+ \left(\partial_i A(x)\right)v_0
    \end{align*}
    where $(\cdot)^+$ denotes the Moore--Penrose pseudoinverse.
  \end{lemma}
  
  \begin{proof}
    %~ See \cite[Section 12]{Andrew93} or also \cite{Lancaster64}.
    See \cite[Chapter 8]{Magnus88} or also \cite{Lancaster64}.
  \end{proof}
  \end{comment}


\newpage
\incsection[appendix/linearization]{Differential geometry for perturbed surfaces}
\newpage

\section{Ordinary differential equations: Existence and sensitivity}
  \begin{theorem}[Picard--Lindel\"of]
    \label{thm:odePicardLindelof}
    Suppose $n \in \N$, $y_0 \in \R^n$, $\tau, r \in \R_{>0}$, and define
    \begin{align*}
      I = [0,\tau], \qquad
      \Omega  = \{ x \in \R^n \mid \norm{x - y_0}_{\infty} \leq r \}
      \text{.}
    \end{align*}
    Let $f \in C(I \times \Omega, \R^n)$ be uniformly Lipschitz continuous with respect to $x \in \Omega$ and define $J := [0, \min( \tau, r/ \norm{f}_{C(I\times\Omega)})]$.
    Then there exists a unique function $y\colon J \rightarrow \R^n$ such that for all $t\in J$
    \begin{align*}
      y'(t) = f(t,y(t))
      ,\qquad y(0) = y_0
      \text{.}
    \end{align*}
  \end{theorem}
  
  \begin{proof}
    \cite[Chapter II, Theorem 1.1]{Hartman02}
  \end{proof}


  \begin{theorem}
    \label{thm:odeSmoothness}
    Let $k,n \in \N$, $\tau \in \R_{>0}$ and let $A \subseteq \R^k \times \R^n$ be open.
    Suppose $m \in \N_{\geq 1}$ and $f \in C^m([0,\tau]\times \overline{A})$ such that all partial derivatives of $f$ are bounded.
    Then there exists a unique function $\eta \in C^m([0,\tau]\times A, \R^n)$ such that for all $t \in [0,\tau]$ and $(x,y_0) \in A$ holds
    \begin{align*}
      \frac{\partial}{\partial t} \eta(t,x,y_0) & = f\left(t,x,\eta(t,x,y_0)\right)
      ,\qquad \eta(0,x,y_0) = y_0
      \text{.}
    \end{align*}
  \end{theorem}
  
  \begin{proof}
    \cite[Chapter V, Theorem 4.1]{Hartman02}
  \end{proof}
  
  
  \begin{theorem}
    \label{thm:odeDerivative}
    Let $\eta(t,x,y_0)$ as in \cref{thm:odeSmoothness} and suppose $\eta \in C^1([0,\tau]\times A, \R^n)$.
    Then $\dd{y_0} \eta = \wt{\eta}$ where $\wt{\eta}$ solves the ODE
    \begin{align*}
      \dd{t} \wt{\eta}(t,x,y_0) = \dd{y_0}f \left(t,x,\eta(t,x,y_0)\right) \cdot \wt{\eta}(t,x,y_0)
      ,\qquad \wt{\eta}(0,x,y_0) = \operatorname{id}\text{.}
    \end{align*}
  \end{theorem}
  
  \begin{proof}
    \cite[Chapter V, Theorem 3.1]{Hartman02} 
  \end{proof}
  
  
  \begin{theorem}
    \label{thm:odeDerivative2}
    Let $\eta(t,x,y_0)$ as in \cref{thm:odeSmoothness} and suppose $\eta \in C^1([0,\tau]\times A, \R^n)$.
    Then $\dd{x} \eta = \wt{\eta}$ where $\wt{\eta}$ solves the ODE
    \begin{align*}
      \dd{t} \wt{\eta}(t,x,y_0) & = \dd{y_0}f \left(t,x,\eta(t,x,y_0)\right) \cdot \wt{\eta}(t,x,y_0) + \dd{x} f \left(t,x,\eta(t,x,y_0)\right)
      \\ \wt{\eta}(0,x,y_0) & = 0\text{.}
    \end{align*}
  \end{theorem}
  
  \begin{proof}
    \cite[Chapter V, Theorem 3.1]{Hartman02} 
  \end{proof}


\begin{comment}
\section{A class of conforming $H^k$ finite element spaces on grids of rectangles}
  In this section we introduce a class of $H^k(\Omega)$ finite elements for rectangular domains $\Omega\subseteq\R^d$ over grids \TK{that are axis-parallel and out of rectangles, without hanging nodes}.

  Let $k\in\N$ and consider the discrete subspace
  \begin{align*}
    \cS := \left\{ v \in C^k(\Omega) \, \big\vert\, v|_{E} \in \cQ_k \text{ for all } E \in \cT \right\} \subseteq H^k(\Omega)
    \text{.}
  \end{align*}
  We construct a basis of $\cS$: For $i \in \{0,\dots,k-1\}$ let $\psi_i\in \cP_{2k}$ the solution of the one-dimensional Hermite interpolation problem
  \begin{align*}
    \forall{j\in\{0,\dots,k-1\}\colon}\ %
    \frac{\partial^j}{\partial x^j} \psi_i(0) = \delta_{ij}
    ,\quad \frac{\partial^j}{\partial x^j} \psi_i(1) = 0\text{.}
  \end{align*}
  Now, for multi-indices $\alpha \in \cA := \{0,1\}^d$ and $\beta\in \cB := \{0,\dots,k-1\}^d$ let
  \begin{align*}
    \Psi_{\alpha,\beta}(x) := \prod_{i=1}^d (-1)^{\alpha_i\beta_i}\psi_{\beta_i}\left(x + \alpha_i (1-2x) \right)\text{.}
  \end{align*}
  It is readily seen that the local basis $(\Psi_{\alpha,\beta})_{\alpha\in\cA,\beta\in\cB}$ spans $\cQ_k$ and has the nodal property on the cube spanned by the set $\cA$
  \begin{align*}
    \partial^{\beta'} \Psi_{\alpha,\beta}(\alpha') = \delta_{\alpha,\alpha'} \, \delta_{\beta,\beta'}\text{.}
  \end{align*}
  This immediately induces a basis on $\cS$ with the corresponding nodal property, which, as of $\cP_k \subseteq \cQ_k$ and \TK{Lemma from Ciarlet} fulfills \TK{the typical approximation property}.

  For $d=2$, the special cases $k=1$ and $k=2$ lead to bilinear and Bogner--Fox--Schmit finite element spaces, respectively.
\end{comment}


\newpage
\incsection[appendix/discretizations]{Schemes related to the fictitious domain stabilized Nitsche method}

\newpage
\incsection[appendix/repulsion]{A smooth repulsion potential for FCHo2 particle--particle interactions}
