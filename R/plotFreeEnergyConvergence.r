# Plots the convergence behavior of the free energy perturbation method.
# Uses data as generated by freeEnergyExample.r -> fDoTTSeq

# Reference value Q:
Q = 0.056710052243983

# Collect data.
data = matrix(nrow=0, ncol=4)
for(coreId in 1:7)
{
  fname = sprintf('../data/FVals_%d.csv', coreId)
  newData = matrix(nrow=0, ncol=4)
  newData = tryCatch(as.matrix(read.csv(file=fname)))
  data  = rbind(data, newData)
}
for(coreId in 1:7)
{
  fname = sprintf('../data/xmg/FVals_%d.csv', coreId)
  newData = matrix(nrow=0, ncol=4)
  newData = tryCatch(as.matrix(read.csv(file=fname)))
  data  = rbind(data, newData)
}
data = data[complete.cases(data),]
colnames(data) <- c('T', 'tau', 'N', 'Fval')

# Extract collective data (namely those where T and tau coincide).
# It is assumed that those are uniquely identified by their T-values
Tvals = sort(unique(data[,'T']))
Fvals = list()
for(Tval in Tvals)
  Fvals[[Tval]] = data[data[,'T'] == Tval, 'Fval']

# Compute mean and variances, and count how many trajectories we have.
n = length(Tvals)
M = vector('numeric',n)
V = vector('numeric',n)
K = vector('numeric',n)
for(i in 1:n)
{
  Fval = Fvals[[Tvals[i]]]
  Eval = abs(Fval-Q)
  
  M[i] = mean(Eval)
  V[i] = var(Eval)
  K[i] = length(Fval)
}

# Plot stuff.
filename = 'free_energy_convergence'
svg_name = sprintf('../gfx/%s.svg',filename)
pdf_name = sprintf('../gfx/%s.pdf',filename)
svg(filename=svg_name)
I = 1/Tvals

# Plot mean curve.
cexLab  = 2
cexMain = 2.5
cexAxis = 1.7
cexLeg  = 2
lwd = c(5)
legPos='topleft'
col = c(1)
par(mar=c(4.2,5,1,0))
lty=c('solid')


ticksX = c(0.001, 0.01, 0.1)
labelsX = c('0.001', '0.01', '0.1')
ticksY = c(0.0005, 0.0015, 0.005)
labelsY = c('5e-4', '1.5e-3', '5e-3')

plot(I, M, log='xy', type='o', pch='',
  lty=lty[1],
  lwd=lwd[1],
  col=col[1],
  xlab=bquote(step~size~tau == 1/T),
  ylab='approximation error',
  xaxt='n',
  yaxt='n',
  bty='n',
  xlim=c(min(ticksX),max(ticksX)),
  ylim=c(min(ticksY),max(ticksY)),
  cex.main=cexMain,
  cex.axis=cexAxis,
  cex.lab=cexLab
)

axis(side = 1, at = ticksX, cex.axis=cexAxis, labels=labelsX)
axis(side = 2, at = ticksY, cex.axis=cexAxis, labels=labelsY)

# Mark mean +- one standard deviation.
tmp = M-sqrt(V);
tmp[tmp < min(ticksY)] = min(ticksY)
polygon( c(I, rev(I)), c(tmp, rev(M+sqrt(V))), col='yellow', border=NA)

# And redraw the mean-curve.
lines(I, M, lty=lty[1], lwd=lwd[1], col=col[1])

# Close file.
dev.off();
  
# Do some conversion magic.
cmd = sprintf('inkscape -D -z --file=%s --export-pdf=%s --export-latex', svg_name, pdf_name)
system(cmd)
