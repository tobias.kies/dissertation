# Plot a particle-particle interaction potential.
require('statmod')

gaussX  = gauss.quad(250*0.2) #4
gaussY  = gauss.quad(100*0.2) #4

cat('std::vector<double> pointsX  = {')
for(x in gaussX$nodes)
  cat(sprintf('%.15f, ', x))
cat('};\n')

cat('std::vector<double> weightsX = {')
for(x in gaussX$weights)
  cat(sprintf('%.15f, ', x))
cat('};\n')
  
cat('std::vector<double> pointsY  = {')
for(x in gaussY$nodes)
  cat(sprintf('%.15f, ', x))
cat('};\n')

cat('std::vector<double> weightsY = {')
for(x in gaussY$weights)
  cat(sprintf('%.15f, ', x))
cat('};\n')
