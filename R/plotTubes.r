require(ggplot2)
require(viridis)

# Set general config.
r <- 0.195
#~ I <- c(0,3,5,10,20,40,68)
#I <- seq(0,266)
#~ I <- c(0, 248)
#~ I <- 0:248
#~ I <- 0:266
#~ path <- '/home/mi/tokies/Backup/Promotion/tube8/iterates/cross_section_%05d%s'
#~ txt <- '.txt'
#~ txt <- ''
#~ savePath <- '../gfx/tubes/%02d.jpg'


I <- c(0, 5, 10, 25, 50, 150, 250)
path <- '/home/mi/tokies/Backup/Promotion/tube10/iterates/cross_section_%05d%s'
txt <- '.txt'
savePath <- '../gfx/tube/%02d.eps'

limits <- c(0,0)
lim <- c(-1,1)
for(i in I)
{
  data <- read.table(sprintf(path, i, txt))

  p <- t(data[1:6,])
  phia <- p[1,4]
  phib <- p[2,4]
  phi  <- if( (phia+phib)/2 > pi/2 ) (phia+phib)/2-pi/2 else (phia+phib)/2
  phi  <- pi/2-phi
  alpha <- data[7:nrow(data),1] + phi
  u <- data[7:nrow(data),2]
  C <- cos(alpha)
  S <- sin(alpha)
  ref <- cbind(C,S)
  pert <- (1+u)*ref

  limits[1] = min(limits[1], u, na.rm=T)
  limits[2] = max(limits[2], u, na.rm=T)
  lim[1] = min(lim[1], pert, na.rm=T)
  lim[2] = max(lim[2], pert, na.rm=T)
  
  lim[1] = -max(abs(lim))
  lim[2] = -lim[1]
}

# Hard coded limits:
cat(sprintf('Got natural limit %f to %f\n', limits[1], limits[2]))
limits[2] <- 0.3

for(i in 1:length(I))
{
  # Load data.
  data <- read.table(sprintf(path, I[i], txt))

  # Parse data further.
  p <- t(data[1:6,])

  phia <- p[1,4]
  phib <- p[2,4]
  phi  <- if( (phia+phib)/2 > pi/2 ) (phia+phib)/2-pi/2 else (phia+phib)/2
  phi  <- pi/2-phi

  xa <- c(cos(phi)*p[1,2]-sin(phi)*p[1,3], sin(phi)*p[1,2]+cos(phi)*p[1,3])
  xb <- c(cos(phi)*p[2,2]-sin(phi)*p[2,3], sin(phi)*p[2,2]+cos(phi)*p[2,3])

  v <- c(r*cos(phia), r*sin(phia))
  v <- c(cos(phi)*v[1]-sin(phi)*v[2], sin(phi)*v[1]+cos(phi)*v[2])
  qa1 <- xa + v
  qa2 <- xa - v
#~   qa1 <- xa + r*c(cos(phi), sin(phi))
#~   qa2 <- xa - r*c(cos(phi), sin(phi))

  v <- c(r*cos(phib), r*sin(phib))
  v <- c(cos(phi)*v[1]-sin(phi)*v[2], sin(phi)*v[1]+cos(phi)*v[2])
  qb1 <- xb + v
  qb2 <- xb - v
#~   qb1 <- xb + r*c(cos(phi), -sin(phi))
#~   qb2 <- xb - r*c(cos(phi), -sin(phi))

  alpha <- data[7:nrow(data),1] + phi
  u <- data[7:nrow(data),2]
  
  beta <- acos( (xa[1]*xb[1]+xa[2]*xb[2])/sqrt((xa[1]^2+xa[2]^2)*(xb[1]^2+xb[2]^2)) )
  cat(sprintf('%d (%d) has separation angle %f\n', i, I[i], beta*180/pi))

  # Compute quantities.
  C <- cos(alpha)
  S <- sin(alpha)
  ref <- cbind(C,S)
  pert <- (1+u)*ref

  #~ plot(pert[,1], pert[,2])
  #~ lines(ref[,1], ref[,2], lty='dashed')
  #~ points( c(xa[1], xb[1]), c(xa[2], xb[2]) )

  #~ lines(c(qa1[1], qa2[1]),c(qa1[2], qa2[2]))
  #~ lines(c(qb1[1], qb2[1]),c(qb1[2], qb2[2]))

  circle <- data.frame(x = ref[,1], y = ref[,2])
  perturbation <- data.frame(x = pert[,1], y = pert[,2])
  lineA <- data.frame(x = c(qa1[1], qa2[1]), y = c(qa1[2], qa2[2]))
  lineB <- data.frame(x = c(qb1[1], qb2[1]), y = c(qb1[2], qb2[2]))
  
  colors <- viridis(100)
  p <- (
    ggplot(circle, aes(x,y))
    + coord_fixed(ratio=1, xlim=lim, ylim=lim)
    + geom_path(linetype='dotted', size=3, color='gray')
    + scale_x_continuous(breaks=c(-1,0,1))
    + scale_y_continuous(breaks=c(-1,0,1))
    + geom_path(data = lineA, aes(x,y), color='red', size=4)
    + geom_path(data = lineB, aes(x,y), color='red', size=4)
    + scale_color_gradientn(colors=colors, limits=limits, breaks = c(-0.3, 0, 0.3))
    + geom_path(data = perturbation, aes(x,y, color=u), size=7, show.legend=(I[i]==0))
    + theme(axis.title.x = element_blank())
    + theme(axis.title.y = element_blank())
    + theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank())
    + theme(panel.background = element_rect(fill = "white"))
    + theme(text = element_text(size=40))
    + theme(legend.key.height = unit(2,'cm'))
    + theme(axis.ticks=element_line(size=2))
  )
  
  if(I[i] != 0)
  {
    p <- (p
      + theme(axis.text=element_blank())
      + theme(axis.ticks=element_blank())
    )
  }
  
  if(length(I) == 1)
    print(p)
  else
#~     ggsave(sprintf(savePath, i), device='jpg')
    ggsave(sprintf(savePath, i), device='eps')
}
