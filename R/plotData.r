# R script to plot the given data.

source("funcs.r")

# Hard config.
# Decide what gets plotted.
fCurveExact             = F
fCurveInexact           = F
fPointExact             = F
fPointInexact           = F
fGradientsCurveInexact  = F
fGradientsPointInexact  = F
fFCHo2Energy            = T


# Annulus curve example with exact solution.
if(fCurveExact)
{
  name = 'convergence_curve_exact'
  data = read.table(sprintf('../data/%s.txt', name))
  normNames = c('L2', 'H1', 'H2')
  for(i in 1:3)
  {
    plotData(data[,1], data[,i+1], normNames[i], 5-i,
      sprintf('convergence_curve_exact_%s', normNames[i])
    )
  }
}

# Curve example with inexact solution.
if(fCurveInexact)
{
  name = 'convergence_curve_inexact2'
  data = read.table(sprintf('../data/%s.txt', name))
  normNames = c('L2', 'H1', 'H2')
  for(i in 1:3)
  {
    plotData(data[,1], data[,i+1], normNames[i], 5-i,
      sprintf('%s_%s', name, normNames[i])
    )
  }
}

# Point example with exact solution.
if(fPointExact)
{
  name = 'convergence_point_exact'
  data = read.table(sprintf('../data/%s.txt', name))
  normNames = c('L2', 'H1', 'H2')
  slopes = c(2,2,1)
  for(i in 1:3)
  {
    plotData(data[,1], data[,i+1], normNames[i], slopes[i],
      sprintf('%s_%s', name, normNames[i])
    )
  }
}

# Point example with inexact solution.
if(fPointInexact)
{
  name = 'convergence_point_inexact2'
  data = read.table(sprintf('../data/%s.txt', name))
  normNames = c('L2', 'H1', 'H2')
  slopes = c(2,2,1)
  for(i in 1:3)
  {
    plotData(data[,1], data[,i+1], normNames[i], slopes[i],
      sprintf('%s_%s', name, normNames[i])
    )
  }
}


if(fGradientsCurveInexact)
{
  name    = 'gradients_curve_inexact'
  name2   = 'difference_quotients_curve_inexact'
  data    = as.matrix(read.table(sprintf('../data/%s.txt', name)))
  
  # Reference is fine grid solution.
  xRef    = data[nrow(data),-1]
  print(xRef)
  errors  = apply(data[-nrow(data),-1], 1, function(x) norm(t(x-xRef)))
#~   errors  = apply(data[,-1], 1, function(x) abs(x[1]-xRef[1]))
  plotData(data[-nrow(data),1], errors, '', 4, name, 'topleft')
  
  # Reference is difference quotient solution.
#~   data2   = as.matrix(read.table(sprintf('%s.txt', name2)))
#~   xRef    = data2[nrow(data2),-1]
#~   print(xRef)
#~   errors  = apply(data[,-1], 1, function(x) norm(t(x[1:3]-xRef[1:3])))
#~   plotData(data[,1], errors, '', 4, sprintf('%s_2',name), 'topleft')
  
  # Convergence of difference quotients against above reference solution.
  data2   = as.matrix(read.table(sprintf('../data/%s.txt', name2)))
  data2   = data2[-(1:5),]
  errors  = apply(data2[,-1], 1, function(x) norm(t(x-xRef)))
  plotData(data2[,1], errors, '', 2, sprintf('%s_2',name), 'topleft')
}


if(fGradientsPointInexact)
{
  name    = 'gradients_point_inexact'
  name2   = 'difference_quotients_point_inexact'
  data    = as.matrix(read.table(sprintf('../data/%s.txt', name)))
  
  # Reference is fine grid solution.
  xRef    = data[nrow(data),-1]
  print(xRef)
  errors  = apply(data[-nrow(data),-1], 1, function(x) norm(t(x-xRef)))
  plotData(data[-nrow(data),1], errors, '', 2, name, 'topleft')
  
  # Convergence of difference quotients against above reference solution.
  data2   = as.matrix(read.table(sprintf('../data/%s.txt', name2)))
  data2   = data2[-(1:20),]
  errors  = apply(data2[,-1], 1, function(x) norm(t(x-xRef)))
  plotData(data2[,1], errors, '', 2, sprintf('%s_2',name), 'topleft')
}


# Development of energies in FCHo2 example.
if(fFCHo2Energy)
{
  name  = 'FCHo2energies'
  data  = as.matrix(read.table(sprintf('../data/%s.txt',name)))
  data  = data[1:401,]
  
  I = 0:(nrow(data)-1)
  E = data[,1]+data[,2]
  P = data[,2]
  J = data[,1]
  
  colorE='black'
  colorP=rgb(65/225,105/225,225/225)
  colorJ=rgb(35/225,75/225,155/225)

  cexLab  = 2
  cexMain = 2.5
  cexAxis = 1.7
  cexLeg  = 2

  par(mar=c(4,4.3,0,0))
  
  lwd = 3

  # Open file.
  filename = name
  svg_name = sprintf('../gfx/%s.svg',filename)
  pdf_name = sprintf('../gfx/%s.pdf',filename)
  svg(filename=svg_name)
  
  # Plot
  plot(I, E, type='o', pch='', ylim=c(min(J)-0.1,max(E)),
    xlab='iteration step',
    ylab=expression('energy /' ~ kappa),
    bty='n',
    cex.lab=cexLab,
    cex.main=cexMain,
    cex.axis=cexAxis,
    lwd=lwd
  )
  polygon(c(I,rev(I)), c(J,rev(E)),
    density = c(10, 10), angle = c(60, 60), col=colorP, border=NA
  )
  lines(I,E,col=colorE,lwd=lwd)
  lines(I,J,col=colorJ,lwd=lwd)
  
  legend('topright', inset=.07,
      col=c(colorE,colorJ), cex=cexLeg, lwd=lwd, lty=1, pch='',
      legend=c('interaction potential', 'membrane energy')
  )
  
  # Close file.
  dev.off();
  
  # Do some conversion magic.
  cmd = sprintf('inkscape -D -z --file=%s --export-pdf=%s --export-latex', svg_name, pdf_name)
  system(cmd)
}
