# Plot a particle-particle interaction potential.

# Load particle positions.
source('fcho2.r')
require(statmod)
require(ggplot2)
require(viridis)
require(ggpubr)

load(file='fcho2_lj.RData')

# Define a primitive interaction potential.
# Should be vectorized and depend on distance only.
# Here: Lennard jones, purely repulsive forces. (Pauli repulsion, cf. https://en.wikipedia.org/wiki/Lennard-Jones_potential )
# See https://www.sciencedirect.com/topics/chemistry/lennard-jones-potential section 4 for possible choices of epsilon and sigma.
# Or also https://en.wikibooks.org/wiki/Molecular_Simulation/The_Lennard-Jones_Potential#Example .
# Our distance of reference is angstrom (total particle length\is almost 200A=20nm).
# The effective distance of the potential with attraction is around 1.12*sigma
epsLJ <- 7
sigma <- 3.5
A <- 4*epsLJ*sigma^12
atomicJ <- function(r)
{
  return(A/r^12)
}


# Define the macro interaction potential.
J0 <- function(x)
{
  R <- sqrt( (atoms[,1]-x[1])^2 + (atoms[,2]-x[2])^2 + 0*atoms[,3]^2 )
  JR <- atomicJ(R)
  I <- is.finite(JR)
  return(sum(JR[I]))
}

# Vectorized version of J0.
J <- function(X)
{
  return(apply(X,1,J0))
}

# Smoothed version of the dirac delta function associated to the atoms.
g_eps <- function(X, eps=10, sparse=1)
{
  if(is.vector(X))
    X <- matrix(X, nrow=1, ncol=2)
    
  N <- seq(1, nrow(atoms),sparse)
    

#~   R2 = apply(X, 1, function(x) (x[1]-atoms[,1])^2 + (x[2]-atoms[,2])^2) / eps^2 # Columns contain now the R2-values
  
#~   # For each R2-column we need to compute the phi-sum
#~   Y = matrix(0, nrow=dim(R2)[1], ncol=dim(R2)[2])
#~   I = (R2 < 1)
#~   Y[I] = (R2[I]-1)^2
#~   res = colSums(Y)/eps^2
  
  res = vector('numeric', dim(X)[1])
#~   for(i in 1:10)
  for(i in N)
  {
    R2 = (X[,1]-atoms[i,1])^2 + (X[,2]-atoms[i,2])^2
    R2 = R2 / eps^2
    I = (R2 < 1)
    res[I] = res[I] + (R2[I]-1)^2
  }
  res = res / eps^2 * (nrow(atoms)/length(N)) / (2*pi/6);

  return(res)
}

# Smoothed interaction
smoothedInteraction <- function(V)
{
  if(is.vector(V))
    V <- matrix(V, nrow=1, ncol=2)


  # Compute gauss quadrature points.
  # Note: Actually, gauss quadrature is not a smart idea here because
  # of the non-smoothness of the integrand. Should use something like
  # piecewise Gauss quadrature instead.
  gaussX  = gauss.quad(250*0.2) #4
  gaussY  = gauss.quad(100*0.2) #4
  lX      = 250
  lY      = 100
  
  gaussX$nodes    = gaussX$nodes * lX/2
  gaussX$weights  = gaussX$weights * lX/2
  
  gaussY$nodes    = gaussY$nodes * lY/2
  gaussY$weights  = gaussY$weights * lY/2


  # Precompute smoothed dirac function over the given quadrature points,
  # and multiply it by the corresponding weights.
  X <- c(outer(gaussX$nodes, gaussY$nodes, function(a,b) a))
  Y <- c(outer(gaussX$nodes, gaussY$nodes, function(a,b) b))
  lambda <- c(gaussX$weights %o% gaussY$weights)
  cat('Set weights\n')
  glambda <- g_eps(cbind(X,Y), eps=30, sparse=100) * lambda
  cat('Computed glambda\n')
  
#~   # Apply the atomic interaction.
#~   R2 <- apply(V, 1, function(x) (x[1]-X)^2 + (x[2]-Y)^2)
#~   cat('Computed R2\n')
#~   res <- apply(R2, 2, function(x){
#~     I = (x > 0) & (x < 4*sigma)
#~     return(sum(pmax(A/R2[I]^6,10) * glambda[I]))
#~   })
#~   cat('Computed res\n')

  res = vector('numeric', nrow(V))
  for(i in 1:nrow(V))
  {
    if(i %% 100 == 0 || i == nrow(V))
      cat('\r', i, '/', nrow(V))
    x   <- V[i,]
    R2  <- (x[1]-X)^2 + (x[2]-Y)^2
    I   <- (R2 > 0) & (R2 < 16*sigma^2)
#~     res[i] <- sum(pmax(A/R2[I]^6,1) * glambda[I])
    res[i] <- sum(1/(100+R2) * glambda)
  }
  cat('\n')

  return(res)
}


# Visualize the macro interaction potential over a sufficiently large domain.
IX = seq(-110,110,length.out=111*10)
IY = seq(-40,40,length.out=41*10)
X = c(outer(IX, IY, function(a,b) a))
Y = c(outer(IX, IY, function(a,b) b))

if(!exists('D'))
{
  Z = log10(J(cbind(X,Y)))
  D <- data.frame(X,Y,Z)
}

if(!exists('E'))
{
  Z = g_eps(cbind(X,Y))
  E <- data.frame(X,Y,Z)
}

if(!exists('G'))
{
  Z = g_eps(cbind(X,Y), eps=30, sparse=100)
  G <- data.frame(X,Y,Z)
}

if(!exists('H'))
{
  IX_ = seq(-110,110,length.out=111*4)
  IY_ = seq(-40,40,length.out=41*4)
  X_ = c(outer(IX_, IY_, function(a,b) a))
  Y_ = c(outer(IX_, IY_, function(a,b) b))
  Z = smoothedInteraction(cbind(X_,Y_))
  H <- data.frame(X_,Y_,Z)
}


# Create plots.
colors <- viridis(100)


fDoOriginal = T
fDoSmoothedDelta = T
fDoSmoothedDeltaSparse = T
fDoSmoothedInteraction = T

# Level set/contour view of original interaction
if(fDoOriginal)
{
  v1 <- (
    ggplot(D, aes(D$X, D$Y, z = D$Z))
    + geom_raster(aes(fill = D$Z))
    + geom_contour(color = 'green', breaks=c(-3,1))
    + scale_fill_gradientn(colours=colors, name=expression(log[10](E[LJ])))
    + scale_x_continuous(position = "top")
    + coord_fixed()
    + theme(axis.title.x = element_blank())
    + theme(axis.title.y = element_blank())
    + theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank())
    + theme(panel.background = element_rect(fill = "white"))
    + theme(text = element_text(size=15),
        axis.text.x = element_text(angle=0))
    + theme(legend.position = c(1.2,0.6))
  )
  ggsave('../gfx/fcho2_lj_contour.eps', device='eps')
#~   print(v1)
}


# Level set/contour view of smoothened delta distribution
if(fDoSmoothedDelta)
{
  v2 <- (
    ggplot(D, aes(E$X, E$Y, z = E$Z))
    + geom_raster(aes(fill = E$Z))
    + geom_contour(color = rgb(0,0,0,alpha=0)) # Dummy contour plot.
    + scale_fill_gradientn(colours=colors, name=expression(g[epsilon]), limits = c(0,1.5))
    + scale_x_continuous(position = "top")
    + coord_fixed()
    + theme(axis.title.x = element_blank())
    + theme(axis.title.y = element_blank())
    + theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank())
    + theme(panel.background = element_rect(fill = "white"))
    + theme(text = element_text(size=15),
        axis.text.x = element_text(angle=0))
    + theme(axis.text.x=element_text(color='white'))
    + theme(axis.ticks.x=element_line(color='white'))
    + theme(legend.position = c(1.154,0.6))
  )
  ggsave('../gfx/fcho2_lj_smoothed.eps', device='eps')
#~   print(v2)
}


# Level set/contour view of smoothened sparse delta distribution
if(fDoSmoothedDeltaSparse)
{
  v3 <- (
    ggplot(G, aes(G$X, G$Y, z = G$Z))
    + geom_raster(aes(fill = G$Z))
    + geom_contour(color = rgb(0,0,0,alpha=0)) # Dummy contour plot.
    + scale_fill_gradientn(colours=colors, name=expression(tilde(g)[epsilon]))
    + scale_x_continuous(position = "top")
    + coord_fixed()
    + theme(axis.title.x = element_blank())
    + theme(axis.title.y = element_blank())
    + theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank())
    + theme(panel.background = element_rect(fill = "white"))
    + theme(text = element_text(size=15),
        axis.text.x = element_text(angle=0))
    + theme(axis.text.x=element_text(color='white'))
    + theme(axis.ticks.x=element_line(color='white'))
    + theme(legend.position = c(1.165,0.6))
  )
  ggsave('../gfx/fcho2_lj_smoothed_sparse.eps', device='eps')
#~   print(v3)
}


# Level set/contour view of the smoothed interaction
if(fDoSmoothedInteraction)
{
  v4 <- (
    ggplot(H, aes(H$X, H$Y, z = (H$Z)))
    + geom_raster(aes(fill = (H$Z)))
    + geom_contour(color = rgb(0,0,0,alpha=0)) # Dummy contour plot.
    + scale_fill_gradientn(colours=colors, name=expression(K%.%tilde(g)[epsilon]))
    + scale_x_continuous(position = "top")
    + coord_fixed()
    + theme(axis.title.x = element_blank())
    + theme(axis.title.y = element_blank())
    + theme(panel.border = element_blank(),
          panel.grid.major = element_blank(),
          panel.grid.minor = element_blank())
    + theme(panel.background = element_rect(fill = "white"))
    + theme(text = element_text(size=15),
        axis.text.x = element_text(angle=0))
    + theme(axis.text.x=element_text(color='white'))
    + theme(axis.ticks.x=element_line(color='white'))
    + theme(legend.position = c(1.14,0.6))
  )
  ggsave('../gfx/fcho2_lj_smoothed_interaction.eps', device='eps')
#~   print(v4)
}

v <- ggarrange(v1, v2, v3, v4, ncol=1, nrow=4)
  ggsave('../gfx/fcho2_lj_all.eps', device='eps')
#~ print(v)
