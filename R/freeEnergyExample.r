
# Set general problem parameters.
beta    = 25
slope0  = 0.1
slope1  = 0.2
L       = 0.9

K       = 1000

Tmax0   = 10
taus    = 0.1^(1:5)

tau0    = 0.1
Tmaxs   = 10^(0:4)

fDoTau      = F
fDoTmax     = F
fDoTmaxTau  = F
fDoTTConv   = F
fDoTTSeq    = T


# Load data.
data = read.table('../data/free_energy_slope.txt')

# Sort by first column.
I    = order(data[,1])
data = data[I,]

# Remove outliers.
# -> Not needed.

# Extract R, f and Df.
R   = data[,1]
fR  = data[,2]
DfR = data[,3]



# Formatted printing.
printf <- function(...) cat(sprintf(...))


# Indexing function.
idx <- function(r)
{
  N = 1:length(R)
  I = max(N[R <= r])
  
  id = I
#~   if(I < length(R) && abs(R[I+1]-r) < abs(R[I]-r)) # <- From an older, non-interpolating version
#~     id = I+1
  if(id == length(R))
    id = id-1
    
  return(id);
}


# A function for evaluation of the energy.
f <- function(x)
{
  if(is.vector(x))
    Rx  = sqrt(x[1]^2 + x[2]^2)
  else
    Rx  = sqrt(x[,1]^2 + x[,2]^2)
  rIds  = unlist(lapply(Rx,idx))
  lambda = (Rx - R[rIds])/(R[rIds+1]-R[rIds])
  fx    = (1-lambda)*fR[rIds] + lambda*fR[rIds+1]
  return(fx)
}


# A function for evaluation of the Jacobian.
Df <- function(x)
{
  if(is.vector(x))
    Rx  = sqrt(x[1]^2 + x[2]^2)
  else
    Rx  = sqrt(x[,1]^2 + x[,2]^2)
  rIds  = unlist(lapply(Rx,idx))
  
  lambda = (Rx - R[rIds])/(R[rIds+1]-R[rIds])
  Dfr = (1-lambda)*DfR[rIds] + lambda*DfR[rIds+1]
  Rx[Rx == 0] = Inf
  if(is.vector(x))
    Dfx = c(x[1]*Dfr/Rx, x[2]*Dfr/Rx)
  else
    Dfx = cbind(Dfr*x[,1]/Rx, Dfr*x[,2]/Rx)
  return(Dfx)
}


# Compute a sequence of iterates.
# To-do: Could consider starting in a random point.
eulerMaruyama <- function(tau, N)
{
  x   = matrix(nrow=N,ncol=2)
  G   = matrix(rnorm(N*2),nrow=N,ncol=2)
  x[1,] = c(0,0)
  if(N > 1)
    for(i in 2:N)
    {
      x[i,] = x[i-1,] - tau*Df(x[i-1,]) + sqrt(2*tau/beta) * G[i,]
      rx = (x[i,1]^2 + x[i,2]^2)
      while(rx > max(R))
      {
        x[i,] = x[i,] * (2*max(R)/rx-1)
        rx = (x[i,1]^2 + x[i,2]^2)
        #cat('.') # For debugging purposes.
      }
    }
    
  return(x)
}


# The observable: exp(-beta*(V1-H0)) = exp(-beta*(s1^2/s0^2-1)*f)
observable <- function(x)
{
  A = exp(-beta*(slope1^2/slope0^2-1)*f(x))
  return(A)
}


# Quadrature-estimate of the free energy difference.
quadEstimate <- function()
{
  # Has been hardcoded. As computed by matlab:
  if(beta != 25)
    stop("Hammer time!")
  return(0.056710052243983) # beta = 25
}


# Perturbation method estimate of the free energy difference.
perturbationMethod <- function(tau, N)
{
#~   printf('Applying perturbationMethod to tau=%.2g and N=%d.\n', tau, N)
  x   = eulerMaruyama(tau,N)
  EF  = sum(observable(x))/N
  Fval = -log(EF)/beta
#~   printf('Got value %.6g\n', Fval)
  return(Fval)
}


# Goal: Total of K trajectories given tau and N.
# Split those among the cores.
repeatedPerturbation <- function(coreId, tau, N)
{
  k = round(K/no_cores)
  if(coreId == no_cores)
    k = max(0, K-(no_cores-1)*k)
  else if(k*(coreId-1) >= K)
    k = 0
  return(unlist(lapply(1:k, function(dummy) perturbationMethod(tau, N))))
}

# Goal: Do one runs of the perturbation method for each element in a
#       list of tau's and N's.
# While doing so, append raw data results to a file.
sequentialPerturbation <- function(coreId, tau, N)
{
  fname = sprintf('../data/FVals_%d.csv', coreId)
  fnameBak = sprintf('../data/FVals_%d.csv.bak', coreId)

  Fvals = matrix(nrow=0, ncol=4)
  if(file.exists(fname))
    Fvals = as.matrix(read.csv(file=fname))
  
  n = min(length(tau), length(N))
  
  while(TRUE) # To-do: Could consider a finite loop here.
  {
    nRows = dim(Fvals)[1]
    Fvals = rbind(Fvals, matrix(nrow=n, ncol=4))
    startTime = Sys.time()
    for(i in 1:n)
    {
      printf('Step i=%d with T=%.4g, tau=%.4g and N=%d; ', i, tau[i]*N[i], tau[i], N[i])
      
      Fval  = perturbationMethod(tau[i], N[i])
      Fvals[nRows+i,] = c(tau[i]*N[i], tau[i], N[i], Fval)

      endTime = Sys.time()
      timeSinceSave = difftime(endTime, startTime, units="secs")
      print(timeSinceSave)
      
      # Dump the results somewhat frequently in order to avoid data loss.
      if(timeSinceSave > 120 && i != n)
      {
        tryCatch(file.copy(fname, fnameBak))
        write.csv(Fvals, file=fname, row.names=FALSE)
        startTime = Sys.time()
      }
    }
      
    # Save results.
    tryCatch(file.copy(fname, fnameBak))
    write.csv(Fvals, file=fname, row.names=FALSE)
  }
  
  return(Fvals)
}


# Set up parallel environment.
require(foreach)
require(doParallel)
no_cores <- detectCores() - 1
cl <- makeCluster(no_cores)
registerDoParallel(cl)


# Get estimate of exact value.
Q = quadEstimate()


# Have a look at the error over tau.
if(fDoTau)
{
  printf('Computations: Behavior in dependence of tau for fixed Tmax=%.2g\n', Tmax0)
  Fvals1 = matrix(nrow = length(taus), ncol = K)
  M1     = vector("numeric", length(taus))
  V1     = vector("numeric", length(taus))
  for(i in 1:length(taus))
  {
    startTime = Sys.time()

    tau = taus[i]
    printf('Executing tau=%.2g\n', tau)
  #   Fvals[i,] = unlist(lapply(1:K, function(dummy) perturbationMethod(tau, round(Tmax/tau))))
  #   Fvals2[i,] = unlist(parLapply(cl, 1:K, function(dummy) perturbationMethod(tau, round(Tmax/tau))))
  #   Fvals2[i,] = unlist(parLapply(cl, 1:no_cores, function(coreId) repeatedPerturbation(coreId,tau, round(Tmax/tau))))
  #   Fvals <- c(Fvals, perturbationMethod(tau, round(Tmax/tau)))
    Fvals1[i,] = foreach(i = 1:no_cores, coreId = 1:no_cores, .combine = 'c') %dopar%
      repeatedPerturbation(coreId, tau, round(Tmax0/tau))
    M1[i]  = mean(Fvals1[i,])
    V1[i]  = var(Fvals1[i,])
    printf( 'Got M=%.4g and V=%.4g\n', M[i], V[i])
    
    endTime = Sys.time()
    timeTaken = endTime - startTime
    print(timeTaken)
  }
  print(Fvals1)
}



# Have a look at the error over T.
if(fDoTmax)
{
  printf('Computations: Behavior in dependence of T for fixed tau=%.2g\n', tau0)
  Fvals2 = matrix(nrow = length(Tmaxs), ncol = K)
  M2     = vector("numeric", length(Tmaxs))
  V2     = vector("numeric", length(Tmaxs))
  for(i in 1:length(Tmaxs))
  {
    startTime = Sys.time()

    Tmax = Tmaxs[i]
    printf('Executing T=%4.2g\n', Tmax)
    Fvals2[i,] = foreach(i = 1:no_cores, coreId = 1:no_cores, .combine = 'c') %dopar%
      repeatedPerturbation(coreId, tau0, round(Tmax/tau0))
    M2[i]  = mean(Fvals2[i,])
    V2[i]  = var(Fvals2[i,])
    printf( 'Got M=%.4g and V=%.4g\n', M2[i], V2[i])
    
    endTime = Sys.time()
    timeTaken = endTime - startTime
    print(timeTaken)
  }
  print(Fvals2)
}


# Have a look at both things simultaneously.
if(fDoTmaxTau)
{
  printf('Computations: Behavior both in dependence of T and tau.\n')
  M3 = matrix(nrow = length(Tmaxs), ncol = length(taus))
  V3 = matrix(nrow = length(Tmaxs), ncol = length(taus))
  for(k in 1:(length(Tmaxs)+length(taus)))
  {
    printf('In iteration k=%d\n', k)
    for(i in 1:length(Tmaxs))
    {
      for(j in 1:length(taus))
      {
        if(i+j != k)
          next
        startTime = Sys.time()
        
        Tmax = Tmaxs[i]
        tau  = taus[j]
        printf('Executing T=%.2g, tau=%.2g: ', Tmax, tau)
        
        Fvals = foreach(i = 1:no_cores, coreId = 1:no_cores, .combine = 'c') %dopar%
          repeatedPerturbation(coreId, tau, round(Tmax/tau))
  #~       Fvals = repeatedPerturbation(1, tau, round(Tmax/tau)) # Only here dor debugging reasons.
        M3[i,j] = mean(Fvals)
        V3[i,j] = var(Fvals)
        printf('Got M=%.4g and V=%.4g; error: %.2g ', M3[i,j], V3[i,j], abs(M3[i,j]-Q)/Q*100)
        
        endTime = Sys.time()
        timeTaken = endTime - startTime
        print(timeTaken)
      }
    }
    printf('\n')
  }
}


# Do convergence computations for (tau,T) -> (0,\infty)
if(fDoTTConv)
{
  printf('Computations: Examine convergence as tau->0 and T->infty\n')
  seqTmax = seq(10,1000,10)
  
  M = vector('numeric', length(seqTmax))
  V = vector('numeric', length(seqTmax))
  for(i in 1:length(seqTmax))
  {
    printf('Executing %d/%d; ', i, length(seqTmax))
    startTime = Sys.time()
    
    Tmax = seqTmax[i]
    tau = 1./Tmax

    Fvals = foreach(i = 1:no_cores, coreId = 1:no_cores, .combine = 'c') %dopar%
      repeatedPerturbation(coreId, tau, seqTmax[i]^2)
    Evals = abs(Fvals-Q)

    M[i]  = mean(Evals)
    V[i]  = var(Evals)
    printf('got m=%.4g, v=%.4g; ', M[i], V[i])

    endTime = Sys.time()
    timeTaken = endTime - startTime
    print(timeTaken)
    
    save.image(file="ttconv.RData")
  }
}


# Create trajectories and evaluate observables in batch.
if(fDoTTSeq)
{
  printf('Computations: Examine convergence as tau->0 and T->infty (Batch mode)\n')

  seqTmax = seq(10,1000,10)
  taus    = 1/seqTmax
  Ns      = seqTmax^2
  
  startTime = Sys.time()
  Fvals = foreach(i = 1:no_cores, coreId = 1:no_cores, .combine = 'c') %dopar%
    sequentialPerturbation(coreId, taus, Ns)
  #Fvals = sequentialPerturbation(1, taus, Ns) # For debugging/testing purposes.
  endTime = Sys.time()
  timeTaken = endTime - startTime
  print(timeTaken)
}

# Close parallel environment.
stopImplicitCluster()
stopCluster(cl)
