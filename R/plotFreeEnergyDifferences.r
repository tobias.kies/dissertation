# Plots free energy differences for various slopes s as obtained by
# the energy perturbation method.

# Set and load data. (Copied and shortened from freeEnergyExample.r)
# See further config below after the functiond definitions.
beta = 25.
slope0  = 0.1
data = read.table('../data/free_energy_slope.txt')
I    = order(data[,1])
data = data[I,]
R   = data[,1]
fR  = data[,2]
DfR = data[,3]

# Load also reference data for free energy differences.
# (As computed by matlab.)
data2 = read.table('../data/free_energy_differences.txt')
Smatlab = data2[,1]
Qmatlab = data2[,2]

# Relevant functions. (Copied and shortened from freeEnergyExample.r)
printf <- function(...) cat(sprintf(...))

idx <- function(r)
{
  N = 1:length(R)
  I = max(N[R <= r])
  
  id = I
  if(id == length(R))
    id = id-1
    
  return(id);
}

f <- function(x)
{
  if(is.vector(x))
    Rx  = sqrt(x[1]^2 + x[2]^2)
  else
    Rx  = sqrt(x[,1]^2 + x[,2]^2)
  rIds  = unlist(lapply(Rx,idx))
  lambda = (Rx - R[rIds])/(R[rIds+1]-R[rIds])
  fx    = (1-lambda)*fR[rIds] + lambda*fR[rIds+1]
  return(fx)
}

Df <- function(x)
{
  Rx  = sqrt(x[1]^2 + x[2]^2)
  rIds  = unlist(lapply(Rx,idx))
  
  lambda = (Rx - R[rIds])/(R[rIds+1]-R[rIds])
  Dfr = (1-lambda)*DfR[rIds] + lambda*DfR[rIds+1]
  Rx[Rx == 0] = Inf
  Dfx = c(x[1]*Dfr/Rx, x[2]*Dfr/Rx)
  return(Dfx)
}

eulerMaruyama <- function(tau, N)
{
  x   = matrix(nrow=N,ncol=2)
  G   = matrix(rnorm(N*2),nrow=N,ncol=2)
  x[1,] = c(0,0)
  if(N > 1)
    for(i in 2:N)
    {
      x[i,] = x[i-1,] - tau*Df(x[i-1,]) + sqrt(2*tau/beta) * G[i,]
      rx = (x[i,1]^2 + x[i,2]^2)
      while(rx > max(R))
      {
        x[i,] = x[i,] * (2*max(R)/rx-1)
        rx = (x[i,1]^2 + x[i,2]^2)
      }
    }
    
  return(x)
}

observable <- function(x, s) # Not used due to efficiency reasons but still here for reference.
{
  A = exp(-beta*(s^2/slope0^2-1)*f(x))
  return(A)
}

perturbationMethod <- function(tau, N, S)
{
  x    = eulerMaruyama(tau,N)
  fx    = f(x)
  EF   = rowSums(outer(S, fx, function(s,y) exp(-beta*(s^2/slope0^2-1)*y)))/N
  Fval = -log(EF)/beta
  return(Fval)
}

###
# Actual Code
###

# Config.
tau   = 0.1
N     = 10
Tmax  = N*tau
K     = 100
printf('Using T=%.4g, tau=%.4g, N=%d\n', Tmax, tau, N)

# Fval data. Do many runs.
S   = seq(0.1,1,1/1000)
FS  = sapply(1:K, function(k) perturbationMethod(tau, N, S)) # Each row contains data for a fixed s
M   = apply(FS, 1, mean)
V   = apply(FS, 1, var)

# Plot.
## Open file.
filename = 'free_energy_differences'
svg_name = sprintf('../gfx/%s.svg',filename)
pdf_name = sprintf('../gfx/%s.pdf',filename)
svg(filename=svg_name)

## Set some somewhat general config.
cexLab  = 2
cexMain = 2.5
cexAxis = 1.7
cexLeg  = 2
lwd = c(5,5)
legPos='topleft'
col = c(1,1)
par(mar=c(4.2,5,0,0))
lty=c('dotted','solid')

## Plot 'exact' values.
I = (Smatlab >= min(S)) & (Smatlab <= max(S))
plot(Smatlab[I], Qmatlab[I], type='o', pch='', lty=lty[1], xlab='slope s', ylab=bquote(free~energy~difference~~Delta~F(s)), cex.lab=cexLab, bty='n', cex.main=cexMain, cex.axis=cexAxis, lwd=lwd[1], col=col[1])


## Mark mean +- one standard deviation.
polygon( c(S, rev(S)), c(M-2*sqrt(V), rev(M+2*sqrt(V))), col='yellow', border=NA)

#~ ## Plot one special trajectory.
#~ lines(S, FS[,1], lty='dotted')

## Plot the obtained mean.
lines(S, M, lwd=lwd[2], col=col[2])

## Plot 'exact' values again (in case the were overwritten).
#~ I = (Smatlab >= min(S)) & (Smatlab <= max(S))
#~ lines(Smatlab[I], Qmatlab[I], type='o', pch='', lty=lty[2])

## Legend.
legend(legPos, inset=.05, pch='', col=col, cex=cexLeg,
  legend=c('quadrature', 'energy perturbation'), lty=lty, lwd=max(lwd))

# Close file.
dev.off();
  
# Do some conversion magic.
cmd = sprintf('inkscape -D -z --file=%s --export-pdf=%s --export-latex', svg_name, pdf_name)
system(cmd)
