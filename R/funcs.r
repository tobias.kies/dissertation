# Plotting function (at least for classical convergence rates.)
plotData <- function(h, e, name, slope, filename, legPos = "bottomright")
{
  # Open file.
  svg_name = sprintf('../gfx/%s.svg',filename)
  pdf_name = sprintf('../gfx/%s.pdf',filename)
  svg(filename=svg_name)

  # Extract data on the ranges.
  min2powX = floor(min(log(h)/log(2)));
  max2powX = ceiling(max(log(h)/log(2)));
  ticksX   = 2^(min2powX:max2powX)

  min2powY = floor(min(log(e)/log(2)));
  max2powY = ceiling(max(log(e)/log(2)));
  ticksY   = 2^(min2powY:max2powY)

  # Set reference line.
  logC    = min(e/h^slope)
  refLine = 0.9*logC * h^slope

  # Set further configuration.
  col = c(4,2)
  lwd = c(1,3)
  
  cexLab  = 2
  cexMain = 2.5
  cexAxis = 1.7
  cexLeg  = 2

  # Carry out the actual plot.
  par(mar=c(4,5.3,cexMain,0))
  
  if(name != '')
    yQuote = bquote(log[2](.(name)~error))
  else
    yQuote = bquote(log[2](error))

  matplot(h, cbind(e, refLine),
    col=col,
    lwd=c(1,3),
    type='l',
    log='xy',
    xlim=c(2^min2powX,2^max2powX),
    ylim=c(2^min2powY,2^max2powY),
    xaxt='n',
    yaxt='n',
    xlab=expression(log[2](h)),
    ylab=yQuote,
    bty='n',
    cex.lab=cexLab,
    cex.main=cexMain
  )

  # Put on some more labels.
  axis(side = 1, at = ticksX, cex.axis=cexAxis,
    labels = sapply(min2powX:max2powX, function(c) sprintf('%d',c))
  )

  axis(side = 2, at = ticksY, cex.axis=cexAxis,
    labels = sapply(min2powY:max2powY, function(c) sprintf('%d',c))
  )

#~   mtext(sprintf('%s error plot', name), cex=cexMain)

  if(name != '')
    legend(legPos, inset=.05, pch='', col=col, cex=cexLeg, lty=1, lwd=3,
      legend=c(sprintf('%s error',name), sprintf('ref. line (slope %g)',slope))
    )
  else
    legend(legPos, inset=.05, pch='', col=col, cex=cexLeg, lty=1, lwd=3,
      legend=c('error', sprintf('ref. line (slope %g)',slope))
    )

  # Close file.
  dev.off();
  
  # Do some conversion magic.
  cmd = sprintf('inkscape -D -z --file=%s --export-pdf=%s --export-latex', svg_name, pdf_name)
  system(cmd)
}
