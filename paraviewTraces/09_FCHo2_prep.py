#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1085, 885]

# Hide orientation axes
renderView1.OrientationAxesVisibility = 0

Filenames=[('/home/mi/tokies/Backup/Promotion/finalData/iterates/FCHo2_2_1.000000_iterate_%05d.vtu' % i) for i in range(0,401)]

# create a new 'XML Unstructured Grid Reader'
FCHo2_iterate_0000 = XMLUnstructuredGridReader(FileName=Filenames)
FCHo2_iterate_0000.CellArrayStatus = []
FCHo2_iterate_0000.PointArrayStatus = ['PerturbedSurface', 'elevation']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.LockDataRange = 0
elevationLUT.InterpretValuesAsCategories = 0
elevationLUT.ShowCategoricalColorsinDataRangeOnly = 0
elevationLUT.RescaleOnVisibilityChange = 0
elevationLUT.EnableOpacityMapping = 0
elevationLUT.RGBPoints = [-0.3259128928184509, 0.231373, 0.298039, 0.752941, -0.16296018035063753, 0.865003, 0.865003, 0.865003, -7.467882824130356e-06, 0.705882, 0.0156863, 0.14902]
elevationLUT.UseLogScale = 0
elevationLUT.ColorSpace = 'Diverging'
elevationLUT.UseBelowRangeColor = 0
elevationLUT.BelowRangeColor = [0.0, 0.0, 0.0]
elevationLUT.UseAboveRangeColor = 0
elevationLUT.AboveRangeColor = [1.0, 1.0, 1.0]
elevationLUT.NanColor = [1.0, 1.0, 0.0]
elevationLUT.Discretize = 1
elevationLUT.NumberOfTableValues = 256
elevationLUT.ScalarRangeInitialized = 1.0
elevationLUT.HSVWrap = 0
elevationLUT.VectorComponent = 0
elevationLUT.VectorMode = 'Magnitude'
elevationLUT.AllowDuplicateScalars = 1
elevationLUT.Annotations = []
elevationLUT.ActiveAnnotatedValues = []
elevationLUT.IndexedColors = []

# show data in view
FCHo2_iterate_0000Display = Show(FCHo2_iterate_0000, renderView1)
# trace defaults for the display properties.
FCHo2_iterate_0000Display.Representation = 'Surface'
FCHo2_iterate_0000Display.AmbientColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.ColorArrayName = ['POINTS', 'elevation']
FCHo2_iterate_0000Display.DiffuseColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.LookupTable = elevationLUT
FCHo2_iterate_0000Display.MapScalars = 1
FCHo2_iterate_0000Display.InterpolateScalarsBeforeMapping = 1
FCHo2_iterate_0000Display.Opacity = 1.0
FCHo2_iterate_0000Display.PointSize = 2.0
FCHo2_iterate_0000Display.LineWidth = 1.0
FCHo2_iterate_0000Display.Interpolation = 'Gouraud'
FCHo2_iterate_0000Display.Specular = 0.0
FCHo2_iterate_0000Display.SpecularColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.SpecularPower = 100.0
FCHo2_iterate_0000Display.Ambient = 0.0
FCHo2_iterate_0000Display.Diffuse = 1.0
FCHo2_iterate_0000Display.EdgeColor = [0.0, 0.0, 0.5]
FCHo2_iterate_0000Display.BackfaceRepresentation = 'Follow Frontface'
FCHo2_iterate_0000Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.BackfaceDiffuseColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.BackfaceOpacity = 1.0
FCHo2_iterate_0000Display.Position = [0.0, 0.0, 0.0]
FCHo2_iterate_0000Display.Scale = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.Orientation = [0.0, 0.0, 0.0]
FCHo2_iterate_0000Display.Origin = [0.0, 0.0, 0.0]
FCHo2_iterate_0000Display.Pickable = 1
FCHo2_iterate_0000Display.Texture = None
FCHo2_iterate_0000Display.Triangulate = 0
FCHo2_iterate_0000Display.NonlinearSubdivisionLevel = 1
FCHo2_iterate_0000Display.UseDataPartitions = 0
FCHo2_iterate_0000Display.OSPRayUseScaleArray = 0
FCHo2_iterate_0000Display.OSPRayScaleArray = 'elevation'
FCHo2_iterate_0000Display.OSPRayScaleFunction = 'PiecewiseFunction'
FCHo2_iterate_0000Display.Orient = 0
FCHo2_iterate_0000Display.OrientationMode = 'Direction'
FCHo2_iterate_0000Display.SelectOrientationVectors = 'PerturbedSurface'
FCHo2_iterate_0000Display.Scaling = 0
FCHo2_iterate_0000Display.ScaleMode = 'No Data Scaling Off'
FCHo2_iterate_0000Display.ScaleFactor = 0.4
FCHo2_iterate_0000Display.SelectScaleArray = 'elevation'
FCHo2_iterate_0000Display.GlyphType = 'Arrow'
FCHo2_iterate_0000Display.SelectionCellLabelBold = 0
FCHo2_iterate_0000Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
FCHo2_iterate_0000Display.SelectionCellLabelFontFamily = 'Arial'
FCHo2_iterate_0000Display.SelectionCellLabelFontSize = 18
FCHo2_iterate_0000Display.SelectionCellLabelItalic = 0
FCHo2_iterate_0000Display.SelectionCellLabelJustification = 'Left'
FCHo2_iterate_0000Display.SelectionCellLabelOpacity = 1.0
FCHo2_iterate_0000Display.SelectionCellLabelShadow = 0
FCHo2_iterate_0000Display.SelectionPointLabelBold = 0
FCHo2_iterate_0000Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
FCHo2_iterate_0000Display.SelectionPointLabelFontFamily = 'Arial'
FCHo2_iterate_0000Display.SelectionPointLabelFontSize = 18
FCHo2_iterate_0000Display.SelectionPointLabelItalic = 0
FCHo2_iterate_0000Display.SelectionPointLabelJustification = 'Left'
FCHo2_iterate_0000Display.SelectionPointLabelOpacity = 1.0
FCHo2_iterate_0000Display.SelectionPointLabelShadow = 0
FCHo2_iterate_0000Display.PolarAxes = 'PolarAxesRepresentation'
FCHo2_iterate_0000Display.ScalarOpacityUnitDistance = 0.055681169883771225
FCHo2_iterate_0000Display.SelectMapper = 'Projected tetra'
FCHo2_iterate_0000Display.SamplingDimensions = [128, 128, 128]
FCHo2_iterate_0000Display.GaussianRadius = 0.2
FCHo2_iterate_0000Display.ShaderPreset = 'Sphere'
FCHo2_iterate_0000Display.Emissive = 0
FCHo2_iterate_0000Display.ScaleByArray = 0
FCHo2_iterate_0000Display.SetScaleArray = ['POINTS', 'elevation']
FCHo2_iterate_0000Display.ScaleTransferFunction = 'PiecewiseFunction'
FCHo2_iterate_0000Display.OpacityByArray = 0
FCHo2_iterate_0000Display.OpacityArray = ['POINTS', 'elevation']
FCHo2_iterate_0000Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
FCHo2_iterate_0000Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'Arrow' selected for 'GlyphType'
FCHo2_iterate_0000Display.GlyphType.TipResolution = 6
FCHo2_iterate_0000Display.GlyphType.TipRadius = 0.1
FCHo2_iterate_0000Display.GlyphType.TipLength = 0.35
FCHo2_iterate_0000Display.GlyphType.ShaftResolution = 6
FCHo2_iterate_0000Display.GlyphType.ShaftRadius = 0.03
FCHo2_iterate_0000Display.GlyphType.Invert = 0

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
FCHo2_iterate_0000Display.PolarAxes.Visibility = 0
FCHo2_iterate_0000Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
FCHo2_iterate_0000Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
FCHo2_iterate_0000Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
FCHo2_iterate_0000Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.EnableCustomRange = 0
FCHo2_iterate_0000Display.PolarAxes.CustomRange = [0.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.PolarAxisVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.RadialAxesVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.DrawRadialGridlines = 1
FCHo2_iterate_0000Display.PolarAxes.PolarArcsVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.DrawPolarArcsGridlines = 1
FCHo2_iterate_0000Display.PolarAxes.NumberOfRadialAxes = 0
FCHo2_iterate_0000Display.PolarAxes.AutoSubdividePolarAxis = 1
FCHo2_iterate_0000Display.PolarAxes.NumberOfPolarAxis = 0
FCHo2_iterate_0000Display.PolarAxes.MinimumRadius = 0.0
FCHo2_iterate_0000Display.PolarAxes.MinimumAngle = 0.0
FCHo2_iterate_0000Display.PolarAxes.MaximumAngle = 90.0
FCHo2_iterate_0000Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
FCHo2_iterate_0000Display.PolarAxes.Ratio = 1.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
FCHo2_iterate_0000Display.PolarAxes.PolarLabelVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
FCHo2_iterate_0000Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
FCHo2_iterate_0000Display.PolarAxes.RadialLabelVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
FCHo2_iterate_0000Display.PolarAxes.RadialLabelLocation = 'Bottom'
FCHo2_iterate_0000Display.PolarAxes.RadialUnitsVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.ScreenSize = 10.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleOpacity = 1.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleBold = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleItalic = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleShadow = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTitleFontSize = 12
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelOpacity = 1.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelBold = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelItalic = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelShadow = 0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisLabelFontSize = 12
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextBold = 0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextItalic = 0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextShadow = 0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTextFontSize = 12
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextColor = [1.0, 1.0, 1.0]
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextBold = 0
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
FCHo2_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
FCHo2_iterate_0000Display.PolarAxes.EnableDistanceLOD = 1
FCHo2_iterate_0000Display.PolarAxes.DistanceLODThreshold = 0.7
FCHo2_iterate_0000Display.PolarAxes.EnableViewAngleLOD = 1
FCHo2_iterate_0000Display.PolarAxes.ViewAngleLODThreshold = 0.7
FCHo2_iterate_0000Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
FCHo2_iterate_0000Display.PolarAxes.PolarTicksVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
FCHo2_iterate_0000Display.PolarAxes.TickLocation = 'Both'
FCHo2_iterate_0000Display.PolarAxes.AxisTickVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.AxisMinorTickVisibility = 0
FCHo2_iterate_0000Display.PolarAxes.ArcTickVisibility = 1
FCHo2_iterate_0000Display.PolarAxes.ArcMinorTickVisibility = 0
FCHo2_iterate_0000Display.PolarAxes.DeltaAngleMajor = 10.0
FCHo2_iterate_0000Display.PolarAxes.DeltaAngleMinor = 5.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisMajorTickSize = 0.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTickRatioSize = 0.3
FCHo2_iterate_0000Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
FCHo2_iterate_0000Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
FCHo2_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
FCHo2_iterate_0000Display.PolarAxes.ArcMajorTickSize = 0.0
FCHo2_iterate_0000Display.PolarAxes.ArcTickRatioSize = 0.3
FCHo2_iterate_0000Display.PolarAxes.ArcMajorTickThickness = 1.0
FCHo2_iterate_0000Display.PolarAxes.ArcTickRatioThickness = 0.5
FCHo2_iterate_0000Display.PolarAxes.Use2DMode = 0
FCHo2_iterate_0000Display.PolarAxes.UseLogAxis = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
FCHo2_iterate_0000Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
FCHo2_iterate_0000Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# show color bar/color legend
FCHo2_iterate_0000Display.SetScalarBarVisibility(renderView1, True)

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.3259128928184509, 0.0, 0.5, 0.0, -7.467882824130356e-06, 1.0, 0.5, 0.0]
elevationPWF.AllowDuplicateScalars = 1
elevationPWF.ScalarRangeInitialized = 1

# Rescale transfer function
elevationLUT.RescaleTransferFunction(-100, 20)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
elevationLUT.ApplyPreset('Parula Matlab', True)

# Properties modified on elevationLUT
elevationLUT.NanColor = [1.0, 1.0, 1.0]

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.AutoOrient = 1
elevationLUTColorBar.Orientation = 'Vertical'
elevationLUTColorBar.Position = [0.85, 0.05]
elevationLUTColorBar.Position2 = [0.12, 0.43]
elevationLUTColorBar.Title = 'elevation'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleJustification = 'Centered'
elevationLUTColorBar.TitleColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.TitleOpacity = 1.0
elevationLUTColorBar.TitleFontFamily = 'Arial'
elevationLUTColorBar.TitleBold = 0
elevationLUTColorBar.TitleItalic = 0
elevationLUTColorBar.TitleShadow = 0
elevationLUTColorBar.TitleFontSize = 7
elevationLUTColorBar.LabelColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.LabelOpacity = 1.0
elevationLUTColorBar.LabelFontFamily = 'Arial'
elevationLUTColorBar.LabelBold = 0
elevationLUTColorBar.LabelItalic = 0
elevationLUTColorBar.LabelShadow = 0
elevationLUTColorBar.LabelFontSize = 7
elevationLUTColorBar.AutomaticLabelFormat = 1
elevationLUTColorBar.NumberOfLabels = 5
elevationLUTColorBar.DrawTickMarks = 1
elevationLUTColorBar.DrawSubTickMarks = 1
elevationLUTColorBar.DrawTickLabels = 1
elevationLUTColorBar.AddRangeLabels = 1
elevationLUTColorBar.DrawAnnotations = 1
elevationLUTColorBar.AddRangeAnnotations = 0
elevationLUTColorBar.AutomaticAnnotations = 0
elevationLUTColorBar.DrawNanAnnotation = 0
elevationLUTColorBar.NanAnnotation = 'NaN'
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '   elev.'
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.TitleFontSize = 25
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelFontSize = 25
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%-.0f'
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%-.0f'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '     elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '      elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.17203389830508475]
elevationLUTColorBar.Position2 = [0.12, 0.4299999999999998]

# change scalar bar placement
elevationLUTColorBar.Position2 = [0.12, 0.813050847457627]

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.01610169491525425]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474576]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 50

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 99

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 30

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 25

# change scalar bar placement
elevationLUTColorBar.Position = [0.79136460554371, 0.01158192090395481]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474575]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 25

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks left/bottom, annotations right/top'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AutoOrient = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7497867803837953, 0.02175141242937849]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 15.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.3723880597014926, 0.031638418079096065]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 9

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 5

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.8457356076759063, 0.024858757062146908]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474572]

# change scalar bar placement
elevationLUTColorBar.Position = [0.83, 0.017]

#### saving camera placements for all active views

# current camera placement for renderView1
#~ renderView1.InteractionMode = '2D'
#~ renderView1.CameraPosition = [0, 0, 1000]
#~ renderView1.CameraFocalPoint = [100, 0, 0.0]
#~ renderView1.CameraParallelScale = 500

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0, 0, 1000]
renderView1.CameraFocalPoint = [115, 0, 0.0]
renderView1.CameraParallelScale = 500

#### uncomment the following to render all views
RenderAllViews()

















####
# Special stuff.
####

import numpy as np

# Hard config:
atomSize = 10
scaling = [100, 100, 10, 100./(20*np.pi*8), 100./(20*np.pi *85), 1000./(20*np.pi*85)]
atomSkip = 1
length = 500

# Load information on iterates from file.
data = []
with open('/home/mi/tokies/Backup/Promotion/finalData/FCHo2_2_1.000000.txt','r') as f:
  data = f.read().split('\n')
  
  data = [data[i].split(' ') for i in range(len(data)-1)]
  for i in range(len(data)):
    del data[i][-1]
    for j in range(len(data[i])):
      data[i][j]  = float(data[i][j])
  data = np.matrix(data);


# Extract rows that have information on each iterate.
# This is each line with a new number.
Id = 0;
Ids = [0];
for i in range(len(data)):
  if data[i,0] != Id:
    Id = data[i,0]
    Ids += [i]
    
    
# List of atoms.
atomPositions = [
    [35.448914000000002, 6.046584000000000, 36.961123999999998],
    [49.777372999999997, 3.534151000000000, 21.536387000000001],
    [39.519002999999998, 10.126398000000000, 18.804169000000002],
    [27.600873000000000, 8.100471000000001, 13.033269000000001],
    [13.117568000000000, 11.384019000000000, 10.945007000000000],
    [-2.461397000000000, 5.089219000000000, 15.054800999999999],
    [-19.157999000000000, 3.974529000000000, 14.483580999999999],
    [-27.466754999999999, 2.612787000000000, 22.755299999999998],
    [-11.426488000000001, 7.947724000000000, 19.464036000000000],
    [4.220300000000000, 12.367309000000001, 17.888117000000001],
    [15.487218000000000, 18.260605999999999, 21.763407999999998],
    [29.508054999999999, 22.199445999999998, 16.438497000000002],
    [45.744874000000003, 18.740355000000001, 18.870595999999999],
    [52.314951999999998, 10.786300000000001, 12.956417000000000],
    [72.143253999999999, 3.172413000000000, 8.491011000000000],
    [84.774021000000005, -1.097356000000000, 7.871460000000000],
    [95.346950000000007, 7.232932000000000, 5.798990000000000],
    [80.718675000000005, 11.271070000000000, 12.837351999999999],
    [66.556663999999998, 13.844241000000000, 13.112413999999999],
    [55.938924000000000, 18.527785000000002, 22.135238999999999],
    [41.305196000000002, 12.884736000000000, 31.387480000000000],
    [27.511309000000001, 11.983777999999999, 21.846817000000001],
    [13.231037000000001, 12.956087999999999, 26.917726999999999],
    [-0.046684000000000, 8.136436000000000, 34.587474999999998],
    [-16.835463000000001, -1.140320000000000, 32.161655000000003],
    [-32.107027000000002, -1.253790000000000, 26.694789000000000],
    [-46.183160999999998, -0.975092000000000, 18.861654000000001],
    [-77.640871000000004, -4.687151000000000, 19.492010000000001],
    [-46.151198999999998, -2.596540000000000, 28.977522000000000],
    [-43.957053000000002, -6.934654000000000, 18.841806999999999],
    [-34.338315000000001, -15.177346000000000, 13.475149000000000],
    [-14.085146000000000, -5.728524000000000, 12.640269000000000],
    [-2.952937000000000, -3.736315000000000, 19.179622999999999],
    [11.125772000000000, -0.721889000000000, 14.483302000000000],
    [30.966885000000001, 4.471928000000000, 19.355025000000001],
    [16.784720000000000, -4.364767000000000, 19.685480999999999],
    [2.707114000000000, -3.956116000000000, 20.992453999999999],
    [-11.148622000000000, -14.693209000000000, 20.325627999999998],
    [-22.146788999999998, -21.298276999999999, 15.386881000000001],
    [-39.985869000000001, -18.920238999999999, 16.421203999999999],
    [-55.163874000000000, -13.157465000000000, 12.772751000000000],
    [-65.946459000000004, -0.986753000000000, 11.052246000000000],
    [-81.467135999999996, 0.159625000000000, 8.904780000000001],
    [-89.340584000000007, 5.499156000000000, 0.692096000000000],
    [-86.993577999999999, -7.968243000000000, 8.312504000000001],
    [-71.259354000000002, -5.338794000000000, 15.054655000000000],
    [-62.913522000000000, -17.223806000000000, 22.493088000000000],
    [-49.066567999999997, -10.770490000000001, 28.751166999999999],
    [-31.885211999999999, -9.995030000000000, 26.218691000000000],
    [-19.836929999999999, -12.411137999999999, 29.323146999999999],
    [-2.802862000000000, -10.675321000000000, 30.672989000000001],
    [12.660871000000000, -1.160465000000000, 33.195729999999998],
    [27.452971999999999, 3.400179000000000, 31.169632000000000],
    [39.910249000000000, -3.899469000000000, 23.171540000000000],
    [63.646572999999997, 3.563858000000000, 26.393429999999999]
  ]

# Transformation function.
def Psi(p,x):
  p   = [p[i]*scaling[i] for i in range(len(p))]
  
  p_  = p[0:3]
  R   = np.matrix([
      [ np.cos(p[4])*np.cos(p[5]), - np.cos(p[3])*np.sin(p[5]) - np.cos(p[5])*np.sin(p[3])*np.sin(p[4]),   np.sin(p[3])*np.sin(p[5]) - np.cos(p[3])*np.cos(p[5])*np.sin(p[4])],
      [ np.cos(p[4])*np.sin(p[5]),   np.cos(p[3])*np.cos(p[5]) - np.sin(p[3])*np.sin(p[4])*np.sin(p[5]), - np.cos(p[5])*np.sin(p[3]) - np.cos(p[3])*np.sin(p[4])*np.sin(p[5])],
      [        np.sin(p[4]),                          np.cos(p[4])*np.sin(p[3]),                          np.cos(p[3])*np.cos(p[4])]]
    )
  
  y = R*np.transpose(np.matrix(x)) + np.transpose(np.matrix(p_))
  y = y.ravel().tolist()[0]
  y[0] = ((y[0]+length) % (2*length)) - length
  y[1] = ((y[1]+length) % (2*length)) - length
  return y

# Extract particle configuration parameters.
nParticleDOFs = 6
nParticles    = (data.shape[1]-5)/4/nParticleDOFs
P = data[Ids,5:(5+nParticles*nParticleDOFs)]

P = [ [P[i,(j*nParticleDOFs):((j+1)*nParticleDOFs)].ravel().tolist()[0]
        for j in range(nParticles) ]
      for i in range(P.shape[0]) ]


def setTime(t, deleteOld = True):
  I = range(0,len(atomPositions),atomSkip)
  
  if(deleteOld):
    for i in range(nParticles*len(I)):
      try:
        source = FindSource('Sphere%d' % (i+1))
        Delete(source)
      except:
        print('Sphere%d not found for deletion.' % (i+1))
        pass
  
  # Set time. (Assumption: Time T coincides with id t.)
  T = t
  renderView = GetActiveView()
  renderView.ViewTime = T
  Render()

  # Create spherical points.
  for particleId in range(nParticles):
    for atomId in I:
      atomPosition = Psi(P[t+1][particleId], np.array(atomPositions[atomId])) # need to add +1 here due to a inconsistency during parsing
     
      sphere = Sphere()
      sphereDisplay = Show(sphere, renderView)
      sphereDisplay.ColorArrayName = [None, '']
      sphereDisplay.GlyphType = 'Arrow'
      
      sphere.Radius = atomSize
      sphere.Center = atomPosition


setTime(0, False)



