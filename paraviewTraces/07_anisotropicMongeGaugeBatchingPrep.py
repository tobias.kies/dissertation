#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active source.
eggCartonPeriodic3_iterate_0000 = GetActiveSource()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [885, 885]

# get display properties
eggCartonPeriodic3_iterate_0000Display = GetDisplayProperties(eggCartonPeriodic3_iterate_0000, view=renderView1)

# hide color bar/color legend
eggCartonPeriodic3_iterate_0000Display.SetScalarBarVisibility(renderView1, False)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0019167898400797951, 0.008409261711403014, 10000.0]
renderView1.CameraFocalPoint = [0.0019167898400797951, 0.008409261711403014, 0.0]
renderView1.CameraParallelScale = 2.01224879528757

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
