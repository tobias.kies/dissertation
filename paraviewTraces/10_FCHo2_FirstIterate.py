#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.ViewTime = 0
RenderAllViews()

# export view
ExportView('/home/mi/tokies/Promotion/Dissertation/gfx/FCHo2_iterate_0.eps', view=renderView1, Plottitle='ParaView GL2PS Export',
    Compressoutputfile=0,
    Drawbackground=1,
    Cullhiddenprimitives=1,
    Linewidthscalingfactor=0.714,
    Pointsizescalingfactor=0.714,
    GL2PSdepthsortmethod='Simple sorting (fast, good)',
    Rasterize3Dgeometry=1,
    Dontrasterizecubeaxes=1,
    Rendertextaspaths=0)
