#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1065, 885]
renderView1.ViewTime = 0

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.433925005777524, 0.0038618068067983362, 10000.0]
renderView1.CameraFocalPoint = [0.433925005777524, 0.0038618068067983362, 0.0]
renderView1.CameraParallelScale = 2.01224879528757

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).

# export view
ExportView('/home/mi/tokies/Promotion/Dissertation/gfx/isotropicMongeGauge_iterate_0.eps', view=renderView1, Plottitle='ParaView GL2PS Export',
    Compressoutputfile=0,
    Drawbackground=1,
    Cullhiddenprimitives=1,
    Linewidthscalingfactor=0.714,
    Pointsizescalingfactor=0.714,
    GL2PSdepthsortmethod='Simple sorting (fast, good)',
    Rasterize3Dgeometry=1,
    Dontrasterizecubeaxes=1,
    Rendertextaspaths=0)
