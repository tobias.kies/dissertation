# state file generated using paraview version 5.3.0

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [939, 885]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [0.0, 0.0, 0.5486238002777102]
renderView1.StereoType = 0
renderView1.CameraPosition = [0.06693749636936978, -3.019690827011996, 2.0450543335876588]
renderView1.CameraFocalPoint = [-0.008419541308345053, 0.42839363732791097, 0.012965515984460899]
renderView1.CameraViewUp = [-0.00582352161692896, 0.5076229662787934, 0.86155963850583]
renderView1.CameraParallelScale = 1.5169008122587173
renderView1.Background = [1.0, 1.0, 1.0]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
pointExactMembrane_solutionvtu = XMLUnstructuredGridReader(FileName=['/home/mi/tokies/Backup/Promotion/finalData/iterates/pointExactMembrane_solution.vtu'])
pointExactMembrane_solutionvtu.PointArrayStatus = ['PerturbedSurface', 'elevation']

# create a new 'Warp By Scalar'
warpByScalar2 = WarpByScalar(Input=pointExactMembrane_solutionvtu)
warpByScalar2.Scalars = ['POINTS', 'elevation']

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.RGBPoints = [0.0, 0.2081, 0.1663, 0.5292, 0.015873, 0.21162, 0.18978, 0.57768, 0.031746, 0.21225, 0.21377, 0.62697, 0.047619, 0.2081, 0.2386, 0.67709, 0.063492, 0.1959, 0.26446, 0.7279, 0.079365, 0.17073, 0.29194, 0.77925, 0.095238, 0.12527, 0.32424, 0.83027, 0.11111, 0.059133, 0.35983, 0.86833, 0.12698, 0.011695, 0.38751, 0.88196, 0.14286, 0.0059571, 0.40861, 0.88284, 0.15873, 0.016514, 0.4266, 0.87863, 0.1746, 0.032852, 0.44304, 0.87196, 0.19048, 0.049814, 0.45857, 0.86406, 0.20635, 0.062933, 0.47369, 0.85544, 0.22222, 0.072267, 0.48867, 0.8467, 0.2381, 0.077943, 0.50399, 0.83837, 0.25397, 0.079348, 0.52002, 0.83118, 0.26984, 0.074943, 0.53754, 0.82627, 0.28571, 0.064057, 0.55699, 0.82396, 0.30159, 0.048771, 0.57722, 0.82283, 0.31746, 0.034343, 0.59658, 0.81985, 0.33333, 0.0265, 0.6137, 0.8135, 0.34921, 0.02389, 0.62866, 0.80376, 0.36508, 0.02309, 0.64179, 0.79127, 0.38095, 0.022771, 0.65349, 0.77676, 0.39683, 0.026662, 0.6642, 0.76072, 0.4127, 0.038371, 0.67427, 0.74355, 0.42857, 0.058971, 0.68376, 0.72539, 0.44444, 0.0843, 0.69283, 0.70617, 0.46032, 0.1133, 0.7015, 0.68586, 0.4761900000000001, 0.14527, 0.70976, 0.66463, 0.49205999999999994, 0.18013, 0.71766, 0.64243, 0.50794, 0.21783, 0.72504, 0.61926, 0.52381, 0.25864, 0.73171, 0.59543, 0.53968, 0.30217, 0.7376, 0.57119, 0.55556, 0.34817, 0.74243, 0.54727, 0.57143, 0.39526, 0.7459, 0.52444, 0.5873, 0.44201, 0.74808, 0.50331, 0.60317, 0.48712, 0.74906, 0.48398, 0.61905, 0.53003, 0.74911, 0.46611, 0.63492, 0.57086, 0.74852, 0.44939, 0.65079, 0.60985, 0.74731, 0.43369, 0.66667, 0.6473, 0.7456, 0.4188, 0.68254, 0.68342, 0.74348, 0.40443, 0.69841, 0.71841, 0.74113, 0.39048, 0.71429, 0.75249, 0.7384, 0.37681, 0.73016, 0.78584, 0.73557, 0.36327, 0.74603, 0.8185, 0.73273, 0.34979, 0.7619, 0.85066, 0.7299, 0.33603, 0.77778, 0.88243, 0.72743, 0.3217, 0.79365, 0.91393, 0.72579, 0.30628, 0.80952, 0.94496, 0.72611, 0.28864, 0.8254, 0.9739, 0.7314, 0.26665, 0.84127, 0.99377, 0.74546, 0.24035, 0.85714, 0.99904, 0.76531, 0.21641, 0.87302, 0.99553, 0.78606, 0.19665, 0.88889, 0.988, 0.8066, 0.17937, 0.90476, 0.97886, 0.82714, 0.16331, 0.9206300000000001, 0.9697, 0.84814, 0.14745, 0.93651, 0.96259, 0.87051, 0.1309, 0.9523800000000002, 0.95887, 0.8949, 0.11324, 0.9682500000000003, 0.95982, 0.92183, 0.094838, 0.98413, 0.9661, 0.95144, 0.075533, 1.0, 0.9763, 0.9831, 0.0538]
elevationLUT.ColorSpace = 'HSV'
elevationLUT.NanColor = [1.0, 1.0, 1.0]
elevationLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pointExactMembrane_solutionvtu
pointExactMembrane_solutionvtuDisplay = Show(pointExactMembrane_solutionvtu, renderView1)
# trace defaults for the display properties.
pointExactMembrane_solutionvtuDisplay.Representation = 'Surface'
pointExactMembrane_solutionvtuDisplay.ColorArrayName = ['POINTS', 'elevation']
pointExactMembrane_solutionvtuDisplay.LookupTable = elevationLUT
pointExactMembrane_solutionvtuDisplay.OSPRayScaleArray = 'elevation'
pointExactMembrane_solutionvtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pointExactMembrane_solutionvtuDisplay.SelectOrientationVectors = 'PerturbedSurface'
pointExactMembrane_solutionvtuDisplay.ScaleFactor = 0.2015625
pointExactMembrane_solutionvtuDisplay.SelectScaleArray = 'elevation'
pointExactMembrane_solutionvtuDisplay.GlyphType = 'Arrow'
pointExactMembrane_solutionvtuDisplay.PolarAxes = 'PolarAxesRepresentation'
pointExactMembrane_solutionvtuDisplay.ScalarOpacityUnitDistance = 0.017584024175705072
pointExactMembrane_solutionvtuDisplay.GaussianRadius = 0.10078125
pointExactMembrane_solutionvtuDisplay.SetScaleArray = ['POINTS', 'elevation']
pointExactMembrane_solutionvtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pointExactMembrane_solutionvtuDisplay.OpacityArray = ['POINTS', 'elevation']
pointExactMembrane_solutionvtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
pointExactMembrane_solutionvtuDisplay.SetScalarBarVisibility(renderView1, True)

# show data from warpByScalar2
warpByScalar2Display = Show(warpByScalar2, renderView1)
# trace defaults for the display properties.
warpByScalar2Display.Representation = 'Surface'
warpByScalar2Display.ColorArrayName = ['POINTS', 'elevation']
warpByScalar2Display.LookupTable = elevationLUT
warpByScalar2Display.OSPRayScaleArray = 'elevation'
warpByScalar2Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByScalar2Display.SelectOrientationVectors = 'PerturbedSurface'
warpByScalar2Display.ScaleFactor = 0.2
warpByScalar2Display.SelectScaleArray = 'elevation'
warpByScalar2Display.GlyphType = 'Arrow'
warpByScalar2Display.PolarAxes = 'PolarAxesRepresentation'
warpByScalar2Display.ScalarOpacityUnitDistance = 0.01850609523377004
warpByScalar2Display.GaussianRadius = 0.1
warpByScalar2Display.SetScaleArray = ['POINTS', 'elevation']
warpByScalar2Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByScalar2Display.OpacityArray = ['POINTS', 'elevation']
warpByScalar2Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
warpByScalar2Display.SetScalarBarVisibility(renderView1, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.Position = [0.8377657952975364, 0.5641242937853108]
elevationLUTColorBar.Position2 = [0.12, 0.42999999999999994]
elevationLUTColorBar.Title = 'elevation'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleJustification = 'Right'
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.TitleFontSize = 15
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelFontSize = 15
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%.2f'
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%.2f'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(pointExactMembrane_solutionvtu)
# ----------------------------------------------------------------