# state file generated using paraview version 5.3.0

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1374, 709]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [3.997669503092766, -1.861225664615631, 1.2476718127727509]
renderView1.StereoType = 0
renderView1.CameraPosition = [-3.2964081377244256, -4.760134653842117, 5.967104023461859]
renderView1.CameraFocalPoint = [3.649608321521492, -1.9789001920427687, 0.5267049760371153]
renderView1.CameraViewUp = [0.3345288196813598, 0.594633016365814, 0.7310964674037825]
renderView1.CameraParallelScale = 5.132450210300671
renderView1.Background = [1.0, 1.0, 1.0]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
tube_interaction_iterate_00250vtu = XMLUnstructuredGridReader(FileName=['/home/mi/tokies/Backup/Promotion/tube10/iterates/tube_interaction_iterate_00250.vtu'])
tube_interaction_iterate_00250vtu.PointArrayStatus = ['PerturbedSurface', 'elevation']

# create a new 'Warp By Vector'
warpByVector2 = WarpByVector(Input=tube_interaction_iterate_00250vtu)
warpByVector2.Vectors = ['POINTS', 'PerturbedSurface']

# create a new 'XML Unstructured Grid Reader'
tube_interaction_iterate_00000vtu = XMLUnstructuredGridReader(FileName=['/home/mi/tokies/Backup/Promotion/tube10/iterates/tube_interaction_iterate_00000.vtu'])
tube_interaction_iterate_00000vtu.PointArrayStatus = ['PerturbedSurface', 'elevation']

# create a new 'Warp By Vector'
warpByVector1 = WarpByVector(Input=tube_interaction_iterate_00000vtu)
warpByVector1.Vectors = ['POINTS', 'PerturbedSurface']

# create a new 'Transform'
transform1 = Transform(Input=warpByVector2)
transform1.Transform = 'Transform'

# init the 'Transform' selected for 'Transform'
transform1.Transform.Translate = [1.5, -3.5, 1.5]
transform1.Transform.Rotate = [15.0, -10.0, 0.0]

# create a new 'Transform'
transform2 = Transform(Input=warpByVector1)
transform2.Transform = 'Transform'

# init the 'Transform' selected for 'Transform'
transform2.Transform.Translate = [0.5, 0.0, 0.0]
transform2.Transform.Rotate = [25.0, -5.0, 0.0]

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.RGBPoints = [-0.3, 0.2081, 0.1663, 0.5292, -0.2904762, 0.21162, 0.18978, 0.57768, -0.2809524, 0.21225, 0.21377, 0.62697, -0.2714286, 0.2081, 0.2386, 0.67709, -0.2619048, 0.1959, 0.26446, 0.7279, -0.252381, 0.17073, 0.29194, 0.77925, -0.2428572, 0.12527, 0.32424, 0.83027, -0.233334, 0.059133, 0.35983, 0.86833, -0.22381199999999996, 0.011695, 0.38751, 0.88196, -0.214284, 0.0059571, 0.40861, 0.88284, -0.204762, 0.016514, 0.4266, 0.87863, -0.19524000000000002, 0.032852, 0.44304, 0.87196, -0.185712, 0.049814, 0.45857, 0.86406, -0.17619, 0.062933, 0.47369, 0.85544, -0.166668, 0.072267, 0.48867, 0.8467, -0.15714, 0.077943, 0.50399, 0.83837, -0.147618, 0.079348, 0.52002, 0.83118, -0.13809599999999997, 0.074943, 0.53754, 0.82627, -0.128574, 0.064057, 0.55699, 0.82396, -0.11904599999999999, 0.048771, 0.57722, 0.82283, -0.10952399999999998, 0.034343, 0.59658, 0.81985, -0.10000199999999998, 0.0265, 0.6137, 0.8135, -0.09047399999999997, 0.02389, 0.62866, 0.80376, -0.080952, 0.02309, 0.64179, 0.79127, -0.07143, 0.022771, 0.65349, 0.77676, -0.061901999999999985, 0.026662, 0.6642, 0.76072, -0.05237999999999998, 0.038371, 0.67427, 0.74355, -0.04285800000000001, 0.058971, 0.68376, 0.72539, -0.03333599999999998, 0.0843, 0.69283, 0.70617, -0.02380800000000005, 0.1133, 0.7015, 0.68586, -0.014286000000000021, 0.14527, 0.70976, 0.66463, -0.0047639999999999905, 0.18013, 0.71766, 0.64243, 0.0047639999999999905, 0.21783, 0.72504, 0.61926, 0.014286000000000021, 0.25864, 0.73171, 0.59543, 0.02380800000000005, 0.30217, 0.7376, 0.57119, 0.03333600000000003, 0.34817, 0.74243, 0.54727, 0.04285800000000001, 0.39526, 0.7459, 0.52444, 0.05238000000000004, 0.44201, 0.74808, 0.50331, 0.06190200000000001, 0.48712, 0.74906, 0.48398, 0.07143, 0.53003, 0.74911, 0.46611, 0.08095200000000002, 0.57086, 0.74852, 0.44939, 0.090474, 0.60985, 0.74731, 0.43369, 0.10000199999999998, 0.6473, 0.7456, 0.4188, 0.10952400000000001, 0.68342, 0.74348, 0.40443, 0.11904599999999999, 0.71841, 0.74113, 0.39048, 0.12857399999999997, 0.75249, 0.7384, 0.37681, 0.138096, 0.78584, 0.73557, 0.36327, 0.14761799999999997, 0.8185, 0.73273, 0.34979, 0.15714, 0.85066, 0.7299, 0.33603, 0.16666799999999998, 0.88243, 0.72743, 0.3217, 0.17618999999999996, 0.91393, 0.72579, 0.30628, 0.185712, 0.94496, 0.72611, 0.28864, 0.19524000000000002, 0.9739, 0.7314, 0.26665, 0.20476199999999994, 0.99377, 0.74546, 0.24035, 0.21428399999999997, 0.99904, 0.76531, 0.21641, 0.22381199999999996, 0.99553, 0.78606, 0.19665, 0.23333399999999999, 0.988, 0.8066, 0.17937, 0.24285600000000002, 0.97886, 0.82714, 0.16331, 0.25237799999999994, 0.9697, 0.84814, 0.14745, 0.2619059999999999, 0.96259, 0.87051, 0.1309, 0.27142799999999995, 0.95887, 0.8949, 0.11324, 0.2809500000000001, 0.95982, 0.92183, 0.094838, 0.29047799999999996, 0.9661, 0.95144, 0.075533, 0.3, 0.9763, 0.9831, 0.0538]
elevationLUT.ColorSpace = 'RGB'
elevationLUT.NanColor = [1.0, 0.0, 0.0]
elevationLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.3, 0.0, 0.5, 0.0, 0.3, 1.0, 0.5, 0.0]
elevationPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from tube_interaction_iterate_00000vtu
tube_interaction_iterate_00000vtuDisplay = Show(tube_interaction_iterate_00000vtu, renderView1)
# trace defaults for the display properties.
tube_interaction_iterate_00000vtuDisplay.Representation = 'Surface'
tube_interaction_iterate_00000vtuDisplay.ColorArrayName = ['POINTS', 'elevation']
tube_interaction_iterate_00000vtuDisplay.LookupTable = elevationLUT
tube_interaction_iterate_00000vtuDisplay.OSPRayScaleArray = 'elevation'
tube_interaction_iterate_00000vtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
tube_interaction_iterate_00000vtuDisplay.SelectOrientationVectors = 'PerturbedSurface'
tube_interaction_iterate_00000vtuDisplay.ScaleFactor = 0.6283185482025146
tube_interaction_iterate_00000vtuDisplay.SelectScaleArray = 'elevation'
tube_interaction_iterate_00000vtuDisplay.GlyphType = 'Arrow'
tube_interaction_iterate_00000vtuDisplay.PolarAxes = 'PolarAxesRepresentation'
tube_interaction_iterate_00000vtuDisplay.ScalarOpacityUnitDistance = 0.13756238972013723
tube_interaction_iterate_00000vtuDisplay.GaussianRadius = 0.3141592741012573
tube_interaction_iterate_00000vtuDisplay.SetScaleArray = ['POINTS', 'elevation']
tube_interaction_iterate_00000vtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
tube_interaction_iterate_00000vtuDisplay.OpacityArray = ['POINTS', 'elevation']
tube_interaction_iterate_00000vtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
tube_interaction_iterate_00000vtuDisplay.SetScalarBarVisibility(renderView1, True)

# show data from tube_interaction_iterate_00250vtu
tube_interaction_iterate_00250vtuDisplay = Show(tube_interaction_iterate_00250vtu, renderView1)
# trace defaults for the display properties.
tube_interaction_iterate_00250vtuDisplay.Representation = 'Surface'
tube_interaction_iterate_00250vtuDisplay.ColorArrayName = ['POINTS', 'elevation']
tube_interaction_iterate_00250vtuDisplay.LookupTable = elevationLUT
tube_interaction_iterate_00250vtuDisplay.OSPRayScaleArray = 'elevation'
tube_interaction_iterate_00250vtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
tube_interaction_iterate_00250vtuDisplay.SelectOrientationVectors = 'PerturbedSurface'
tube_interaction_iterate_00250vtuDisplay.ScaleFactor = 0.6283185482025146
tube_interaction_iterate_00250vtuDisplay.SelectScaleArray = 'elevation'
tube_interaction_iterate_00250vtuDisplay.GlyphType = 'Arrow'
tube_interaction_iterate_00250vtuDisplay.PolarAxes = 'PolarAxesRepresentation'
tube_interaction_iterate_00250vtuDisplay.ScalarOpacityUnitDistance = 0.13756238972013723
tube_interaction_iterate_00250vtuDisplay.GaussianRadius = 0.3141592741012573
tube_interaction_iterate_00250vtuDisplay.SetScaleArray = ['POINTS', 'elevation']
tube_interaction_iterate_00250vtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
tube_interaction_iterate_00250vtuDisplay.OpacityArray = ['POINTS', 'elevation']
tube_interaction_iterate_00250vtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
tube_interaction_iterate_00250vtuDisplay.SetScalarBarVisibility(renderView1, True)

# show data from warpByVector1
warpByVector1Display = Show(warpByVector1, renderView1)
# trace defaults for the display properties.
warpByVector1Display.Representation = 'Surface'
warpByVector1Display.ColorArrayName = ['POINTS', 'elevation']
warpByVector1Display.LookupTable = elevationLUT
warpByVector1Display.OSPRayScaleArray = 'elevation'
warpByVector1Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByVector1Display.SelectOrientationVectors = 'PerturbedSurface'
warpByVector1Display.ScaleFactor = 0.6000000000000001
warpByVector1Display.SelectScaleArray = 'elevation'
warpByVector1Display.GlyphType = 'Arrow'
warpByVector1Display.PolarAxes = 'PolarAxesRepresentation'
warpByVector1Display.ScalarOpacityUnitDistance = 0.10749852787197085
warpByVector1Display.GaussianRadius = 0.30000000000000004
warpByVector1Display.SetScaleArray = ['POINTS', 'elevation']
warpByVector1Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByVector1Display.OpacityArray = ['POINTS', 'elevation']
warpByVector1Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
warpByVector1Display.SetScalarBarVisibility(renderView1, True)

# show data from warpByVector2
warpByVector2Display = Show(warpByVector2, renderView1)
# trace defaults for the display properties.
warpByVector2Display.Representation = 'Surface'
warpByVector2Display.ColorArrayName = ['POINTS', 'elevation']
warpByVector2Display.LookupTable = elevationLUT
warpByVector2Display.OSPRayScaleArray = 'elevation'
warpByVector2Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByVector2Display.SelectOrientationVectors = 'PerturbedSurface'
warpByVector2Display.ScaleFactor = 0.6000000000000001
warpByVector2Display.SelectScaleArray = 'elevation'
warpByVector2Display.GlyphType = 'Arrow'
warpByVector2Display.PolarAxes = 'PolarAxesRepresentation'
warpByVector2Display.ScalarOpacityUnitDistance = 0.10697888795152889
warpByVector2Display.GaussianRadius = 0.30000000000000004
warpByVector2Display.SetScaleArray = ['POINTS', 'elevation']
warpByVector2Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByVector2Display.OpacityArray = ['POINTS', 'elevation']
warpByVector2Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
warpByVector2Display.SetScalarBarVisibility(renderView1, True)

# show data from transform1
transform1Display = Show(transform1, renderView1)
# trace defaults for the display properties.
transform1Display.Representation = 'Surface'
transform1Display.ColorArrayName = ['POINTS', 'elevation']
transform1Display.LookupTable = elevationLUT
transform1Display.OSPRayScaleArray = 'elevation'
transform1Display.OSPRayScaleFunction = 'PiecewiseFunction'
transform1Display.SelectOrientationVectors = 'PerturbedSurface'
transform1Display.ScaleFactor = 0.6000000000000001
transform1Display.SelectScaleArray = 'elevation'
transform1Display.GlyphType = 'Arrow'
transform1Display.PolarAxes = 'PolarAxesRepresentation'
transform1Display.ScalarOpacityUnitDistance = 0.10697888795152889
transform1Display.GaussianRadius = 0.30000000000000004
transform1Display.SetScaleArray = ['POINTS', 'elevation']
transform1Display.ScaleTransferFunction = 'PiecewiseFunction'
transform1Display.OpacityArray = ['POINTS', 'elevation']
transform1Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
transform1Display.SetScalarBarVisibility(renderView1, True)

# show data from transform2
transform2Display = Show(transform2, renderView1)
# trace defaults for the display properties.
transform2Display.Representation = 'Surface'
transform2Display.ColorArrayName = ['POINTS', 'elevation']
transform2Display.LookupTable = elevationLUT
transform2Display.OSPRayScaleArray = 'elevation'
transform2Display.OSPRayScaleFunction = 'PiecewiseFunction'
transform2Display.SelectOrientationVectors = 'PerturbedSurface'
transform2Display.ScaleFactor = 0.6000000000000001
transform2Display.SelectScaleArray = 'elevation'
transform2Display.GlyphType = 'Arrow'
transform2Display.PolarAxes = 'PolarAxesRepresentation'
transform2Display.ScalarOpacityUnitDistance = 0.10749852787197085
transform2Display.GaussianRadius = 0.30000000000000004
transform2Display.SetScaleArray = ['POINTS', 'elevation']
transform2Display.ScaleTransferFunction = 'PiecewiseFunction'
transform2Display.OpacityArray = ['POINTS', 'elevation']
transform2Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
transform2Display.SetScalarBarVisibility(renderView1, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.Position = [0.9082866309291747, 0.07173811423551513]
elevationLUTColorBar.Position2 = [0.11999999999999911, 0.4299999999999998]
elevationLUTColorBar.Title = 'u'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%-#.2f'
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%.2f'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(warpByVector1)
# ----------------------------------------------------------------