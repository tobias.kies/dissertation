#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [885, 885]

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0019167898400797951, 0.008409261711403014, 10000.0]
renderView1.CameraFocalPoint = [0.0019167898400797951, 0.008409261711403014, 0.0]
renderView1.CameraParallelScale = 2.01224879528757

nMax = 9;
T = [10, 25, 50, 75, 100, 150, 200, 250, 300]
for i in range(1,nMax+1):
  print('Processing iterate %d/%d...' % (i,nMax))
  
  # Set time and render.
  renderView1.ViewTime = T[i-1]
  Render()
  
  # export view
  ExportView('/home/mi/tokies/Promotion/Dissertation/gfx/anisotropicMongeGauge_iterate_%d.eps' % i, view=renderView1, Plottitle='ParaView GL2PS Export',
    Compressoutputfile=0,
    Drawbackground=1,
    Cullhiddenprimitives=1,
    Linewidthscalingfactor=0.714,
    Pointsizescalingfactor=0.714,
    GL2PSdepthsortmethod='Simple sorting (fast, good)',
    Rasterize3Dgeometry=1,
    Dontrasterizecubeaxes=1,
    Rendertextaspaths=0)

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
