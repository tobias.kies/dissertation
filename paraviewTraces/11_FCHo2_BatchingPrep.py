#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# find source
xMLUnstructuredGridReader1 = FindSource('XMLUnstructuredGridReader1')

# set active source
SetActiveSource(xMLUnstructuredGridReader1)

# get display properties
xMLUnstructuredGridReader1Display = GetDisplayProperties(xMLUnstructuredGridReader1, view=renderView1)

# hide color bar/color legend
xMLUnstructuredGridReader1Display.SetScalarBarVisibility(renderView1, False)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# uncomment following to set a specific view size
renderView1.ViewSize = [885, 885]

# get display properties
FCHo2_iterate_0000Display = GetDisplayProperties(FCHo2_iterate_0000, view=renderView1)

# hide color bar/color legend
FCHo2_iterate_0000Display.SetScalarBarVisibility(renderView1, False)

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0, 0, 1000]
renderView1.CameraFocalPoint = [0, 0, 0.0]
renderView1.CameraParallelScale = 500

#### uncomment the following to render all views
RenderAllViews()
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
