#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1065, 885]

# Hide orientation axes
renderView1.OrientationAxesVisibility = 0


FileNames=[('/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic4_iterate_%05d.vtu' % i) for i in range(0,301)]

# create a new 'XML Unstructured Grid Reader'
eggCartonPeriodic3_iterate_0000 = XMLUnstructuredGridReader(FileName=FileNames)
eggCartonPeriodic3_iterate_0000.CellArrayStatus = []
eggCartonPeriodic3_iterate_0000.PointArrayStatus = ['PerturbedSurface', 'elevation']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.LockDataRange = 0
elevationLUT.InterpretValuesAsCategories = 0
elevationLUT.ShowCategoricalColorsinDataRangeOnly = 0
elevationLUT.RescaleOnVisibilityChange = 0
elevationLUT.EnableOpacityMapping = 0
elevationLUT.RGBPoints = [-0.3259128928184509, 0.231373, 0.298039, 0.752941, -0.16296018035063753, 0.865003, 0.865003, 0.865003, -7.467882824130356e-06, 0.705882, 0.0156863, 0.14902]
elevationLUT.UseLogScale = 0
elevationLUT.ColorSpace = 'Diverging'
elevationLUT.UseBelowRangeColor = 0
elevationLUT.BelowRangeColor = [0.0, 0.0, 0.0]
elevationLUT.UseAboveRangeColor = 0
elevationLUT.AboveRangeColor = [1.0, 1.0, 1.0]
elevationLUT.NanColor = [1.0, 1.0, 0.0]
elevationLUT.Discretize = 1
elevationLUT.NumberOfTableValues = 256
elevationLUT.ScalarRangeInitialized = 1.0
elevationLUT.HSVWrap = 0
elevationLUT.VectorComponent = 0
elevationLUT.VectorMode = 'Magnitude'
elevationLUT.AllowDuplicateScalars = 1
elevationLUT.Annotations = []
elevationLUT.ActiveAnnotatedValues = []
elevationLUT.IndexedColors = []

# show data in view
eggCartonPeriodic3_iterate_0000Display = Show(eggCartonPeriodic3_iterate_0000, renderView1)
# trace defaults for the display properties.
eggCartonPeriodic3_iterate_0000Display.Representation = 'Surface'
eggCartonPeriodic3_iterate_0000Display.AmbientColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.ColorArrayName = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.DiffuseColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.LookupTable = elevationLUT
eggCartonPeriodic3_iterate_0000Display.MapScalars = 1
eggCartonPeriodic3_iterate_0000Display.InterpolateScalarsBeforeMapping = 1
eggCartonPeriodic3_iterate_0000Display.Opacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PointSize = 2.0
eggCartonPeriodic3_iterate_0000Display.LineWidth = 1.0
eggCartonPeriodic3_iterate_0000Display.Interpolation = 'Gouraud'
eggCartonPeriodic3_iterate_0000Display.Specular = 0.0
eggCartonPeriodic3_iterate_0000Display.SpecularColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.SpecularPower = 100.0
eggCartonPeriodic3_iterate_0000Display.Ambient = 0.0
eggCartonPeriodic3_iterate_0000Display.Diffuse = 1.0
eggCartonPeriodic3_iterate_0000Display.EdgeColor = [0.0, 0.0, 0.5]
eggCartonPeriodic3_iterate_0000Display.BackfaceRepresentation = 'Follow Frontface'
eggCartonPeriodic3_iterate_0000Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.BackfaceDiffuseColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.BackfaceOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.Position = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Scale = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.Orientation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Origin = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Pickable = 1
eggCartonPeriodic3_iterate_0000Display.Texture = None
eggCartonPeriodic3_iterate_0000Display.Triangulate = 0
eggCartonPeriodic3_iterate_0000Display.NonlinearSubdivisionLevel = 1
eggCartonPeriodic3_iterate_0000Display.UseDataPartitions = 0
eggCartonPeriodic3_iterate_0000Display.OSPRayUseScaleArray = 0
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleArray = 'elevation'
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleFunction = 'PiecewiseFunction'
eggCartonPeriodic3_iterate_0000Display.Orient = 0
eggCartonPeriodic3_iterate_0000Display.OrientationMode = 'Direction'
eggCartonPeriodic3_iterate_0000Display.SelectOrientationVectors = 'PerturbedSurface'
eggCartonPeriodic3_iterate_0000Display.Scaling = 0
eggCartonPeriodic3_iterate_0000Display.ScaleMode = 'No Data Scaling Off'
eggCartonPeriodic3_iterate_0000Display.ScaleFactor = 0.4
eggCartonPeriodic3_iterate_0000Display.SelectScaleArray = 'elevation'
eggCartonPeriodic3_iterate_0000Display.GlyphType = 'Arrow'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelFontSize = 18
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelJustification = 'Left'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelFontSize = 18
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelJustification = 'Left'
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes = 'PolarAxesRepresentation'
eggCartonPeriodic3_iterate_0000Display.ScalarOpacityUnitDistance = 0.055681169883771225
eggCartonPeriodic3_iterate_0000Display.SelectMapper = 'Projected tetra'
eggCartonPeriodic3_iterate_0000Display.SamplingDimensions = [128, 128, 128]
eggCartonPeriodic3_iterate_0000Display.GaussianRadius = 0.2
eggCartonPeriodic3_iterate_0000Display.ShaderPreset = 'Sphere'
eggCartonPeriodic3_iterate_0000Display.Emissive = 0
eggCartonPeriodic3_iterate_0000Display.ScaleByArray = 0
eggCartonPeriodic3_iterate_0000Display.SetScaleArray = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.ScaleTransferFunction = 'PiecewiseFunction'
eggCartonPeriodic3_iterate_0000Display.OpacityByArray = 0
eggCartonPeriodic3_iterate_0000Display.OpacityArray = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'Arrow' selected for 'GlyphType'
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipResolution = 6
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipRadius = 0.1
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipLength = 0.35
eggCartonPeriodic3_iterate_0000Display.GlyphType.ShaftResolution = 6
eggCartonPeriodic3_iterate_0000Display.GlyphType.ShaftRadius = 0.03
eggCartonPeriodic3_iterate_0000Display.GlyphType.Invert = 0

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Visibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableCustomRange = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.CustomRange = [0.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialAxesVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DrawRadialGridlines = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarArcsVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DrawPolarArcsGridlines = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.NumberOfRadialAxes = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AutoSubdividePolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.NumberOfPolarAxis = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MinimumRadius = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MinimumAngle = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MaximumAngle = 90.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Ratio = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelLocation = 'Bottom'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialUnitsVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ScreenSize = 10.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableDistanceLOD = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DistanceLODThreshold = 0.7
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableViewAngleLOD = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ViewAngleLODThreshold = 0.7
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarTicksVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.TickLocation = 'Both'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AxisTickVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AxisMinorTickVisibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMinorTickVisibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DeltaAngleMajor = 10.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DeltaAngleMinor = 5.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Use2DMode = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.UseLogAxis = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
eggCartonPeriodic3_iterate_0000Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
eggCartonPeriodic3_iterate_0000Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# show color bar/color legend
eggCartonPeriodic3_iterate_0000Display.SetScalarBarVisibility(renderView1, True)

# Rescale transfer function
elevationLUT.RescaleTransferFunction(-0.4, 0.0)

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.3259128928184509, 0.0, 0.5, 0.0, -7.467882824130356e-06, 1.0, 0.5, 0.0]
elevationPWF.AllowDuplicateScalars = 1
elevationPWF.ScalarRangeInitialized = 1

# Rescale transfer function
elevationPWF.RescaleTransferFunction(-0.4, 0.0)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
elevationLUT.ApplyPreset('Parula Matlab', True)

# Properties modified on elevationLUT
elevationLUT.NanColor = [1.0, 1.0, 1.0]

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.AutoOrient = 1
elevationLUTColorBar.Orientation = 'Vertical'
elevationLUTColorBar.Position = [0.85, 0.05]
elevationLUTColorBar.Position2 = [0.12, 0.43]
elevationLUTColorBar.Title = 'elevation'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleJustification = 'Centered'
elevationLUTColorBar.TitleColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.TitleOpacity = 1.0
elevationLUTColorBar.TitleFontFamily = 'Arial'
elevationLUTColorBar.TitleBold = 0
elevationLUTColorBar.TitleItalic = 0
elevationLUTColorBar.TitleShadow = 0
elevationLUTColorBar.TitleFontSize = 7
elevationLUTColorBar.LabelColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.LabelOpacity = 1.0
elevationLUTColorBar.LabelFontFamily = 'Arial'
elevationLUTColorBar.LabelBold = 0
elevationLUTColorBar.LabelItalic = 0
elevationLUTColorBar.LabelShadow = 0
elevationLUTColorBar.LabelFontSize = 7
elevationLUTColorBar.AutomaticLabelFormat = 1
elevationLUTColorBar.LabelFormat = '%-#6.3g'
elevationLUTColorBar.NumberOfLabels = 5
elevationLUTColorBar.DrawTickMarks = 1
elevationLUTColorBar.DrawSubTickMarks = 1
elevationLUTColorBar.DrawTickLabels = 1
elevationLUTColorBar.AddRangeLabels = 1
elevationLUTColorBar.RangeLabelFormat = '%4.3e'
elevationLUTColorBar.DrawAnnotations = 1
elevationLUTColorBar.AddRangeAnnotations = 0
elevationLUTColorBar.AutomaticAnnotations = 0
elevationLUTColorBar.DrawNanAnnotation = 0
elevationLUTColorBar.NanAnnotation = 'NaN'
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '   elev.'
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.TitleFontSize = 25
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelFontSize = 25
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%-#.1f'
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%-#.1f'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '     elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '      elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.17203389830508475]
elevationLUTColorBar.Position2 = [0.12, 0.4299999999999998]

# change scalar bar placement
elevationLUTColorBar.Position2 = [0.12, 0.813050847457627]

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.01610169491525425]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474576]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 50

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 99

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 30

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 25

# change scalar bar placement
elevationLUTColorBar.Position = [0.79136460554371, 0.01158192090395481]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474575]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 25

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks left/bottom, annotations right/top'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AutoOrient = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7497867803837953, 0.02175141242937849]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 15.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.3723880597014926, 0.031638418079096065]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 9

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 5

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.8457356076759063, 0.024858757062146908]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474572]

# change scalar bar placement
elevationLUTColorBar.Position = [0.8386118320748378, 0.016949152542372836]

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.433925005777524, 0.0038618068067983362, 10000.0]
renderView1.CameraFocalPoint = [0.433925005777524, 0.0038618068067983362, 0.0]
renderView1.CameraParallelScale = 2.01224879528757

####################

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.RGBPoints = [-0.4, 0.2081, 0.1663, 0.5292, -0.3936508, 0.21162, 0.18978, 0.57768, -0.3873016, 0.21225, 0.21377, 0.62697, -0.3809524, 0.2081, 0.2386, 0.67709, -0.3746032, 0.1959, 0.26446, 0.7279, -0.368254, 0.17073, 0.29194, 0.77925, -0.3619048, 0.12527, 0.32424, 0.83027, -0.35555600000000004, 0.059133, 0.35983, 0.86833, -0.349208, 0.011695, 0.38751, 0.88196, -0.34285600000000005, 0.0059571, 0.40861, 0.88284, -0.33650800000000003, 0.016514, 0.4266, 0.87863, -0.33016, 0.032852, 0.44304, 0.87196, -0.323808, 0.049814, 0.45857, 0.86406, -0.31746, 0.062933, 0.47369, 0.85544, -0.311112, 0.072267, 0.48867, 0.8467, -0.30476000000000003, 0.077943, 0.50399, 0.83837, -0.298412, 0.079348, 0.52002, 0.83118, -0.292064, 0.074943, 0.53754, 0.82627, -0.285716, 0.064057, 0.55699, 0.82396, -0.279364, 0.048771, 0.57722, 0.82283, -0.27301600000000004, 0.034343, 0.59658, 0.81985, -0.266668, 0.0265, 0.6137, 0.8135, -0.260316, 0.02389, 0.62866, 0.80376, -0.25396799999999997, 0.02309, 0.64179, 0.79127, -0.24762, 0.022771, 0.65349, 0.77676, -0.241268, 0.026662, 0.6642, 0.76072, -0.23492000000000002, 0.038371, 0.67427, 0.74355, -0.228572, 0.058971, 0.68376, 0.72539, -0.222224, 0.0843, 0.69283, 0.70617, -0.215872, 0.1133, 0.7015, 0.68586, -0.20952400000000002, 0.14527, 0.70976, 0.66463, -0.20317600000000002, 0.18013, 0.71766, 0.64243, -0.19682400000000003, 0.21783, 0.72504, 0.61926, -0.190476, 0.25864, 0.73171, 0.59543, -0.184128, 0.30217, 0.7376, 0.57119, -0.177776, 0.34817, 0.74243, 0.54727, -0.17142800000000002, 0.39526, 0.7459, 0.52444, -0.16508, 0.44201, 0.74808, 0.50331, -0.158732, 0.48712, 0.74906, 0.48398, -0.15238000000000002, 0.53003, 0.74911, 0.46611, -0.146032, 0.57086, 0.74852, 0.44939, -0.13968400000000003, 0.60985, 0.74731, 0.43369, -0.133332, 0.6473, 0.7456, 0.4188, -0.12698399999999999, 0.68342, 0.74348, 0.40443, -0.12063600000000002, 0.71841, 0.74113, 0.39048, -0.114284, 0.75249, 0.7384, 0.37681, -0.10793599999999998, 0.78584, 0.73557, 0.36327, -0.10158800000000001, 0.8185, 0.73273, 0.34979, -0.09523999999999999, 0.85066, 0.7299, 0.33603, -0.08888799999999997, 0.88243, 0.72743, 0.3217, -0.08254, 0.91393, 0.72579, 0.30628, -0.07619199999999998, 0.94496, 0.72611, 0.28864, -0.06984000000000001, 0.9739, 0.7314, 0.26665, -0.06349199999999999, 0.99377, 0.74546, 0.24035, -0.05714399999999997, 0.99904, 0.76531, 0.21641, -0.050792000000000004, 0.99553, 0.78606, 0.19665, -0.04444400000000004, 0.988, 0.8066, 0.17937, -0.03809600000000002, 0.97886, 0.82714, 0.16331, -0.031748, 0.9697, 0.84814, 0.14745, -0.02539600000000003, 0.96259, 0.87051, 0.1309, -0.01904800000000001, 0.95887, 0.8949, 0.11324, -0.012699999999999989, 0.95982, 0.92183, 0.094838, -0.00634800000000002, 0.9661, 0.95144, 0.075533, 0.0, 0.9763, 0.9831, 0.0538]
elevationLUT.ColorSpace = 'RGB'
elevationLUT.NanColor = [1.0, 1.0, 1.0]
elevationLUT.ScalarRangeInitialized = 1.0

# Rescale transfer function
elevationLUT.RescaleTransferFunction(-0.3, 0.1)

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.4, 0.0, 0.5, 0.0, 0.0, 1.0, 0.5, 0.0]
elevationPWF.ScalarRangeInitialized = 1

# Rescale transfer function
elevationPWF.RescaleTransferFunction(-0.3, 0.1)

######################

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
