#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1065, 885]

# Hide orientation axes
renderView1.OrientationAxesVisibility = 0

# create a new 'XML Unstructured Grid Reader'
eggCartonPeriodic3_iterate_0000 = XMLUnstructuredGridReader(FileName=['/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00000.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00001.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00002.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00003.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00004.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00005.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00006.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00007.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00008.vtu', '/home/mi/tokies/Backup/Promotion/finalData/iterates/eggCartonPeriodic3_iterate_00009.vtu'])
eggCartonPeriodic3_iterate_0000.CellArrayStatus = []
eggCartonPeriodic3_iterate_0000.PointArrayStatus = ['PerturbedSurface', 'elevation']

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.LockDataRange = 0
elevationLUT.InterpretValuesAsCategories = 0
elevationLUT.ShowCategoricalColorsinDataRangeOnly = 0
elevationLUT.RescaleOnVisibilityChange = 0
elevationLUT.EnableOpacityMapping = 0
elevationLUT.RGBPoints = [-0.3259128928184509, 0.231373, 0.298039, 0.752941, -0.16296018035063753, 0.865003, 0.865003, 0.865003, -7.467882824130356e-06, 0.705882, 0.0156863, 0.14902]
elevationLUT.UseLogScale = 0
elevationLUT.ColorSpace = 'Diverging'
elevationLUT.UseBelowRangeColor = 0
elevationLUT.BelowRangeColor = [0.0, 0.0, 0.0]
elevationLUT.UseAboveRangeColor = 0
elevationLUT.AboveRangeColor = [1.0, 1.0, 1.0]
elevationLUT.NanColor = [1.0, 1.0, 0.0]
elevationLUT.Discretize = 1
elevationLUT.NumberOfTableValues = 256
elevationLUT.ScalarRangeInitialized = 1.0
elevationLUT.HSVWrap = 0
elevationLUT.VectorComponent = 0
elevationLUT.VectorMode = 'Magnitude'
elevationLUT.AllowDuplicateScalars = 1
elevationLUT.Annotations = []
elevationLUT.ActiveAnnotatedValues = []
elevationLUT.IndexedColors = []

# show data in view
eggCartonPeriodic3_iterate_0000Display = Show(eggCartonPeriodic3_iterate_0000, renderView1)
# trace defaults for the display properties.
eggCartonPeriodic3_iterate_0000Display.Representation = 'Surface'
eggCartonPeriodic3_iterate_0000Display.AmbientColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.ColorArrayName = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.DiffuseColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.LookupTable = elevationLUT
eggCartonPeriodic3_iterate_0000Display.MapScalars = 1
eggCartonPeriodic3_iterate_0000Display.InterpolateScalarsBeforeMapping = 1
eggCartonPeriodic3_iterate_0000Display.Opacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PointSize = 2.0
eggCartonPeriodic3_iterate_0000Display.LineWidth = 1.0
eggCartonPeriodic3_iterate_0000Display.Interpolation = 'Gouraud'
eggCartonPeriodic3_iterate_0000Display.Specular = 0.0
eggCartonPeriodic3_iterate_0000Display.SpecularColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.SpecularPower = 100.0
eggCartonPeriodic3_iterate_0000Display.Ambient = 0.0
eggCartonPeriodic3_iterate_0000Display.Diffuse = 1.0
eggCartonPeriodic3_iterate_0000Display.EdgeColor = [0.0, 0.0, 0.5]
eggCartonPeriodic3_iterate_0000Display.BackfaceRepresentation = 'Follow Frontface'
eggCartonPeriodic3_iterate_0000Display.BackfaceAmbientColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.BackfaceDiffuseColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.BackfaceOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.Position = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Scale = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.Orientation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Origin = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.Pickable = 1
eggCartonPeriodic3_iterate_0000Display.Texture = None
eggCartonPeriodic3_iterate_0000Display.Triangulate = 0
eggCartonPeriodic3_iterate_0000Display.NonlinearSubdivisionLevel = 1
eggCartonPeriodic3_iterate_0000Display.UseDataPartitions = 0
eggCartonPeriodic3_iterate_0000Display.OSPRayUseScaleArray = 0
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleArray = 'elevation'
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleFunction = 'PiecewiseFunction'
eggCartonPeriodic3_iterate_0000Display.Orient = 0
eggCartonPeriodic3_iterate_0000Display.OrientationMode = 'Direction'
eggCartonPeriodic3_iterate_0000Display.SelectOrientationVectors = 'PerturbedSurface'
eggCartonPeriodic3_iterate_0000Display.Scaling = 0
eggCartonPeriodic3_iterate_0000Display.ScaleMode = 'No Data Scaling Off'
eggCartonPeriodic3_iterate_0000Display.ScaleFactor = 0.4
eggCartonPeriodic3_iterate_0000Display.SelectScaleArray = 'elevation'
eggCartonPeriodic3_iterate_0000Display.GlyphType = 'Arrow'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelColor = [0.0, 1.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelFontSize = 18
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelJustification = 'Left'
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.SelectionCellLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelColor = [1.0, 1.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelFontSize = 18
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelJustification = 'Left'
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.SelectionPointLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes = 'PolarAxesRepresentation'
eggCartonPeriodic3_iterate_0000Display.ScalarOpacityUnitDistance = 0.055681169883771225
eggCartonPeriodic3_iterate_0000Display.SelectMapper = 'Projected tetra'
eggCartonPeriodic3_iterate_0000Display.SamplingDimensions = [128, 128, 128]
eggCartonPeriodic3_iterate_0000Display.GaussianRadius = 0.2
eggCartonPeriodic3_iterate_0000Display.ShaderPreset = 'Sphere'
eggCartonPeriodic3_iterate_0000Display.Emissive = 0
eggCartonPeriodic3_iterate_0000Display.ScaleByArray = 0
eggCartonPeriodic3_iterate_0000Display.SetScaleArray = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.ScaleTransferFunction = 'PiecewiseFunction'
eggCartonPeriodic3_iterate_0000Display.OpacityByArray = 0
eggCartonPeriodic3_iterate_0000Display.OpacityArray = ['POINTS', 'elevation']
eggCartonPeriodic3_iterate_0000Display.OpacityTransferFunction = 'PiecewiseFunction'

# init the 'PiecewiseFunction' selected for 'OSPRayScaleFunction'
eggCartonPeriodic3_iterate_0000Display.OSPRayScaleFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'Arrow' selected for 'GlyphType'
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipResolution = 6
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipRadius = 0.1
eggCartonPeriodic3_iterate_0000Display.GlyphType.TipLength = 0.35
eggCartonPeriodic3_iterate_0000Display.GlyphType.ShaftResolution = 6
eggCartonPeriodic3_iterate_0000Display.GlyphType.ShaftRadius = 0.03
eggCartonPeriodic3_iterate_0000Display.GlyphType.Invert = 0

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Visibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Translation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Scale = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Orientation = [0.0, 0.0, 0.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableCustomBounds = [0, 0, 0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.CustomBounds = [0.0, 1.0, 0.0, 1.0, 0.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableCustomRange = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.CustomRange = [0.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialAxesVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DrawRadialGridlines = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarArcsVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DrawPolarArcsGridlines = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.NumberOfRadialAxes = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AutoSubdividePolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.NumberOfPolarAxis = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MinimumRadius = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MinimumAngle = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.MaximumAngle = 90.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialAxesOriginToPolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Ratio = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarArcsColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryPolarArcsColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitle = 'Radial Distance'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleLocation = 'Bottom'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelFormat = '%-#6.3g'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarLabelExponentLocation = 'Labels'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelFormat = '%-#3.1f'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialLabelLocation = 'Bottom'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.RadialUnitsVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ScreenSize = 10.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTitleFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisLabelFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTextFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextColor = [1.0, 1.0, 1.0]
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextOpacity = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontFamily = 'Arial'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextBold = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextItalic = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextShadow = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SecondaryRadialAxesTextFontSize = 12
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableDistanceLOD = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DistanceLODThreshold = 0.7
eggCartonPeriodic3_iterate_0000Display.PolarAxes.EnableViewAngleLOD = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ViewAngleLODThreshold = 0.7
eggCartonPeriodic3_iterate_0000Display.PolarAxes.SmallestVisiblePolarAngle = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarTicksVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTicksOriginToPolarAxis = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.TickLocation = 'Both'
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AxisTickVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.AxisMinorTickVisibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickVisibility = 1
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMinorTickVisibility = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DeltaAngleMajor = 10.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.DeltaAngleMinor = 5.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.PolarAxisTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.LastRadialAxisTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMajorTickSize = 0.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickRatioSize = 0.3
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcMajorTickThickness = 1.0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.ArcTickRatioThickness = 0.5
eggCartonPeriodic3_iterate_0000Display.PolarAxes.Use2DMode = 0
eggCartonPeriodic3_iterate_0000Display.PolarAxes.UseLogAxis = 0

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
eggCartonPeriodic3_iterate_0000Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
eggCartonPeriodic3_iterate_0000Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.0, 0.0, 10000.0]

# show color bar/color legend
eggCartonPeriodic3_iterate_0000Display.SetScalarBarVisibility(renderView1, True)

# Rescale transfer function
elevationLUT.RescaleTransferFunction(-0.4, 0.0)

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.3259128928184509, 0.0, 0.5, 0.0, -7.467882824130356e-06, 1.0, 0.5, 0.0]
elevationPWF.AllowDuplicateScalars = 1
elevationPWF.ScalarRangeInitialized = 1

# Rescale transfer function
elevationPWF.RescaleTransferFunction(-0.4, 0.0)

# Apply a preset using its name. Note this may not work as expected when presets have duplicate names.
elevationLUT.ApplyPreset('Parula Matlab', True)

# Properties modified on elevationLUT
elevationLUT.NanColor = [1.0, 1.0, 1.0]

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.AutoOrient = 1
elevationLUTColorBar.Orientation = 'Vertical'
elevationLUTColorBar.Position = [0.85, 0.05]
elevationLUTColorBar.Position2 = [0.12, 0.43]
elevationLUTColorBar.Title = 'elevation'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleJustification = 'Centered'
elevationLUTColorBar.TitleColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.TitleOpacity = 1.0
elevationLUTColorBar.TitleFontFamily = 'Arial'
elevationLUTColorBar.TitleBold = 0
elevationLUTColorBar.TitleItalic = 0
elevationLUTColorBar.TitleShadow = 0
elevationLUTColorBar.TitleFontSize = 7
elevationLUTColorBar.LabelColor = [1.0, 1.0, 1.0]
elevationLUTColorBar.LabelOpacity = 1.0
elevationLUTColorBar.LabelFontFamily = 'Arial'
elevationLUTColorBar.LabelBold = 0
elevationLUTColorBar.LabelItalic = 0
elevationLUTColorBar.LabelShadow = 0
elevationLUTColorBar.LabelFontSize = 7
elevationLUTColorBar.AutomaticLabelFormat = 1
elevationLUTColorBar.LabelFormat = '%-#6.3g'
elevationLUTColorBar.NumberOfLabels = 5
elevationLUTColorBar.DrawTickMarks = 1
elevationLUTColorBar.DrawSubTickMarks = 1
elevationLUTColorBar.DrawTickLabels = 1
elevationLUTColorBar.AddRangeLabels = 1
elevationLUTColorBar.RangeLabelFormat = '%4.3e'
elevationLUTColorBar.DrawAnnotations = 1
elevationLUTColorBar.AddRangeAnnotations = 0
elevationLUTColorBar.AutomaticAnnotations = 0
elevationLUTColorBar.DrawNanAnnotation = 0
elevationLUTColorBar.NanAnnotation = 'NaN'
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '   elev.'
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.TitleFontSize = 25
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelFontSize = 25
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%-#.1f'
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%-#.1f'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '     elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.Title = '      elev.'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.17203389830508475]
elevationLUTColorBar.Position2 = [0.12, 0.4299999999999998]

# change scalar bar placement
elevationLUTColorBar.Position2 = [0.12, 0.813050847457627]

# change scalar bar placement
elevationLUTColorBar.Position = [0.7103411513859275, 0.01610169491525425]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474576]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 50

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 99

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 30

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TitleFontSize = 25

# change scalar bar placement
elevationLUTColorBar.Position = [0.79136460554371, 0.01158192090395481]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474575]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.LabelFontSize = 25

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks left/bottom, annotations right/top'
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AutoOrient = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.7497867803837953, 0.02175141242937849]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 15.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# change scalar bar placement
elevationLUTColorBar.Position = [0.3723880597014926, 0.031638418079096065]

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 10.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.AspectRatio = 20.0

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.TextPosition = 'Ticks right/top, annotations left/bottom'

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 9

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.NumberOfLabels = 5

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 1

# Properties modified on elevationLUTColorBar
elevationLUTColorBar.DrawSubTickMarks = 0

# change scalar bar placement
elevationLUTColorBar.Position = [0.8457356076759063, 0.024858757062146908]
elevationLUTColorBar.Position2 = [0.12, 0.9689830508474572]

# change scalar bar placement
elevationLUTColorBar.Position = [0.8386118320748378, 0.016949152542372836]

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.433925005777524, 0.0038618068067983362, 10000.0]
renderView1.CameraFocalPoint = [0.433925005777524, 0.0038618068067983362, 0.0]
renderView1.CameraParallelScale = 2.01224879528757

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
