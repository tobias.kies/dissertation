# state file generated using paraview version 5.3.0

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1612, 885]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [0.0, 0.0, 0.15311800688505173]
renderView1.StereoType = 0
renderView1.CameraPosition = [0.14642849385211815, -9.350826062046197, 6.505014015699501]
renderView1.CameraFocalPoint = [0.14113833845070395, -6.8792654227870225, 4.586576580758063]
renderView1.CameraViewUp = [-0.0013048098235636314, 0.6131638709803572, 0.7899546599620185]
renderView1.CameraParallelScale = 1.434572301342088
renderView1.Background = [1.0, 1.0, 1.0]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
pointInexactMembrane_solutionvtu = XMLUnstructuredGridReader(FileName=['/home/mi/tokies/Backup/Promotion/finalData/iterates/pointInexactMembrane_solution.vtu'])
pointInexactMembrane_solutionvtu.PointArrayStatus = ['PerturbedSurface', 'elevation']

# create a new 'Warp By Scalar'
warpByScalar2 = WarpByScalar(Input=pointInexactMembrane_solutionvtu)
warpByScalar2.Scalars = ['POINTS', 'elevation']

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'elevation'
elevationLUT = GetColorTransferFunction('elevation')
elevationLUT.RGBPoints = [-0.5, 0.2081, 0.1663, 0.5292, -0.484127, 0.21162, 0.18978, 0.57768, -0.46825399999999995, 0.21225, 0.21377, 0.62697, -0.4523809999999999, 0.2081, 0.2386, 0.67709, -0.4365079999999997, 0.1959, 0.26446, 0.7279, -0.42063499999999987, 0.17073, 0.29194, 0.77925, -0.40476199999999984, 0.12527, 0.32424, 0.83027, -0.38888999999999996, 0.059133, 0.35983, 0.86833, -0.3730199999999999, 0.011695, 0.38751, 0.88196, -0.35713999999999996, 0.0059571, 0.40861, 0.88284, -0.3412699999999999, 0.016514, 0.4266, 0.87863, -0.3254, 0.032852, 0.44304, 0.87196, -0.3095199999999999, 0.049814, 0.45857, 0.86406, -0.2936500000000001, 0.062933, 0.47369, 0.85544, -0.27777999999999986, 0.072267, 0.48867, 0.8467, -0.2619, 0.077943, 0.50399, 0.83837, -0.24602999999999997, 0.079348, 0.52002, 0.83118, -0.23015999999999998, 0.074943, 0.53754, 0.82627, -0.21428999999999998, 0.064057, 0.55699, 0.82396, -0.19840999999999992, 0.048771, 0.57722, 0.82283, -0.18253999999999998, 0.034343, 0.59658, 0.81985, -0.16666999999999998, 0.0265, 0.6137, 0.8135, -0.15078999999999998, 0.02389, 0.62866, 0.80376, -0.13492000000000004, 0.02309, 0.64179, 0.79127, -0.11904999999999999, 0.022771, 0.65349, 0.77676, -0.10317000000000004, 0.026662, 0.6642, 0.76072, -0.08729999999999999, 0.038371, 0.67427, 0.74355, -0.07143, 0.058971, 0.68376, 0.72539, -0.05555999999999989, 0.0843, 0.69283, 0.70617, -0.03968000000000005, 0.1133, 0.7015, 0.68586, -0.023809999999999942, 0.14527, 0.70976, 0.66463, -0.007939999999999947, 0.18013, 0.71766, 0.64243, 0.007939999999999947, 0.21783, 0.72504, 0.61926, 0.023809999999999998, 0.25864, 0.73171, 0.59543, 0.03968000000000005, 0.30217, 0.7376, 0.57119, 0.055560000000000054, 0.34817, 0.74243, 0.54727, 0.07143, 0.39526, 0.7459, 0.52444, 0.08730000000000004, 0.44201, 0.74808, 0.50331, 0.10316999999999998, 0.48712, 0.74906, 0.48398, 0.1190500000000001, 0.53003, 0.74911, 0.46611, 0.13492000000000004, 0.57086, 0.74852, 0.44939, 0.15078999999999998, 0.60985, 0.74731, 0.43369, 0.16666999999999998, 0.6473, 0.7456, 0.4188, 0.18254000000000004, 0.68342, 0.74348, 0.40443, 0.19840999999999986, 0.71841, 0.74113, 0.39048, 0.21428999999999987, 0.75249, 0.7384, 0.37681, 0.23015999999999992, 0.78584, 0.73557, 0.36327, 0.24602999999999997, 0.8185, 0.73273, 0.34979, 0.2619, 0.85066, 0.7299, 0.33603, 0.2777799999999999, 0.88243, 0.72743, 0.3217, 0.2936500000000001, 0.91393, 0.72579, 0.30628, 0.3095199999999999, 0.94496, 0.72611, 0.28864, 0.3253999999999999, 0.9739, 0.7314, 0.26665, 0.34126999999999996, 0.99377, 0.74546, 0.24035, 0.35714, 0.99904, 0.76531, 0.21641, 0.37302000000000013, 0.99553, 0.78606, 0.19665, 0.38888999999999996, 0.988, 0.8066, 0.17937, 0.4047599999999999, 0.97886, 0.82714, 0.16331, 0.42062999999999995, 0.9697, 0.84814, 0.14745, 0.43651000000000006, 0.96259, 0.87051, 0.1309, 0.4523800000000001, 0.95887, 0.8949, 0.11324, 0.46825000000000017, 0.95982, 0.92183, 0.094838, 0.48412999999999984, 0.9661, 0.95144, 0.075533, 0.5, 0.9763, 0.9831, 0.0538]
elevationLUT.ColorSpace = 'HSV'
elevationLUT.NanColor = [1.0, 1.0, 1.0]
elevationLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'elevation'
elevationPWF = GetOpacityTransferFunction('elevation')
elevationPWF.Points = [-0.5, 0.0, 0.5, 0.0, 0.5, 1.0, 0.5, 0.0]
elevationPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from pointInexactMembrane_solutionvtu
pointInexactMembrane_solutionvtuDisplay = Show(pointInexactMembrane_solutionvtu, renderView1)
# trace defaults for the display properties.
pointInexactMembrane_solutionvtuDisplay.Representation = 'Surface'
pointInexactMembrane_solutionvtuDisplay.ColorArrayName = ['POINTS', 'elevation']
pointInexactMembrane_solutionvtuDisplay.LookupTable = elevationLUT
pointInexactMembrane_solutionvtuDisplay.OSPRayScaleArray = 'elevation'
pointInexactMembrane_solutionvtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
pointInexactMembrane_solutionvtuDisplay.SelectOrientationVectors = 'PerturbedSurface'
pointInexactMembrane_solutionvtuDisplay.ScaleFactor = 0.8
pointInexactMembrane_solutionvtuDisplay.SelectScaleArray = 'elevation'
pointInexactMembrane_solutionvtuDisplay.GlyphType = 'Arrow'
pointInexactMembrane_solutionvtuDisplay.PolarAxes = 'PolarAxesRepresentation'
pointInexactMembrane_solutionvtuDisplay.ScalarOpacityUnitDistance = 0.07015387801933584
pointInexactMembrane_solutionvtuDisplay.GaussianRadius = 0.4
pointInexactMembrane_solutionvtuDisplay.SetScaleArray = ['POINTS', 'elevation']
pointInexactMembrane_solutionvtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
pointInexactMembrane_solutionvtuDisplay.OpacityArray = ['POINTS', 'elevation']
pointInexactMembrane_solutionvtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
pointInexactMembrane_solutionvtuDisplay.SetScalarBarVisibility(renderView1, True)

# show data from warpByScalar2
warpByScalar2Display = Show(warpByScalar2, renderView1)
# trace defaults for the display properties.
warpByScalar2Display.Representation = 'Surface'
warpByScalar2Display.ColorArrayName = ['POINTS', 'elevation']
warpByScalar2Display.LookupTable = elevationLUT
warpByScalar2Display.OSPRayScaleArray = 'elevation'
warpByScalar2Display.OSPRayScaleFunction = 'PiecewiseFunction'
warpByScalar2Display.SelectOrientationVectors = 'PerturbedSurface'
warpByScalar2Display.ScaleFactor = 0.8
warpByScalar2Display.SelectScaleArray = 'elevation'
warpByScalar2Display.GlyphType = 'Arrow'
warpByScalar2Display.PolarAxes = 'PolarAxesRepresentation'
warpByScalar2Display.ScalarOpacityUnitDistance = 0.07044931377403565
warpByScalar2Display.GaussianRadius = 0.4
warpByScalar2Display.SetScaleArray = ['POINTS', 'elevation']
warpByScalar2Display.ScaleTransferFunction = 'PiecewiseFunction'
warpByScalar2Display.OpacityArray = ['POINTS', 'elevation']
warpByScalar2Display.OpacityTransferFunction = 'PiecewiseFunction'

# show color legend
warpByScalar2Display.SetScalarBarVisibility(renderView1, True)

# setup the color legend parameters for each legend in this view

# get color legend/bar for elevationLUT in view renderView1
elevationLUTColorBar = GetScalarBar(elevationLUT, renderView1)
elevationLUTColorBar.Position = [0.8745451182161479, 0.5517231638418076]
elevationLUTColorBar.Position2 = [0.1200000000000001, 0.43000000000000105]
elevationLUTColorBar.Title = '        elevation'
elevationLUTColorBar.ComponentTitle = ''
elevationLUTColorBar.TitleColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.TitleFontSize = 15
elevationLUTColorBar.LabelColor = [0.0, 0.0, 0.0]
elevationLUTColorBar.LabelFontSize = 15
elevationLUTColorBar.AutomaticLabelFormat = 0
elevationLUTColorBar.LabelFormat = '%.1f'
elevationLUTColorBar.NumberOfLabels = 3
elevationLUTColorBar.DrawSubTickMarks = 0
elevationLUTColorBar.RangeLabelFormat = '%.1f'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(warpByScalar2)
# ----------------------------------------------------------------