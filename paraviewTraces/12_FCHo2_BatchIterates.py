#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()


T = [10, 25, 50, 75, 100, 150, 200, 300, 400] # Time steps to be printed later on
nMax = len(T)
for i in range(nMax):
  print('Processing iterate %d/%d...' % (i+1,nMax))
  
  # Set time and render.
  renderView1.ViewTime = T[i]
  setTime(T[i], True)
  Render()
  
  # export view
  ExportView('/home/mi/tokies/Promotion/Dissertation/gfx/FCHo2_iterate_%d.eps' % (i+1), view=renderView1, Plottitle='ParaView GL2PS Export',
    Compressoutputfile=0,
    Drawbackground=1,
    Cullhiddenprimitives=1,
    Linewidthscalingfactor=0.714,
    Pointsizescalingfactor=0.714,
    GL2PSdepthsortmethod='Simple sorting (fast, good)',
    Rasterize3Dgeometry=1,
    Dontrasterizecubeaxes=1,
    Rendertextaspaths=0)

#### uncomment the following to render all views
RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
