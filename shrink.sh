#!/bin/sh

inp="diss.pdf"
out="diss_compressed2.pdf"
res=150

outA="out1.pdf"
outB="out2.pdf"
outC="out3.pdf"

# http://www.alfredklomp.com/programming/shrinkpdf
# Licensed under the 3-clause BSD license:
#
# Copyright (c) 2014, Alfred Klomp
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


shrink ()
{
  # This approach turns pages with pictures into pictures. Even including the text!
	gs					\
	  -q -dNOPAUSE -dBATCH -dSAFER		\
	  -sDEVICE=pdfwrite			\
	  -dCompatibilityLevel=1.3		\
	  -dPDFSETTINGS=/screen			\
	  -dEmbedAllFonts=true			\
	  -dSubsetFonts=true			\
	  -dAutoRotatePages=/None		\
	  -dColorImageDownsampleType=/Bicubic	\
	  -dColorImageResolution=$res		\
	  -dGrayImageDownsampleType=/Bicubic	\
	  -dGrayImageResolution=$res		\
	  -dMonoImageDownsampleType=/Bicubic	\
	  -dMonoImageResolution=$res		\
	  -sOutputFile="$outA"			\
	  "$inp"
    
  # pdftk approach
  tmp="superunlikelytemporaryfile.ps"
  #pdf2ps "$inp" "$tmp"
  pdftops -r $res "$inp" "$tmp"
  ps2pdf -r150 "$tmp" "$outB"
  rm "$tmp"
  
  # "Classic" GhostScript approach
  gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile="$outC" "$inp"
}

check_smaller ()
{
	# If $inp and $out are regular files, we can compare file sizes to
	# see if we succeeded in shrinking. If not, we copy $inp over $out:
	if [ ! -f "$inp" -o ! -f "$out" ]; then
		return 0;
	fi
	ISIZE="$(echo $(wc -c "$inp") | cut -f1 -d\ )"
	OSIZE="$(echo $(wc -c "$out") | cut -f1 -d\ )"
	if [ "$ISIZE" -lt "$OSIZE" ]; then
		echo "Input smaller than output, doing straight copy" >&2
		cp "$inp" "$out"
	fi
}

usage ()
{
	echo "Reduces PDF filesize by lossy recompressing with Ghostscript."
	echo "Not guaranteed to succeed, but usually works."
	echo "  Usage: $inp infile [outfile] [resolution_in_dpi]"
}

IFILE="$inp"

# Need an input file:
if [ -z "$IFILE" ]; then
	usage "$0"
	exit 1
fi

# Output filename defaults to "-" (stdout) unless given:
if [ ! -z "$out" ]; then
	OFILE="$out"
else
	OFILE="-"
fi

# Output resolution defaults to 72 unless given:
if [ ! -z "$res" ]; then
	res="$res"
else
	res="72"
fi

shrink "$IFILE" "$OFILE" "$res" || exit $?

check_smaller "$IFILE" "$OFILE"
